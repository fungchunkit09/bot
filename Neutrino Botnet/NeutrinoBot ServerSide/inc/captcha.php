<?php
session_start();
$code = rand(1000,9999);
$_SESSION["code"] = $code;
$im = imagecreatetruecolor(50, 30);
imagesavealpha($im, true);
$bg = imagecolorallocatealpha($im, 0, 0, 0, 127); //background color blue
$fg = imagecolorallocate($im, 0, 0, 0);//text color white
imagefill($im, 0, 0, $bg);
imagestring($im, 8, 8, 8,  $code, $fg);
header("Cache-Control: no-cache, must-revalidate");
header('Content-type: image/png');
imagepng($im);
imagedestroy($im);
?>