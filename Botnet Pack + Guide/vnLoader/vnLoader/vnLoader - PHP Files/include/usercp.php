<?php
	//check if included in project
	if ( !defined ( 'vnLoader' ) ) header ( 'location: ../' );

	//set variables
	$usr = $user['name'];

	//include
	include ( './include/function.php' ) ;

	//remove user
	if ( instring( '' , $permission ) ) 
		{
		if ( isset ( $_POST['removeuser_submit'] ) )
			{
			$page .= '				User: ' . $_POST['removeuser_username'] . ' has been removed' . "\n";
			$page .= '				<br />' . "\n";
			$page .= '				<br />' . "\n";
			mysql_query("DELETE FROM userlist WHERE id = '" . $_POST['removeuser_username'] . "'");
			}
		}

	//change password
	if ( isset ( $_POST['newpass_submit'] ) )
		{
		if ( trim ( $_POST['newpass_oldpass'] ) != '' && trim ( $_POST['newpass_newpass'] ) != '' && trim ( $_POST['newpass_newpass2'] ) != '' )
			{
			$result = mysql_query("SELECT pass FROM userlist WHERE id = '" . $user['name'] . "'");
			$row = mysql_fetch_array ( $result );
			if ( md5 ( md5 ( trim ( $_POST['newpass_oldpass'] ) ) ) == $row['pass'] )
				{
				if ( trim ( $_POST['newpass_newpass'] ) == trim ( $_POST['newpass_newpass2'] ) ) 
					{
					mysql_query("UPDATE userlist SET pass = '" . md5 ( md5 ( trim ( $_POST['newpass_newpass'] ) ) ) . "' WHERE id = '" . $user['name'] . "'");
					$page .= '				Password changed.' . "\n";
					$page .= '				<br />' . "\n";
					$page .= '				<br />' . "\n";
					$_SESSION['pass'] = md5 ( md5 ( $_POST['newpass_newpass'] ) );
					}
				else
					{
					$page .= '				An error has occurred. Unable to change password.' . "\n";
					$page .= '				<br />' . "\n";
					$page .= '				<br />' . "\n";
					}
				}
			else
				{
				$page .= '				An error has occurred. Unable to change password.' . "\n";
				$page .= '				<br />' . "\n";
				$page .= '				<br />' . "\n";
				}
			}
		else 
			{
			$page .= '				An error has occurred. Unable to change password.' . "\n";
			$page .= '				<br />' . "\n";
			$page .= '				<br />' . "\n";
			}
		}

	//add user
	if ( instring( '' , $permission ) ) 
		{
		if ( isset ( $_POST['newuser_submit'] ) )
			{
			if ( trim ( $_POST['newuser_username'] ) != '' && trim ( $_POST['newuser_newpass'] ) != '' && trim ( $_POST['newuser_newpass2'] ) != '' )
				{
				if ( trim ( $_POST['newuser_newpass'] ) == trim ( $_POST['newuser_newpass2'] ) ) 
					{
					if ( $_POST['admin'] == 'on' ) 
						{
						$setpermission = '*';
						}
					else
						{
						if ( $_POST['check1'] == 'on' ) { $setpermission .= '1'; }
						if ( $_POST['check2'] == 'on' ) { $setpermission .= '2'; }
						if ( $_POST['check3'] == 'on' ) { $setpermission .= '3'; }
						if ( $_POST['check4'] == 'on' ) { $setpermission .= '4'; }
						if ( $_POST['check5'] == 'on' ) { $setpermission .= '5'; }
						if ( $_POST['check6'] == 'on' ) { $setpermission .= '6'; }
						if ( $_POST['check7'] == 'on' ) { $setpermission .= '7'; }
						if ( $_POST['check8'] == 'on' ) { $setpermission .= '8'; }
						if ( $_POST['check9'] == 'on' ) { $setpermission .= '9'; }
						}
	
					$page .= '				User added.' . "\n";
					$page .= '				<br />' . "\n";
					$page .= '				<br />' . "\n";
					mysql_query("INSERT INTO userlist (id, pass, permission) VALUES ('" .  trim ( $_POST['newuser_username'] ) . "' , '" . md5 ( md5 ( trim ( $_POST['newuser_newpass'] ) ) ) . "' , '" . $setpermission . "')");
					}
				else
					{
					$page .= '				An error has occurred. Unable to add user.' . "\n";
					$page .= '				<br />' . "\n";
					$page .= '				<br />' . "\n";
					}
				}
			else
				{
				$page .= '				An error has occurred. Unable to add user.' . "\n";
				$page .= '				<br />' . "\n";
				$page .= '				<br />' . "\n";
				}
			}	
		}

	//script
	$page .= '				<script>' . "\n";
	$page .= '				function SetState(obj_checkbox, obj1, obj2, obj3, obj4, obj5, obj6, obj7, obj8, obj9)' . "\n";
	$page .= '					{' . "\n";
	$page .= '					if(obj_checkbox.checked)' . "\n";
	$page .= '						{' . "\n";
	$page .= '						obj1.disabled = true;' . "\n";
	$page .= '						obj2.disabled = true;' . "\n";
	$page .= '						obj3.disabled = true;' . "\n";
	$page .= '						obj4.disabled = true;' . "\n";
	$page .= '						obj5.disabled = true;' . "\n";
	$page .= '						obj8.disabled = true;' . "\n";
	$page .= '						obj9.disabled = true;' . "\n";
	$page .= '						}' . "\n";
	$page .= '					else' . "\n";
	$page .= '						{' . "\n";
	$page .= '						obj1.disabled = false;' . "\n";
	$page .= '						obj2.disabled = false;' . "\n";
	$page .= '						obj3.disabled = false;' . "\n";
	$page .= '						obj4.disabled = false;' . "\n";
	$page .= '						obj5.disabled = false;' . "\n";
	$page .= '						obj8.disabled = false;' . "\n";
	$page .= '						obj9.disabled = false;' . "\n";
	$page .= '						}' . "\n";
	$page .= '					}' . "\n";
	$page .= '				</script>' . "\n";

	//User Control Panel
	$page .= '				<table id="table">' . "\n";
	$page .= '					<form action="index.php?pid=6" method="post">' . "\n";
	$page .= '						<tr>' . "\n";
	$page .= '							<td><u>Change Password</u></td>' . "\n";
	$page .= '							<td></td>' . "\n";
	$page .= '						</tr>' . "\n";
	$page .= '						<tr>' . "\n";
	$page .= '							<td>Old Password:</td>' . "\n";
	$page .= '							<td><input type="password" size="30" name="newpass_oldpass" /></td>' . "\n";
	$page .= '						</tr>' . "\n";
	$page .= '						<tr>' . "\n";
	$page .= '							<td>New Password:</td>' . "\n";
	$page .= '							<td><input type="password" size="30" name="newpass_newpass" /></td>' . "\n";
	$page .= '						</tr>' . "\n";
	$page .= '						<tr>' . "\n";
	$page .= '							<td>Password: (confirmation)</td>' . "\n";
	$page .= '							<td><input type="password" size="30" name="newpass_newpass2" /></td>' . "\n";
	$page .= '						</tr>' . "\n";
	$page .= '						<tr>' . "\n";
	$page .= '							<td><input type="submit" name="newpass_submit" value="Change Password" style="width: 100%" /></td>' . "\n";
	$page .= '							<td></td>' . "\n";
	$page .= '						</tr>' . "\n";
	$page .= '					</form>' . "\n";

	if ( instring( '' , $permission ) ) 
		{
		$page .= '					<tr><td>&nbsp;</td><td></td></tr>' . "\n";
		$page .= '					<form action="index.php?pid=6" method="post">' . "\n";
		$page .= '						<tr>' . "\n";
		$page .= '							<td><u>Add User</u></td>' . "\n";
		$page .= '							<td></td>' . "\n";
		$page .= '						</tr>' . "\n";
		$page .= '						<tr>' . "\n";
		$page .= '							<td>Username:</td>' . "\n";
		$page .= '							<td><input type="text" size="30" name="newuser_username" /></td>' . "\n";
		$page .= '						</tr>' . "\n";
		$page .= '						<tr>' . "\n";
		$page .= '							<td>Password:</td>' . "\n";
		$page .= '							<td><input type="password" size="30" name="newuser_newpass" /></td>' . "\n";
		$page .= '						</tr>' . "\n";
		$page .= '						<tr>' . "\n";
		$page .= '							<td>Password: (confirmation)</td>' . "\n";
		$page .= '							<td><input type="password" size="30" name="newuser_newpass2" /></td>' . "\n";
		$page .= '						</tr>' . "\n";
		$page .= '						<tr>' . "\n";
		$page .= '							<td><u>Permissions</u></td>' . "\n";
		$page .= '							<td></td>' . "\n";
		$page .= '						</tr>' . "\n";
		$page .= '						<tr>' . "\n";
		$page .= '							<td></td>' . "\n";
		$page .= '							<td><input type="checkbox" name="admin" onclick="SetState(this, this.form.check1, this.form.check2, this.form.check3, this.form.check4, this.form.check5, this.form.check6, this.form.check7, this.form.check8, this.form.check9 );" /> Administrator</td>' . "\n";
		$page .= '						</tr>' . "\n";
		$page .= '						<tr>' . "\n";
		$page .= '							<td>Commands:</td>' . "\n";
		$page .= '							<td></td>' . "\n";
		$page .= '						</tr>' . "\n";
		$page .= '						<tr>' . "\n";
		$page .= '							<td></td>' . "\n";
		$page .= '							<td><input type="checkbox" name="check1" /> Download & Execute</td>' . "\n";
		$page .= '						</tr>' . "\n";
		$page .= '						<tr>' . "\n";
		$page .= '							<td></td>' . "\n";
		$page .= '							<td><input type="checkbox" name="check2" /> Update</td>' . "\n";
		$page .= '						</tr>' . "\n";
		$page .= '						<tr>' . "\n";
		$page .= '							<td></td>' . "\n";
		$page .= '							<td><input type="checkbox" name="check3" /> Remove</td>' . "\n";
		$page .= '						</tr>' . "\n";
		$page .= '						<tr>' . "\n";
		$page .= '							<td></td>' . "\n";
		$page .= '							<td><input type="checkbox" name="check4" /> Browse Website</td>' . "\n";
		$page .= '						</tr>' . "\n";
		$page .= '						<tr>' . "\n";
		$page .= '							<td></td>' . "\n";
		$page .= '							<td><input type="checkbox" name="check5" /> Visit Website</td>' . "\n";
		$page .= '						</tr>' . "\n";
		$page .= '						<tr>' . "\n";
		$page .= '							<td></td>' . "\n";
		$page .= '							<td><input type="checkbox" name="check8" /> UDP-Flood</td>' . "\n";
		$page .= '						</tr>' . "\n";
		$page .= '						<tr>' . "\n";
		$page .= '							<td></td>' . "\n";
		$page .= '							<td><input type="checkbox" name="check9" /> HTTP-Flood</td>' . "\n";
		$page .= '						</tr>' . "\n";
		$page .= '						<tr>' . "\n";
		$page .= '							<td><input type="submit" name="newuser_submit" value="Add User" style="width: 100%" /></td>' . "\n";
		$page .= '							<td></td>' . "\n";
		$page .= '						</tr>' . "\n";
		$page .= '					</form>' . "\n";
		$page .= '					<tr>' . "\n";
		$page .= '						<td>&nbsp;</td>' . "\n";
		$page .= '						<td></td>' . "\n";
		$page .= '					</tr>' . "\n";
		$page .= '					<form action="index.php?pid=6" method="post">' . "\n";
		$page .= '						<tr>' . "\n";
		$page .= '							<td><u>Userlist</u></td>' . "\n";
		$page .= '							<td></td>' . "\n";
		$page .= '						</tr>' . "\n";

		$result = mysql_query("SELECT * FROM userlist");
		while($rows = mysql_fetch_array($result))
			{
			if ( $rows['id'] != $user['name'] ) 
				{
				$page .= '						<tr>' . "\n";
				$page .= '							<td>' . $rows['id'] . '<input type="hidden" name="removeuser_username" value="' . $rows['id'] . '" /></td>' . "\n";
				$page .= '							<td><input type="submit" name="removeuser_submit" value="Remove" style="width: 100%" /></td>' . "\n";
				$page .= '						</tr>' . "\n";
				}
			}
		}

	$page .= '					</form>' . "\n";
	$page .= '				</table>' . "\n";
?>