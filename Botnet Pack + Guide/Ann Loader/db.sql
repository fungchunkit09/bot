-- 
-- Table structure for table `bots`
-- 

CREATE TABLE `bots` (
  `id` varchar(8) NOT NULL default '',
  `ip` int(10) unsigned NOT NULL default '0',
  `country` tinyint(3) unsigned NOT NULL default '0',
  `birthday` int(10) unsigned NOT NULL default '0',
  `version` int(10) unsigned NOT NULL default '0',
  `lastknock` int(10) unsigned NOT NULL default '0',
  `nextload` int(10) unsigned NOT NULL default '0',
  `orders` text NOT NULL,
  `onboard` tinyint(3) unsigned NOT NULL default '0',
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;