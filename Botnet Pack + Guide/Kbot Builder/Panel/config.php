<?php //config.php
	//Database
	define('DB_SERVER', "localhost");
	define('DB_USER', "kb"); 	//database login name
	define('DB_PASS', "D9X3yz5L6dtCbbpE"); //database login password
	define('DB_DATABASE', "kb"); //database name
?>

<?php //authclass.php
	class AuthClass {
		function AuthClass() {  
		   @session_start();
		   
		   if (!isset($_SESSION['logged'])) { 
			  $this->session_defaults();        
		   }

		}
		
		function session_defaults() {
			@session_start();   
			$_SESSION['logged'] = false;
			$_SESSION['uid'] = 0;
			$_SESSION['username'] = '';
		}

		function checkLogin($username,$password) {
			global $db;
			$username = $db->escape($username);
			$password = $db->escape($password);
			$password = sha1($password);

			$sql = "SELECT `acc_id`,`acc_name` FROM tbl_account WHERE acc_name='$username' and acc_pw='$password'";
			$match = $db->query_first($sql);
					
			if (!empty($match)) {
				$this->setSession($match);
				return true;
			} else {
				$_SESSION['msg'] = "Invalid username or password!";			
				return false;
			}
		}    
		
		function setSession($match) {
			@session_start();
			global $db;
			$_SESSION['uid'] = $match['acc_id'];
			$_SESSION['username'] = htmlspecialchars($match['acc_name']);
			$_SESSION['logged'] = true;
			$_SESSION['msg'] = "";
			
			$ip = $_SERVER['REMOTE_ADDR'];
			$db->query("UPDATE tbl_account SET acc_session = '".session_id()."', acc_lastip = '$ip' WHERE acc_id = '".$_SESSION['uid']."'");
		}
		
		function checkSession() {
			@session_start();
			global $db;
			$ip = $_SERVER['REMOTE_ADDR'];
			$row = $db->query_first("SELECT count(*) FROM tbl_account WHERE (acc_id = '".$_SESSION['uid']."') AND (acc_session = '".session_id()."') AND (acc_lastip = '$ip')");
			
			if($_SESSION['logged'] && ($row['count(*)']!=0)){
				return true;
			}
			
			if($_SESSION['logged'] && ($row['count(*)']==0)){
				$_SESSION['msg'] = "One login per ip and user!";
			}
			
			$this->session_defaults();
			return false;
		}        

		function set_privileges($username){
			global $db;
			$res = $db->query_first("SELECT acc_type FROM tbl_account WHERE acc_name = '{$username}'");
			$_SESSION['privileges'] = $res['acc_type'];
		}
				
		function authenticate($username,$password) {
			@session_start();
			$ret = $this->checkLogin($username,$password);
			$this->set_privileges($username); //Set account privileges
			return $ret;        			
		}
		
		function verify() {
			return $this->checkSession();
		}
				
		function logout() {
			@session_start();
			@session_unset();
			@session_destroy();
		}		
	}
?>

<?php //Database.singleton.php
	class Database{

		// debug flag for showing error messages
		public	$debug = true;

		// Store the single instance of Database
		private static $instance;

		private	$server   = ""; //database server
		private	$user     = ""; //database login name
		private	$pass     = ""; //database login password
		private	$database = ""; //database name

		private	$error = "";

		#######################
		//number of rows affected by SQL query
		public	$affected_rows = 0;

		private	$link_id = 0;
		private	$query_id = 0;

		private function __construct($server=null, $user=null, $pass=null, $database=null){
			// error catching if not passed in
			if($server==null || $user==null || $pass==null || $database==null){
				$this->oops("Database information must be passed in when the object is first created.");
			}

			$this->server=$server;
			$this->user=$user;
			$this->pass=$pass;
			$this->database=$database;
		}#-#constructor()

		public function __destruct(){
		$this->close();
		}

		public static function obtain($server=null, $user=null, $pass=null, $database=null){
			if (!self::$instance){ 
				self::$instance = new Database($server, $user, $pass, $database); 
			} 

			return self::$instance; 
		}#-#obtain()

		public function connect($new_link=false){
			$this->link_id=@mysql_connect($this->server,$this->user,$this->pass,$new_link);

			if (!$this->link_id){//open failed
				$this->oops("Could not connect to server: <b>$this->server</b>.");
				}

			if(!@mysql_select_db($this->database, $this->link_id)){//no database
				$this->oops("Could not open database: <b>$this->database</b>.");
				}

			// unset the data so it can't be dumped
			$this->server='';
			$this->user='';
			$this->pass='';
			$this->database='';
		}#-#connect()

		public function close(){
			if(!@mysql_close($this->link_id)){
				$this->oops("Connection close failed.");
			}
		}#-#close()

		public function escape($string){
			if(get_magic_quotes_runtime()) $string = stripslashes($string);
			return @mysql_real_escape_string($string,$this->link_id);
		}#-#escape()

		public function query($sql){
			// do query
			$this->query_id = @mysql_query($sql, $this->link_id);

			if (!$this->query_id){
				$this->oops("<b>MySQL Query fail:</b> $sql");
				return 0;
			}
			
			$this->affected_rows = @mysql_affected_rows($this->link_id);

			return $this->query_id;
		}#-#query()

		public function query_first($query_string){
			$query_id = $this->query($query_string);
			$out = $this->fetch($query_id);
			$this->free_result($query_id);
			return $out;
		}#-#query_first()

		public function fetch($query_id=-1){
			// retrieve row
			if ($query_id!=-1){
				$this->query_id=$query_id;
			}

			if (isset($this->query_id)){
				$record = @mysql_fetch_assoc($this->query_id);
			}else{
				$this->oops("Invalid query_id: <b>$this->query_id</b>. Records could not be fetched.");
			}

			return $record;
		}#-#fetch()

		public function fetch_array($sql){
			$query_id = $this->query($sql);
			$out = array();

			while ($row = $this->fetch($query_id)){
				$out[] = $row;
			}

			$this->free_result($query_id);
			return $out;
		}#-#fetch_array()

		public function update($table, $data, $where='1'){
			$q="UPDATE `$table` SET ";

			foreach($data as $key=>$val){
				if(strtolower($val)=='null') $q.= "`$key` = NULL, ";
				elseif(strtolower($val)=='now()') $q.= "`$key` = NOW(), ";
				elseif(preg_match("/^increment\((\-?\d+)\)$/i",$val,$m)) $q.= "`$key` = `$key` + $m[1], "; 
				else $q.= "`$key`='".$this->escape($val)."', ";
			}

			$q = rtrim($q, ', ') . ' WHERE '.$where.';';

			return $this->query($q);
		}#-#update()

		public function insert($table, $data){
			$q="INSERT INTO `$table` ";
			$v=''; $n='';

			foreach($data as $key=>$val){
				$n.="`$key`, ";
				if(strtolower($val)=='null') $v.="NULL, ";
				elseif(strtolower($val)=='now()') $v.="NOW(), ";
				else $v.= "'".$this->escape($val)."', ";
			}

			$q .= "(". rtrim($n, ', ') .") VALUES (". rtrim($v, ', ') .");";

			if($this->query($q)){
				return mysql_insert_id($this->link_id);
			}
			else return false;

		}#-#insert()

		private function free_result($query_id=-1){
			if ($query_id!=-1){
				$this->query_id=$query_id;
			}
			if($this->query_id!=0 && !@mysql_free_result($this->query_id)){
				$this->oops("Result ID: <b>$this->query_id</b> could not be freed.");
			}
		}#-#free_result()

		private function oops($msg=''){
			if(!empty($this->link_id)){
				$this->error = mysql_error($this->link_id);
			}
			else{
				$this->error = mysql_error();
				$msg="<b>WARNING:</b> No link_id found. Likely not be connected to database.<br />$msg";
			}

			if(!$this->debug) return;
			?>
				<table align="center" border="1" cellspacing="0" style="background:white;color:black;width:80%;">
				<tr><th colspan=2>Database Error</th></tr>
				<tr><td align="right" valign="top">Message:</td><td><?php echo $msg; ?></td></tr>
				<?php if(!empty($this->error)) echo '<tr><td align="right" valign="top" nowrap>MySQL Error:</td><td>'.$this->error.'</td></tr>'; ?>
				<tr><td align="right">Date:</td><td><?php echo date("l, F j, Y \a\\t g:i:s A"); ?></td></tr>
				<?php if(!empty($_SERVER['REQUEST_URI'])) echo '<tr><td align="right">Script:</td><td><a href="'.$_SERVER['REQUEST_URI'].'">'.$_SERVER['REQUEST_URI'].'</a></td></tr>'; ?>
				<?php if(!empty($_SERVER['HTTP_REFERER'])) echo '<tr><td align="right">Referer:</td><td><a href="'.$_SERVER['HTTP_REFERER'].'">'.$_SERVER['HTTP_REFERER'].'</a></td></tr>'; ?>
				</table>
			<?php
		}#-#oops()
	}
?>

<?php	
	$db = Database::obtain(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
	$db->connect();

	$auth = new AuthClass();
?>


<?php //account.php
	error_reporting(0);
	
	class Account{
		public $id;
		public $username;
		
		public function __construct($username)
		{
			$this->username = $username;
			$this->id = $this->id_get();
		}
		
		public function id_get()
		{
			global $db;
			$data = $db->query_first("SELECT acc_id FROM tbl_account WHERE acc_name = '$this->username'");
			return $data['id'];
		}
		
		public function last_ip_get()
		{
			global $db;
			$data = $db->query_first("SELECT acc_lastip FROM tbl_account WHERE acc_name = '$this->username'");
			return $data['acc_lastip'];
		}
		
		public function last_logon()
		{
			global $db;
			$data = $db->query_first("SELECT acc_lastlogon FROM tbl_account WHERE acc_name = '$this->username'");
			return $data['acc_lastlogon'];
		}	
		
		public function expire()
		{
			global $db;
			$data = $db->query_first("SELECT acc_expire FROM tbl_account WHERE acc_name = '$this->username'");
			return $data['acc_expire'];
		}	
	}
?>