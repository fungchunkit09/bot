-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Erstellungszeit: 23. Februar 2011 um 20:39
-- Server Version: 5.1.41
-- PHP-Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `mpac`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `lst_bots`
--

CREATE TABLE IF NOT EXISTS `lst_bots` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `UID` mediumtext CHARACTER SET ascii NOT NULL,
  `country` mediumtext CHARACTER SET ascii NOT NULL,
  `commands` mediumtext CHARACTER SET ascii NOT NULL,
  `version` mediumtext CHARACTER SET ascii COLLATE ascii_bin NOT NULL,
  `lasttime` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `lst_bots`
--


-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `lst_commands`
--

CREATE TABLE IF NOT EXISTS `lst_commands` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `command` mediumtext CHARACTER SET ascii NOT NULL,
  `parameters` longtext CHARACTER SET ascii NOT NULL,
  `max` int(11) NOT NULL,
  `done` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `lst_commands`
--


-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `lst_countries`
--

CREATE TABLE IF NOT EXISTS `lst_countries` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `countryname` mediumtext CHARACTER SET ascii NOT NULL,
  `countrycode` mediumtext CHARACTER SET ascii NOT NULL,
  `totalbots` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `lst_countries`
--


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
