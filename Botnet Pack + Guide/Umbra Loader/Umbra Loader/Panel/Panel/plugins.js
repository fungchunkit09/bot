
Ext.onReady(function(){
    function renderIcon_country(val) 
    { 
        return '<img src="' + val + '" >';
    }
    
    var store_plugins = new Ext.data.Store(
    {
	    proxy: new Ext.data.HttpProxy(
        {
            url: 'list_plugins.php',
        }),
	    reader: new Ext.data.JsonReader(
        {
            totalProperty: 'total',
		    root:'results'
        },
        [{name: 'name'}, {name: 'icon'}])
    });	
    
    var lstPlugins = new Ext.grid.GridPanel(
    {
        store: store_plugins,
        columns: [
            {
                width: 24,
                dataIndex: 'icon',
                renderer: renderIcon_country              
            },
            {
                header   : 'Pluginname', 
                width    : 120, 
                sortable : true,
                dataIndex: 'name'
            },
            {
                xtype: 'actioncolumn',
                width: 24,
                items: [
                {
                    icon   : './graphics/Visit.png',
                    handler: function(grid, rowIndex, colIndex) 
                    {
                        var rec = store_plugins.getAt(rowIndex);
                        var redirect = './plugins/' + rec.get('name') + '/index.php'; 
                        window.location.href = redirect;
                    }
                }               
                ]
                }],
        stripeRows: true
    });
            
    var pan_plugins = new Ext.Panel(
    {
        id:'images-view',
        collapsible:false,
        layout:'fit',
        title:'Plugins',
        tbar: new Ext.Toolbar({
            enableOverflow: true,
            items: [{
                text: 'Refresh',
                iconCls: 'refreshicon',
                handler:function()
                {
                    store_plugins.load();
                }
            }]
        }),
        items: lstPlugins
    });
    
	
    var win_plugins = new Ext.Window(
    {
        layout:'fit',
        width:240,
        height:300,
        x:850,
        y:115,
        closable: false,
        resizable: true,
        plain: true,
        border: false,
        items: [pan_plugins]
	});
    
    store_plugins.load();
    
	win_plugins.show();
    
});