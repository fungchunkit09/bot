<?php include('auth.php'); ?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <title>Umbra Loader</title>
    <link rel="stylesheet" type="text/css" href="../resources/css/ext-all.css" />
 	<script type="text/javascript" src="../adapter/ext/ext-base.js"></script>
    <script type="text/javascript" src="../ext-all.js"></script>
    <link rel="stylesheet" href="../Ext.ux.form.CheckboxCombo/Ext.ux.form.CheckboxCombo.min.css" />
    <script type="text/javascript" src="../Ext.ux.form.CheckboxCombo/Ext.ux.form.CheckboxCombo.min.js"></script>
    
    <link rel="Stylesheet" type="text/css" href="style.css" media="screen" />
    
</head>
<body>
    <div class="root">
        <div id="header"></div>
    </div>
    <script type="text/javascript" src="list-view.js"></script>
    <script type="text/javascript" src="plugins.js"></script>
</body>
</html>
