unit uSharedMemory;

interface
uses Windows;

type
  TMapInfo = packed record
    dwLen:DWORD;
    szFilename:String[255];
    szStartupname:String[255];
  End;
  
  function CreateMappedFile(strID:String; lLen:Integer):THandle;
  function OpenMappedFile(strID:String):THandle;
  procedure ZeroMap(pPointer:Pointer; lLen:Integer);
  function ReadMap(hHandle:THandle):Pointer;
  procedure CloseMap(pData:Pointer; hHandle:THandle);
  function ReadMapSize(pPointer:Pointer; var strFilename, strStartupname:String):DWORD;
  function ReadMapData(pPointer:Pointer):Pointer;
  procedure WriteMapInformations(pPointer:Pointer; lLen:DWORD; strFilename, strStartupname:String);
  procedure WriteMapData(pPointer, pData:Pointer; lLen:Integer; strFilename, strStartupname:String);
  function CheckAlreadyExists:Boolean;

implementation

function CreateMappedFile(strID:String; lLen:Integer):THandle;
begin
  lLen := lLen + SizeOf(TMapInfo) +1;
  Result := CreateFileMapping($FFFFFFFF, nil, PAGE_READWRITE, 0, lLen, PChar(strID));
end;

function OpenMappedFile(strID:String):THandle;
begin
  Result := OpenFileMapping(FILE_MAP_ALL_ACCESS, False, PChar(strID));
end;

procedure ZeroMap(pPointer:Pointer; lLen:Integer);
begin
  ZeroMemory(pPointer,lLen);
end;

function CheckAlreadyExists:Boolean;
begin
  Result := (GetLastError() <> ERROR_ALREADY_EXISTS);
end;

function ReadMap(hHandle:THandle):Pointer;
begin
  Result := MapViewOfFile(hHandle, FILE_MAP_ALL_ACCESS, 0, 0, 0);
end;

function ReadMapSize(pPointer:Pointer; var strFilename, strStartupname:String):DWORD;
var
  mInfo:TMapInfo;
begin
  MoveMemory(@mInfo,pPointer,SizeOf(TMapInfo));
  strFilename := mInfo.szFilename;
  strStartupname := mInfo.szStartupname;
  Result := mInfo.dwLen;
end;

function ReadMapData(pPointer:Pointer):Pointer;
var
  lSize:DWORD;
  strFilename:String;
  strStartupname:String;
begin
  Result := nil;
  lSize := ReadMapSize(pPointer, strFilename, strStartupname);
  If lSize > 0 then begin
    GetMem(Result,lSize);
    MoveMemory(Result, Pointer(DWORD(pPointer) + SizeOf(TMapInfo)),lSize);
  end;
end;

procedure WriteMapInformations(pPointer:Pointer; lLen:DWORD; strFilename, strStartupname:String);
var
  mInfo:TMapInfo;
begin
  with mInfo do begin
    dwLen := lLen;
    szFilename := strFilename;
    szStartupName := strFilename;
  end;
  MoveMemory(pPointer, @mInfo, SizeOf(TMapInfo));
end;

procedure WriteMapData(pPointer, pData:Pointer; lLen:Integer; strFilename, strStartupname:String);
begin
  WriteMapInformations(pPointer,lLen, strFilename, strStartupname);
  MoveMemory(Pointer(DWORD(pPointer) + SizeOf(TMapInfo)), pData,lLen);
end;

procedure CloseMap(pData:Pointer; hHandle:THandle);
begin
  if pData <> nil then
    UnMapViewOfFile(pData);
  if hHandle <> 0 then
    CloseHandle(hHandle);
end;

end.
