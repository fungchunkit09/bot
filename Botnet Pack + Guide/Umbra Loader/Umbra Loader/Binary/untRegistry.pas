unit untRegistry;

interface
uses windows;

function AddRegKey(KEY:HKEY; Path, Keyname, Value, RegType: String):boolean;
function RegKeyExists(RootKey: HKEY; Name, Value: String): boolean;
function DeleteKey(RootKey: HKEY; Name, Value: String):boolean;
Function GetRegKey(Key:HKEY; Path:string; Value, Default: string): string;

implementation

Function GetRegKey(Key:HKEY; Path:string; Value, Default: string): string;
Var
  Handle:hkey;
  RegType:integer;
  DataSize:integer;
begin
  Result := Default;
  if (RegOpenKeyEx(Key, pchar(Path), 0, $0001, Handle) = 0) then
  begin
    if RegQueryValueEx(Handle, pchar(Value), nil, @RegType, nil, @DataSize) = 0 then
    begin
      SetLength(Result, Datasize);
      RegQueryValueEx(Handle, pchar(Value), nil, @RegType, PByte(pchar(Result)), @DataSize);
      SetLength(Result, Datasize - 1);
    end;
    RegCloseKey(Handle);
  end;
end;

function DeleteKey(RootKey: HKEY; Name, Value: String):boolean;
var
  hTemp: HKEY;
begin
  RegOpenKeyEx(RootKey, PChar(Name), 0, KEY_SET_VALUE, hTemp);
  Result := (RegDeleteValue(hTemp, PChar(Value)) = ERROR_SUCCESS);
  RegCloseKey(hTemp);
end;

function RegKeyExists(RootKey: HKEY; Name, Value: String): boolean;
var
  hTemp: HKEY;
begin
  Result := False;
  if RegOpenKeyEx(RootKey, PChar(Name), 0, KEY_READ, hTemp) = ERROR_SUCCESS then begin
    If not (Value = '') then
      Result := (RegQueryValueEx(hTemp, PChar(Value), nil, nil, nil, nil) = ERROR_SUCCESS)
    else
      Result := True;
    RegCloseKey(hTemp);
  end;
end;

function AddRegKey(KEY:HKEY; Path, Keyname, Value, RegType: String):boolean;
var
  phkResult: HKEY;
begin
  Result := False;
  if RegType = 'Key' then begin
    RegOpenKeyEx(KEY, PChar(Path), 0, KEY_CREATE_SUB_KEY, phkResult);
    Result := (RegCreateKey(phkResult, PChar(Keyname), phkResult) = ERROR_SUCCESS);
    RegCloseKey(phkResult);
  end else begin
    if RegOpenKeyEx(KEY, PChar(Path), 0, KEY_SET_VALUE, phkResult) = ERROR_SUCCESS then
    begin
      Result := (RegSetValueEx(phkResult, Pchar(Keyname), 0, REG_SZ, Pchar(Value), Length(Value)) = ERROR_SUCCESS);
      RegCloseKey(phkResult);
    end else
      Result := False;
  end;
end;

end.
