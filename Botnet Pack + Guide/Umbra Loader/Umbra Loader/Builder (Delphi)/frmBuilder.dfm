object Form1: TForm1
  Left = 524
  Top = 365
  BorderStyle = bsSingle
  Caption = 'Umbra Loader Builder'
  ClientHeight = 256
  ClientWidth = 249
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object lbl1: TLabel
    Left = 12
    Top = 16
    Width = 26
    Height = 13
    Caption = 'Host:'
  end
  object lbl2: TLabel
    Left = 12
    Top = 40
    Width = 80
    Height = 13
    Caption = 'Path to bot.php:'
  end
  object lbl3: TLabel
    Left = 12
    Top = 64
    Width = 46
    Height = 13
    Caption = 'Filename:'
  end
  object lbl4: TLabel
    Left = 32
    Top = 152
    Width = 57
    Height = 13
    Caption = 'Startupkey:'
    Enabled = False
  end
  object lbl5: TLabel
    Left = 12
    Top = 176
    Width = 101
    Height = 13
    Caption = 'Connection Intervall:'
  end
  object lbl6: TLabel
    Left = 124
    Top = 176
    Width = 41
    Height = 13
    Caption = '1 Minute'
  end
  object lbl7: TLabel
    Left = 12
    Top = 88
    Width = 34
    Height = 13
    Caption = 'Mutex:'
  end
  object lbl8: TLabel
    Left = 12
    Top = 224
    Width = 54
    Height = 13
    Caption = 'Version 0.1'
  end
  object lbl9: TLabel
    Left = 12
    Top = 240
    Width = 86
    Height = 13
    Caption = 'Codename: Bozok'
  end
  object edt1: TEdit
    Left = 100
    Top = 12
    Width = 141
    Height = 21
    TabOrder = 0
    Text = 'ballablu.hostei.com'
  end
  object edt2: TEdit
    Left = 100
    Top = 36
    Width = 141
    Height = 21
    TabOrder = 1
    Text = '/Panel/bot.php'
  end
  object chk1: TCheckBox
    Left = 16
    Top = 108
    Width = 97
    Height = 17
    Caption = 'Installation'
    Checked = True
    State = cbChecked
    TabOrder = 2
  end
  object chk2: TCheckBox
    Left = 16
    Top = 132
    Width = 97
    Height = 13
    Caption = 'Startup'
    Checked = True
    State = cbChecked
    TabOrder = 3
    OnClick = chk2Click
  end
  object edt3: TEdit
    Left = 100
    Top = 60
    Width = 141
    Height = 21
    TabOrder = 4
    Text = 'winsvchost.exe'
  end
  object edt4: TEdit
    Left = 100
    Top = 148
    Width = 141
    Height = 21
    Enabled = False
    TabOrder = 5
    Text = 'Windows Updater'
  end
  object TrackBar1: TTrackBar
    Left = 12
    Top = 192
    Width = 233
    Height = 25
    Max = 60
    Min = 1
    Position = 1
    TabOrder = 6
    OnChange = TrackBar1Change
  end
  object edt5: TEdit
    Left = 100
    Top = 84
    Width = 141
    Height = 21
    TabOrder = 7
    Text = 'SD"$%'#167'!C('#167'!!'
  end
  object btn1: TButton
    Left = 160
    Top = 224
    Width = 85
    Height = 29
    Caption = 'Build'
    TabOrder = 8
    OnClick = btn1Click
  end
  object XPManifest1: TXPManifest
    Left = 56
    Top = 8
  end
end
