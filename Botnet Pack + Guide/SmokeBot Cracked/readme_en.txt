To install the panels on the hosting you will need:
- Apache 2
- PHP 5.2 + php_mbstring
- MySQL
- for owners SOCKS-module - need function on fsockopen in PHP 5.2

ATTENTION!!! Don't use FileZilla for upload admin panel files. Use FTP or server control panels.

ATTENTION!!! Test bot work only on real systems or only on VirtualBox.

ATTENTION!!! If you want delete loader from your computer, run HowToDelete.exe (from tools folder) and make steps from message

To install:
- Copy the contents of the archive for hosting
- Set right on the 0777 folder "data","exe" and "shell"
- In the file "./inc/cfg.php" to register the data to access the database, the admin name and password (the database must be pre-create through phpMyAdmin or similar tools)
- Run the file "install.php" (after the script work is over, check the permissions on the folders "exe", "data", "shell" - 0777)
- Delete the file "install.php"

Admin panel paths:
- The control center is located at http://"domain/folder"/control.php (domain and folder specified by customer)
- Bot information sended to http://"domain/folder"/index.php (domain and folder specified by customer)
* Please check availability to admin panel index.php file (must return 404 error =) )

Usage:

1) Menu - contains the navigation menu to control the bots and create jobs for them
- STATS - general statistics for bots, the overall total amount today, online, number of tasks, successful launches and downloads, bots to update the statistics on versions of Windows and statistics on countries, doubles counter, bots count for any seller id

- BOTS - Detailed statistics for bots,ID, IP, last access time, version of Windows and country, seller id, command for personal task. Also work search for some parameters.

- TASKS - work with the tasks for bots, the ability to upload a file to hosting or remote boot it (the bot itself will download a file from a remote server and execute it), showgirl on each task (loading and running, local or remote download), delete, editing or "pause" for each job. You can also set a limit of downloads for each job. You can load DLL's and run it from LoadLibrary (in address space of loader process) or regsrv32, all variants run OEP code of DLL.

- OPTIONS - clear/delete all tasks(including files) or only personal tasks(w/o files), cleaning all the statistics, sets for bot updating (2) and setting reserve admin panel address (7)

- LOGS - work with logs grabber (download, delete) *only if you have a module

- SOCKS - working with socks proxy (list and ip permissions for bots) *only if you have a module (4)

- HOSTS - work with hosts file (data for the replace) *only if you have a module (8)

- CMD SHELL - work with console commands (command and list of reports) *only if you have a module (9)

2) Self-update - bots can update themselves on the newer version. To do this, select OPTIONS menu and set update file (local or remote).
- How to update the bots? - Make sure you have the bots;) (All bots> 0), specify the update method and specify the file or URL to update the page with summary statistics for bots in the line "For update" to be displayed with the number of bots to update (usually = All bots), as soon as the bot will update and successfully (!!!) restarted, in database will be set a flag for a successful update of the bot and in the future he will not try to update itself for the next update, which will set the owner of the bot.

3) Geo-targeting (download for specific countries) - this feature allows you to select a country or several countries for filtering download tasks of bots. In the textbox to indicate the "index" (usually two characters) that characterizes the country, for example - RU,US,GB (ie, for this assignment will be considered only boats from Russia, USA and UK). By default, geo-targeting is configured to bot from all countries (all), is also worth noting that the panel may not be able to identify the country and this bot bot is assigned a code - XX. Parameter index case insensitive, ie you can enter and uppercase and lowercase letters. To see the current geo-targeting settings for a specific job to point the mouse cursor on the icon of the globe in line with this task.

4) SOCKS-module - a module that will add functionality to your botnet, Socks5 protocol is used without authorization, but with the ability to restrict access to the bots by IP-address. It is also possible to get a list of all alive bots on the appropriate link (can be used in various software and so on). Module does not work on machines that use NAT. The principle of operation is very simple: install loader, then in the admin panel, you enter IP-addresses that are allowed access to the SOCKS-module.

5) Guest statistics - a special opportunity to admin, which allows us to show the short stats on a particular back (useful if you sell someone downloads). Ability to use access by login/password, which are set the same for all options and set in the file "./inc/cfg.php". To get a link to the guest stats, simply click on the link "Stat's" in the appropriate column of the table with data on all tasks (menu - TASKS)

6) Work with builders for sellers:
- To work, run "Builder.exe" (the folder with the builder should be present file "Loader.exe") and enter Seller ID (which should be equal to the Seller ID for a particular task), if builder will notify and ready build the loader will be placed in the folder "sellers" and will have the file name is the same as the Seller ID (so keep in mind that you can not use forbidden characters for file names in Windows)
- Then create a task in admin panel with the required Seller ID (loader will load all files with that Seller ID)
- If you want use task for all bots, then set Seller ID for this task equal to "0" (default value)

7) Protection against loss of a botnet - currently implemented the following system:
- Every build of loader includes two urls for reports, the first address - main and the second (!!!) is activated only when main not work (5 attempts)
- Loader send to the admin panel a special request, which gets an extra address for reports (if it is specified in menu section OPTIONS), the address will be used instead of the second (!!!) in loader, in case of unavailability of first.
In other words, for minimize chance of losing the botnet is recommended after the installation of the admin immediately enter the reserve address, but not necessarily immediately prescribe DNS for this domain (of course the domain must already be registered so that it could not use the other). When main (first) address will blocked, the bot will turn to reserve address, where you can set the update on a new build of the loader, which must contain the current address (previous reserve) and a new main address, will thus be kept to a minimum loss of bots blocking/disabling domains.

8) The substitution HOSTS - a module that allows to replace the contents of the hosts file on the target machine. Through this module you can deny access to certain domains or redirect the call to a domain on a replacement IP-address. For example, for spoofing IP-addresses for the domain "vk.com" should be in the box to add the line "1.1.1.1 vk.com", where "1.1.1.1" - IP-address, which will move the user when accessing the domain "vk.com ". To restore the original hosts-file, you must enter a string - "restore_orig" in the field for the data and wait until all the bots will be updated. It should be noted that the change in the margin for the data - for all bots reset flag replacing.

9) Commands module - set task for all bots - execute console utils, like a "net view", "ipconfig /all" and etc. All reporst of console output of this commands will be saved in text files on admin panel. And they maybe downloaded or deleted.
