-- phpMyAdmin SQL Dump
-- version 3.3.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 15, 2012 at 07:27 PM
-- Server version: 5.1.52
-- PHP Version: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: ``
--

-- --------------------------------------------------------

--
-- Table structure for table `l`
--

CREATE TABLE IF NOT EXISTS `l` (
  `k` varchar(32) COLLATE latin1_general_ci NOT NULL,
  `n` varchar(32) COLLATE latin1_general_ci NOT NULL,
  `time` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `l`
--


-- --------------------------------------------------------

--
-- Table structure for table `n`
--

CREATE TABLE IF NOT EXISTS `n` (
  `ip` varchar(15) COLLATE latin1_general_ci NOT NULL,
  `n` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `n`
--


-- --------------------------------------------------------

--
-- Table structure for table `td`
--

CREATE TABLE IF NOT EXISTS `td` (
  `ip2` varchar(15) COLLATE latin1_general_ci NOT NULL,
  `ip` varchar(15) COLLATE latin1_general_ci NOT NULL,
  `time` int(10) NOT NULL,
  UNIQUE KEY `ip2` (`ip2`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `td`
--

