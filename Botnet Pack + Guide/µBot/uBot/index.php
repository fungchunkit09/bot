<?php
session_start();

require_once('inc/config.php');

	if($_SESSION['login']) { header('Location: stats.php'); exit(); }

	$user = $_POST[user];
	$pass = $_POST[pass];
	if(isset($_POST['login'])){
		if($user == $correctuser && $pass == $correctpass)
			{	
				$_SESSION['login'] = true;
				$_SESSION['username'] = $user;
				header('Location: stats.php');
			}else{
			
				$error = 'Login Incorrect.';
			}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<title>µBOT</title>

<link rel="stylesheet" type="text/css" href="css/style.css"/>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>

<div id="wrapper" align="center" >
<div id="header"><img src="img/logo.png" alt="" style="border: 0px;" width="900" height="300"/></div>

<div id="navi">
	<ul>
	</ul>
</div>

<div id="content" style="text-align: center;">
	<p><form action="index.php" method="post">
		<p><label>User</label><input type="text" name="user" class="tb" /></p>
		<p><label>Password</label><input type="password" name="pass" class="tb" /></p>
		<input type="submit" name="login" value="Login" class="btn" />
	</form></p>
	<?php echo '<p style="color: red;">'.$error.'</p>'; ?>
</div>

<div id="footer">&nbsp;</div>

</div>

</body>
</html>