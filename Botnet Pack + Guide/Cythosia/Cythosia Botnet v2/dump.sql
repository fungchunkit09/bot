-- phpMyAdmin SQL Dump
-- version 3.1.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Erstellungszeit: 20. Juni 2010 um 17:53
-- Server Version: 5.1.30
-- PHP-Version: 5.2.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Datenbank: `hydra`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur f�r Tabelle `hydra_socks`
--

CREATE TABLE IF NOT EXISTS `hydra_socks` (
  `hwid` varchar(30) NOT NULL,
  `country` varchar(10) NOT NULL,
  `ip` varchar(15) NOT NULL,
  `port` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Daten f�r Tabelle `hydra_socks`
--


-- --------------------------------------------------------

--
-- Tabellenstruktur f�r Tabelle `hydra_tasks`
--

CREATE TABLE IF NOT EXISTS `hydra_tasks` (
  `taskID` int(11) NOT NULL AUTO_INCREMENT,
  `time` int(20) NOT NULL,
  `elapsed` int(20) NOT NULL,
  `command` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bots` int(10) NOT NULL,
  PRIMARY KEY (`taskID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Daten f�r Tabelle `hydra_tasks`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur f�r Tabelle `hydra_task_done`
--

CREATE TABLE IF NOT EXISTS `hydra_task_done` (
  `taskID` int(11) NOT NULL,
  `vicID` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten f�r Tabelle `hydra_task_done`
-- --------------------------------------------------------

--
-- Tabellenstruktur f�r Tabelle `hydra_victims`
--

CREATE TABLE IF NOT EXISTS `hydra_victims` (
  `ID` int(12) NOT NULL AUTO_INCREMENT,
  `PCName` tinytext NOT NULL,
  `BotVersion` tinytext NOT NULL,
  `InstTime` tinytext NOT NULL,
  `ConTime` tinytext NOT NULL,
  `Country` tinytext NOT NULL,
  `WinVersion` tinytext NOT NULL,
  `HWID` tinytext NOT NULL,
  `IP` tinytext NOT NULL,
  `taskID` int(11) NOT NULL,
  KEY `ID` (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Daten f�r Tabelle `hydra_victims`
