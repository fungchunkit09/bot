<?php require_once('../inc/session.php'); require_once('../inc/config.php'); require_once('../funcs/safe.php'); ?>

		<script type="text/javascript">	
			var chart;
			$(document).ready(function() {
				chart = new Highcharts.Chart({
					chart: {
						renderTo: 'container',
						margin: [20, 200, 60, 170]
					},
					title: {
						text: 'Country statistic'
					},
					plotArea: {
						shadow: null,
						borderWidth: null,
						backgroundColor: null
					},
					tooltip: {
						formatter: function() {
							return this.point.name +': '+ this.y +' %';
						}
					},
					plotOptions: {
						pie: {
							allowPointSelect: true,
							cursor: 'pointer',
							dataLabels: {
								enabled: false,
								formatter: function() {
									if (this.y > 5) return this.point.name;
								},
								color: 'white',
								style: {
									font: '13px Trebuchet MS, Verdana, sans-serif'
								}
							}
						}
					},
					legend: {
						layout: 'vertical',
						style: {
							left: 'auto',
							bottom: 'auto',
							right: '50px',
							top: '100px'
						}
					},
				    series: [{
						type: 'pie',
						name: '',
						data: [
							<?php
						   	$q1 = mysql_query("SELECT * FROM zombies");
							$alle = mysql_num_rows($q1);
							  
							$query2 = mysql_query("SELECT * FROM zombies GROUP BY countrylong HAVING count(countrylong) >= 1");
							while($row = mysql_fetch_array($query2))
							{
								
								$country = $row['countrylong'];
		
								$q3 = mysql_query("SELECT * FROM zombies WHERE countrylong LIKE '".safe_sql($country)."'");
								
								$zahl = mysql_num_rows($q3);
								$gesamt = $alle;
								$total  = $zahl/$gesamt*100;
								
								echo "['".$country." [".$zahl." Bots]', ".round($total, 1)."],\r\n";		
							}
						?>
						]
					}]
				});
			});			
		</script>	

		<div id="container" style="width: 800px; height: 400px; margin: 0 auto"></div>		