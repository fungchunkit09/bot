<?php 
require_once('../inc/session.php');
require_once('../inc/config.php'); 

echo '<p><label>All bots:</label><b>'.stats('all').'</b></p>
	  <p><label>Online bots:</label><b>'.stats('on').'</b></p>
	  <p><label>Offline bots:</label><b>'.stats('off').'</b></p>
	  <p><label>Today new:</label><b>'.stats('todaynew').'</b></p>
	  <p><label>Yesterday new:</label><b>'.stats('yesterdaynew').'</b></p>';

function stats($a){
	$zombies = mysql_query("SELECT * FROM zombies");
	$zomball = mysql_num_rows($zombies);
	
	if($a == 'all'){
		$all    = mysql_query("SELECT * FROM zombies");
		return $zomball.'&nbsp;(100%)';
	}
	
	if($a == 'on'){
		$on    = mysql_num_rows(mysql_query("SELECT * FROM zombies WHERE status LIKE '1'"));
		$onend = $on/$zomball*100;
		
		return $on.'&nbsp;('.round($onend,0).'%)';
	}
	
	if($a == 'off'){
		$off    = mysql_num_rows(mysql_query("SELECT * FROM zombies WHERE status LIKE '0'"));
		$offend = $off/$zomball*100; 
		
		return $off.'&nbsp;('.round($offend,0).'%)';
	}
	
	if($a == 'todaynew'){
		$date     = date('Y-m-d');
		$today    = mysql_num_rows(mysql_query("SELECT * FROM zombies WHERE install LIKE '%$date%'"));
		$todayend = $today/$zomball*100;
		
		return $today.'&nbsp;('.round($todayend,0).'%)';
	}
	
	if($a == 'yesterdaynew'){
		$dateyest 	  = date('Y-m-d', time() - 86400);		
		$yesterday 	  = mysql_num_rows(mysql_query("SELECT * FROM zombies WHERE install LIKE '%$dateyest%'"));
		$yesterdayend = $yesterday/$zomball*100;
		
		return $yesterday.'&nbsp;('.round($yesterdayend,0).'%)';
	}	
}
?>

