<?php require_once('inc/session.php'); require_once('inc/body.php'); require_once('inc/config.php'); ?>	

<script language="javascript">
$(document).ready(function() {
    $('a[rel*=facebox]').facebox();
})
</script>

<p>Country stats <b>|</b> Monthly stats <b>|</b> Other stats</p>

<a href="facebox/stats.php" rel="facebox" style="margin-left: 20px;"><img src="img/icons/stats.png" /></a>&nbsp;
<a href="facebox/statsmo.php" rel="facebox" style="margin-left: 50px;"><img src="img/icons/statsmo.png" /></a>&nbsp;
<a href="facebox/statsother.php" rel="facebox" style="margin-left: 50px;"><img src="img/icons/statsother.png" /></a>

<?php require_once('inc/footer.php'); ?>