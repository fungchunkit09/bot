<?php
#################################################
#             Elite Loader v3.0                 #
#                                               #
#    (c)oded by [PRO]MAKE.ME TeaM :P            #
#                                               #
# To BBC and McAfee:                            #
# Project to capture The World                  #
#                      in the implementation.   #
#                                               #
#            You lose! Who next? :)             #
#                                               #
#################################################
if(empty($_GET['type'])){ die('error'); }
$time = microtime(true);
$encoding = isset($_SERVER['HTTP_ACCEPT_ENCODING']) ? $_SERVER['HTTP_ACCEPT_ENCODING'] : '';
switch(true){
    case    strpos($encoding, 'deflate') !== false:
        $encoding   = 'deflate';
        header('Content-Encoding: '.$encoding);
        break;
    case    strpos($encoding, 'gzip') !== false:
        $encoding   = 'gzip';
        header('Content-Encoding: '.$encoding);
        break;
    default:
        $encoding   = 'txt';
        break;
}
$hash = '"'.file_get_contents($_GET['type'].'.sha1sum.'.$encoding).'"';
header('ETag: '.$hash);
header('Cache-Control: public');
header('Content-Type: text/javascript; charset=UTF-8');
switch(isset($_SERVER['HTTP_IF_NONE_MATCH']) && $hash === $_SERVER['HTTP_IF_NONE_MATCH']){
    case true:
        header('HTTP/1.1 304 Not Modified');
        break;
    case false:
        $project = file_get_contents($_GET['type'].'.source.'.$encoding);
        header('Content-Length: '.strlen($project));
        header('X-Served-By: [PRO]MAKE.ME');
        header('X-Served-In: '.round(microtime(true) - $time),4);
        echo $project;
        break;
}
?>
