﻿namespace FileGenerationByVSBoring
{
    using System.CodeDom.Compiler;
    using System.Configuration;
    using System.Runtime.CompilerServices;

    [CompilerGenerated, GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "10.0.0.0")]
    internal sealed class GeneratedFileByVS : ApplicationSettingsBase
    {
        private static GeneratedFileByVS class13_0 = ((GeneratedFileByVS) SettingsBase.Synchronized(new GeneratedFileByVS()));

        public static GeneratedFileByVS Class13_0
        {
            get
            {
                return class13_0;
            }
        }
    }
}

