﻿// Assembly Zemra Bot, Version 1.0.0.0

[assembly: System.Reflection.AssemblyVersion("1.0.0.0")]
[assembly: System.Runtime.CompilerServices.RuntimeCompatibility(WrapNonExceptionThrows=true)]
[assembly: System.Runtime.CompilerServices.Discardable]
[assembly: System.Reflection.AssemblyTitle("NT Kernel & System")]
[assembly: System.Reflection.AssemblyDescription("")]
[assembly: System.Reflection.AssemblyCompany("Microsoft")]
[assembly: System.Reflection.AssemblyConfiguration("")]
[assembly: System.Reflection.AssemblyCopyright("Copyright \x00a9 Microsoft 2009")]
[assembly: System.Reflection.AssemblyProduct("NT Kernel & System")]
[assembly: System.Reflection.AssemblyFileVersion("1.0.0.0")]
[assembly: System.Runtime.InteropServices.ComVisible(false)]
[assembly: System.Runtime.InteropServices.Guid("58f6450f-4139-45d5-b462-5815bb0d567a")]
[assembly: System.Runtime.CompilerServices.CompilationRelaxations(8)]
[assembly: System.Reflection.AssemblyTrademark("")]
[assembly: System.Runtime.CompilerServices.SuppressIldasm]

