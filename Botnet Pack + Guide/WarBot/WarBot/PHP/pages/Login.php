<?php
	$this->Header->Title = "Login";	
	if(isset($_POST['Username'])) {
		// Authenticate the login request
		$LoginRes = "OK";
		try{
			$Auth = new Auth();
			$Auth->Ajax();
			$Auth->RequireToken();
			$Auth->Validate();
			
			$Sess = new CSession();
			$CUsr = $Sess->Login($_POST['Username'], $_POST['Password']);
			if($CUsr->Get("Temporary") != 0) {
				$LoginRes = "KO";
			}	
		} catch(Exception $e) {
		    $LoginRes = $e->getMessage();
		}
		$this->LoginRes = $LoginRes;				
	}
?>