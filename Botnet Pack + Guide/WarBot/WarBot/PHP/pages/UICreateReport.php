<?php
	$Output = "Unknown error occured, please try again later.";
	try {
		$Auth = new Auth();
		$Auth->RequireSession();
		$Auth->Ajax();
		$Auth->Validate();
		$CUsr = User::$CUsr;
	
		if($CUsr->Get("Temporary") != 0)
			throw new Exception("You do not have access to this page.  Your IP address has been logged :)");
		if(isset($_POST['Username']) && isset($_POST['Password']) && isset($_POST['CommandID'])) {
			User::createNew($_POST['Username'], $_POST['Password'], TRUE, $_POST['CommandID']);
			$Output = "OK";
		}
			
	} catch(Exception $e) {
		$Output = $e->getMessage();
	}
	
	$this->Output = $Output;
?>