<html>
	<head>
		<title>Warbot Installer</title>
		<link href="../themes/Default/css/global.css" rel="stylesheet" />
		<link href="installer.css" rel="stylesheet" />
	</head>
	<body>
		<div id="InstallContainer" style="text-align:center">
			<span class="PanelHeader">INSTALL</span>
			<span class="PanelSubHeader">[WARBOT]</span><br /><br />
			<form action="install.php" method="post">
				<table style="position:absolute;left:37%;right:37%;top:125px">
					<tr>
						<td class="InstallerIdentifier">Desired username</td>
						<td class="InstallerParameter"><input name="AdminUsername" type="text" /></td>
					</tr>
					<tr>
						<td class="InstallerIdentifier">Desired password</td>
						<td class="InstallerParameter"><input name="AdminPassword" type="text" /></td>
					</tr>
					<tr>
						<td class="InstallerIdentifier">Database username</td>
						<td class="InstallerParameter"><input name="DatabaseUsername" type="text" /></td>
					</tr>
					<tr>
						<td class="InstallerIdentifier">Database password </td>
						<td class="InstallerParameter"><input name="DatabasePassword" type="text" /></td>
					</tr>
					<tr>
						<td class="InstallerIdentifier">Database hostname </td>
						<td class="InstallerParameter"><input name="DatabaseHost" type="text" /></td>
					</tr>
					<tr>
						<td class="InstallerIdentifier">Database name</td>
						<td class="InstallerParameter"><input name="DatabaseName" type="text" /></td>
					</tr>
					
					<tr>
						<td>
							<input name="reset" value="Reset" type="reset" />
						</td>
						<td>
							<input name="submit" value="Install" type="submit" />
						</td>
					</tr>	
				</table>
			</form>
		</div>	
	</body>
</html>