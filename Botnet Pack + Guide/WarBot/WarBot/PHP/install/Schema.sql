SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `bots`
-- ----------------------------
DROP TABLE IF EXISTS `Bots`;
CREATE TABLE `Bots` (
  `BotID` bigint(255) NOT NULL AUTO_INCREMENT,
  `UniqueID` varchar(128) DEFAULT NULL,
  `IP` varchar(15) DEFAULT NULL,
  `Country` varchar(2) DEFAULT NULL,
  `OSVerMajor` tinyint(1) DEFAULT NULL,
  `OSVerMinor` tinyint(1) DEFAULT NULL,
  `BotVerMajor` tinyint(2) DEFAULT NULL,
  `BotVerMinor` tinyint(1) DEFAULT NULL,
  `LastReq_Date` int(15) DEFAULT NULL,
  PRIMARY KEY (`BotID`),
  UNIQUE KEY `UniqueID` (`UniqueID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for `bots_command_log`
-- ----------------------------
DROP TABLE IF EXISTS `bots_command_log`;
CREATE TABLE `bots_command_log` (
  `CommandLogID` bigint(255) NOT NULL AUTO_INCREMENT,
  `BotID` bigint(255) NOT NULL,
  `CommandID` bigint(255) NOT NULL,
  `RunDate` int(50) NOT NULL,
  PRIMARY KEY (`CommandLogID`),
  KEY `BotID` (`BotID`),
  KEY `CommandID` (`CommandID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for `commands`
-- ----------------------------
DROP TABLE IF EXISTS `commands`;
CREATE TABLE `commands` (
  `CommandID` bigint(255) NOT NULL AUTO_INCREMENT,
  `CommandString` text,
  `CommandQuota` int(15) DEFAULT NULL,
  `CommandCount` int(15) NOT NULL DEFAULT '0',
  `Filters` text,
  `TargetBotID` bigint(20) DEFAULT NULL,
  `IssueDate` int(50) DEFAULT NULL,
  PRIMARY KEY (`CommandID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `Users`;
CREATE TABLE `Users` (
  `UserID` bigint(255) NOT NULL AUTO_INCREMENT,
  `Username` varchar(255) DEFAULT NULL,
  `Password` varchar(128) DEFAULT NULL,
  `Temporary` tinyint(1) NOT NULL DEFAULT '0',
  `CommandID` bigint(255) DEFAULT NULL,
  `Settings` mediumtext NOT NULL,
  PRIMARY KEY (`UserID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- View structure for `v_bots_command_log`
-- ----------------------------
DROP VIEW IF EXISTS `v_bots_command_log`;
CREATE ALGORITHM=UNDEFINED DEFINER=CURRENT_USER SQL SECURITY DEFINER VIEW `v_bots_command_log` AS select `bots_command_log`.`CommandLogID` AS `CommandLogID`,`bots_command_log`.`BotID` AS `BotID`,`bots_command_log`.`CommandID` AS `CommandID`,`bots_command_log`.`RunDate` AS `RunDate`,`commands`.`CommandString` AS `CommandString`,`Bots`.`UniqueID` AS `UniqueID`,`Bots`.`Country` AS `Country`,`Bots`.`OSVerMajor` AS `OSVerMajor`,`Bots`.`OSVerMinor` AS `OSVerMinor`,`Bots`.`BotVerMajor` AS `BotVerMajor`,`Bots`.`BotVerMinor` AS `BotVerMinor`,`Bots`.`LastReq_Date` AS `LastReq_Date` from ((`bots_command_log` join `commands` on((`bots_command_log`.`CommandID` = `commands`.`CommandID`))) join `Bots` on((`bots_command_log`.`BotID` = `Bots`.`BotID`)));