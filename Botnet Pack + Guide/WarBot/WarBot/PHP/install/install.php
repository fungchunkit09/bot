<?php
	if (isset($_POST["submit"])) {
		//Load in everything!
		require_once("../Requires.php");
		
		//Set up vars
		$DBUsr = $_POST["DatabaseUsername"];
		$DBPsw = $_POST["DatabasePassword"];
		$DBHost = $_POST["DatabaseHost"];
		$DBName = $_POST["DatabaseName"];
		$Username = $_POST["AdminUsername"];
		$Password = $_POST["AdminPassword"];
		
		//Test Database info.
		try {
			$Con = DB::Connect($DBHost, $DBUsr, $DBPsw, $DBName);
		} catch (Exception $E) {
			//DB Error, gogo sub!
			echo "Database fail, try again!";
			die();
		}
		
		//Parse the Schema.sql into singular queries!
		$Queries = explode(";", file_get_contents("Schema.sql"));
		try {
			foreach ($Queries as $Query)
				DB::Query($Query);
		} catch (Exception $E) {
			echo "A database error occurred: " . DB::GetError();
			die();
		}
		
		//Make the user
		try {
			$User = new User();
			$User->Set("Username", $Username);
			$User->Set("Password", $Password);
		} catch (Exception $E) {
			echo "An error occurred while creating the administrator:<br>";
			echo $E->getMessage();
			die();
		}
		
		//Write the database infos
		$Settings = <<<EOD
<?php
define("DB_User", '%s');
define("DB_Pass", '%s');
define("DB_Host", '%s');
define("DB_Name", '%s');
?>
EOD;
		$Settings = sprintf($Settings, $DBUsr, $DBPsw, $DBHost, $DBName);
		file_put_contents("../Settings.php", $Settings);
		echo "Installation is complete!";
		die();
	}
?>