<?php
	/**
	 * Used in association with the Base class.
	 * @author SAC
	 * @version 1.0.0
	 */
	class Validator {
		private $Error = false;
		private $Value = false;
		public function __construct($Value = null) {
			$this->SetValue($Value);
		}
		public function GetError() {
			return $this->Error;
		}
		public function Error($Err) {
			$this->Error = $Err;
		}
		public function SetValue($Value) {
			$this->Value = $Value;
		}
		public function GetValue() {
			return $this->Value;
		}
	}
?>