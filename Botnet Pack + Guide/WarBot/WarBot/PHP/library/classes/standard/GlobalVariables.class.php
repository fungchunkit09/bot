<?php
	/**
	 * GlobalVariables (GVar) Class - STATIC
	 * A static class which allows you easy access to variables anywhere in your script
	 * @author SAC
	 * @version 1.0.0
	 */
	class GVar {
		/**
		 * All the variables stored in memory
		 * @var Array
		 */
		private static $Variables = array();
		
		/**
		 * Sets the key (variable) with the value passed
		 * @return Null
		 * @param $Key Mixed Variable to assign value to
		 * @param $Value Mixed Value to assign
		 */
		public static function Set($Key, $Value) {
			self::$Variables[$Key] = $Value;
		}
		
		/**
		 * Alias of Set
		 */
		public static function S($Key, $Value) {
			self::Set($Key, $Value);
		}
		
		/**
		 * Gets the key/variables value
		 * @return Mixed
		 * @param $Key Mixed Variable to get value for
		 */
		public static function Get($Key) {
			return @self::$Variables[$Key];
		}
		
		/**
		 * Alias of Get
		 */
		public static function G($Key) {
			return self::Get($Key);
		}
	}
?>