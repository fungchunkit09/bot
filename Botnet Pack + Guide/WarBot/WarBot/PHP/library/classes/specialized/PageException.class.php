<?php
	class PageException extends Exception {
		protected $page = "";
		
		public function __construct($message, $page = false) {
			$this->message = $message;
			$this->page = $page;
		}
		
		public function getPage() {
			return $this->page;
		}
	}
?>