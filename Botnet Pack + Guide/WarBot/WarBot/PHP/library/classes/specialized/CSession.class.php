<?php
	define("FLASH_NOTICE", "Notice");
	define("FLASH_ERROR", "Error");
	class CSession extends Session {
		public function __construct() {
			if ($this->HasMemory() && !$this->IsLogged()) {
				$Username = DB::Escape(base64_decode($_COOKIE['Username']));
				$Hash = $_COOKIE['Hash'];
				$Users = User::Find(array("WHERE" => "Username = '$Username'"));
				if (count($Users) > 0) {
					$Usr = $Users[0];
					$CHash = $this->GenRemHash($Usr);
					if ($Hash == $CHash) //I REMEMBER!!!
						$this->Login($Usr->Get("Username"), $Usr->Get("Password"), true);
					else //User agent might've changed, or editing cookies
						$this->ClearMemory();
				} else //Their username changed...
					$this->ClearMemory();
			}
		}
		
		public function Login($Username, $Password, $Remember = false) {
			$Username = DB::Escape($Username);
			$Password = DB::Escape($Password);
			$Users = User::Find(array("WHERE" => "Username = '$Username' AND Password = '$Password'"));
			if (count($Users) > 0) {
				$Usr = $Users[0];
				$this->Hash = $this->GenHash();
				$this->User = $Usr->Get("Username");
				$this->Pass = $Usr->Get("Password");
				$this->ID = $Usr->Get("UserID");
				if ($Remember)
					$this->Remember($Usr);
				return $Usr;
			} else
				throw new Exception("Incorrect username or password!");
			return false;
		}
		
		public function Logout() {
			unset($this->Hash);
			unset($this->User);
			unset($this->Pass);
			unset($this->ID);
			$this->ClearMemory();
		}
				
		public function IsLogged() {
			$Hash = $this->GenHash();
			if ($this->IsVal("Hash", $Hash)) {
				$Check = array("User", "Pass", "ID");
				foreach ($Check as $V) {
					if (!isset($this->$V)) return false;
				}
			} else return false;
			return true;
		}
		public function CheckSession(User $Usr) {
			if ($Usr->Get("Username") != $this->User || $Usr->Get("Password") != $this->Pass) {
				$this->Logout();
				return false;
			}
			return true;
		}
		
		private function GenRemHash(Player $Usr) {
			return sha1(sha1($Usr->Get("Username")) . "lolwut??" . sha1($Usr->Get("Password")) . sha1($_SERVER['HTTP_USER_AGENT']));
		}
		
		public function Remember(Player $Usr) {
			$Username = base64_encode($Usr->Get("Username"));
			$Hash = $this->GenRemHash($Usr);
			$Time = strtotime("+1 year");
			setcookie("Username", $Username, $Time);
			setcookie("Hash", $Hash, $Time);
		}
		
		public function HasMemory() {
			return (isset($_COOKIE['Username']) && isset($_COOKIE['Hash']));
		}
		
		public function ClearMemory() {
			setcookie("Username", "", time()-3600);
			setcookie("Hash", "", time()-3600);
		}
		
		public function Flash($Message, $Type = FLASH_NOTICE, $Get = false) {
			$Stamp = $Type . "_Stamp";
			if ($Get) {
				if (isset($this->$Type)) {
					if ($this->$Stamp > time() && count($this->$Type) > 0) {
						$Message = "<ul>";
						foreach ($this->$Type as $i)
							$Message .= "<li>$i</li>";
						$Message .= "</ul>";
						unset($this->$Type);
						unset($this->$Stamp);
						return $Message;
					}
					unset($this->$Type);
					unset($this->$Stamp);
				}
				return false;
			}
			if (!isset($this->$Type))
				$this->$Type = array();
			$Get = $this->$Type;
			$Get[] = $Message;
			$this->$Type = $Get;
			$this->$Stamp = time() + 20;
		}
		
		public function RegenToken() {
			$this->Token = sha1(microtime() . $_SERVER["REMOTE_ADDR"]);
			return $this->Token;
		}
	}
?>