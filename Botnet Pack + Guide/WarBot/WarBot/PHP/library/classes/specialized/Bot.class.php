<?php
	class Bot extends Base {
		public static $Table = "Bots";
		private static $IntOptions = array("options"=>array("min_range"=>0, "max_range" => 99999));
		
		public function __construct($ID = false, $Row = false) {
			parent::__construct($ID, $this, $Row, self::$Table);
		}
		
		public function runNextCommands() {
			$CommandSeperator = chr(13) . chr(10);
			
			//Get date of last command run, if any
			$LastDate = 0;
			
			$Override = array(
				"WHERE"		=>	"BotID = '" . $this->Get("BotID") . "'",
				"ORDER BY"	=>	"RunDate DESC",
				"LIMIT"		=>	"1");
			$CommandLogs = BotCommandLog::Find($Override);
			if (count($CommandLogs) > 0) {
				$CommandLog = $CommandLogs[0];
				$LastDate = $CommandLog->Get("RunDate");
			}
			
			//Find all commands that match filters and have not have their quotas met...
			$Override = array(
				"WHERE"		=>	"IssueDate > '$LastDate' AND CommandCount < CommandQuota",
				"ORDER BY"	=>	"IssueDate ASC");
			$Commands = BotCommand::Find($Override);
			if (count($Commands) > 0) {
				$Output = "";
				foreach ($Commands as $Command) {
					if ($Command->botCanRun($this))
						$Output .= $CommandSeperator . $Command->Get("CommandString");
					$Command->Run($this);
				}
				return $Output;
			}
			return "";
		}
		
		//VALSAN
		public function ValSan_Country($Input) {
			$V = new Validator($Input);
			if (strlen($Input) != 2) {
				$V->SetValue("VA");
			}
			$Input = strtoupper($Input);
			$V->SetValue($Input);
			return $V;
		}
		public function ValSan_OSVerMajor($Input) {
			$Input = intval($Input);
			$V = new Validator($Input);
			return $V;
		}
		public function ValSan_OSVerMinor($Input) {
			$Input = intval($Input);
			$V = new Validator($Input);
			return $V;
		}
		public function ValSan_BotVerMajor($Input) {
			$Input = intval($Input);
			$V = new Validator($Input);
			return $V;
		}
		public function ValSan_BotVerMinor($Input) {
			$Input = intval($Input);
			$V = new Validator($Input);
			return $V;
		}
		
		//Static
		static public function generateUID($IP) {
			return sha1(microtime(true) . $IP);
		}
	}
	
	class BotIP extends Base {
		public static $Table = "Bots_IPs";
		public function __construct($ID = false, $Row = false) {
			parent::__construct($ID, $this, $Row, self::$Table);
		}	
	}
?>