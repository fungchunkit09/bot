<?php
	class User extends Base {
		public static $CUsr = false;
		public static $Table = "Users";
		public function __construct($ID = false, $Row = false) {
			$this->_SaveOnDestruct = true;
			parent::__construct($ID, $this, $Row, self::$Table);
		}
		
		public function getAllSettings() {
			$Settings = $this->Get("Settings");
			if (!empty($Settings))
				$Settings = json_decode($Settings, true);
			else
				$Settings = array();
			return $Settings;
		}
		public function __set($Key, $Value) {
			$Settings = $this->getAllSettings();
				
			$Settings[$Key] = $Value;
			
			$this->Set("Settings", json_encode($Settings));
		}
		public function __get($Key) {
			$Settings = $this->getAllSettings();
			
			if (array_key_exists($Key, $Settings))
				return $Settings[$Key];
			
			throw new Exception("Setting not found.");
		}
		public function __isset($Key) {
			$Settings = $this->getAllSettings();
			return array_key_exists($Key, $Settings);
		}
		public function __unset($Key) {
			$Settings = $this->getAllSettings();
			unset($Settings[$Key]);
		}
		function getIconXY($Name) {
			if (isset($this->UIState_Icons)) {
				foreach ($this->UIState_Icons as $Icon) {
					if($Icon["Icon"] == $Name) {
						return $Icon["X"] . ", " . $Icon["Y"];
						break;	
					}
				}
			}
			return "0, 0";
		}
		function getLastIP() {
			if(!isset($this->LoginIP_Current)) {
				$this->LoginIP_Current = $_SERVER['REMOTE_ADDR'];
				$this->LoginIP_Last = "never";
			}
			if($this->LoginIP_Current != $_SERVER['REMOTE_ADDR']) {
				$this->LoginIP_Last = $this->LoginIP_Current;
				$this->LoginIP_Current = $_SERVER['REMOTE_ADDR'];
			}
			return $this->LoginIP_Last;
		}
		/*ValSan Methods*/
		function ValSan_Username($Username) {
			$Val = new Validator($Username);
			$Len = strlen($Username);
			if ($Len < 4) {
				$Val->Error("Username too short, must be atleast 4 characters");
				return $Val;
			} elseif ($Len > 16) {
				$Val->Error("Username too long, it must be less than 16 characters");
				return $Val;
			}
			if (!eregi("[A-Z0-9_]+", $Username)) {
				$Val->Error("Username may only contain alphanumeric (a-z, 0-9) characters including underscores");
				return $Val;
			}
			$Val->SetValue($Username);
			return $Val;
		}
		function ValSan_Password($Password) {
			$Val = new Validator($Password);
			$Len = strlen($Password);
			if ($Len < 4) {
				$Val->Error("Password too short, must be atleast 4 characters");
				return $Val;
			} elseif ($Len > 16) {
				$Val->Error("Password too long, it must be less than 16 characters");
				return $Val;
			}
			if (!eregi("[A-Z0-9_]+", $Password)) {
				$Val->Error("Password may only contain alphanumeric (a-z, 0-9) characters including underscores");
				return $Val;
			}
			$Password = sha1($Password);
			$Val->SetValue($Password);
			return $Val;
		}
		/*Static Methods*/
		static function createNew($Username, $Password, $Temporary = false, $CommandID = -1) {
			$Username_S = DB::Escape($Username);
			$Override = array(
				"WHERE"	=>	"Username = '$Username_S'");
			$Users = User::Find($Override);
			if (count($Users) > 0)
				throw new Exception("That username is already taken");
			
			if ($CommandID != -1) {
				$CommandID_S = DB::Escape($CommandID);
				$Override = array(
					"WHERE"	=>	"CommandID = '$CommandID_S'");
				$Commands = BotCommand::Find($Override);
				if (count($Commands) <= 0)
					throw new Exception("The CommandID provided does not reference a real command!");
			}
			
			$Usr = new User();
			$Usr->Set("Username", $Username);
			$Usr->Set("Password", $Password);
			if ($Temporary) {
				$Usr->Set("Temporary", 1);
				$Usr->Set("CommandID", $CommandID);
			}
		}
	}
?>