<?php
	if (!defined("IP"))
		define("IP", $_SERVER["REMOTE_ADDR"]);
	
	class GeoLocation {
		static private $URL_Full = "http://ipinfodb.com/ip_query.php?ip=%s&output=json";
		static private $URL_Country = "http://ipinfodb.com/ip_query_country.php?ip=%s&output=json";
		private $GeoObject;
		private $IP;
		
		public function __construct($IP = IP, $Full = true) {
			$this->IP = $IP;
			if ($Full)
				$this->GeoObject = sprintf(self::$URL_Full, $IP);
			else
				$this->GeoObject = sprintf(self::$URL_Country, $IP);
			$this->GeoObject = json_decode(file_get_contents($this->GeoObject));
		}
		
		public function getIP() {
			return $this->IP;
		}
		public function getStatus() {
			return $this->GeoObject->Status;
		}
		public function getCountryCode() {
			return $this->GeoObject->CountryCode;
		}
		public function getCountryName() {
			return $this->GeoObject->CountryName;
		}
		public function getRegionCode() {
			return $this->GeoObject->RegionCode;
		}
		public function getRegionName() {
			return $this->GeoObject->RegionName;
		}
		public function getCity() {
			return $this->GeoObject->City;
		}
		public function getZipPostalCode() {
			return $this->GeoObject->ZipPostalCode;
		}
		public function getLatitude() {
			return $this->GeoObject->Latitude;
		}
		public function getLongitude() {
			return $this->GeoObject->Longitude;
		}
		public function getTimezone() {
			return $this->GeoObject->Timezone;
		}
		public function getGMTOffset() {
			return $this->GeoObject->Gmtoffset;
		}
		public function getDSTOffset() {
			return $this->GeoObject->Dstoffset;
		}
	}
?>