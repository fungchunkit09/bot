<?php
	define("AUTH_INVALID_REASON_NOLOG", "You must be logged in to access that page!");
	define("AUTH_INVALID_REASON_NOTEMP", "You are not authorized to access this page!");
	define("AUTH_INVALID_REASON_NOTOKEN", "Your request has been cancelled due to an invalid or missing token.");
	define("AUTH_TOKEN_VARIABLE", "auth");
	class Auth {
		private $Req_Logged = false;
		private $Req_Token = false;
		private $Req_NoTemp = false;
		private $RetURL = false;
		private $CUsr = false;
		private $Ajax = false;
		
		public function __construct($RetURL = false, $AjaxAuth = false) {
			$this->CUsr = User::$CUsr;
			$this->URL($RetURL);
			$this->Ajax = $AjaxAuth;
		}
		public function Ajax() {
			$this->Ajax = true;
		}
		public function RequireSession($AllowTemp = false) {
			$this->Req_Logged = true;
			$this->Req_NoTemp = !$AllowTemp;
			if (!$this->RetURL)
				$this->URL("Login");
		}
		public function RequireToken() {
			$this->Req_Token = true;
		}
		public function URL($URL) {
			if ($URL != false)
				$this->RetURL = $URL;
		}
		private function Invalid($Reason) {
			if ($this->Ajax)
				throw new Exception($Reason);
			else
				throw new PageException($Reason, $this->RetURL);
		}
		
		public function Validate($ResetIfValid = true) {
			if ($this->Req_Logged)
				if (!$this->CUsr)
					$this->Invalid(AUTH_INVALID_REASON_NOLOG);
				else {
					if ($this->CUsr->Get("Temporary") != 0 && $this->Req_NoTemp)
						throw new Exception(AUTH_INVALID_REASON_NOTEMP);
				}
			if ($this->Req_Token)
				if (!((array_key_exists(AUTH_TOKEN_VARIABLE, $_GET) || array_key_exists(AUTH_TOKEN_VARIABLE, $_POST)) && (@$_GET[AUTH_TOKEN_VARIABLE] == PREVTOKEN || @$_POST[AUTH_TOKEN_VARIABLE] == PREVTOKEN)) || PREVTOKEN == "")
					$this->Invalid(AUTH_INVALID_REASON_NOTOKEN);
			if ($ResetIfValid)
				$this->Reset();
		}
		public function Reset() {
			$this->Req_Logged = false;
			$this->Req_Token = false;
			$this->RetURL = false;
		}
	}
?>