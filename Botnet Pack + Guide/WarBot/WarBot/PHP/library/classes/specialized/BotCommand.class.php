<?php
	class BotCommand extends Base {
		public static $Table = "commands";
		public function __construct($ID = false, $Row = false) {
			parent::__construct($ID, $this, $Row, self::$Table);
		}
		public function botCanRun(Bot $Bot) {
			$TargetBotID = $this->Get("TargetBotID");
			if ($TargetBotID != 0) {
				if ($Bot->Get("BotID") == $TargetBotID)
					return true;
				else
					return false;
			}
			$Filters = $this->Get("Filters");
			if ($Filters == "")
				return true;
			$Filters = json_decode($Filters, true);
			foreach ($Filters as $Key => $Values) {
				$Matched = false;
				foreach ($Values as $Value) {
					if ($Bot->Get($Key) == $Value) {
						$Matched = true;
						break;
					}
				}
				if (!$Matched)
					return false;
			}
			return true;
		}
		public function Run(Bot $Bot) {
			$this->Set("CommandCount", $this->Get("CommandCount") + 1);
			$this->Save();
			
			$CommandLog = new BotCommandLog();
			$CommandLog->Set("BotID", $Bot->Get("BotID"));
			$CommandLog->Set("CommandID", $this->Get("CommandID"));
			$CommandLog->Set("RunDate", time());
			$CommandLog->Save();
		}
	}
?>