<?php
	if (!empty($LoginRes)):
		echo $LoginRes;
?>
	<script type="text/javascript">
		TOKEN = "<?php echo TOKEN; ?>";
	</script>
<?php
		return;
	endif;
?>
		<title>.:Warbot:.</title>
		<link rel="stylesheet" href="<?php echo $P_Path; ?>css/Login.css" type="text/css" media="all">
		<script type="text/javascript" src="<?php echo $P_Path; ?>js/sha1.js"></script>
		<script type="text/javascript" src="<?php echo $P_Path; ?>js/jquery-1.4.1.min.js"></script>
		<script type="text/javascript">
			//<!--
			var TOKEN = "<?php echo TOKEN; ?>";
			function DoLogin(frm) {
				var szPassword;
				var szUsername;
				$("#LoginControls").hide();
				szUsername = frm.elements["LoginUsername"].value;
				szPassword = hex_sha1(frm.elements["LoginPassword"].value);
				
				$.post("index.php?p=Login&notemplate=1", {Username: szUsername, Password: szPassword, <?php echo AUTH_TOKEN_VARIABLE; ?>: TOKEN}, 
					function(data){
						if (data.substr(0, 2) == "OK") {
							setTimeout("window.location=/index.php?p=Main/.source", 3000);
							$("#LoginResponse").html("You have successfully logged in.<br /><a href=\"index.php?p=Main\">Click here</a> to continue.");
						} else if(data.substr(0, 2) == "KO") { // just.. lol
							setTimeout("window.location=/index.php?p=Task/.source", 3000);
							$("#LoginResponse").html("You have successfully logged in.<br /><a href=\"index.php?p=Task\">Click here</a> to continue.");
						} else {
							$("#LoginResponse").html(data);
							$("#LoginControls").fadeIn("slow");
						}
 				});
				return false;
			}
			$(document).ready(function() {
				$("#ButtonLoginCancel").click(function(){
				    window.location="about:blank";	
				});
			});
			//-->
		</script>
	</head>
	<body>
		<div id="LoginPanel">
			<span class="PanelHeader">LOGIN</span>
			<span class="PanelSubHeader">[WARBOT]</span><br />
			<div id="LoginResponse">
				
			</div>
			<div id="LoginControls">
				<form action="index.php?p=Login&notemplate=1" method="POST" onsubmit="return DoLogin(this);">
				Username <br /><input type="text" name="LoginUsername" maxlength="25"><br /><br />
				Password <br /><input type="password" name="LoginPassword" maxlength="64"><br /><br />
				<input type="submit" name="LoginSubmit" value="Login "> <input id="ButtonLoginCancel" type="button" name="LoginCancel" value="Cancel"> <br />
				</form>
			</div>

		</div>