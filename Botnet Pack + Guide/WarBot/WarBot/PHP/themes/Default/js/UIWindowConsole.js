function processConsole(obj) {
	//console.debug(obj);
	var command=$("#console-command").val();

	if (command.charAt(0) == "#") {
		var response="";
		switch(command) {
			case "#clr":
				$("#console-display").text("");
				$("#console-command").val("");
				return false;
				break;
			case "#mspin":
				window.location="http://meatspin.com";
				break;
			case "#cls":
				$("#console-display").text("");
				$("#console-command").val("");
				return false;
				break;
			case "#rdsk":
				var SerializedIcons;
				var iz;
				SerializedIcons = "";
				for (iz = 0; iz < DesktopIcons.length; iz++) {
					SerializedIcons += "&icon[]=" + DesktopIcons[iz].Name + ":" + "0:0";
				}
				$.ajax({ async:true, url: "index.php?p=UISaveState&notemplate=1" + SerializedIcons, type: "POST", data: SerializedIcons});
				response = "Icons reset to their default positions.  Refresh the page to see changes.";
				break;
			default:
				response="JS command not found";
				
		}
		if (response != "") {
			var mydiv = document.getElementById("console-display");
			$("#console-display").text($("#console-display").text() + String.fromCharCode(10) + "> " + command + String.fromCharCode(10) + response);
			mydiv.scrollTop = mydiv.scrollHeight;
		}
		$("#console-command").val("");
		return false;
	}
	$.ajax({url: "index.php?p=UIWindowConsole&notemplate=1&command=" + escape(command), success: function(data) {
		var mydiv = document.getElementById("console-display");
		$("#console-display").text($("#console-display").text() + String.fromCharCode(10) + "> " + command + String.fromCharCode(10) + data);
		mydiv.scrollTop = mydiv.scrollHeight;
	}});
	
	$("#console-command").val("");

	return false;
}