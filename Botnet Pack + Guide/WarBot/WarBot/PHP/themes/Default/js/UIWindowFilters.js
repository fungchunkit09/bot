var filtersBuffer='';
var filtersCountries = new Array();
function preloadCountries() {
	$.ajax({ async:true, url: "index.php?p=UIWindowFiltersGetCountries&notemplate=1", type: "GET", success: function(data) {
		filtersCountries = data.split(",");
	}});
}

function processFilters(obj) {
	var Callee;
	var preBuff='';
	Callee = obj.getAttribute("id").replace("Filters_", "");
	if(Callee.indexOf("_") != -1) {
		// It is a value
		// Add teh value to the buffer and reset list!
		preBuff = Callee.split("_").join("=");
		filtersBuffer += preBuff + ";";
		//$("#Filters_Buffer").val(filtersBuffer);
		$("#Loader_Filters").val(filtersBuffer);
		//$("#Filters_Buffer").focus();
	} else {
		// It is not a value.. :)
		//displayBase();
		switch(Callee) {
			case 'Version':
				preBuff = "BotVer=1.1";
				filtersBuffer += preBuff + ";";
				//$("#Filters_Buffer").val(filtersBuffer);
				$("#Loader_Filters").val(filtersBuffer);
				//$("#Filters_Buffer").focus();
				break;
			case 'ID':
				preBuff = "BotID=0";
				filtersBuffer += preBuff + ";";
				//$("#Filters_Buffer").val(filtersBuffer);
				$("#Loader_Filters").val(filtersBuffer);
				//$("#Filters_Buffer").focus();
				break;
			case 'IP':
				preBuff = "BotIP=127.0.0.1";
				filtersBuffer += preBuff + ";";
				//$("#Filters_Buffer").val(filtersBuffer);
				$("#Loader_Filters").val(filtersBuffer);
				//$("#Filters_Buffer").focus();
				break;
			case 'Country':
				displayCountries();
				break;
			case 'OS':
				displayOS();
				break;
			default:
				displayBase();
		}
	}
}


function displayBase() {
	clearDisplay();
	var baseElement;
	var newElement;
	baseElement = document.getElementById('Filters_List');
	newElement = document.createElement('span');
	newElement.setAttribute("id", "Filters_Country");
	newElement.setAttribute("class", "menuItem");
	newElement.setAttribute("onclick", "processFilters(this)");
	newElement.innerHTML = "Country";
	baseElement.appendChild(newElement);
	newElement = document.createElement('span');
	newElement.setAttribute("id", "Filters_OS");
	newElement.setAttribute("class", "menuItem");
	newElement.setAttribute("onclick", "processFilters(this)");
	newElement.innerHTML = "OS";
	baseElement.appendChild(newElement);
	newElement = document.createElement('span');
	newElement.setAttribute("id", "Filters_Version");
	newElement.setAttribute("class", "menuItem");
	newElement.setAttribute("onclick", "processFilters(this)");
	newElement.innerHTML = "Version";
	baseElement.appendChild(newElement);
	newElement = document.createElement('span');
	newElement.setAttribute("id", "Filters_ID");
	newElement.setAttribute("class", "menuItem");
	newElement.setAttribute("onclick", "processFilters(this)");
	newElement.innerHTML = "ID";
	baseElement.appendChild(newElement);
	newElement = document.createElement('span');
	newElement.setAttribute("id", "Filters_IP");
	newElement.setAttribute("class", "menuItem");
	newElement.setAttribute("onclick", "processFilters(this)");
	newElement.innerHTML = "IP";
	baseElement.appendChild(newElement);
}
function displayCountries() {
	var baseElement;
	var newElement;
	var Countries = {
		AF: "AFGHANISTAN", AF_Icon: "themes/Default/img/flags/af.png",
		AX: "ÅLAND ISLANDS", AX_Icon: "themes/Default/img/flags/ax.png",
		AL: "ALBANIA", AL_Icon: "themes/Default/img/flags/al.png",
		DZ: "ALGERIA", DZ_Icon: "themes/Default/img/flags/dz.png",
		AS: "AMERICAN SAMOA", AS_Icon: "themes/Default/img/flags/as.png",
AD: "ANDORRA", AD_Icon: "themes/Default/img/flags/ad.png",
AO: "ANGOLA", AO_Icon: "themes/Default/img/flags/ao.png",
AI: "ANGUILLA", AI_Icon: "themes/Default/img/flags/ai.png",
AQ: "ANTARCTICA", AQ_Icon: "themes/Default/img/flags/aq.png",
AG: "ANTIGUA AND BARBUDA", AG_Icon: "themes/Default/img/flags/ag.png",
AR: "ARGENTINA", AR_Icon: "themes/Default/img/flags/ar.png",
AM: "ARMENIA", AM_Icon: "themes/Default/img/flags/am.png",
AW: "ARUBA", AW_Icon: "themes/Default/img/flags/aw.png",
AU: "AUSTRALIA", AU_Icon: "themes/Default/img/flags/au.png",
AT: "AUSTRIA", AT_Icon: "themes/Default/img/flags/at.png",
AZ: "AZERBAIJAN", AZ_Icon: "themes/Default/img/flags/az.png",
BS: "BAHAMAS", BS_Icon: "themes/Default/img/flags/bs.png",
BH: "BAHRAIN", BH_Icon: "themes/Default/img/flags/bh.png",
BD: "BANGLADESH", BD_Icon: "themes/Default/img/flags/bd.png",
BB: "BARBADOS", BB_Icon: "themes/Default/img/flags/bb.png",
BY: "BELARUS", BY_Icon: "themes/Default/img/flags/by.png",
BE: "BELGIUM", BE_Icon: "themes/Default/img/flags/be.png",
BZ: "BELIZE", BZ_Icon: "themes/Default/img/flags/bz.png",
BJ: "BENIN", BJ_Icon: "themes/Default/img/flags/bj.png",
BM: "BERMUDA", BM_Icon: "themes/Default/img/flags/bm.png",
BT: "BHUTAN", BT_Icon: "themes/Default/img/flags/bt.png",
BO: "BOLIVIA, PLURINATIONAL STATE OF", BO_Icon: "themes/Default/img/flags/bo.png",
BA: "BOSNIA AND HERZEGOVINA", BA_Icon: "themes/Default/img/flags/ba.png",
BW: "BOTSWANA", BW_Icon: "themes/Default/img/flags/bw.png",
BV: "BOUVET ISLAND", BV_Icon: "themes/Default/img/flags/bv.png",
BR: "BRAZIL", BR_Icon: "themes/Default/img/flags/br.png",
IO: "BRITISH INDIAN OCEAN TERRITORY", IO_Icon: "themes/Default/img/flags/io.png",
BN: "BRUNEI DARUSSALAM", BN_Icon: "themes/Default/img/flags/bn.png",
BG: "BULGARIA", BG_Icon: "themes/Default/img/flags/bg.png",
BF: "BURKINA FASO", BF_Icon: "themes/Default/img/flags/bf.png",
BI: "BURUNDI", BI_Icon: "themes/Default/img/flags/bi.png",
KH: "CAMBODIA", KH_Icon: "themes/Default/img/flags/kh.png",
CM: "CAMEROON", CM_Icon: "themes/Default/img/flags/cm.png",
CA: "CANADA", CA_Icon: "themes/Default/img/flags/ca.png",
CV: "CAPE VERDE", CV_Icon: "themes/Default/img/flags/cv.png",
KY: "CAYMAN ISLANDS", KY_Icon: "themes/Default/img/flags/ky.png",
CF: "CENTRAL AFRICAN REPUBLIC", CF_Icon: "themes/Default/img/flags/cf.png",
TD: "CHAD", TD_Icon: "themes/Default/img/flags/td.png",
CL: "CHILE", CL_Icon: "themes/Default/img/flags/cl.png",
CN: "CHINA", CN_Icon: "themes/Default/img/flags/cn.png",
CX: "CHRISTMAS ISLAND", CX_Icon: "themes/Default/img/flags/cx.png",
CC: "COCOS (KEELING) ISLANDS", CC_Icon: "themes/Default/img/flags/cc.png",
CO: "COLOMBIA", CO_Icon: "themes/Default/img/flags/co.png",
KM: "COMOROS", KM_Icon: "themes/Default/img/flags/km.png",
CG: "CONGO", CG_Icon: "themes/Default/img/flags/cg.png",
CD: "CONGO, THE DEMOCRATIC REPUBLIC OF THE", CD_Icon: "themes/Default/img/flags/cd.png",
CK: "COOK ISLANDS", CK_Icon: "themes/Default/img/flags/ck.png",
CR: "COSTA RICA", CR_Icon: "themes/Default/img/flags/cr.png",
CI: "CÔTE D'IVOIRE", CI_Icon: "themes/Default/img/flags/ci.png",
HR: "CROATIA", HR_Icon: "themes/Default/img/flags/hr.png",
CU: "CUBA", CU_Icon: "themes/Default/img/flags/cu.png",
CY: "CYPRUS", CY_Icon: "themes/Default/img/flags/cy.png",
CZ: "CZECH REPUBLIC", CZ_Icon: "themes/Default/img/flags/cz.png",
DK: "DENMARK", DK_Icon: "themes/Default/img/flags/dk.png",
DJ: "DJIBOUTI", DJ_Icon: "themes/Default/img/flags/dj.png",
DM: "DOMINICA", DM_Icon: "themes/Default/img/flags/dm.png",
DO: "DOMINICAN REPUBLIC", DO_Icon: "themes/Default/img/flags/do.png",
EC: "ECUADOR", EC_Icon: "themes/Default/img/flags/ec.png",
EG: "EGYPT", EG_Icon: "themes/Default/img/flags/eg.png",
SV: "EL SALVADOR", SV_Icon: "themes/Default/img/flags/sv.png",
GQ: "EQUATORIAL GUINEA", GQ_Icon: "themes/Default/img/flags/gq.png",
ER: "ERITREA", ER_Icon: "themes/Default/img/flags/er.png",
EE: "ESTONIA", EE_Icon: "themes/Default/img/flags/ee.png",
ET: "ETHIOPIA", ET_Icon: "themes/Default/img/flags/et.png",
FK: "FALKLAND ISLANDS (MALVINAS)", FK_Icon: "themes/Default/img/flags/fk.png",
FO: "FAROE ISLANDS", FO_Icon: "themes/Default/img/flags/fo.png",
FJ: "FIJI", FJ_Icon: "themes/Default/img/flags/fj.png",
FI: "FINLAND", FI_Icon: "themes/Default/img/flags/fi.png",
FR: "FRANCE", FR_Icon: "themes/Default/img/flags/fr.png",
GF: "FRENCH GUIANA", GF_Icon: "themes/Default/img/flags/gf.png",
PF: "FRENCH POLYNESIA", PF_Icon: "themes/Default/img/flags/pf.png",
TF: "FRENCH SOUTHERN TERRITORIES", TF_Icon: "themes/Default/img/flags/tf.png",
GA: "GABON", GA_Icon: "themes/Default/img/flags/ga.png",
GM: "GAMBIA", GM_Icon: "themes/Default/img/flags/gm.png",
GE: "GEORGIA", GE_Icon: "themes/Default/img/flags/ge.png",
DE: "GERMANY", DE_Icon: "themes/Default/img/flags/de.png",
GH: "GHANA", GH_Icon: "themes/Default/img/flags/gh.png",
GI: "GIBRALTAR", GI_Icon: "themes/Default/img/flags/gi.png",
GR: "GREECE", GR_Icon: "themes/Default/img/flags/gr.png",
GL: "GREENLAND", GL_Icon: "themes/Default/img/flags/gl.png",
GD: "GRENADA", GD_Icon: "themes/Default/img/flags/gd.png",
GP: "GUADELOUPE", GP_Icon: "themes/Default/img/flags/gp.png",
GU: "GUAM", GU_Icon: "themes/Default/img/flags/gu.png",
GT: "GUATEMALA", GT_Icon: "themes/Default/img/flags/gt.png",
GG: "GUERNSEY", GG_Icon: "themes/Default/img/flags/gg.png",
GN: "GUINEA", GN_Icon: "themes/Default/img/flags/gn.png",
GW: "GUINEA-BISSAU", GW_Icon: "themes/Default/img/flags/gw.png",
GY: "GUYANA", GY_Icon: "themes/Default/img/flags/gy.png",
HT: "HAITI", HT_Icon: "themes/Default/img/flags/ht.png",
HM: "HEARD ISLAND AND MCDONALD ISLANDS", HM_Icon: "themes/Default/img/flags/hm.png",
VA: "HOLY SEE (VATICAN CITY STATE)", VA_Icon: "themes/Default/img/flags/va.png",
HN: "HONDURAS", HN_Icon: "themes/Default/img/flags/hn.png",
HK: "HONG KONG", HK_Icon: "themes/Default/img/flags/hk.png",
HU: "HUNGARY", HU_Icon: "themes/Default/img/flags/hu.png",
IS: "ICELAND", IS_Icon: "themes/Default/img/flags/is.png",
IN: "INDIA", IN_Icon: "themes/Default/img/flags/in.png",
ID: "INDONESIA", ID_Icon: "themes/Default/img/flags/id.png",
IR: "IRAN, ISLAMIC REPUBLIC OF", IR_Icon: "themes/Default/img/flags/ir.png",
IQ: "IRAQ", IQ_Icon: "themes/Default/img/flags/iq.png",
IE: "IRELAND", IE_Icon: "themes/Default/img/flags/ie.png",
IM: "ISLE OF MAN", IM_Icon: "themes/Default/img/flags/im.png",
IL: "ISRAEL", IL_Icon: "themes/Default/img/flags/il.png",
IT: "ITALY", IT_Icon: "themes/Default/img/flags/it.png",
JM: "JAMAICA", JM_Icon: "themes/Default/img/flags/jm.png",
JP: "JAPAN", JP_Icon: "themes/Default/img/flags/jp.png",
/*JE: "JERSEY SHORE", JE_Icon: "themes/Default/img/flags/je.png",*/
JO: "JORDAN", JO_Icon: "themes/Default/img/flags/jo.png",
KZ: "KAZAKHSTAN", KZ_Icon: "themes/Default/img/flags/kz.png",
KE: "KENYA", KE_Icon: "themes/Default/img/flags/ke.png",
KI: "KIRIBATI", KI_Icon: "themes/Default/img/flags/ki.png",
KP: "KOREA, DEMOCRATIC PEOPLE'S REPUBLIC OF", KP_Icon: "themes/Default/img/flags/kp.png",
KR: "KOREA, REPUBLIC OF", KR_Icon: "themes/Default/img/flags/kr.png",
KW: "KUWAIT", KW_Icon: "themes/Default/img/flags/kw.png",
KG: "KYRGYZSTAN", KG_Icon: "themes/Default/img/flags/kg.png",
LA: "LAO PEOPLE'S DEMOCRATIC REPUBLIC", LA_Icon: "themes/Default/img/flags/la.png",
LV: "LATVIA", LV_Icon: "themes/Default/img/flags/lv.png",
LB: "LEBANON", LB_Icon: "themes/Default/img/flags/lb.png",
LS: "LESOTHO", LS_Icon: "themes/Default/img/flags/ls.png",
LR: "LIBERIA", LR_Icon: "themes/Default/img/flags/lr.png",
LY: "LIBYAN ARAB JAMAHIRIYA", LY_Icon: "themes/Default/img/flags/ly.png",
LI: "LIECHTENSTEIN", LI_Icon: "themes/Default/img/flags/li.png",
LT: "LITHUANIA", LT_Icon: "themes/Default/img/flags/lt.png",
LU: "LUXEMBOURG", LU_Icon: "themes/Default/img/flags/lu.png",
MO: "MACAO", MO_Icon: "themes/Default/img/flags/mo.png",
MK: "MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF", MK_Icon: "themes/Default/img/flags/mk.png",
MG: "MADAGASCAR", MG_Icon: "themes/Default/img/flags/mg.png",
MW: "MALAWI", MW_Icon: "themes/Default/img/flags/mw.png",
MY: "MALAYSIA", MY_Icon: "themes/Default/img/flags/my.png",
MV: "MALDIVES", MV_Icon: "themes/Default/img/flags/mv.png",
ML: "MALI", ML_Icon: "themes/Default/img/flags/ml.png",
MT: "MALTA", MT_Icon: "themes/Default/img/flags/mt.png",
MH: "MARSHALL ISLANDS", MH_Icon: "themes/Default/img/flags/mh.png",
MQ: "MARTINIQUE", MQ_Icon: "themes/Default/img/flags/mq.png",
MR: "MAURITANIA", MR_Icon: "themes/Default/img/flags/mr.png",
MU: "MAURITIUS", MU_Icon: "themes/Default/img/flags/mu.png",
YT: "MAYOTTE", YT_Icon: "themes/Default/img/flags/yt.png",
MX: "MEXICO", MX_Icon: "themes/Default/img/flags/mx.png",
FM: "MICRONESIA, FEDERATED STATES OF", FM_Icon: "themes/Default/img/flags/fm.png",
MD: "MOLDOVA, REPUBLIC OF", MD_Icon: "themes/Default/img/flags/md.png",
MC: "MONACO", MC_Icon: "themes/Default/img/flags/mc.png",
MN: "MONGOLIA", MN_Icon: "themes/Default/img/flags/mn.png",
ME: "MONTENEGRO", ME_Icon: "themes/Default/img/flags/me.png",
MS: "MONTSERRAT", MS_Icon: "themes/Default/img/flags/ms.png",
MA: "MOROCCO", MA_Icon: "themes/Default/img/flags/ma.png",
MZ: "MOZAMBIQUE", MZ_Icon: "themes/Default/img/flags/mz.png",
MM: "MYANMAR", MM_Icon: "themes/Default/img/flags/mm.png",
NA: "NAMIBIA", NA_Icon: "themes/Default/img/flags/na.png",
NR: "NAURU", NR_Icon: "themes/Default/img/flags/nr.png",
NP: "NEPAL", NP_Icon: "themes/Default/img/flags/np.png",
NL: "NETHERLANDS", NL_Icon: "themes/Default/img/flags/nl.png",
AN: "NETHERLANDS ANTILLES", AN_Icon: "themes/Default/img/flags/an.png",
NC: "NEW CALEDONIA", NC_Icon: "themes/Default/img/flags/nc.png",
NZ: "NEW ZEALAND", NZ_Icon: "themes/Default/img/flags/nz.png",
NI: "NICARAGUA", NI_Icon: "themes/Default/img/flags/ni.png",
NE: "NIGER", NE_Icon: "themes/Default/img/flags/ne.png",
NG: "NIGERIA", NG_Icon: "themes/Default/img/flags/ng.png",
NU: "NIUE", NU_Icon: "themes/Default/img/flags/nu.png",
NF: "NORFOLK ISLAND", NF_Icon: "themes/Default/img/flags/nf.png",
MP: "NORTHERN MARIANA ISLANDS", MP_Icon: "themes/Default/img/flags/mp.png",
NO: "NORWAY", NO_Icon: "themes/Default/img/flags/no.png",
OM: "OMAN", OM_Icon: "themes/Default/img/flags/om.png",
PK: "PAKISTAN", PK_Icon: "themes/Default/img/flags/pk.png",
PW: "PALAU", PW_Icon: "themes/Default/img/flags/pw.png",
PS: "PALESTINIAN TERRITORY, OCCUPIED", PS_Icon: "themes/Default/img/flags/ps.png",
PA: "PANAMA", PA_Icon: "themes/Default/img/flags/pa.png",
PG: "PAPUA NEW GUINEA", PG_Icon: "themes/Default/img/flags/pg.png",
PY: "PARAGUAY", PY_Icon: "themes/Default/img/flags/py.png",
PE: "PERU", PE_Icon: "themes/Default/img/flags/pe.png",
PH: "PHILIPPINES", PH_Icon: "themes/Default/img/flags/ph.png",
PN: "PITCAIRN", PN_Icon: "themes/Default/img/flags/pn.png",
PL: "POLAND", PL_Icon: "themes/Default/img/flags/pl.png",
PT: "PORTUGAL", PT_Icon: "themes/Default/img/flags/pt.png",
PR: "PUERTO RICO", PR_Icon: "themes/Default/img/flags/pr.png",
QA: "QATAR", QA_Icon: "themes/Default/img/flags/qa.png",
/*RE: "RÉUNION", RE_Icon: "themes/Default/img/flags/re.png",*/
RO: "ROMANIA", RO_Icon: "themes/Default/img/flags/ro.png",
RU: "RUSSIAN FEDERATION", RU_Icon: "themes/Default/img/flags/ru.png",
RW: "RWANDA", RW_Icon: "themes/Default/img/flags/rw.png",
BL: "SAINT BARTHÉLEMY", BL_Icon: "themes/Default/img/flags/bl.png",
SH: "SAINT HELENA, ASCENSION AND TRISTAN DA CUNHA", SH_Icon: "themes/Default/img/flags/sh.png",
KN: "TTS AND NEVIS", KN_Icon: "themes/Default/img/flags/kn.png",
LC: "SAINT LUCIA", LC_Icon: "themes/Default/img/flags/lc.png",
MF: "SAINT MARTIN", MF_Icon: "themes/Default/img/flags/mf.png",
PM: "SAINT PIERRE AND MIQUELON", PM_Icon: "themes/Default/img/flags/pm.png",
VC: "SAINT VINCENT AND THE GRENADINES", VC_Icon: "themes/Default/img/flags/vc.png",
WS: "SAMOA", WS_Icon: "themes/Default/img/flags/ws.png",
SM: "SAN MARINO", SM_Icon: "themes/Default/img/flags/sm.png",
ST: "SAO TOME AND PRINCIPE", ST_Icon: "themes/Default/img/flags/st.png",
SA: "SAUDI ARABIA", SA_Icon: "themes/Default/img/flags/sa.png",
SN: "SENEGAL", SN_Icon: "themes/Default/img/flags/sn.png",
RS: "SERBIA", RS_Icon: "themes/Default/img/flags/rs.png",
SC: "SEYCHELLES", SC_Icon: "themes/Default/img/flags/sc.png",
SL: "SIERRA LEONE", SL_Icon: "themes/Default/img/flags/sl.png",
SG: "SINGAPORE", SG_Icon: "themes/Default/img/flags/sg.png",
SK: "SLOVAKIA", SK_Icon: "themes/Default/img/flags/sk.png",
SI: "SLOVENIA", SI_Icon: "themes/Default/img/flags/si.png",
SB: "SOLOMON ISLANDS", SB_Icon: "themes/Default/img/flags/sb.png",
SO: "SOMALIA", SO_Icon: "themes/Default/img/flags/so.png",
ZA: "SOUTH AFRICA", ZA_Icon: "themes/Default/img/flags/za.png",
GS: "SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS", GS_Icon: "themes/Default/img/flags/gs.png",
ES: "SPAIN", ES_Icon: "themes/Default/img/flags/es.png",
LK: "SRI LANKA", LK_Icon: "themes/Default/img/flags/lk.png",
SD: "SUDAN", SD_Icon: "themes/Default/img/flags/sd.png",
SR: "SURINAME", SR_Icon: "themes/Default/img/flags/sr.png",
SJ: "SVALBARD AND JAN MAYEN", SJ_Icon: "themes/Default/img/flags/sj.png",
SZ: "SWAZILAND", SZ_Icon: "themes/Default/img/flags/sz.png",
SE: "SWEDEN", SE_Icon: "themes/Default/img/flags/se.png",
CH: "SWITZERLAND", CH_Icon: "themes/Default/img/flags/ch.png",
SY: "SYRIAN ARAB REPUBLIC", SY_Icon: "themes/Default/img/flags/sy.png",
TW: "TAIWAN, PROVINCE OF CHINA", TW_Icon: "themes/Default/img/flags/tw.png",
TJ: "TAJIKISTAN", TJ_Icon: "themes/Default/img/flags/tj.png",
TZ: "TANZANIA, UNITED REPUBLIC OF", TZ_Icon: "themes/Default/img/flags/tz.png",
TH: "THAILAND", TH_Icon: "themes/Default/img/flags/th.png",
TL: "TIMOR-LESTE", TL_Icon: "themes/Default/img/flags/tl.png",
TG: "TOGO", TG_Icon: "themes/Default/img/flags/tg.png",
TK: "TOKELAU", TK_Icon: "themes/Default/img/flags/tk.png",
TO: "TONGA", TO_Icon: "themes/Default/img/flags/to.png",
TT: "TRINIDAD AND TOBAGO", TT_Icon: "themes/Default/img/flags/tt.png",
TN: "TUNISIA", TN_Icon: "themes/Default/img/flags/tn.png",
TR: "TURKEY", TR_Icon: "themes/Default/img/flags/tr.png",
TM: "TURKMENISTAN", TM_Icon: "themes/Default/img/flags/tm.png",
TC: "TURKS AND CAICOS ISLANDS", TC_Icon: "themes/Default/img/flags/tc.png",
TV: "TUVALU", TV_Icon: "themes/Default/img/flags/tv.png",
UG: "UGANDA", UG_Icon: "themes/Default/img/flags/ug.png",
UA: "UKRAINE", UA_Icon: "themes/Default/img/flags/ua.png",
AE: "UNITED ARAB EMIRATES", AE_Icon: "themes/Default/img/flags/ae.png",
GB: "UNITED KINGDOM", GB_Icon: "themes/Default/img/flags/gb.png",
US: "UNITED STATES", US_Icon: "themes/Default/img/flags/us.png",
UM: "UNITED STATES MINOR OUTLYING ISLANDS", UM_Icon: "themes/Default/img/flags/um.png",
UY: "URUGUAY", UY_Icon: "themes/Default/img/flags/uy.png",
UZ: "UZBEKISTAN", UZ_Icon: "themes/Default/img/flags/uz.png",
VU: "VANUATU", VU_Icon: "themes/Default/img/flags/vu.png",
VE: "VENEZUELA, BOLIVARIAN REPUBLIC OF", VE_Icon: "themes/Default/img/flags/ve.png",
VN: "VIET NAM", VN_Icon: "themes/Default/img/flags/vn.png",
VG: "VIRGIN ISLANDS, BRITISH", VG_Icon: "themes/Default/img/flags/vg.png",
VI: "VIRGIN ISLANDS, U.S.", VI_Icon: "themes/Default/img/flags/vi.png",
WF: "WALLIS AND FUTUNA", WF_Icon: "themes/Default/img/flags/wf.png",
EH: "WESTERN SAHARA", EH_Icon: "themes/Default/img/flags/eh.png",
YE: "YEMEN", E_Icon: "themes/Default/img/flags/ye.png",
ZM: "ZAMBIA", ZM_Icon: "themes/Default/img/flags/zm.png",
ZW: "ZIMBABWE", ZW_Icon: "themes/Default/img/flags/zw.png"
		};
	clearDisplay();
	baseElement = document.getElementById('Filters_List');
	newElement = document.createElement('span');
	newElement.setAttribute("id", "Filters_Back");
	newElement.setAttribute("class", "menuItem");
	newElement.setAttribute("onclick", "processFilters(this)");
	newElement.innerHTML = "Back";
	baseElement.appendChild(newElement);
	var i = 0;
	for(i=0;i<filtersCountries.length-1;i++) {
		newElement = document.createElement('span');
		newElement.setAttribute("id", "Filters_Country_" + filtersCountries[i]);
		newElement.setAttribute("class", "menuItem");
		newElement.setAttribute("onclick", "processFilters(this)");
		newElement.innerHTML = "<img src=\"" + Countries[filtersCountries[i] + "_Icon"] + "\" width=\"16px\" height=\"11px\" /> " + Countries[filtersCountries[i]];
		baseElement.appendChild(newElement);
	}
}
function displayOS() {
	var baseElement;
	var newElement;
	clearDisplay();
	baseElement = document.getElementById('Filters_List');
	var OS = {
		'5.0': "Windows 2000", 
		'5.1': "Windows XP", 
		'6.0': "Windows Vista", 
		'6.1': "Windows 7", 
		'5.2': "Windows Server 2003"};
	newElement = document.createElement('span');
	newElement.setAttribute("id", "Filters_Back");
	newElement.setAttribute("class", "menuItem");
	newElement.setAttribute("onclick", "processFilters(this)");
	newElement.innerHTML = "Back";
	baseElement.appendChild(newElement);
	for (var i in OS) {
		//alert('key is: ' + i + ', value is: ' + myArray[i]);
		newElement = document.createElement('span');
		newElement.setAttribute("id", "Filters_OSVer_" + i);
		newElement.setAttribute("class", "menuItem");
		newElement.setAttribute("onclick", "processFilters(this)");
		newElement.innerHTML = OS[i];
		baseElement.appendChild(newElement);
	}
}
function clearDisplay() {
	$("#Filters_List").html("");
}
