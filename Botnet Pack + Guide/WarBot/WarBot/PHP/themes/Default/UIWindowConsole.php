<?php
	if (!empty($Output)) {
		echo $Output;
		return;
	}
?>
<div id="console-display" style="white-space:pre;overflow:scroll;position:absolute;bottom:1px;top:26px;right:-1px;left:-1px;padding-left:2px;">Warbot [Version <?php echo(WARBOT_VERSION); ?>]
Copyright (c) 2010 Scenario11.  All rights reserved.
</div>
<form action="index.php?p=UIWindowConsole" method="get" onsubmit="return processConsole(this)">
	<input id="console-command" type="text" style="position:absolute;left:-1px;bottom:-1px;vertical-alignment:100%;width:100%;z-order:2;" />
</form>